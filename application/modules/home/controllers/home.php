<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('finance/finance_model');
        $this->load->model('appointment/appointment_model');
        $this->load->model('doctor/doctor_model');
        $this->load->model('patient/patient_model');
        $this->load->model('hospital/hospital_model');
        $this->load->model('notice/notice_model');
        $this->load->model('home_model');
    }

    public function index() {
        if (!$this->ion_auth->in_group(array('superadmin'))) {
            $data = array();
            $data['settings'] = $this->settings_model->getSettings();
            $data['sum'] = $this->home_model->getSum('gross_total', 'payment');
            $data['payments'] = $this->finance_model->getPayment();
            $data['notices'] = $this->notice_model->getNotice();
            $data['this_month'] = $this->finance_model->getThisMonth();
            $data['expenses'] = $this->finance_model->getExpense();
            if ($this->ion_auth->in_group(array('Doctor'))) {
                redirect('doctor/details');
            } else {
                $data['appointments'] = $this->appointment_model->getAppointment();
            }

            if ($this->ion_auth->in_group(array('Accountant', 'Receptionist'))) {
                redirect('home/asisten');
            }

            if ($this->ion_auth->in_group(array('Pharmacist'))) {
                redirect('finance/pharmacy/home');
            }

            if ($this->ion_auth->in_group(array('Patient'))) {
                redirect('patient/medicalHistory');
            }

            $data['this_month']['payment'] = $this->finance_model->thisMonthPayment();
            $data['this_month']['expense'] = $this->finance_model->thisMonthExpense();
            $data['this_month']['appointment'] = $this->finance_model->thisMonthAppointment();

            $data['this_day']['payment'] = $this->finance_model->thisDayPayment();
            $data['this_day']['expense'] = $this->finance_model->thisDayExpense();
            $data['this_day']['appointment'] = $this->finance_model->thisDayAppointment();

            $data['this_year']['payment'] = $this->finance_model->thisYearPayment();
            $data['this_year']['expense'] = $this->finance_model->thisYearExpense();
            $data['this_year']['appointment'] = $this->finance_model->thisYearAppointment();

            $data['this_month']['appointment'] = $this->finance_model->thisMonthAppointment();
            $data['this_month']['appointment_treated'] = $this->finance_model->thisMonthAppointmentTreated();
            $data['this_month']['appointment_cancelled'] = $this->finance_model->thisMonthAppointmentCancelled();
            $data['this_month']['appointment_pending_confirmation'] = $this->finance_model->thisMonthAppointmentPendingConfirmation();
            $data['this_month']['appointment_confirmed'] = $this->finance_model->thisMonthAppointmentConfirmed();
            $data['this_month']['appointment_requested'] = $this->finance_model->thisMonthAppointmentRequested();
            $data['this_year']['payment_per_month'] = $this->finance_model->getPaymentPerMonthThisYear();
            $data['this_year']['new_patient_per_month'] = $this->finance_model->getNewPatientPerMonthThisYear();
            

            $data['this_year']['expense_per_month'] = $this->finance_model->getExpensePerMonthThisYear();

            $data['pasien_baru'] = $this->finance_model->thisMonthNewPatient();
            $data['pasien_baru_bulan_lalu'] = $this->finance_model->prevMonthNewPatient();

            $this->load->view('dashboard'); // just the header file
            $this->load->view('home', $data);
            $this->load->view('footer', $data);
        } else {
            // $data['hospitals'] = $this->hospital_model->getHospital();

            // $data['pasien_superadmin'] = $this->finance_model->thisMonthPatientSP();
            // $data['pasien_baru_superadmin'] = $this->finance_model->thisMonthNewPatientSP();
            // $data['pasien_bulan_lalu_superadmin'] = $this->finance_model->prevMonthPatientSP();
            // $data['pasien_baru_bulan_lalu_superadmin'] = $this->finance_model->prevMonthNewPatientSP();

            // $data['this_year']['payment_per_month'] = $this->finance_model->getPaymentPerMonthThisYear();
            // $data['this_year']['new_patient_per_month'] = $this->finance_model->getNewPatientPerMonthThisYear();
            // $data['this_year']['patient_per_month'] = $this->finance_model->getPatientPerMonthThisYear();

            $this->load->view('dashboard'); // just the header file
            $this->load->view('home', $data);
            $this->load->view('footer');
        }
    }

    public function rekap() {
        if (!$this->ion_auth->in_group(array('superadmin'))) {
            $data = array();
            $data['settings'] = $this->settings_model->getSettings();
            $data['sum'] = $this->home_model->getSum('gross_total', 'payment');
            $data['payments'] = $this->finance_model->getPayment();
            $data['notices'] = $this->notice_model->getNotice();
            $data['this_month'] = $this->finance_model->getThisMonth();
            $data['expenses'] = $this->finance_model->getExpense();
            if ($this->ion_auth->in_group(array('Doctor'))) {
                redirect('doctor/details');
            } else {
                $data['appointments'] = $this->appointment_model->getAppointment();
            }

            if ($this->ion_auth->in_group(array('Accountant', 'Receptionist'))) {
                redirect('home/asisten');
            }

            if ($this->ion_auth->in_group(array('Pharmacist'))) {
                redirect('finance/pharmacy/home');
            }

            if ($this->ion_auth->in_group(array('Patient'))) {
                redirect('patient/medicalHistory');
            }

            $data['this_month']['payment'] = $this->finance_model->thisMonthPayment();
            $data['this_month']['expense'] = $this->finance_model->thisMonthExpense();
            $data['this_month']['appointment'] = $this->finance_model->thisMonthAppointment();

            $data['this_day']['payment'] = $this->finance_model->thisDayPayment();
            $data['this_day']['expense'] = $this->finance_model->thisDayExpense();
            $data['this_day']['appointment'] = $this->finance_model->thisDayAppointment();

            $data['this_year']['payment'] = $this->finance_model->thisYearPayment();
            $data['this_year']['expense'] = $this->finance_model->thisYearExpense();
            $data['this_year']['appointment'] = $this->finance_model->thisYearAppointment();

            $data['this_month']['appointment'] = $this->finance_model->thisMonthAppointment();
            $data['this_month']['appointment_treated'] = $this->finance_model->thisMonthAppointmentTreated();
            $data['this_month']['appointment_cancelled'] = $this->finance_model->thisMonthAppointmentCancelled();
            $data['this_month']['appointment_pending_confirmation'] = $this->finance_model->thisMonthAppointmentPendingConfirmation();
            $data['this_month']['appointment_confirmed'] = $this->finance_model->thisMonthAppointmentConfirmed();
            $data['this_month']['appointment_requested'] = $this->finance_model->thisMonthAppointmentRequested();
            $data['this_year']['payment_per_month'] = $this->finance_model->getPaymentPerMonthThisYear();
            $data['this_year']['new_patient_per_month'] = $this->finance_model->getNewPatientPerMonthThisYear();
            

            $data['this_year']['expense_per_month'] = $this->finance_model->getExpensePerMonthThisYear();

            $data['pasien_baru'] = $this->finance_model->thisMonthNewPatient();
            $data['pasien_baru_bulan_lalu'] = $this->finance_model->prevMonthNewPatient();

            $this->load->view('dashboard'); // just the header file
            $this->load->view('rekap', $data);
            $this->load->view('footer', $data);
        } else {
            $data['hospitals'] = $this->hospital_model->getHospital();

            $data['pasien_superadmin'] = $this->finance_model->thisMonthPatientSP();
            $data['pasien_baru_superadmin'] = $this->finance_model->thisMonthNewPatientSP();
            $data['pasien_bulan_lalu_superadmin'] = $this->finance_model->prevMonthPatientSP();
            $data['pasien_baru_bulan_lalu_superadmin'] = $this->finance_model->prevMonthNewPatientSP();

            $data['this_month']['appointment_treated'] = $this->finance_model->thisMonthAppointmentTreated();
            $data['this_month']['appointment_cancelled'] = $this->finance_model->thisMonthAppointmentCancelled();
            $data['this_month']['appointment_pending_confirmation'] = $this->finance_model->thisMonthAppointmentPendingConfirmation();
            $data['this_month']['appointment_confirmed'] = $this->finance_model->thisMonthAppointmentConfirmed();
            $data['this_month']['appointment_requested'] = $this->finance_model->thisMonthAppointmentRequested();

            $data['this_year']['payment_per_month'] = $this->finance_model->getPaymentPerMonthThisYear();
            $data['this_year']['new_patient_per_month'] = $this->finance_model->getNewPatientPerMonthThisYear();
            $data['this_year']['patient_per_month'] = $this->finance_model->getPatientPerMonthThisYear();

            $data['this_month']['payment'] = $this->finance_model->thisMonthPayment();
            $data['this_month']['expense'] = $this->finance_model->thisMonthExpense();
            $data['this_month']['appointment'] = $this->finance_model->thisMonthAppointment();

            $data['this_day']['payment'] = $this->finance_model->thisDayPayment();
            $data['this_day']['expense'] = $this->finance_model->thisDayExpense();
            $data['this_day']['appointment'] = $this->finance_model->thisDayAppointment();

            $data['this_year']['payment'] = $this->finance_model->thisYearPayment();
            $data['this_year']['expense'] = $this->finance_model->thisYearExpense();
            $data['this_year']['appointment'] = $this->finance_model->thisYearAppointment();

            $this->load->view('dashboard'); // just the header file
            $this->load->view('rekap', $data);
            $this->load->view('footer');
        }
    }

    public function trackingrm(){

        $dokter = $this->doctor_model->getDoctorSuperadmin();

        $x = 0;
        foreach($dokter as $d){
            $data['dokterid'][$x] = $d->id;
            $data['namadokter'][$x] = $d->name;
            $data['jumlahpasien'][$x] = $this->appointment_model->getAppointmentByDoctorByMonth($d->id);
            $data['jumlahrm'][$x] =  $this->appointment_model->getRMByDoctorByMonth($d->ion_user_id);
            if ($d->hospital_id == 1){
                $data['cabang'][$x] = 'Buduran Sidoarjo';
            } elseif ($d->hospital_id == 2) {
                $data['cabang'][$x] = 'Taman Sidoarjo';
            } elseif ($d->hospital_id == 3) {
                $data['cabang'][$x] = 'Wonoayu Sidoarjo';
            } elseif ($d->hospital_id == 4) {
                $data['cabang'][$x] = 'Candi Sidoarjo';
            } elseif ($d->hospital_id == 5) {
                $data['cabang'][$x] = 'Tanggulangin Sidoarjo';
            } elseif ($d->hospital_id == 6) {
                $data['cabang'][$x] = 'GKB Gresik';
            } elseif ($d->hospital_id == 7) {
                $data['cabang'][$x] = 'Waru Sidoarjo';
            } elseif ($d->hospital_id == 8) {
                $data['cabang'][$x] = 'Pangsud Gresik';
            } elseif ($d->hospital_id == 9) {
                $data['cabang'][$x] = '';
            } elseif ($d->hospital_id == 10) {
                $data['cabang'][$x] = 'Klampis Surabaya';
            } elseif ($d->hospital_id == 11) {
                $data['cabang'][$x] = 'Sedati Sidoarjo';
            } elseif ($d->hospital_id == 12) {
                $data['cabang'][$x] = 'Gadingrejo Pasuruan';
            } elseif ($d->hospital_id == 13) {
                $data['cabang'][$x] = 'Blimbing Malang';
            } elseif ($d->hospital_id == 14) {
                $data['cabang'][$x] = 'Gunung Anyar Surabaya';
            }
            $x++;
        }

        $y = 0;
        foreach($dokter as $d){
            $data['dokterids'][$y] = $d->id;
            $data['namadokters'][$y] = $d->name;
            $data['jumlahpasiens'][$y] = $this->appointment_model->getAppointmentByDoctorByMonthSebelumnya($d->id);
            $data['jumlahrms'][$y] =  $this->appointment_model->getRMByDoctorByMonthSebelumnya($d->ion_user_id);
            if ($d->hospital_id == 1){
                $data['cabangs'][$y] = 'Buduran Sidoarjo';
            } elseif ($d->hospital_id == 2) {
                $data['cabangs'][$y] = 'Taman Sidoarjo';
            } elseif ($d->hospital_id == 3) {
                $data['cabangs'][$y] = 'Wonoayu Sidoarjo';
            } elseif ($d->hospital_id == 4) {
                $data['cabangs'][$y] = 'Candi Sidoarjo';
            } elseif ($d->hospital_id == 5) {
                $data['cabangs'][$y] = 'Tanggulangin Sidoarjo';
            } elseif ($d->hospital_id == 6) {
                $data['cabangs'][$y] = 'GKB Gresik';
            } elseif ($d->hospital_id == 7) {
                $data['cabangs'][$y] = 'Waru Sidoarjo';
            } elseif ($d->hospital_id == 8) {
                $data['cabangs'][$y] = 'Pangsud Gresik';
            } elseif ($d->hospital_id == 9) {
                $data['cabangs'][$y] = '';
            } elseif ($d->hospital_id == 10) {
                $data['cabangs'][$y] = 'Klampis Surabaya';
            } elseif ($d->hospital_id == 11) {
                $data['cabangs'][$y] = 'Sedati Sidoarjo';
            } elseif ($d->hospital_id == 12) {
                $data['cabangs'][$y] = 'Gadingrejo Pasuruan';
            } elseif ($d->hospital_id == 13) {
                $data['cabangs'][$y] = 'Blimbing Malang';
            } elseif ($d->hospital_id == 14) {
                $data['cabangs'][$y] = 'Gunung Anyar Surabaya';
            }
            $y++;
        }

        $this->load->view('dashboard'); // just the header file
        $this->load->view('trackingrm', $data);
        $this->load->view('footer');
    }

    public function trackingrmbycabang(){

        $dokter = $this->doctor_model->getDoctorSuperadmin();

        $hospital = $this->hospital_model->getHospitalBuka();

        $x=0;
        foreach($hospital as $h){
            $data['idcabang'][$x] = $h->id;
            $data['namacabang'][$x] = $h->name;
            $data['jumlahpasien'][$x] = $this->appointment_model->getAppointmentByCabangByMonth($h->id);
            $data['jumlahrm'][$x] = $this->appointment_model->getRMByCabangByMonth($h->id);
            $x++;
        }

        $y=0;
        foreach($hospital as $h){
            $data['idcabangs'][$y] = $h->id;
            $data['namacabangs'][$y] = $h->name;
            $data['jumlahpasiens'][$y] = $this->appointment_model->getAppointmentByCabangByMonthSebelumnya($h->id);
            $data['jumlahrms'][$y] = $this->appointment_model->getRMByCabangByMonthSebelumnya($h->id);
            $y++;
        }

        $this->load->view('dashboard'); // just the header file
        $this->load->view('trackingrmbycabang', $data);
        $this->load->view('footer');
    }

    public function perawatan() {

        if ($this->ion_auth->in_group('superadmin')){
            $dokter = $this->doctor_model->getDoctorSuperadmin();
        } else if($this->ion_auth->in_group('Doctor')){
            $dokter = $this->doctor_model->getDoctorByIonUserIdd($this->ion_auth->get_user_id());
        }

        $x = 0;
        foreach($dokter as $d){

            $data['dokteridbulanini'][$x] = $d->id;
            $data['namadokterbulanini'][$x] = $d->name;
            
            // rm bulan ini
            $data['rmbulanini'][$x] = $this->patient_model->getMedicalHistoryAllMonth($d->ion_user_id);

            $perawatanbulanini[$x] = array();
            $bulanini=0;
            foreach($data['rmbulanini'][$x] as $rm){
                $perawatanbulanini[$x][$bulanini] = $rm->title;
                $bulanini++;
            }

            $newArraybulanini[$x] = array_map(function($item) {
                return explode(", ", $item);
            }, $perawatanbulanini[$x]);
            
            $finalArraybulanini[$x] = array();
            
            foreach ($newArraybulanini[$x] as $items) {
                foreach ($items as $item) {
                    $finalArraybulanini[$x][] = trim($item);
                }
            }

            $countArraybulanini[$x] = array_count_values($finalArraybulanini[$x]);

            $bulaninii = 0;
            foreach ($countArraybulanini[$x] as $key => $value) {
                $data['rmbulanini_jenisperawatan'][$x][$bulaninii] = $key;
                $data['rmbulanini_jumlahperawatan'][$x][$bulaninii] = $value;
                $bulaninii++;
            }

            $x++;
        }

        $this->load->view('dashboard');
        $this->load->view('perawatan', $data);
        $this->load->view('footer');
    }

    public function permission() {
        $this->load->view('permission');
    }

    public function asisten(){
        if ($this->ion_auth->in_group(array('Patient'))) {
            redirect('home/permission');
        }

        $data['payment'] = $this->finance_model->thisDayPayment();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctorAppKhususCS();
        $data['settings'] = $this->settings_model->getSettings();
        $data['appointments'] = $this->appointment_model->getAppointment();

        $this->load->view('dashboard');
        $this->load->view('asisten', $data);
        $this->load->view('footer');
    }

    public function cabang() {

        if (!$this->ion_auth->in_group(array('superadmin'))) {
            redirect('home/permission');
        }

        $data = array();
        $data['hospitals'] = $this->hospital_model->getHospital();

        foreach($data['hospitals'] as $h) {
            $data['this_month']['appointment_treated'][$h->id] = $this->finance_model->thisMonthAppointmentTreatedId($h->id);
            $data['this_month']['appointment_cancelled'][$h->id] = $this->finance_model->thisMonthAppointmentCancelledId($h->id);
            $data['this_month']['appointment_pending_confirmation'][$h->id] = $this->finance_model->thisMonthAppointmentPendingConfirmationId($h->id);
            $data['this_month']['appointment_confirmed'][$h->id] = $this->finance_model->thisMonthAppointmentConfirmedId($h->id);
            $data['this_month']['appointment_requested'][$h->id] = $this->finance_model->thisMonthAppointmentRequestedId($h->id);

            $data['this_year']['payment_per_month'][$h->id] = $this->finance_model->getPaymentPerMonthThisYearId($h->id);
            $data['this_year']['new_patient_per_month'][$h->id] = $this->finance_model->getNewPatientPerMonthThisYearId($h->id);
            $data['this_year']['patient_per_month'][$h->id] = $this->finance_model->getPatientPerMonthThisYearId($h->id);
        }

        $this->load->view('dashboard');
        $this->load->view('cabang', $data);
        $this->load->view('footer');
    }

    public function pasienultah() {

        if (!$this->ion_auth->in_group(array('superadmin','admin'))) {
            redirect('home/permission');
        }

        $data = array();
        $data['pasienultah'] = $this->patient_model->getPasienUltah();

        $this->load->view('dashboard');
        $this->load->view('pasienultah', $data);
        $this->load->view('footer');
    }

    public function promat() {

        if (!$this->ion_auth->in_group(array('superadmin','admin'))) {
            redirect('home/permission');
        }

        $data = array();
        $data['promat'] = $this->finance_model->getPaymentPromat();

        // var_dump($data['promat']);

        $this->load->view('dashboard');
        $this->load->view('pasienpromat', $data);
        $this->load->view('footer');
    }

    public function cs(){
        if ($this->ion_auth->in_group(array('Patient'))) {
            redirect('home/permission');
        }

        $data['payment'] = $this->finance_model->thisDayPayment();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctorAppKhususCS();
        $data['settings'] = $this->settings_model->getSettings();
        $data['appointments'] = $this->appointment_model->getAppointmentCS();

        $this->load->view('dashboard');
        $this->load->view('cs', $data);
        $this->load->view('footer');
    }



}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
