<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- <section class="panel">
            <header class="panel-heading">
                Omset Hari Ini: <b>Rp <?php echo $payment ?></b>
            </header>
        </section> -->
        <section class="panel">
            <header class="panel-heading">
                Appointment Hari Ini & Besok Semua Cabang
                <div class="col-md-4 clearfix pull-right">
                    <div class="pull-right"></div>
                    <div class="pull-right">
                        <?php if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) { ?>
                            <a data-toggle="modal" href="#myModal">
                                <div class="btn-group pull-right">
                                    <button id="" class="btn green btn-xs">
                                        <i class="fa fa-plus-circle"></i><?php echo lang('add_appointment'); ?> 
                                    </button>
                                </div>
                            </a>
                        <?php } ?>
                    </div>
                    <button class="export" onclick="javascript:window.print();">Print</button>  
                </div>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Cabang</th>
                                <th>Pasien</th>
                                <th>Telp</th>
                                <th>Dokter</th>
                                <th>Tanggal</th>
                                <th>Catatan</th>
                                <th>Status</th>
                                <th>Aksi</th>
                                
                            </tr>
                        </thead>
                        <tbody>

                        <style>

                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }

                        </style>

                        <?php
                        foreach ($appointments as $appointment) {
                            // if ($appointment->date == strtotime(date('Y-m-d')) || $appointment->date == strtotime(date('Y-m-d')) + 86400) {
                                ?>
                                <tr class="">
                                    <td ><?php echo $appointment->id; ?></td>
                                    <td >
                                        <?php
                                        if ($appointment->hospital_id == '1'){
                                            echo 'Buduran Sidoarjo';
                                        } elseif ($appointment->hospital_id == '2') {
                                            echo 'Taman Sidoarjo';
                                        } elseif ($appointment->hospital_id == '3') {
                                            echo 'Wonoayu Sidoarjo';
                                        } elseif ($appointment->hospital_id == '4') {
                                            echo 'Candi Sidoarjo';
                                        } elseif ($appointment->hospital_id == '5') {
                                            echo 'Tanggulangin Sidoarjo';
                                        } elseif ($appointment->hospital_id == '6') {
                                            echo 'GKB Gresik';
                                        } elseif ($appointment->hospital_id == '7') {
                                            echo 'Waru Sidoarjo';
                                        } elseif ($appointment->hospital_id == '8') {
                                            echo 'Pangsud Gresik';
                                        } elseif ($appointment->hospital_id == '10') {
                                            echo 'Klampis Surabaya';
                                        } elseif ($appointment->hospital_id == '11') {
                                            echo 'Sedati Sidoarjo';
                                        } elseif ($appointment->hospital_id == '12') {
                                            echo 'Gadingrejo Pasuruan';
                                        } elseif ($appointment->hospital_id == '13') {
                                            echo 'Blimbing Malang';
                                        } elseif ($appointment->hospital_id == '14') {
                                            echo 'Gunung Anyar Surabaya';
                                        }
                                        ?>
                                    </td>
                                    
                                    <td>
                                        <?php
                                            $phone = false;
                                            $patient = $this->patient_model->getPatientById($appointment->patient);
                                            $doctorname = $this->db->get_where('doctor', array('id' => $appointment->doctor))->row()->name;
                                            if (!empty($patient)) {
                                                $patientname = $patient->name;
                                                if($patient->phone){
                                                    $phone = str_replace(" ","",trim($patient->phone));
                                                    if(substr($phone, 0, 1)=='0'){
                                                        $phone = '+62'.substr($phone, 1);
                                                    }
                                                }
                                            } else {
                                                $patientname = '';
                                            }

                                            if ($appointment->hospital_id == '1'){
                                                $cabang = 'Buduran Sidoarjo';
                                            } elseif ($appointment->hospital_id == '2') {
                                                $cabang = 'Taman Sidoarjo';
                                            } elseif ($appointment->hospital_id == '3') {
                                                $cabang = 'Wonoayu Sidoarjo';
                                            } elseif ($appointment->hospital_id == '4') {
                                                $cabang = 'Candi Sidoarjo';
                                            } elseif ($appointment->hospital_id == '5') {
                                                $cabang = 'Tanggulangin Sidoarjo';
                                            } elseif ($appointment->hospital_id == '6') {
                                                $cabang = 'GKB Gresik';
                                            } elseif ($appointment->hospital_id == '7') {
                                                $cabang = 'Waru Sidoarjo';
                                            } elseif ($appointment->hospital_id == '8') {
                                                $cabang = 'Pangsud Gresik';
                                            } elseif ($appointment->hospital_id == '10') {
                                                $cabang = 'Klampis Surabaya';
                                            } elseif ($appointment->hospital_id == '11') {
                                                $cabang = 'Sedati Sidoarjo';
                                            } elseif ($appointment->hospital_id == '12') {
                                                $cabang = 'Gadingrejo Pasuruan';
                                            } elseif ($appointment->hospital_id == '13') {
                                                $cabang =  'Blimbing Malang';
                                            } elseif ($appointment->hospital_id == '14') {
                                                $cabang =  'Gunung Anyar Surabaya';
                                            }

                                            $reminder = "Selamat Siang... 😊%0a%0aKak ".$patientname.", perkenalkan kami dari DOKTER B FRESH DENTAL CARE. Mohon maaf ijin mengingatkan bahwa kakak telah melakukan reservasi perawatan pada ".date('d F Y', $appointment->date)." ".$appointment->s_time."-".$appointment->e_time." di cabang ".$cabang.".%0a%0aApakah sudah benar jadwal tersebut adalah jadwalnya kak?%0a%0aJika sudah benar maka sebaiknya datang 15 menit lebih awal, apabila mengalami keterlambatan untuk konfirmasi maka perawatan akan dilakukan setelah pasien yang sebelumnya tiba ya kak, terima kasih.";
                                            $hmin1 = "Selamat Siang... 😊%0a%0aKak ".$patientname.", perkenalkan kami dari DOKTER B FRESH DENTAL CARE. Mohon maaf ijin mengingatkan bahwa kakak ada jadwal kontrol pada ".date('d F Y', $appointment->date)." ".$appointment->s_time."-".$appointment->e_time." dengan dokter ".$doctorname." di cabang ".$cabang.".%0a%0aMohon maaf, kakak bisa datang kontrol besok jam berapa ya kak? Info ini kami perlukan agar kakak bisa dirawat dokter tanpa antri kak. terima kasih.. 😊";
                                            $hplus2 = "Selamat pagi Kak ".$patientname.", perkenalkan kami dari DOKTER GIGI B FRESH DENTAL CARE.%0a%0aSemoga Anda sekeluarga dalam keadaan sehat dan tetap produktif ya B'Friendss 😊%0a%0aMohon maaf kami ingin meminta pendapat tentang pelayanan kemarin, apakah sudah cukup puas atau belum ?%0a%0aJika merasa puas dengan pelayanan kami bisa mengetik ❤ dan merasa belum puas maka silahkan ketik 💔 (boleh memberikan angka 1-10) disertai dengan alasannya.%0a%0aAtau juga bisa mengisi survey di bawah ini! 😊%0a%0ahttps://forms.gle/7vESXVC5fkPJhNM98 %0a%0aB FRESH DENTAL CARE  berkomitmen untuk melayanimu sebaik mungkin. Terimakasih sekali lagi Kami ucapkan atas kepercayaan B'Friendss masih memilih B FRESH DENTAL CARE sebagai destinasi Perawatan Gigimu!";
                                            echo $patientname;
                                        ?>
                                    </td>

                                    <td>
                                        <?php
                                            $phone = false;
                                            $patient = $this->patient_model->getPatientById($appointment->patient);
                                            $doctorname = $this->db->get_where('doctor', array('id' => $appointment->doctor))->row()->name;
                                            if (!empty($patient)) {
                                                $patientname = $patient->name;
                                                if($patient->phone){
                                                    $phone = str_replace(" ","",trim($patient->phone));
                                                    if(substr($phone, 0, 1)=='0'){
                                                        $phone = '62'.substr($phone, 1);
                                                    }
                                                }
                                            } else {
                                                $patientname = '';
                                            }
                                            echo $phone
                                        ?>
                                    </td>

                                    <td><?php
                                        if (!empty($appointment->doctor)) {
                                            echo $this->db->get_where('doctor', array('id' => $appointment->doctor))->row()->name;
                                        }
                                        ?>
                                    </td>
                                    
                                    <td class="center"><?php echo date('d-m-Y', $appointment->date); ?> <strong> <?php echo $appointment->s_time; ?> - <?php echo $appointment->e_time; ?> </strong></td>
                                    
                                    <td>
                                        <?php echo $appointment->remarks; ?>
                                    </td>

                                    <td>
                                        <?php echo $appointment->status; ?>
                                    </td>

                                    <td>
                                        <button type="button" class="btn btn-info btn-xs btn_width editbutton" data-toggle="modal" data-id="<?php echo $appointment->id; ?>"><i class="fa fa-edit"> <?php echo lang('edit'); ?></i></button>   

                                        <a class="btn btn-info btn-xs btn_width delete_button" href="appointment/delete?id=<?php echo $appointment->id; ?>" <?php echo lang('delete'); ?> onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"> </i></a>
                                        <?php if($phone): ?>
                                            <a target="_blank" class="btn btn-success" href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>&text=<?php echo $hmin1; ?>">H -1</a>
                                            <a target="_blank" class="btn btn-success" href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>&text=<?php echo $reminder; ?>">Reminder</a>
                                            <a target="_blank" class="btn btn-success" href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>&text=<?php echo $hplus2; ?>">H +2</a>
                                        <?php endif ?>
                                    </td>
                                </tr>
                                <?php
                            // }
                        }
                        ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </section>
</section>

<!-- add appointment -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('add_appointment'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" action="appointment/addNew" method="post" class="clearfix" enctype="multipart/form-data">
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('patient'); ?></label> 
                        <select class="form-control m-bot15" id="pasien" name="patient" value=''> 
                            <option value=""><?php echo lang('select'); ?></option>
                            <!-- <option value="add_new" style="color: #41cac0 !important;"><?php echo lang('add_new'); ?></option> -->
                            <!-- <?php foreach ($patients as $patient) { ?>
                                <option value="<?php echo $patient->id; ?>" <?php
                                if (!empty($payment->patient)) {
                                    if ($payment->patient == $patient->id) {
                                        echo 'selected';
                                    }
                                }
                                ?> ><?php echo $patient->name; ?> </option>
                                    <?php } ?> -->
                        </select>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1">  <?php echo lang('doctor'); ?></label> 
                        <select class="form-control m-bot15 js-example-basic-single" id="adoctors" name="doctor" value=''>  
                            <option value="">Select</option>
                            <?php foreach ($doctors as $doctor) { ?>
                                
                                <?php
                                if ($doctor->hospital_id == '1'){
                                    $cabang = 'Buduran Sidoarjo';
                                } elseif ($doctor->hospital_id == '2') {
                                    $cabang = 'Taman Sidoarjo';
                                } elseif ($doctor->hospital_id == '3') {
                                    $cabang = 'Wonoayu Sidoarjo';
                                } elseif ($doctor->hospital_id == '4') {
                                    $cabang = 'Candi Sidoarjo';
                                } elseif ($doctor->hospital_id == '5') {
                                    $cabang = 'Tanggulangin Sidoarjo';
                                } elseif ($doctor->hospital_id == '6') {
                                    $cabang = 'GKB Gresik';
                                } elseif ($doctor->hospital_id == '7') {
                                    $cabang = 'Waru Sidoarjo';
                                } elseif ($doctor->hospital_id == '8') {
                                    $cabang = 'Pangsud Gresik';
                                } elseif ($doctor->hospital_id == '10') {
                                    $cabang = 'Klampis Surabaya';
                                } elseif ($doctor->hospital_id == '11') {
                                    $cabang = 'Sedati Sidoarjo';
                                } elseif ($doctor->hospital_id == '12') {
                                    $cabang = 'Gadingrejo Pasuruan';
                                }
                                ?>

                                <option value="<?php echo $doctor->id; ?>"><?php echo '['.$cabang.'] '.$doctor->name; ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('date'); ?></label>
                        <input type="text" class="form-control default-date-picker" id="date" readonly="" name="date" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1">Available Slots</label>
                        <select class="form-control m-bot15" name="time_slot" id="aslots" value=''> 

                        </select>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('appointment'); ?> <?php echo lang('status'); ?></label> 
                        <select class="form-control m-bot15" name="status" value=''>
                            <option value="Pending Confirmation" <?php
                            ?> > <?php echo lang('pending_confirmation'); ?> </option>
                            <option value="Confirmed" <?php
                            ?> > <?php echo lang('confirmed'); ?> </option>
                            <option value="Treated" <?php
                            ?> > <?php echo lang('treated'); ?> </option>
                            <option value="Cancelled" <?php
                            ?> > <?php echo lang('cancelled'); ?> </option>
                        </select>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('remarks'); ?></label>
                        <input type="text" class="form-control" name="remarks" id="exampleInputEmail1" value='' placeholder="">
                        <input type="hidden" class="form-control" name="redirect" value='home/cs'>
                    </div>
                    <div class="col-md-12 panel">
                        <button type="submit" name="submit" class="btn btn-info pull-right"> <?php echo lang('submit'); ?></button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<!-- edit appointment -->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('edit_appointment'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="editAppointmentForm" action="appointment/addNew" class="clearfix" method="post" enctype="multipart/form-data">
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('patient'); ?></label> 
                        <select class="form-control m-bot15" id="pasien" name="patient" value=''> 
                            <option value=""><?php echo lang('select'); ?></option>
                            <option value="add_new" style="color: #41cac0 !important;"><?php echo lang('add_new'); ?></option>
                            <?php foreach ($patients as $patient) { ?>
                                <option value="<?php echo $patient->id; ?>" <?php
                                if (!empty($payment->patient)) {
                                    if ($payment->patient == $patient->id) {
                                        echo 'selected';
                                    }
                                }
                                ?> ><?php echo $patient->name; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div class="pos_client clearfix col-md-6">
                        <div class="payment pad_bot pull-right">
                            <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('name'); ?></label> 
                            <input type="text" class="form-control pay_in" name="p_name" value='' placeholder="">
                        </div>
                        <div class="payment pad_bot pull-right">
                            <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('email'); ?></label>
                            <input type="text" class="form-control pay_in" name="p_email" value='' placeholder="">
                        </div>
                        <div class="payment pad_bot pull-right">
                            <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('phone'); ?></label>
                            <input type="text" class="form-control pay_in" name="p_phone" value='' placeholder="">
                        </div>
                        <div class="payment pad_bot pull-right">
                            <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('age'); ?></label> 
                            <input type="text" class="form-control pay_in" name="p_age" value='' placeholder="">
                        </div> 
                        <div class="payment pad_bot"> 
                            <label for="exampleInputEmail1"> <?php echo lang('patient'); ?> <?php echo lang('gender'); ?></label>
                            <select class="form-control" name="p_gender" value=''>

                                <option value="Male" <?php
                                if (!empty($patient->sex)) {
                                    if ($patient->sex == 'Male') {
                                        echo 'selected';
                                    }
                                }
                                ?> > Male </option>   
                                <option value="Female" <?php
                                if (!empty($patient->sex)) {
                                    if ($patient->sex == 'Female') {
                                        echo 'selected';
                                    }
                                }
                                ?> > Female </option>
                                <option value="Others" <?php
                                if (!empty($patient->sex)) {
                                    if ($patient->sex == 'Others') {
                                        echo 'selected';
                                    }
                                }
                                ?> > Others </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1">  <?php echo lang('doctor'); ?></label> 
                        <select class="form-control m-bot15 js-example-basic-single doctor" id="adoctors1" name="doctor" value=''>  
                            <option value="">Select</option>
                            <?php foreach ($doctors as $doctor) { ?>

                                <?php
                                if ($doctor->hospital_id == '1'){
                                    $cabang = 'Buduran Sidoarjo';
                                } elseif ($doctor->hospital_id == '2') {
                                    $cabang = 'Taman Sidoarjo';
                                } elseif ($doctor->hospital_id == '3') {
                                    $cabang = 'Wonoayu Sidoarjo';
                                } elseif ($doctor->hospital_id == '4') {
                                    $cabang = 'Candi Sidoarjo';
                                } elseif ($doctor->hospital_id == '5') {
                                    $cabang = 'Tanggulangin Sidoarjo';
                                } elseif ($doctor->hospital_id == '6') {
                                    $cabang = 'GKB Gresik';
                                } elseif ($doctor->hospital_id == '7') {
                                    $cabang = 'Waru Sidoarjo';
                                } elseif ($doctor->hospital_id == '8') {
                                    $cabang = 'Pangsud Gresik';
                                } elseif ($doctor->hospital_id == '10') {
                                    $cabang = 'Klampis Surabaya';
                                } elseif ($doctor->hospital_id == '11') {
                                    $cabang = 'Sedati Sidoarjo';
                                } elseif ($doctor->hospital_id == '12') {
                                    $cabang = 'Gadingrejo Pasuruan';
                                }
                                ?>

                                <option value="<?php echo $doctor->id; ?>"><?php echo '['.$cabang.'] '.$doctor->name; ?> </option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('date'); ?></label>
                        <input type="text" class="form-control default-date-picker" id="date1" readonly="" name="date" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1">Available Slots</label>
                        <select class="form-control m-bot15" name="time_slot" id="aslots1" value=''> 

                        </select>
                    </div>
                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('appointment'); ?> <?php echo lang('status'); ?></label> 
                        <select class="form-control m-bot15" name="status" value=''>
                            <option value="Pending Confirmation" <?php
                            ?> > <?php echo lang('pending_confirmation'); ?> </option>
                            <option value="Confirmed" <?php
                            ?> > <?php echo lang('confirmed'); ?> </option>
                            <option value="Treated" <?php
                            ?> > <?php echo lang('treated'); ?> </option>
                            <option value="Cancelled" <?php
                            ?> > <?php echo lang('cancelled'); ?> </option>
                        </select>
                    </div>

                    <div class="col-md-6 panel">
                        <label for="exampleInputEmail1"> <?php echo lang('remarks'); ?></label>
                        <input type="text" class="form-control" name="remarks" id="exampleInputEmail1" value='' placeholder="">
                        <input type="hidden" class="form-control" name="redirect" value='home/cs'>
                    </div>
                    <!-- <div class="col-md-6 panel">
                        <label> <?php echo lang('send_sms'); ?> ? </label> <br>
                        <input type="checkbox" name="sms" class="" value="sms">  <?php echo lang('yes'); ?>
                    </div> -->
                    <input type="hidden" name="id" id="appointment_id" value=''>
                    <div class="col-md-12 panel">
                        <button type="submit" name="submit" class="btn btn-info pull-right"> <?php echo lang('submit'); ?></button>
                    </div>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<script src="common/js/codearistos.min.js"></script>

<script>
 $(document).ready(function(){
    $('#pasien').select2({
        ajax: {
            url:"patient/tampilPasien",
            dataType:"json",
            type:"post",
            delay:"250",
            data: function(params){
                return{
                    cari: params.term,
                }
            },
            processResults: function(data){
                console.log(data)
                return{
                    results: data,
                }
            },
            cache: true
        },
        placeholder: "Cari Pasien",
        minimumInputLength: 3,       
    });
 })
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $(".editbutton").click(function (e) {
            e.preventDefault(e);
            // Get the record's ID via attribute  
            var iid = $(this).attr('data-id');
            var id = $(this).attr('data-id');

            $('#editAppointmentForm').trigger("reset");
            $('#myModal2').modal('show');
            $.ajax({
                url: 'appointment/editAppointmentByJason?id=' + iid,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).success(function (response) {
                var de = response.appointment.date * 1000;
                var d = new Date(de);
                var da = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
                // Populate the form fields with the data returned from server
                $('#editAppointmentForm').find('[name="id"]').val(response.appointment.id).end()
                $('#editAppointmentForm').find('[name="patient"]').val(response.appointment.patient).end()
                $('#editAppointmentForm').find('[name="doctor"]').val(response.appointment.doctor).end()
                $('#editAppointmentForm').find('[name="date"]').val(da).end()
                $('#editAppointmentForm').find('[name="status"]').val(response.appointment.status).end()
                $('#editAppointmentForm').find('[name="remarks"]').val(response.appointment.remarks).end()

                $('.js-example-basic-single.doctor').val(response.appointment.doctor).trigger('change');
                $('.js-example-basic-single.patient').val(response.appointment.patient).trigger('change');




                var date = $('#date1').val();
                var doctorr = $('#adoctors1').val();
                var appointment_id = $('#appointment_id').val();
                // $('#default').trigger("reset");
                $.ajax({
                    url: 'schedule/getAvailableSlotByDoctorByDateByAppointmentIdByJason?date=' + date + '&doctor=' + doctorr + '&appointment_id=' + appointment_id,
                    method: 'GET',
                    data: '',
                    dataType: 'json',
                }).success(function (response) {
                    $('#aslots1').find('option').remove();
                    var slots = response.aslots;
                    $.each(slots, function (key, value) {
                        $('#aslots1').append($('<option>').text(value).val(value)).end();
                    });

                    $("#aslots1").val(response.current_value)
                            .find("option[value=" + response.current_value + "]").attr('selected', true);
                    //  $('#aslots1 option[value=' + response.current_value + ']').attr("selected", "selected");
                    //   $("#default-step-1 .button-next").trigger("click");
                    if ($('#aslots1').has('option').length == 0) {                    //if it is blank. 
                        $('#aslots1').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
                    }
                    // Populate the form fields with the data returned from server
                    //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
                });
            });
        });
    });
</script>

<script>
    $(document).ready(function () {
        $('.pos_client').hide();
        $(document.body).on('change', '#pos_select', function () {

            var v = $("select.pos_select option:selected").val()
            if (v == 'add_new') {
                $('.pos_client').show();
            } else {
                $('.pos_client').hide();
            }
        });

    });


</script>

<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],
                    }
                },
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 50,
            "order": [[4, "asc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            }
        });
        table.buttons().container().appendTo('.custom_buttons');
    });
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#adoctors").change(function () {
            // Get the record's ID via attribute  
            var iid = $('#date').val();
            var doctorr = $('#adoctors').val();
            $('#aslots').find('option').remove();
            // $('#default').trigger("reset");
            $.ajax({
                url: 'schedule/getAvailableSlotByDoctorByDateByJason?date=' + iid + '&doctor=' + doctorr,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).success(function (response) {
                var slots = response.aslots;
                $.each(slots, function (key, value) {
                    $('#aslots').append($('<option>').text(value).val(value)).end();
                });
                //   $("#default-step-1 .button-next").trigger("click");
                if ($('#aslots').has('option').length == 0) {                    //if it is blank. 
                    $('#aslots').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
                }
                // Populate the form fields with the data returned from server
                //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
            });
        });

    });

    $(document).ready(function () {
        var iid = $('#date').val();
        var doctorr = $('#adoctors').val();
        $('#aslots').find('option').remove();
        // $('#default').trigger("reset");
        $.ajax({
            url: 'schedule/getAvailableSlotByDoctorByDateByJason?date=' + iid + '&doctor=' + doctorr,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            var slots = response.aslots;
            $.each(slots, function (key, value) {
                $('#aslots').append($('<option>').text(value).val(value)).end();
            });
            //   $("#default-step-1 .button-next").trigger("click");
            if ($('#aslots').has('option').length == 0) {                    //if it is blank. 
                $('#aslots').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
            }
            // Populate the form fields with the data returned from server
            //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
        });

    });




    $(document).ready(function () {
        $('#date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
        })
                //Listen for the change even on the input
                .change(dateChanged)
                .on('changeDate', dateChanged);
    });

    function dateChanged() {
        // Get the record's ID via attribute  
        var iid = $('#date').val();
        var doctorr = $('#adoctors').val();
        $('#aslots').find('option').remove();
        // $('#default').trigger("reset");
        $.ajax({
            url: 'schedule/getAvailableSlotByDoctorByDateByJason?date=' + iid + '&doctor=' + doctorr,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            var slots = response.aslots;
            $.each(slots, function (key, value) {
                $('#aslots').append($('<option>').text(value).val(value)).end();
            });
            //   $("#default-step-1 .button-next").trigger("click");
            if ($('#aslots').has('option').length == 0) {                    //if it is blank. 
                $('#aslots').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
            }


            // Populate the form fields with the data returned from server
            //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
        });

    }




</script>

<script type="text/javascript">
    $(document).ready(function () {
        $("#adoctors1").change(function () {
            // Get the record's ID via attribute 
            var id = $('#appointment_id').val();
            var date = $('#date1').val();
            var doctorr = $('#adoctors1').val();
            $('#aslots1').find('option').remove();
            // $('#default').trigger("reset");
            $.ajax({
                url: 'schedule/getAvailableSlotByDoctorByDateByAppointmentIdByJason?date=' + date + '&doctor=' + doctorr + '&appointment_id=' + id,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).success(function (response) {
                var slots = response.aslots;
                $.each(slots, function (key, value) {
                    $('#aslots1').append($('<option>').text(value).val(value)).end();
                });
                //   $("#default-step-1 .button-next").trigger("click");
                if ($('#aslots1').has('option').length == 0) {                    //if it is blank. 
                    $('#aslots1').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
                }
                // Populate the form fields with the data returned from server
                //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
            });
        });
    });

    $(document).ready(function () {
        var id = $('#appointment_id').val();
        var date = $('#date1').val();
        var doctorr = $('#adoctors1').val();
        $('#aslots1').find('option').remove();
        // $('#default').trigger("reset");
        $.ajax({
            url: 'schedule/getAvailableSlotByDoctorByDateByAppointmentIdByJason?date=' + date + '&doctor=' + doctorr + '&appointment_id=' + id,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            var slots = response.aslots;
            $.each(slots, function (key, value) {
                $('#aslots1').append($('<option>').text(value).val(value)).end();
            });
            //   $("#default-step-1 .button-next").trigger("click");
            if ($('#aslots1').has('option').length == 0) {                    //if it is blank. 
                $('#aslots1').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
            }
            // Populate the form fields with the data returned from server
            //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
        });

    });




    $(document).ready(function () {
        $('#date1').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
        })
                //Listen for the change even on the input
                .change(dateChanged1)
                .on('changeDate', dateChanged1);
    });

    function dateChanged1() {
        // Get the record's ID via attribute  
        var id = $('#appointment_id').val();
        var iid = $('#date1').val();
        var doctorr = $('#adoctors1').val();
        $('#aslots1').find('option').remove();
        // $('#default').trigger("reset");
        $.ajax({
            url: 'schedule/getAvailableSlotByDoctorByDateByAppointmentIdByJason?date=' + iid + '&doctor=' + doctorr + '&appointment_id=' + id,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            var slots = response.aslots;
            $.each(slots, function (key, value) {
                $('#aslots1').append($('<option>').text(value).val(value)).end();
            });
            //   $("#default-step-1 .button-next").trigger("click");
            if ($('#aslots1').has('option').length == 0) {                    //if it is blank. 
                $('#aslots1').append($('<option>').text('No Further Time Slots').val('Not Selected')).end();
            }


            // Populate the form fields with the data returned from server
            //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
        });

    }




</script>

<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>