<!--sidebar end-->
<!--main content start-->
<script type="text/javascript" src="common/js/google-loader.js"></script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="state-overview col-md-12" style="padding: 23px 0px;">


            <div style="margin-bottom:10px;">
                Periode : <strong><?php echo date('F',strtotime('-1 month')).' - '.date('F Y') ?></strong>
            </div>

            <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Cabang</th>
                        <th>Tanggal</th>
                        <th>Jumlah Perawatan</th>
                        <th>Jenis Perawatan</th>
                        <th>Harga</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($promat as $p){ ?>
                    <tr>
                        <td>
                            <?php
                            $patient_info = $this->db->get_where('patient', array('id' => $p['patient']))->row();    
                            ?>
                            <a href="finance/invoice?id=<?php echo $p['id']; ?>"><?php echo $patient_info->name." - ".$p['id']; ?></a>
                        </td>
                        <td>
                            <?php 
                            if($p['hospital_id'] == 1){
                                echo "Buduran Sidoarjo";
                            } elseif($p['hospital_id'] == 2) {
                                echo "Taman Sidoarjo";
                            } elseif($p['hospital_id'] == 3) {
                                echo "Wonoayu Sidoarjo";
                            } elseif($p['hospital_id'] == 4) {
                                echo "Candi Sidoarjo";
                            } elseif($p['hospital_id'] == 5) {
                                echo "Tanggulangin Sidoarjo";
                            } elseif($p['hospital_id'] == 6) {
                                echo "GKB Gresik";
                            } elseif($p['hospital_id'] == 7) {
                                echo "Waru Sidoarjo";
                            } elseif($p['hospital_id'] == 8) {
                                echo "Pangsud Gresik";
                            } elseif($p['hospital_id'] == 10) {
                                echo "Klampis Surabaya";
                            } elseif($p['hospital_id'] == 11) {
                                echo "Sedati Sidoarjo";
                            } elseif($p['hospital_id'] == 12) {
                                echo "Gadingrejo Pasuruan";
                            } elseif($p['hospital_id'] == 13) {
                                echo "Blimbing Malang";
                            } elseif($p['hospital_id'] == 14) {
                                echo "Gunung Anyar Surabaya";
                            }
                            ?>
                        </td>
                        <td><?php echo date('d-m-Y',$p['date']) ?></td>
                        <td>
                            <?php
                                $layanan = explode(",",$p['category_name']);
                                echo count($layanan);
                            ?>
                        </td>
                        <td>
                            <?php
                               // Memecah string menjadi elemen-elemen yang dipisahkan oleh koma
                                $entries = explode(',', $p['category_name']);

                                // Inisialisasi array untuk menyimpan hasil
                                $resultArray = array();

                                // Inisialisasi variabel untuk melacak koma
                                $commaAdded = false;

                                // Loop melalui setiap elemen
                                foreach ($entries as $entry) {
                                    // Memecah elemen menjadi bagian-bagian yang dipisahkan oleh "*"
                                    $parts = explode('*', $entry);

                                    // Memeriksa apakah elemen memiliki angka kedua yang valid
                                    if (count($parts) >= 2) {
                                        $id = $parts[0];
                                        $layanann = $this->db->get_where('payment_category', array('id' => $id))->row();
                                        if ($layanann) {
                                            if ($commaAdded) {
                                                echo ' <> ';
                                            }
                                            echo $layanann->category;
                                            $commaAdded = true;
                                        }
                                    }
                                }
                            ?>
                        </td>
                        <td><?php echo 'Rp '.number_format($p['gross_total'],0,",",".") ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>

        <style>

            table{
                box-shadow: none;
            }

            .fc-head{

                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12);

            }

            .panel-body{
                background: #fff;
            }

            thead{
                background: #fff;
            }

            .panel-body {
                background: #fff;
            }

            .panel-heading {
                border-radius: 0px;
                background: #fff !important;
                color: #000;
                padding-left: 10px;
                font-size: 13px !important;
                margin-top: 3px;
                text-align: center;
            }

            .add_patient{
                background: #009988;
            }

            .add_appointment{
                background: #f8d347;
            }

            .add_prescription{
                background: blue;
            }

            .add_lab_report{

            }

            .y-axis li span {
                display: block;
                margin: -20px 0 0 -25px;
                padding: 0 20px;
                width: 40px;
            }

            .sale_color{
                background: #69D2E7 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            .expense_color{
                background: #F38630 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            audio, canvas, progress, video {
                display: inline-block;
                vertical-align: baseline;
                width: 100% !important;
                height: 101% !important;
                margin-bottom: 18%;
            }  


            .panel-heading{
                margin-top: 0px;
            }


        </style>
    </section>

</section>
</section>

<script src="common/js/codearistos.min.js"></script>
<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            dom: 'lfrBtip',
            responsive: true,
            buttons: [
                'copyHtml5',
                'excelHtml5',
            ],
            iDisplayLength: 100,
        });  
    });
</script>