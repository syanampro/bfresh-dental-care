<!--sidebar end-->
<!--main content start-->
<script type="text/javascript" src="common/js/google-loader.js"></script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <!--state overview start-->

        <?php if (!$this->ion_auth->in_group('superadmin')) { ?>
            
            <?php
        } else {
            ?>
                <div class="state-overview col-md-12" style="padding: 23px 0px;">

                            <!-- Section 4 -->

                            <div class="col-lg-6 col-sm-6">
                                <div id="trackingpasiendokter"></div>
                                <section class="panel">
                                    <header class="panel-heading">
                                        <?php echo 'Tracking RM By Cabang Per Bulan '.date('F Y'); ?>
                                    </header>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover table-bordered" id="datatable">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Cabang</th>
                                                    <th>Pasien</th>
                                                    <th>RM</th>
                                                    <th>%</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php for($x=0;$x<count($idcabang);$x++){ ?>
                                                    <tr>
                                                        <td><?php echo $idcabang[$x] ?></td>
                                                        <td><?php echo $namacabang[$x] ?></td>
                                                        <td><?php echo $jumlahpasien[$x] ?></td>
                                                        <td><?php echo $jumlahrm[$x] ?></td>
                                                        <td><?php echo round($jumlahrm[$x]/$jumlahpasien[$x]*100) ?></td>
                                                        <?php if($jumlahpasien[$x] > $jumlahrm[$x]){ ?>
                                                            <td style="color:red;font-weight:900;">KURANG</td>
                                                            <?php } else { ?>
                                                            <td style="color:green;">AMAN</td>
                                                            <?php }  ?>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>

                            <div class="col-lg-6 col-sm-6">
                                <div id="trackingpasiendoktersebelumnya"></div>
                                <section class="panel">
                                    <header class="panel-heading">
                                        <?php echo 'Tracking RM by Cabang Per Bulan '.date('F Y', strtotime('-1 months')); ?>
                                    </header>
                                    <div class="panel-body">
                                        <table class="table table-striped table-hover table-bordered" id="datatable2">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Cabang</th>
                                                    <th>Pasien</th>
                                                    <th>RM</th>
                                                    <th>%</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php for($x=0;$x<count($idcabangs);$x++){ ?>
                                                    <tr>
                                                        <td><?php echo $idcabangs[$x] ?></td>
                                                        <td><?php echo $namacabangs[$x] ?></td>
                                                        <td><?php echo $jumlahpasiens[$x] ?></td>
                                                        <td><?php echo $jumlahrms[$x] ?></td>
                                                        <td><?php echo round($jumlahrms[$x]/$jumlahpasiens[$x]*100) ?></td>
                                                        <?php if($jumlahpasiens[$x] > $jumlahrms[$x]){ ?>
                                                            <td style="color:red;font-weight:900;">KURANG</td>
                                                            <?php } else { ?>
                                                            <td style="color:green;">AMAN</td>
                                                            <?php }  ?>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </section>
                            </div>
                </div>

        <?php } ?>

        <style>

            table{
                box-shadow: none;
            }

            .fc-head{

                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12);

            }

            .panel-body{
                background: #fff;
            }

            thead{
                background: #fff;
            }

            .panel-body {
                background: #fff;
            }

            .panel-heading {
                border-radius: 0px;
                background: #fff !important;
                color: #000;
                padding-left: 10px;
                font-size: 13px !important;
                margin-top: 3px;
                text-align: center;
            }

            .add_patient{
                background: #009988;
            }

            .add_appointment{
                background: #f8d347;
            }

            .add_prescription{
                background: blue;
            }

            .add_lab_report{

            }

            .y-axis li span {
                display: block;
                margin: -20px 0 0 -25px;
                padding: 0 20px;
                width: 40px;
            }

            .sale_color{
                background: #69D2E7 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            .expense_color{
                background: #F38630 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            audio, canvas, progress, video {
                display: inline-block;
                vertical-align: baseline;
                width: 100% !important;
                height: 101% !important;
                margin-bottom: 18%;
            }  


            .panel-heading{
                margin-top: 0px;
            }


        </style>

    </section>
</section>
</section>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script>
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBarColors);

        function drawBarColors() {
            var data = google.visualization.arrayToDataTable([
                ['City', 'Jumlah Pasien', 'Jumlah RM'],
                ['<?php echo date('F Y') ?>', <?php echo array_sum($jumlahpasien) ?>, <?php echo array_sum($jumlahrm) ?>],
                
            ]);

            var options = {
                title: 'Tracking Jumlah Pasien <?php echo date('F Y') ?>',
                chartArea: {width: '50%'},
                colors: ['green', 'red'],
                hAxis: {
                title: '',
                minValue: 0
                },
                vAxis: {
                title: ''
                }
            };
            var chart = new google.visualization.BarChart(document.getElementById('trackingpasiendokter'));
            chart.draw(data, options);
            }
</script>

<script>
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBarColors);

        function drawBarColors() {
            var data = google.visualization.arrayToDataTable([
                ['City', 'Jumlah Pasien', 'Jumlah RM'],
                ['<?php echo date('F Y', strtotime('-1 months')) ?>', <?php echo array_sum($jumlahpasiens) ?>, <?php echo array_sum($jumlahrms) ?>],
                
            ]);

            var options = {
                title: 'Tracking Jumlah Pasien <?php echo date('F Y', strtotime('-1 months')) ?>',
                chartArea: {width: '50%'},
                colors: ['green', 'red'],
                hAxis: {
                title: '',
                minValue: 0
                },
                vAxis: {
                title: ''
                }
            };
            var chart = new google.visualization.BarChart(document.getElementById('trackingpasiendoktersebelumnya'));
            chart.draw(data, options);
            }
</script>

<script src="common/js/codearistos.min.js"></script>
<script>
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            dom: 'lfrBtip',
            responsive: true,
            searching: false, 
            paging: false,
            buttons: [
                'copyHtml5',
                'excelHtml5',
            ],
            iDisplayLength: 1000,
        });  
    });
</script>

<script>
    $(document).ready(function () {
        var table = $('#datatable2').DataTable({
            dom: 'lfrBtip',
            responsive: true,
            searching: false, 
            paging: false,
            buttons: [
                'copyHtml5',
                'excelHtml5',
            ],
            iDisplayLength: 1000,
        });  
    });
</script>