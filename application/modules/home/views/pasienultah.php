<!--sidebar end-->
<!--main content start-->
<script type="text/javascript" src="common/js/google-loader.js"></script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="state-overview col-md-12" style="padding: 23px 0px;">

            <table class="table table-striped table-hover table-bordered" id="editable-sample">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>No Telp</th>
                        <th>Tanggal Lahir</th>
                        <th>Cabang</th>
                        <th>Hubungi Whatsapp</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($pasienultah as $pasien){ ?>
                    <tr>
                        <td><?php echo $pasien['name'] ?></td>
                        <td>
                            <?php 
                                $phone = str_replace(" ","",trim($pasien['phone']));
                                if(substr($phone, 0, 1)=='0'){
                                    echo $phone = '62'.substr($phone, 1);
                                } else {
                                    echo $phone;
                                }
                            ?>
                        </td>
                        <td><?php echo $pasien['birthdate'] ?></td>
                        <td>
                            <?php 
                            if($pasien['hospital_id'] == 1){
                                echo "Buduran Sidoarjo";
                            } elseif($pasien['hospital_id'] == 2) {
                                echo "Taman Sidoarjo";
                            } elseif($pasien['hospital_id'] == 3) {
                                echo "Wonoayu Sidoarjo";
                            } elseif($pasien['hospital_id'] == 4) {
                                echo "Candi Sidoarjo";
                            } elseif($pasien['hospital_id'] == 5) {
                                echo "Tanggulangin Sidoarjo";
                            } elseif($pasien['hospital_id'] == 6) {
                                echo "GKB Gresik";
                            } elseif($pasien['hospital_id'] == 7) {
                                echo "Waru Sidoarjo";
                            } elseif($pasien['hospital_id'] == 8) {
                                echo "Pangsud Gresik";
                            } elseif($pasien['hospital_id'] == 10) {
                                echo "Klampis Surabaya";
                            } elseif($pasien['hospital_id'] == 11) {
                                echo "Sedati Sidoarjo";
                            } elseif($pasien['hospital_id'] == 12) {
                                echo "Gadingrejo Pasuruan";
                            } elseif($pasien['hospital_id'] == 13) {
                                echo "Blimbing Malang";
                            } elseif($pasien['hospital_id'] == 14) {
                                echo "Gunung Anyar Surabaya";
                            }
                            ?>
                        </td>
                        <td>
                            <?php 
                                if($pasien['phone']){
                                    $phone = str_replace(" ","",trim($pasien['phone']));
                                    if(substr($phone, 0, 1)=='0'){
                                        $phone = '62'.substr($phone, 1);
                                        $wa = '<a target="_blank" href="https://wa.me/'.$phone.'?text=Selamat pagi *KAK '.$pasien['name'].'*😊✨%0A%0APerkenalkan kami dari DOKTER GIGI B FRESH DENTAL CARE.%0A%0AMengucapkan *SELAMAT ULANG TAHUN* 🎂🎁🎉 kepada *KAK. '.$pasien['name'].'* Semoga selalu diberikan kesehatan dan umur yang berkah. Semoga senantiasa semakin puas serta nyaman atas pelayanan kami 🥰%0A%0A*Sebagai tanda cinta, kami hadiahkan PROMAT PERAWATAN:*%0A🎁 Behel ~3juta~ mulai dari 999rb%0A🎁 Kontrol Behel ~150rb~ mulai dari 99rb%0A🎁 Bleaching ~3juta~ mulai dari 459rb%0A🎁 Scaling ~400rb~ mulai dari 99rb%0A🎁 Tambal ~500rb~ mulai dari 149rb%0A🎁 Cabut ~400rb~ mulai dari 149rb%0A%0A*Kak '.$pasien['name'].'* pilih yang mana aja kak? 🥰%0A%0AUntuk tanda bukti mendapatkan hadiah ini, wajib follow Instagram @bfreshdentalcare, dan memberi ulasan GMB, serta hanya perlu menunjukkan WA ini dan KTP/SIM berlaku 🥰 ke asisten atau dokter B FRESH.%0A%0A_Salam Hangat Penuh Cinta dari DOKTER GIGI B FRESH DENTAL CARE_%0A%0ATerimakasih 🙏💕%0A%0A*Penawaran ini berlaku salah satu anggota keluarga sampai 7 hari dan khusus perawatan Behel dan Bleaching berlaku sampai 30 hari setelah WA ini diterima ya kak..*
                                        ">Kirim Pesan</a>';
                                    } else {
                                        $phone = null;
                                        $wa = null; 
                                    }
                                }
                                echo $wa;
                            ?>
                            
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>

        </div>

        <style>

            table{
                box-shadow: none;
            }

            .fc-head{

                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12);

            }

            .panel-body{
                background: #fff;
            }

            thead{
                background: #fff;
            }

            .panel-body {
                background: #fff;
            }

            .panel-heading {
                border-radius: 0px;
                background: #fff !important;
                color: #000;
                padding-left: 10px;
                font-size: 13px !important;
                margin-top: 3px;
                text-align: center;
            }

            .add_patient{
                background: #009988;
            }

            .add_appointment{
                background: #f8d347;
            }

            .add_prescription{
                background: blue;
            }

            .add_lab_report{

            }

            .y-axis li span {
                display: block;
                margin: -20px 0 0 -25px;
                padding: 0 20px;
                width: 40px;
            }

            .sale_color{
                background: #69D2E7 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            .expense_color{
                background: #F38630 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            audio, canvas, progress, video {
                display: inline-block;
                vertical-align: baseline;
                width: 100% !important;
                height: 101% !important;
                margin-bottom: 18%;
            }  


            .panel-heading{
                margin-top: 0px;
            }


        </style>
    </section>

</section>
</section>

<script src="common/js/codearistos.min.js"></script>

<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2, 3, 4, 5],
                    }
                },
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 50,
            "order": [[4, "asc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            }
        });
        table.buttons().container().appendTo('.custom_buttons');
    });
</script>
