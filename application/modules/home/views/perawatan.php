<!--sidebar end-->
<!--main content start-->
<script type="text/javascript" src="common/js/google-loader.js"></script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <!--state overview start-->

        <div class="state-overview col-md-12" style="padding: 23px 0px;">

                    <!-- Section 1 -->

                    <div class="col-lg-12 col-sm-6">
                        <h1 style="text-align:center; background-color:white; padding:10px;">Statistik Jenis Perawatan (All)</h1>
                    </div>

                    <?php $a=0; ?>
                    <?php foreach($dokteridbulanini as $bulanini){ ?>

                    <div class="col-lg-12 col-sm-6">
                        <div id="<?php echo 'perawatanbulanini'.$dokteridbulanini[$a] ?>" style="padding:10px 0px;"></div>
                    </div>

                    <?php $a++ ?>
                    <?php } ?>
        </div>

        <style>

            table{
                box-shadow: none;
            }

            .fc-head{

                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12);

            }

            .panel-body{
                background: #fff;
            }

            thead{
                background: #fff;
            }

            .panel-body {
                background: #fff;
            }

            .panel-heading {
                border-radius: 0px;
                background: #fff !important;
                color: #000;
                padding-left: 10px;
                font-size: 13px !important;
                margin-top: 3px;
                text-align: center;
            }

            .add_patient{
                background: #009988;
            }

            .add_appointment{
                background: #f8d347;
            }

            .add_prescription{
                background: blue;
            }

            .add_lab_report{

            }

            .y-axis li span {
                display: block;
                margin: -20px 0 0 -25px;
                padding: 0 20px;
                width: 40px;
            }

            .sale_color{
                background: #69D2E7 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            .expense_color{
                background: #F38630 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            audio, canvas, progress, video {
                display: inline-block;
                vertical-align: baseline;
                width: 100% !important;
                height: 101% !important;
                margin-bottom: 18%;
            }  


            .panel-heading{
                margin-top: 0px;
            }


        </style>


        <!--state overview end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
<!--footer end-->
</section>

<!-- js placed at the end of the document so the pages load faster -->

<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


<?php $a=0; ?>
<?php foreach($dokteridbulanini as $bulanini){ ?>

<script>
        google.charts.load('current', {packages: ['corechart', 'bar']});
        google.charts.setOnLoadCallback(drawBarColors);

        function drawBarColors() {

            var dataArr = [['Jenis Perawatan', 'Jumlah Perawatan']];
            var jenisperawatan = <?php echo json_encode($rmbulanini_jenisperawatan[$a]) ?>;
            var jumlahperawatan = <?php echo json_encode($rmbulanini_jumlahperawatan[$a]) ?>;

            for (var i = 0; i < jenisperawatan.length; i++) {
            var row = [jenisperawatan[i], jumlahperawatan[i]];
            dataArr.push(row);
            }

            var data = google.visualization.arrayToDataTable(dataArr);

            var options = {
                title: '<?php echo $namadokterbulanini[$a]?>',
                chartArea: {width:"50%"},
                colors: ['#b0120a', '#ffab91'],
                height:800,
                hAxis: {
                title: '',
                minValue: 0
                },
                vAxis: {
                title: jenisperawatan.length + ' Jenis Perawatan'
                }
            };
            var chart = new google.visualization.BarChart(document.getElementById('<?php echo 'perawatanbulanini'.$dokteridbulanini[$a] ?>'));
            chart.draw(data, options);
            }
</script>

<?php $a++ ?>
<?php } ?>