<!--sidebar end-->
<!--main content start-->
<script type="text/javascript" src="common/js/google-loader.js"></script>
<section id="main-content">
    <section class="wrapper site-min-height">
        <div class="state-overview col-md-12" style="padding: 23px 0px;">

            <?php foreach($hospitals as $h){ ?>    
            
            <div class="col-lg-12 col-sm-6">
                <h1 style="text-align:center; background-color:white; padding:10px;"><?php echo $h->name ?></h1>
            </div>

            <div class="col-lg-3 col-sm-6">
                <a href="#">
                <section class="panel home_sec_blue">
                    <div class="symbol blue">
                        <i class="fa fa-users"></i>
                    </div>
                    <div class="value">
                        <h3 class="">
                            <?php
                            $this->db->from('patient');
                            $this->db->where('hospital_id',$h->id);
                            $count = $this->db->count_all_results();
                            echo $count;
                            ?>
                        </h3>
                        <p>Total Pasien</p>
                    </div>
                </section>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6">
                <a href="#">
                <section class="panel home_sec_yellow">
                    <div class="symbol yellow">
                        <i class="fa fa-plus-square-o"></i>
                    </div>
                    <div class="value">
                        <h3 class="">
                            <?php
                            $this->db->from('appointment');
                            $this->db->where('hospital_id',$h->id);
                            $count = $this->db->count_all_results();
                            echo $count;
                            ?>
                        </h3>
                        <p>Appointment</p>
                    </div>
                </section>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6">
                <a href="#">
                <section class="panel home_sec_blue">
                    <div class="symbol blue">
                        <i class="fa fa-medkit"></i>
                    </div>
                    <div class="value">
                        <h3 class="">
                            <?php
                            $this->db->from('medical_history');
                            $this->db->where('hospital_id',$h->id);
                            $count = $this->db->count_all_results();
                            echo $count;
                            ?>
                        </h3>
                        <p>Rekam Medis</p>
                    </div>
                </section>
                </a>
            </div>

            <div class="col-lg-3 col-sm-6">
                <a href="#">
                <section class="panel home_sec_yellow">
                    <div class="symbol yellow">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="value">
                        <h3 class="">
                            <?php
                            $this->db->from('payment');
                            $this->db->where('hospital_id',$h->id);
                            $count = $this->db->count_all_results();
                            echo $count;
                            ?>
                        </h3>
                        <p>Invoice Pembayaran</p>
                    </div>
                </section>
                </a>
            </div>

            <div class="col-lg-7 col-sm-6">
                <div id="chart_div_admin<?php echo $h->id ?>" class="panel" style=""></div>
            </div>

            <div class="col-lg-5 col-sm-6">
                <div id="donutchart_admin<?php echo $h->id ?>" class="panel" style=""></div>
            </div>

            <div class="col-lg-7 col-sm-6">
                <div id="chart_div2_admin<?php echo $h->id ?>" class="panel" style=""></div>
            </div>

            <div class="col-lg-5 col-sm-6">
                <div id="donutchart2_admin<?php echo $h->id ?>" class="panel" style=""></div>
            </div>

            <?php } ?>    

        </div>

        <style>

            table{
                box-shadow: none;
            }

            .fc-head{

                box-shadow: 0 2px 5px 0 rgba(0, 0, 0, .16), 0 2px 10px 0 rgba(0, 0, 0, .12);

            }

            .panel-body{
                background: #fff;
            }

            thead{
                background: #fff;
            }

            .panel-body {
                background: #fff;
            }

            .panel-heading {
                border-radius: 0px;
                background: #fff !important;
                color: #000;
                padding-left: 10px;
                font-size: 13px !important;
                margin-top: 3px;
                text-align: center;
            }

            .add_patient{
                background: #009988;
            }

            .add_appointment{
                background: #f8d347;
            }

            .add_prescription{
                background: blue;
            }

            .add_lab_report{

            }

            .y-axis li span {
                display: block;
                margin: -20px 0 0 -25px;
                padding: 0 20px;
                width: 40px;
            }

            .sale_color{
                background: #69D2E7 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            .expense_color{
                background: #F38630 !important;
                padding: 10px !important;
                font-size: 5px;
                margin-right: 10px;
            }

            audio, canvas, progress, video {
                display: inline-block;
                vertical-align: baseline;
                width: 100% !important;
                height: 101% !important;
                margin-bottom: 18%;
            }  


            .panel-heading{
                margin-top: 0px;
            }


        </style>
    </section>

</section>
</section>

<!-- js placed at the end of the document so the pages load faster -->

<?php foreach($hospitals as $h){ ?>    

    <script type="text/javascript">
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Scaling','12'],
                ['Tambal','8'],
                ['Behel','4'],
                ['Bahall','15'],
            ]);

            var options = {
                title:'Jenis Perawatan',
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart_admin<?php echo $h->id ?>'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawVisualization);

        function drawVisualization() {
            // Some raw data (not necessarily accurate)
            var data = google.visualization.arrayToDataTable([
                ['Month', 'Pemasukan'],
                ['Jan', <?php echo $this_year['payment_per_month'][$h->id]['january']; ?>],
                ['Feb', <?php echo $this_year['payment_per_month'][$h->id]['february']; ?>],
                ['Mar', <?php echo $this_year['payment_per_month'][$h->id]['march']; ?>],
                ['Apr', <?php echo $this_year['payment_per_month'][$h->id]['april']; ?>],
                ['May', <?php echo $this_year['payment_per_month'][$h->id]['may']; ?>],
                ['June', <?php echo $this_year['payment_per_month'][$h->id]['june']; ?>],
                ['July', <?php echo $this_year['payment_per_month'][$h->id]['july']; ?>],
                ['Aug', <?php echo $this_year['payment_per_month'][$h->id]['august']; ?>],
                ['Sep', <?php echo $this_year['payment_per_month'][$h->id]['september']; ?>],
                ['Oct', <?php echo $this_year['payment_per_month'][$h->id]['october']; ?>],
                ['Nov', <?php echo $this_year['payment_per_month'][$h->id]['november']; ?>],
                ['Dec', <?php echo $this_year['payment_per_month'][$h->id]['december']; ?>],
            ]);

            var options = {
                title: new Date().getFullYear() + ' Omset',
                vAxis: {title: '<?php echo $settings->currency; ?>'},
                hAxis: {title: '<?php echo 'Bulan'; ?>'},
                seriesType: 'bars',
                series: {5: {type: 'line'}}
            };

            var chart = new google.visualization.ComboChart(document.getElementById('chart_div_admin<?php echo $h->id ?>'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load('current', {'packages': ['corechart']});
        google.charts.setOnLoadCallback(drawVisualization);

        function drawVisualization() {
            // Some raw data (not necessarily accurate)
            var data = google.visualization.arrayToDataTable([
                ['Month', 'Pasien Baru', 'Total Pasien'],
                ['Jan', <?php echo $this_year['new_patient_per_month'][$h->id]['january']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['january']; ?>],
                ['Feb', <?php echo $this_year['new_patient_per_month'][$h->id]['february']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['february']; ?>],
                ['Mar', <?php echo $this_year['new_patient_per_month'][$h->id]['march']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['march']; ?>],
                ['Apr', <?php echo $this_year['new_patient_per_month'][$h->id]['april']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['april']; ?>],
                ['May', <?php echo $this_year['new_patient_per_month'][$h->id]['may']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['may']; ?>],
                ['June', <?php echo $this_year['new_patient_per_month'][$h->id]['june']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['june']; ?>],
                ['July', <?php echo $this_year['new_patient_per_month'][$h->id]['july']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['july']; ?>],
                ['Aug', <?php echo $this_year['new_patient_per_month'][$h->id]['august']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['august']; ?>],
                ['Sep', <?php echo $this_year['new_patient_per_month'][$h->id]['september']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['september']; ?>],
                ['Oct', <?php echo $this_year['new_patient_per_month'][$h->id]['october']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['october']; ?>],
                ['Nov', <?php echo $this_year['new_patient_per_month'][$h->id]['november']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['november']; ?>],
                ['Dec', <?php echo $this_year['new_patient_per_month'][$h->id]['december']; ?>, <?php echo $this_year['patient_per_month'][$h->id]['december']; ?>],
            ]);

            var options = {
                title: new Date().getFullYear() + ' Total Pasien',
                vAxis: {title: '<?php echo $settings->currency; ?>'},
                hAxis: {title: '<?php echo 'Bulan'; ?>'},
                seriesType: 'bars',
                series: {5: {type: 'line'}}
            };

            var chart = new google.visualization.ComboChart(document.getElementById('chart_div2_admin<?php echo $h->id ?>'));
            chart.draw(data, options);
        }
    </script>

    <script type="text/javascript">
        google.charts.load("current", {packages: ["corechart"]});
        google.charts.setOnLoadCallback(drawChart);
        function drawChart() {

            var months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
                "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

            var d = new Date();
            var selectedMonthName = months[d.getMonth()] + ', ' + d.getFullYear();

            var data = google.visualization.arrayToDataTable([
                ['Task', 'Hours per Day'],
                ['Selesai', <?php
            if (!empty($this_month['appointment_treated'][$h->id])) {
                echo $this_month['appointment_treated'][$h->id];
            } else {
                echo '0';
            }
            ?>],
                ['Belum Terkonfirmasi', <?php
            if (!empty($this_month['appointment_pending_confirmation'][$h->id])) {
                echo $this_month['appointment_pending_confirmation'][$h->id];
            } else {
                echo '0';
            }
            ?>],
                ['Terkonfirmasi', <?php
            if (!empty($this_month['appointment_confirmed'][$h->id])) {
                echo $this_month['appointment_confirmed'][$h->id];
            } else {
                echo '0';
            }
            ?>],
                ['Permintaan Pasien', <?php
            if (!empty($this_month['appointment_requested'][$h->id])) {
                echo $this_month['appointment_requested'][$h->id];
            } else {
                echo '0';
            }
            ?>],
                ['Batal', <?php
            if (!empty($this_month['appointment_cancelled'][$h->id])) {
                echo $this_month['appointment_cancelled'][$h->id];
            } else {
                echo '0';
            }
            ?>],
            ]);

            var options = {
                title: selectedMonthName + ' Appointment',
                pieHole: 0.4,
            };

            var chart = new google.visualization.PieChart(document.getElementById('donutchart2_admin<?php echo $h->id ?>'));
            chart.draw(data, options);
        }
    </script>

<?php } ?>   