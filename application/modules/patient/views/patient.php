<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="">

            <header class="panel-heading">
                Data Pasien
                <div class="col-md-4 no-print pull-right"> 
                    <?php if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) { ?> 
                        <a data-toggle="modal" href="#myModal">
                            <div class="btn-group pull-right">
                                <button id="" class="btn green btn-xs">
                                    <i class="fa fa-plus-circle"></i> Tambah Baru
                                </button>
                            </div>
                        </a>
                    <?php } ?>
                </div>
            </header>
            <div class="panel-body">

                <div class="adv-table editable-table ">

                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>ID Pasien</th>                        
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Email Pembayaran</th>
                                <th>Telepon</th>
                                <th>Tanggal Daftar</th>
                                <?php if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist','spv'))) { ?>
                                    <th><?php echo lang('due_balance'); ?></th>
                                <?php } ?>
                                <th class="no-print">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <style>
                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }
                        </style>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->

<!-- Add Patient Modal-->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Daftar Pasien</h4>
            </div>
            <div class="modal-body row">
                <form role="form" action="patient/addNew" class="clearfix" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">NIK / No KTP</label>
                        <input type="text" class="form-control" name="nik" id="exampleInputEmail1" value='' placeholder="" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Nama</label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Email</label>
                        <input type="text" class="form-control" name="email" id="exampleInputEmail1" value='' placeholder="" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Email Nota</label>
                        <input type="text" class="form-control" name="email_bayar" id="exampleInputEmail1" value='' placeholder="" required>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Password</label>
                        <input type="password" class="form-control" name="password" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Alamat</label>
                        <input type="text" class="form-control" name="address" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">RT / RW</label>
                        <input type="text" class="form-control" name="rtrw" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kelurahan</label>
                        <input type="text" class="form-control" name="kelurahan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kecamatan</label>
                        <input type="text" class="form-control" name="kecamatan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kota</label>
                        <input type="text" class="form-control" name="kota" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kode Pos</label>
                        <input type="text" class="form-control" name="kodepos" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Provinsi</label>
                        <input type="text" class="form-control" name="provinsi" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Telepon</label>
                        <input type="text" class="form-control" name="phone" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                        <select class="form-control m-bot15" name="sex" value=''>

                            <option value="Male" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Male') {
                                    echo 'selected';
                                }
                            }
                            ?> > Laki - Laki </option>
                            <option value="Female" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Female') {
                                    echo 'selected';
                                }
                            }
                            ?> > Perempuan </option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label>Tanggal Lahir</label>
                        <input class="form-control form-control-inline input-medium default-date-picker" type="text" name="birthdate" value="" placeholder="" readonly="">      
                    </div>
                    <div class="form-group col-md-6">    
                        <label for="exampleInputEmail1">Dokter</label>
                        <select class="form-control js-example-basic-single"  name="doctor" value=''> 
                            <option value=""> </option>
                            <?php foreach ($doctors as $doctor) { ?>                                        
                                <option value="<?php echo $doctor->id; ?>"><?php echo $doctor->name; ?> </option>
                            <?php } ?> 
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Agama</label>
                        <input type="text" class="form-control" name="agama" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Instagram</label>
                        <input type="text" class="form-control" name="instagram" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Nick Name</label>
                        <input type="text" class="form-control" name="nickname" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Pekerjaan</label>
                        <input type="text" class="form-control" name="pekerjaan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Catatan / Keterangan Pasien</label>
                        <input type="text" class="form-control" name="catatan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group last col-md-6">
                        <label class="control-label">Foto</label>
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="img_url"/>
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <h4><b>Riwayat Pasien</b></h4>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Sebelumnya pernah ke dokter gigi?</label>
                        <select class="form-control m-bot15" name="pernah_ke_dg" value=''>
                            <option value="0" <?php
                                if (!empty($patient->pernah_ke_dg)) {
                                    if ($patient->pernah_ke_dg == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->pernah_ke_dg)) {
                                if ($patient->pernah_ke_dg == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai penyakit diabetes / kencing manis?</label>
                        <select class="form-control m-bot15" name="diabetes" value=''>
                            <option value="0" <?php
                                if (!empty($patient->diabetes)) {
                                    if ($patient->diabetes == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->diabetes)) {
                                if ($patient->diabetes == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai penyakit hipertensi / darah tinggi?</label>
                        <select class="form-control m-bot15" name="hipertensi" value=''>
                        <option value="0" <?php
                            if (!empty($patient->hipertensi)) {
                                if ($patient->hipertensi == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->hipertensi)) {
                                if ($patient->hipertensi == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai sakit jantung?</label>
                        <select class="form-control m-bot15" name="sakit_jantung" value=''>
                            <option value="0" <?php
                                if (!empty($patient->sakit_jantung)) {
                                    if ($patient->sakit_jantung == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->sakit_jantung)) {
                                if ($patient->sakit_jantung == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai sakit asma?</label>
                        <select class="form-control m-bot15" name="sakit_asma" value=''>
                        <option value="0" <?php
                            if (!empty($patient->sakit_asma)) {
                                if ($patient->sakit_asma == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->sakit_asma)) {
                                if ($patient->sakit_asma == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai sakit maag?</label>
                        <select class="form-control m-bot15" name="maag" value=''>
                            <option value="0" <?php
                                if (!empty($patient->maag)) {
                                    if ($patient->maag == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->maag)) {
                                if ($patient->maag == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit typus?</label>
                        <select class="form-control m-bot15" name="typus" value=''>
                            <option value="0" <?php
                                if (!empty($patient->typus)) {
                                    if ($patient->typus == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->typus)) {
                                if ($patient->typus == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit demam berdarah?</label>
                        <select class="form-control m-bot15" name="demam_berdarah" value=''>
                            <option value="0" <?php
                                if (!empty($patient->demam_berdarah)) {
                                    if ($patient->demam_berdarah == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->demam_berdarah)) {
                                if ($patient->demam_berdarah == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit TBC?</label>
                        <select class="form-control m-bot15" name="tbc" value=''>
                            <option value="0" <?php
                                if (!empty($patient->tbc)) {
                                    if ($patient->tbc == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->tbc)) {
                                if ($patient->tbc == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit pembekuan darah?</label>
                        <select class="form-control m-bot15" name="pembekuan_darah" value=''>
                            <option value="0" <?php
                                if (!empty($patient->pembekuan_darah)) {
                                    if ($patient->pembekuan_darah == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->pembekuan_darah)) {
                                if ($patient->pembekuan_darah == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit sinusitis?</label>
                        <select class="form-control m-bot15" name="sinusitis" value=''>
                            <option value="0" <?php
                                if (!empty($patient->sinusitis)) {
                                    if ($patient->sinusitis == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->sinusitis)) {
                                if ($patient->sinusitis == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai alergi obat?</label>
                        <select class="form-control m-bot15" name="alergi_obat" value=''>
                            <option value="0" <?php
                                if (!empty($patient->alergi_obat)) {
                                    if ($patient->alergi_obat == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->alergi_obat)) {
                                if ($patient->alergi_obat == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai alergi obat?</label>
                        <select class="form-control m-bot15" name="alergi_makanan" value=''>
                            <option value="0" <?php
                                if (!empty($patient->alergi_makanan)) {
                                    if ($patient->alergi_makanan == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->alergi_makanan)) {
                                if ($patient->alergi_makanan == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai alergi lainnya</label>
                        <input type="text" class="form-control" name="alergi_lain" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Wanita: sedang hamil?</label>
                        <select class="form-control m-bot15" name="wanita_hamil" value=''>
                            <option value="0" <?php
                                if (!empty($patient->wanita_hamil)) {
                                    if ($patient->wanita_hamil == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->wanita_hamil)) {
                                if ($patient->wanita_hamil == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Wanita: sedang menyusui?</label>
                        <select class="form-control m-bot15" name="wanita_menyusui" value=''>
                            <option value="0" <?php
                                if (!empty($patient->wanita_menyusui)) {
                                    if ($patient->wanita_menyusui == '0') {
                                        echo 'selected';
                                    }
                                }
                            ?> > Tidak </option>
                            <option value="1" <?php
                            if (!empty($patient->wanita_menyusui)) {
                                if ($patient->wanita_menyusui == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Obat jalan yang sedang dilakukan (jika ada, sebutkan nama penyakit & obatnya)</label>
                        <input type="text" class="form-control" name="obat_jalan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Terapi jalan yang sedang dilakukan (jika ada, sebutkan nama penyakit & terapinya)</label>
                        <input type="text" class="form-control" name="terapi_jalan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <section class="col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </section>
                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Add Patient Modal-->

<!-- Edit Patient Modal-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Edit Pasien</h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="editPatientForm" action="patient/addNew" class="clearfix" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">NIK</label>
                        <input type="text" class="form-control" name="nik" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('name'); ?></label>
                        <input type="text" class="form-control" name="name" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('email'); ?></label>
                        <input type="text" class="form-control" name="email" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Email Nota</label>
                        <input type="text" class="form-control" name="email_bayar" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('change'); ?><?php echo lang('password'); ?></label>
                        <input type="password" class="form-control" name="password" id="exampleInputEmail1" placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Alamat</label>
                        <input type="text" class="form-control" name="address" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">RT / RW</label>
                        <input type="text" class="form-control" name="rtrw" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kelurahan</label>
                        <input type="text" class="form-control" name="kelurahan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kecamatan</label>
                        <input type="text" class="form-control" name="kecamatan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kota</label>
                        <input type="text" class="form-control" name="kota" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Kode Pos</label>
                        <input type="text" class="form-control" name="kodepos" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Provinsi</label>
                        <input type="text" class="form-control" name="provinsi" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Telepon</label>
                        <input type="text" class="form-control" name="phone" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('sex'); ?></label>
                        <select class="form-control m-bot15" name="sex" value=''>

                            <option value="Male" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Male') {
                                    echo 'selected';
                                }
                            }
                            ?> > Male </option>
                            <option value="Female" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Female') {
                                    echo 'selected';
                                }
                            }
                            ?> > Female </option>
                            <option value="Others" <?php
                            if (!empty($patient->sex)) {
                                if ($patient->sex == 'Others') {
                                    echo 'selected';
                                }
                            }
                            ?> > Others </option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label><?php echo lang('birth_date'); ?></label>
                        <input class="form-control form-control-inline input-medium default-date-picker" type="text" name="birthdate" value="" placeholder="" readonly="">      
                    </div>
                    <div class="form-group col-md-6">    
                        <label for="exampleInputEmail1"><?php echo lang('doctor'); ?></label>
                        <select class="form-control js-example-basic-single doctor"  name="doctor" value=''> 
                            <option value=""> </option>
                            <?php foreach ($doctors as $doctor) { ?>                                        
                                <option value="<?php echo $doctor->id; ?>"><?php echo $doctor->name; ?> </option>
                            <?php } ?> 
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Agama</label>
                        <input type="text" class="form-control" name="agama" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Instagram</label>
                        <input type="text" class="form-control" name="instagram" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Nick Name</label>
                        <input type="text" class="form-control" name="nickname" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Pekerjaan</label>
                        <input type="text" class="form-control" name="pekerjaan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Catatan / Keterangan</label>
                        <input type="text" class="form-control" name="catatan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group last col-md-6">
                        <label class="control-label">Image Upload</label>
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="img" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                                <div>
                                    <span class="btn btn-white btn-file">
                                        <span class="fileupload-new"><i class="fa fa-paper-clip"></i> Select image</span>
                                        <span class="fileupload-exists"><i class="fa fa-undo"></i> Change</span>
                                        <input type="file" class="default" name="img_url"/>
                                    </span>
                                    <a href="#" class="btn btn-danger fileupload-exists" data-dismiss="fileupload"><i class="fa fa-trash"></i> Remove</a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <h4><b>Riwayat Pasien</b></h4>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Sebelumnya pernah ke dokter gigi?</label>
                        <select class="form-control m-bot15" name="pernah_ke_dg" value=''>
                            <option value="1" <?php
                            if (!empty($patient->pernah_ke_dg)) {
                                if ($patient->pernah_ke_dg == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->pernah_ke_dg)) {
                                if ($patient->pernah_ke_dg == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai penyakit diabetes / kencing manis?</label>
                        <select class="form-control m-bot15" name="diabetes" value=''>
                            <option value="1" <?php
                            if (!empty($patient->diabetes)) {
                                if ($patient->diabetes == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->diabetes)) {
                                if ($patient->diabetes == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai penyakit hipertensi / darah tinggi?</label>
                        <select class="form-control m-bot15" name="hipertensi" value=''>
                            <option value="1" <?php
                            if (!empty($patient->hipertensi)) {
                                if ($patient->hipertensi == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->hipertensi)) {
                                if ($patient->hipertensi == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai sakit jantung?</label>
                        <select class="form-control m-bot15" name="sakit_jantung" value=''>
                            <option value="1" <?php
                            if (!empty($patient->sakit_jantung)) {
                                if ($patient->sakit_jantung == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->sakit_jantung)) {
                                if ($patient->sakit_jantung == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai sakit asma?</label>
                        <select class="form-control m-bot15" name="sakit_asma" value=''>
                            <option value="1" <?php
                            if (!empty($patient->sakit_asma)) {
                                if ($patient->sakit_asma == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->sakit_asma)) {
                                if ($patient->sakit_asma == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai sakit maag?</label>
                        <select class="form-control m-bot15" name="maag" value=''>
                            <option value="1" <?php
                            if (!empty($patient->maag)) {
                                if ($patient->maag == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->maag)) {
                                if ($patient->maag == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit typus?</label>
                        <select class="form-control m-bot15" name="typus" value=''>
                            <option value="1" <?php
                            if (!empty($patient->typus)) {
                                if ($patient->typus == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->typus)) {
                                if ($patient->typus == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit demam berdarah?</label>
                        <select class="form-control m-bot15" name="demam_berdarah" value=''>
                            <option value="1" <?php
                            if (!empty($patient->demam_berdarah)) {
                                if ($patient->demam_berdarah == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->demam_berdarah)) {
                                if ($patient->demam_berdarah == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit TBC?</label>
                        <select class="form-control m-bot15" name="tbc" value=''>
                            <option value="1" <?php
                            if (!empty($patient->tbc)) {
                                if ($patient->tbc == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->tbc)) {
                                if ($patient->tbc == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit pembekuan darah?</label>
                        <select class="form-control m-bot15" name="pembekuan_darah" value=''>
                            <option value="1" <?php
                            if (!empty($patient->pembekuan_darah)) {
                                if ($patient->pembekuan_darah == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->pembekuan_darah)) {
                                if ($patient->pembekuan_darah == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai atau pernah sakit sinusitis?</label>
                        <select class="form-control m-bot15" name="sinusitis" value=''>
                            <option value="1" <?php
                            if (!empty($patient->sinusitis)) {
                                if ($patient->sinusitis == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->sinusitis)) {
                                if ($patient->sinusitis == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai alergi obat?</label>
                        <select class="form-control m-bot15" name="alergi_obat" value=''>
                            <option value="1" <?php
                            if (!empty($patient->alergi_obat)) {
                                if ($patient->alergi_obat == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->alergi_obat)) {
                                if ($patient->alergi_obat == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai alergi makanan?</label>
                        <select class="form-control m-bot15" name="alergi_makanan" value=''>
                            <option value="1" <?php
                            if (!empty($patient->alergi_makanan)) {
                                if ($patient->alergi_makanan == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->alergi_makanan)) {
                                if ($patient->alergi_makanan == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Mempunyai alergi lainnya</label>
                        <input type="text" class="form-control" name="alergi_lain" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Wanita: sedang hamil?</label>
                        <select class="form-control m-bot15" name="wanita_hamil" value=''>
                            <option value="1" <?php
                            if (!empty($patient->wanita_hamil)) {
                                if ($patient->wanita_hamil == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->wanita_hamil)) {
                                if ($patient->wanita_hamil == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Wanita: sedang menyusui?</label>
                        <select class="form-control m-bot15" name="wanita_menyusui" value=''>
                            <option value="1" <?php
                            if (!empty($patient->wanita_menyusui)) {
                                if ($patient->wanita_menyusui == '1') {
                                    echo 'selected';
                                }
                            }
                            ?> > Ya </option>
                            <option value="0" <?php
                            if (!empty($patient->wanita_menyusui)) {
                                if ($patient->wanita_menyusui == '0') {
                                    echo 'selected';
                                }
                            }
                            ?> > Tidak </option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label>Obat jalan yang sedang dilakukan (jika ada, sebutkan nama penyakit & obatnya)</label>
                        <input type="text" class="form-control" name="obat_jalan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label>Terapi jalan yang sedang dilakukan (jika ada, sebutkan nama penyakit & terapinya)</label>
                        <input type="text" class="form-control" name="terapi_jalan" id="exampleInputEmail1" value='' placeholder="">
                    </div>

                    <input type="hidden" name="id" value=''>
                    <input type="hidden" name="p_id" value='<?php
                    if (!empty($patient->patient_id)) {
                        echo $patient->patient_id;
                    }
                    ?>'>
                    <section class="col-md-12">
                        <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                    </section>

                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>
<!-- Edit Patient Modal-->

<div class="modal fade" id="infoModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg"> 
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  Info Pasien</h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="editPatientForm" action="patient/addNew" class="clearfix" method="post" enctype="multipart/form-data">

                    <div class="form-group last col-md-4">
                        <div class="">
                            <div class="fileupload fileupload-new" data-provides="fileupload">
                                <div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">
                                    <img src="//www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" id="img1" alt="" />
                                </div>
                                <div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>
                            </div>
                            <div class="col-md-12">
                                <label for="exampleInputEmail1">ID Pasien: <span class="patientIdClass"></span></label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">NIK</label>
                        <div class="nikClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Nama</label>
                        <div class="nameClass"></div>
                    </div>


                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Email</label>
                        <div class="emailClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Email Bayar</label>
                        <div class="emailbayarClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Umur</label>
                        <div class="ageClass"></div>     
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Alamat</label>
                        <div class="addressClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">RT / RW</label>
                        <div class="rtrwClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Kelurahan</label>
                        <div class="kelurahanClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Kecamatan</label>
                        <div class="kecamatanClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Kota</label>
                        <div class="kotaClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Kode Pos</label>
                        <div class="kodeposClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Provinsi</label>
                        <div class="provinsiClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Jenis Kelamin</label>
                        <div class="genderClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="exampleInputEmail1">Telepon</label>
                        <div class="phoneClass"></div>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Tanggal Lahir</label>
                        <div class="birthdateClass"></div>     
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1">Agama</label>
                        <div class="agamaClass"></div>
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1">Instagram</label>
                        <div class="instagramClass"></div>
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1">Nick Name</label>
                        <div class="nicknameClass"></div>
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1">Pekerjaan</label>
                        <div class="pekerjaanClass"></div>
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1">Catatan</label>
                        <div class="catatanClass"></div>
                    </div>
                    <div class="form-group col-md-4">    
                        <label for="exampleInputEmail1">Dokter</label>
                        <div class="doctorClass"></div>
                    </div>
                    <hr class="form-group col-md-12">
                    <div class="form-group col-md-4">    
                        <h4><b>Riwayat Pasien</b></h4>
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Sebelumnya pernah ke dokter gigi?</label>
                        <div class="pernah_ke_dgClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai penyakit diabetes / kencing manis?</label>
                        <div class="diabetesClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai penyakit hipertensi / darah tinggi?</label>
                        <div class="hipertensiClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai sakit jantung?</label>
                        <div class="sakit_jantungClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai sakit asma?</label>
                        <div class="sakit_asmaClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai sakit maag?</label>
                        <div class="maagClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai sakit typus?</label>
                        <div class="typusClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai atau pernah sakit demam berdarah?</label>
                        <div class="demam_berdarahClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai atau pernah sakit TBC?</label>
                        <div class="tbcClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai atau pernah sakit pembekuan darah?</label>
                        <div class="pembekuan_darahClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai atau pernah sakit sinusitis?</label>
                        <div class="sinusitisClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai alergi obat?</label>
                        <div class="alergi_obatClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai alergi makanan?</label>
                        <div class="tbcClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Mempunyai alergi lainnya?</label>
                        <div class="alergi_lainClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Wanita: sedang hamil?</label>
                        <div class="wanita_hamilClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Wanita: sedang menyusui?</label>
                        <div class="wanita_menyusuiClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Obat jalan yang sedang dilakukan (jika ada, sebutkan nama penyakit & obatnya)</label>
                        <div class="obat_jalanClass"></div>
                    </div>
                    <div class="form-group col-md-4"> 
                    </div>
                    <div class="form-group col-md-8">    
                        <label for="exampleInputEmail1">Terapi jalan yang sedang dilakukan (jika ada, sebutkan nama penyakit & terapinya)</label>
                        <div class="terapi_jalanClass"></div>
                    </div>
                    

                </form>

            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
</div>

<script src="common/js/codearistos.min.js"></script>

<!--
<script>


    var video = document.getElementById('video');
    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({video: true}).then(function (stream) {
            video.src = window.URL.createObjectURL(stream);
            video.play();
        });
    }

    // Elements for taking the snapshot
    var canvas = document.getElementById('canvas');
    var context = canvas.getContext('2d');
    var video = document.getElementById('video');
    // Trigger photo take
    document.getElementById("snap").addEventListener("click", function () {
        context.drawImage(video, 0, 0, 200, 200);
    });

</script>
-->

<script type="text/javascript">

    $(".table").on("click", ".editbutton", function () {
        //    e.preventDefault(e);
        // Get the record's ID via attribute  
        var iid = $(this).attr('data-id');
        $("#img").attr("src", "uploads/cardiology-patient-icon-vector-6244713.jpg");
        $('#editPatientForm').trigger("reset");
        $.ajax({
            url: 'patient/editPatientByJason?id=' + iid,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            // Populate the form fields with the data returned from server

            $('#editPatientForm').find('[name="id"]').val(response.patient.id).end()
            $('#editPatientForm').find('[name="name"]').val(response.patient.name).end()
            $('#editPatientForm').find('[name="nik"]').val(response.patient.nik).end()
            $('#editPatientForm').find('[name="password"]').val(response.patient.password).end()
            $('#editPatientForm').find('[name="email"]').val(response.patient.email).end()
            $('#editPatientForm').find('[name="email_bayar"]').val(response.patient.email_bayar).end()
            $('#editPatientForm').find('[name="address"]').val(response.patient.address).end()
            $('#editPatientForm').find('[name="phone"]').val(response.patient.phone).end()
            $('#editPatientForm').find('[name="sex"]').val(response.patient.sex).end()
            $('#editPatientForm').find('[name="birthdate"]').val(response.patient.birthdate).end()
            $('#editPatientForm').find('[name="bloodgroup"]').val(response.patient.bloodgroup).end()
            $('#editPatientForm').find('[name="agama"]').val(response.patient.agama).end()
            $('#editPatientForm').find('[name="instagram"]').val(response.patient.instagram).end()
            $('#editPatientForm').find('[name="nickname"]').val(response.patient.nickname).end()
            $('#editPatientForm').find('[name="pekerjaan"]').val(response.patient.pekerjaan).end()
            $('#editPatientForm').find('[name="catatan"]').val(response.patient.catatan).end()
            $('#editPatientForm').find('[name="p_id"]').val(response.patient.patient_id).end()

            $('#editPatientForm').find('[name="rtrw"]').val(response.patient.rtrw).end()
            $('#editPatientForm').find('[name="kelurahan"]').val(response.patient.kelurahan).end()
            $('#editPatientForm').find('[name="kecamatan"]').val(response.patient.kecamatan).end()
            $('#editPatientForm').find('[name="kota"]').val(response.patient.kota).end()
            $('#editPatientForm').find('[name="provinsi"]').val(response.patient.provinsi).end()
            $('#editPatientForm').find('[name="kodepos"]').val(response.patient.kodepos).end()

            $('#editPatientForm').find('[name="pernah_ke_dg"]').val(response.patient.pernah_ke_dg).end()
            $('#editPatientForm').find('[name="diabetes"]').val(response.patient.diabetes).end()
            $('#editPatientForm').find('[name="hipertensi"]').val(response.patient.hipertensi).end()
            $('#editPatientForm').find('[name="sakit_jantung"]').val(response.patient.sakit_jantung).end()
            $('#editPatientForm').find('[name="sakit_asma"]').val(response.patient.sakit_asma).end()
            $('#editPatientForm').find('[name="maag"]').val(response.patient.maag).end()
            $('#editPatientForm').find('[name="typus"]').val(response.patient.typus).end()
            $('#editPatientForm').find('[name="demam_berdarah"]').val(response.patient.demam_berdarah).end()
            $('#editPatientForm').find('[name="tbc"]').val(response.patient.tbc).end()
            $('#editPatientForm').find('[name="pembekuan_darah"]').val(response.patient.pembekuan_darah).end()
            $('#editPatientForm').find('[name="sinusitis"]').val(response.patient.sinusitis).end()
            $('#editPatientForm').find('[name="alergi_obat"]').val(response.patient.instagram).end()
            $('#editPatientForm').find('[name="alergi_makanan"]').val(response.patient.alergi_makanan).end()
            $('#editPatientForm').find('[name="alergi_lain"]').val(response.patient.alergi_lain).end()
            $('#editPatientForm').find('[name="wanita_hamil"]').val(response.patient.wanita_hamil).end()
            $('#editPatientForm').find('[name="wanita_menyusui"]').val(response.patient.wanita_menyusui).end()
            $('#editPatientForm').find('[name="obat_jalan"]').val(response.patient.obat_jalan).end()
            $('#editPatientForm').find('[name="terapi_jalan"]').val(response.patient.terapi_jalan).end()

            if (typeof response.patient.img_url !== 'undefined' && response.patient.img_url != '') {
                $("#img").attr("src", response.patient.img_url);
            }


            $('.js-example-basic-single.doctor').val(response.patient.doctor).trigger('change');

            $('#myModal2').modal('show');

        });
    });

</script>

<script type="text/javascript">

    $(".table").on("click", ".inffo", function () {
        //    e.preventDefault(e);
        // Get the record's ID via attribute  
        var iid = $(this).attr('data-id');
        
        $("#img1").attr("src", "uploads/cardiology-patient-icon-vector-6244713.jpg");
        $('.patientIdClass').html("").end()
        $('.nikClass').html("").end()
        $('.nameClass').html("").end()
        $('.emailClass').html("").end()
        $('.emailbayarClass').html("").end()
        $('.addressClass').html("").end()
        $('.phoneClass').html("").end()
        $('.genderClass').html("").end()
        $('.birthdateClass').html("").end()
        $('.bloodgroupClass').html("").end()
        $('.patientidClass').html("").end()
        $('.agamaClass').html("").end()
        $('.instagramClass').html("").end()
        $('.nicknameClass').html("").end()
        $('.pekerjaanClass').html("").end()
        $('.catatanClass').html("").end()

        $('.rtrwClass').html("").end()
        $('.kelurahanClass').html("").end()
        $('.kecamatanClass').html("").end()
        $('.kotaClass').html("").end()
        $('.provinsiClass').html("").end()
        $('.kodeposClass').html("").end()
        
        $('.pernah_ke_dgClass').html("").end()
        $('.diabetesClass').html("").end()
        $('.hipertensiClass').html("").end()
        $('.sakit_jantungClass').html("").end()
        $('.sakit_asmaClass').html("").end()
        $('.maagClass').html("").end()
        $('.typusClass').html("").end()
        $('.demam_berdarahClass').html("").end()
        $('.tbcClass').html("").end()
        $('.pembekuan_darahClass').html("").end()
        $('.sinusitisClass').html("").end()
        $('.alergi_obatClass').html("").end()
        $('.alergi_makananClass').html("").end()
        $('.alergi_lainClass').html("").end()
        $('.wanita_hamilClass').html("").end()
        $('.wanita_menyusuiClass').html("").end()
        $('.obat_jalanClass').html("").end()
        $('.terapi_jalanClass').html("").end()

        $('.doctorClass').html("").end()
        $('.ageClass').html("").end()
        $.ajax({
            url: 'patient/getPatientByJason?id=' + iid,
            method: 'GET', 
            data: '',
            dataType: 'json', 
        }).success(function (response) {
            // Populate the form fields with the data returned from server

            $('.patientIdClass').append(response.patient.id).end()
            $('.nikClass').append(response.patient.nik).end()
            $('.nameClass').append(response.patient.name).end()
            $('.emailClass').append(response.patient.email).end()
            $('.emailbayarClass').append(response.patient.email_bayar).end()
            $('.addressClass').append(response.patient.address).end()
            $('.phoneClass').append(response.patient.phone).end()
            $('.genderClass').append(response.patient.sex).end()
            $('.birthdateClass').append(response.patient.birthdate).end()
            $('.ageClass').append(response.age).end()
            $('.bloodgroupClass').append(response.patient.bloodgroup).end()
            $('.patientidClass').append(response.patient.patient_id).end()
            $('.doctorClass').append(response.doctor.name).end()
            $('.agamaClass').append(response.patient.agama).end()
            $('.instagramClass').append(response.patient.instagram).end()
            $('.nicknameClass').append(response.patient.nickname).end()
            $('.pekerjaanClass').append(response.patient.pekerjaan).end()
            $('.catatanClass').append(response.patient.catatan).end()

            $('.rtrwClass').append(response.patient.rtrw).end()
            $('.kelurahanClass').append(response.patient.kelurahan).end()
            $('.kecamatanClass').append(response.patient.kecamatan).end()
            $('.kotaClass').append(response.patient.kota).end()
            $('.provinsiClass').append(response.patient.provinsi).end()
            $('.kodeposClass').append(response.patient.kodepos).end()

            $('.pernah_ke_dgClass').append(response.patient.pernah_ke_dg).end()
            $('.diabetesClass').append(response.patient.diabetes).end()
            $('.hipertensiClass').append(response.patient.hipertensi).end()
            $('.sakit_jantungClass').append(response.patient.sakit_jantung).end()
            $('.sakit_asmaClass').append(response.patient.sakit_asma).end()
            $('.maagClass').append(response.patient.maag).end()
            $('.typusClass').append(response.patient.typus).end()
            $('.demam_berdarahClass').append(response.patient.demam_berdarah).end()
            $('.tbcClass').append(response.patient.tbc).end()
            $('.pembekuan_darahClass').append(response.patient.pembekuan_darah).end()
            $('.sinusitisClass').append(response.patient.sinusitis).end()
            $('.alergi_obatClass').append(response.patient.alergi_obat).end()
            $('.alergi_makananClass').append(response.patient.alergi_makanan).end()
            $('.alergi_lainClass').append(response.patient.alergi_lain).end()
            $('.wanita_hamilClass').append(response.patient.wanita_hamil).end()
            $('.wanita_menyusuiClass').append(response.patient.wanita_menyusui).end()
            $('.obat_jalanClass').append(response.patient.obat_jalan).end()
            $('.terapi_jalanClass').append(response.patient.terapi_jalan).end()

            if (typeof response.patient.img_url !== 'undefined' && response.patient.img_url != '') {
                $("#img1").attr("src", response.patient.img_url);
            }


            $('#infoModal').modal('show');

        });
    });

</script>

<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,
            //   dom: 'lfrBtip',
            "processing": true,
            "serverSide": true,
            "searchable": true,
            "ajax": {
                url: "patient/getPatient",
                type: 'POST',
            },
            scroller: {
                loadingIndicator: true
            },
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2],
                    }
                },
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 50,
            "order": [[0, "desc"]],
            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            }
        });
        table.buttons().container().appendTo('.custom_buttons');
    });

</script>

<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>