<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-12">
            <header class="panel-heading">
                <?php echo lang('add'); ?> <?php echo lang('case'); ?> 
            </header> 

            <div class=""> 
                <form role="form" action="patient/addMedicalHistory" class="clearfix" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Appointment</label>
                        <select class="form-control m-bot15 js-example-basic-single" name="appointment_id" value="">
                            <?php foreach ($appointmentinput as $app) { ?>
                                <option value="<?php echo $app->id; ?>"> <?php echo $app->id; ?> - <?php echo $app->name; ?> </option> 
                            <?php } ?> 
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="exampleInputEmail1">Dokter</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="dokter" id="exampleInputEmail1" value='<?php echo $this->session->userdata('username'); ?>' readonly='true'>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Kompetensi</label>
                        <select class="form-control m-bot15 js-example-basic-single" name="title[]" value='' multiple>
                            <option value="Akut">Akut</option>
                            <option value="Scaling">Scaling</option>
                            <option value="Tambal Sementara">Tambal Sementara</option> 
                            <option value="Tambal Permanen">Tambal Permanen</option> 
                            <option value="Oral Hygiene">Oral Hygiene</option> 
                            <option value="Cabut">Cabut</option> 
                            <option value="Veneer">Veneer</option> 
                            <option value="Orthodonti (Insersi)">Orthodonti (Insersi)</option> 
                            <option value="Orthodonti (Pindahan)">Orthodonti (Pindahan)</option> 
                            <option value="Orthodonti (Kontrol)">Orthodonti (Kontrol)</option> 
                            <option value="Endodontik">Endodontik</option> 
                            <option value="Prostodonsia">Prostodonsia</option> 
                            <option value="Bleaching">Bleaching</option> 
                            <option value="Bedah Minor">Bedah Minor</option> 
                            <option value="Perdarahan">Perdarahan</option> 
                            <option value="Sinkop">Sinkop</option> 
                            <option value="Anafilaksis Syok">Anafilaksis Syok</option> 
                            <option value="Kecelakaan">Kecelakaan</option>
                            <option value="Cabut Tunda">Cabut Belum Berhasil</option> 
                            <option value="Splinting">Splinting</option> 
                            <option value="TMD">TMD</option> 
                            <option value="Penyakit Mulut">Penyakit Mulut</option> 
                            <option value="Odontektomi">Odontektomi</option> 
                            <option value="Gingivektomi">Gingivektomi</option> 
                            <option value="Cabut Anak">Cabut Anak</option> 
                            <option value="Garansi">Garansi</option> 
                            <option value="Lainnya">Lainnya</option> 
                            <option value="Rujukan">Rujukan</option> 
                            <option value="Selling-Cross Selling">Selling-Cross Selling</option>
                            <option value="Selling-Up Selling">Selling-Up Selling</option>
                            <option value="Veneer Indirect">Veneer Indirect</option>
                            <option value="Veneer Direct">Veneer Direct</option>
                            <option value="Scaling Emergency">Scaling Emergency</option>
                            <option value="Scaling Rutin 6 Bulan">Scaling Rutin 6 Bulan</option>
                            <option value="Scaling Promat">Scaling Promat</option>
                            <option value="Scaling Cross Selling">Scaling Cross Selling</option>
                            <option value="Prosto GTL">Prosto GTL</option>
                            <option value="Prosto Kehilangan Gigi > 70%">Prosto Kehilangan Gigi > 70%</option>
                            <option value="Endo Tunggal">Endo Tunggal</option>
                            <option value="Endo Ganda">Endo Ganda</option>
                            <option value="Ortho Retainer">Ortho Retainer</option>
                            <option value="Ortho Lepas Behel">Ortho Lepas Behel</option>
                            <option value="Ortho Lepasan">Ortho Lepasan</option>
                            <option value="Incline Bite Plane">Incline Bite Plane</option>
                            <option value="Prosto Bridge">Prosto Bridge</option>
                            <option value="Prosto Crown">Prosto Crown</option>
                            <option value="Konservasi Pasak">Konservasi Pasak</option>
                            <option value="Endo Pasak">Endo Pasak</option>
                            <option value="Endo Restorasi">Endo Restorasi</option>
                            <option value="Endo Flare Up">Endo Flare Up</option>
                            <option value="Endo Obturasi">Endo Obturasi</option>
                            <option value="Endo Re-treatment">Endo Re-treatment</option>
                            <option value="Cabut Kompleks">Cabut Kompleks</option>
                            <option value="Perdarahan">Perdarahan</option>
                            <option value="Ortho Crowded">Ortho Crowded</option>
                            <option value="Ortho Crossbite">Ortho Crossbite</option>
                            <option value="Ortho Kelas 3">Ortho Kelas 3</option>
                            <option value="Ortho Multiple Diastema">Ortho Multiple Diastema</option>
                            <option value="Ortho Central Diastema">Ortho Central Diastema</option>
                            <option value="Ortho Protrusi">Ortho Protrusi</option>
                            <option value="Ortho Open Bite">Ortho Open Bite</option>
                            <option value="Ortho Deep Bite">Ortho Deep Bite</option>
                            <option value="Cabut Posterior">Cabut Posterior</option>
                            <option value="Cabut Anterior">Cabut Anterior</option>
                            <option value="Cabut Sisa Akar">Cabut Sisa Akar</option>
                            <option value="Prosto Cekat">Prosto Cekat</option>
                            <option value="Prosto Lepasan">Prosto Lepasan</option>
                            <option value="Abses">Abses</option>
                            <option value="Sakit Gusi">Sakit Gusi</option>
                            <option value="Kuretase">Kuretase</option>
                            <option value="Sakit Gigi">Sakit Gigi</option>
                            <option value="Scaling Manual">Scaling Manual</option>
                            <option value="Merujuk">Merujuk</option>
                            <option value="Perikoronitis">Perikoronitis</option>
                            <option value="Operculectomi">Operculectomi</option>
                            <option value="Insisi Fistule">Insisi Fistule</option>
                            <option value="Insisi Abses">Insisi Abses</option>
                            <option value="Eksisi Mucocele">Eksisi Mucocele</option>
                            <option value="Eksisi Gingiva">Eksisi Gingiva</option>
                            <option value="Eksisi Polip">Eksisi Polip</option>
                            <option value="Implan">Implan</option>
                            <option value="Ortho Self Ligating">Ortho Self Ligating</option>
                            <option value="Ortho Ceramic">Ortho Ceramic</option>
                            <option value="Ortho Versi">Ortho Versi</option>
                            <option value="Ortho Gresi">Ortho Gresi</option>
                            <option value="Ortho Diastema">Ortho Diastema</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Suspect Penyakit Mulut</label>
                        <a href="https://forms.gle/f25gcv9H1VMyDCtGA" target="_blank">Form Google</a>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Screening TMD</label>
                        <a href="https://bit.ly/INDEKS-DIAGNOSTIK-TMD" target="_blank">Form Google</a>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Anamnesa</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="anamnesa" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Catatan CRM</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="catatan" id="exampleInputEmail1" value='' placeholder="minat, hobi, keunikan, kondisi khusus, kepribadian pasien">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Diagnosa</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="diagnosa" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Terapi</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="terapi" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Diskusi Medik</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="diskusi_medik" id="exampleInputEmail1" value='' placeholder="tanya jawab, saran, masukan antar dokter dan medik">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Resep</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="resep" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Jadwal Kontrol Berikutnya</label>
                        <input type="text" class="form-control form-control-inline input-medium default-date-picker" name="kontrol" id="exampleInputEmail1" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Nominal Harga</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="qtyharga" id="exampleInputEmail1" value='' placeholder="" required>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Promat</label>
                        <select class="form-control m-bot15 js-example-basic-single" name="promat" value=''>
                            <option value="Tidak">Tidak Promat</option>
                            <option value="Promat">Ikut Promat</option>
                        </select>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1"> Foto Kasus / Rontgen / Rujukan / Informed Consent / Dll </label>
                        <input type="file" name="img_url">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1"> Foto </label>
                        <input type="file" name="img_before">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1"> Foto Setelah Perawatan (Wajib & Dilarang Posting Foto Bukan Kasus Pasien)</label>
                        <input type="file" name="img_after" required>
                    </div>
                    <input type="hidden" name="redirect" value='patient/caseList'>
                    <?php if ($this->ion_auth->in_group(array('admin', 'Doctor'))) { ?>
                    <section class="col-md-12">
                        <button type="submit" name="submit" class="btn btn-info submit_button pull-right"><?php echo lang('submit'); ?></button>
                    </section>
                    <?php } ?>
                </form>
            </div>

        </section>


        <section class="col-md-12">
            <header class="panel-heading">
                <?php echo lang('all'); ?> <?php echo lang('case'); ?>
            </header> 
            <div class="panel-body"> 

                <div class="adv-table editable-table table-responsive">
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Payment</th>
                                <th><?php echo lang('date'); ?></th>
                                <th><?php echo lang('patient'); ?></th>
                                <th><?php echo lang('address'); ?></th>
                                <th><?php echo lang('phone'); ?></th>
                                <th>Kompetensi</th>
                                <th><?php echo lang('hospital'); ?></th>
                                <th>Dokter</th>
                                <!-- <th>Dokter Tamu</th> -->
                                <th>Kontrol</th>
                                <th class="no-print">View</th>
                                <?php 
                                    // $current_user = $this->ion_auth->get_user_id();
                                    // $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
                                    if ($this->ion_auth->in_group(array('admin', 'Doctor'))) {
                                ?>
                                <th class="no-print">Action</th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>

        </section>

    </section>
    <!-- page end-->
</section>
</section>
<!--main content end-->
<!--footer start-->

<!-- Edit Department Modal-->
<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('edit_medical_history'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="medical_historyEditForm" class="clearfix" action="patient/addMedicalHistory" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-6">
                        <!-- <label for="exampleInputEmail1">Tanggal Perawatan</label> -->
                        <!-- <input type="text" class="form-control form-control-inline input-medium default-date-picker" name="date" id="exampleInputEmail1" value='' placeholder="" disabled> -->
                        <input type="hidden" class="form-control form-control-inline input-medium default-date-picker" name="appointment_id" id="exampleInputEmail1" value='' placeholder="">
                        <input type="hidden" class="form-control form-control-inline input-medium default-date-picker" name="id" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Dokter</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="dokter" id="exampleInputEmail1" value='<?php echo $this->session->userdata('username'); ?>' readonly='true'>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Kompetensi</label>
                        <select class="form-control m-bot15 js-example-basic-single patient" name="title[]" value='' multiple>
                            <option value="Akut">Akut</option>
                            <option value="Scaling">Scaling</option>
                            <option value="Tambal Sementara">Tambal Sementara</option> 
                            <option value="Tambal Permanen">Tambal Permanen</option> 
                            <option value="Oral Hygiene">Oral Hygiene</option> 
                            <option value="Cabut">Cabut</option> 
                            <option value="Veneer">Veneer</option> 
                            <option value="Orthodonti (Insersi)">Orthodonti (Insersi)</option> 
                            <option value="Orthodonti (Pindahan)">Orthodonti (Pindahan)</option> 
                            <option value="Orthodonti (Kontrol)">Orthodonti (Kontrol)</option> 
                            <option value="Endodontik">Endodontik</option> 
                            <option value="Prostodonsia">Prostodonsia</option> 
                            <option value="Bleaching">Bleaching</option> 
                            <option value="Bedah Minor">Bedah Minor</option> 
                            <option value="Perdarahan">Perdarahan</option> 
                            <option value="Sinkop">Sinkop</option> 
                            <option value="Anafilaksis Syok">Anafilaksis Syok</option> 
                            <option value="Kecelakaan">Kecelakaan</option>
                            <option value="Cabut Tunda">Cabut Belum Berhasil</option> 
                            <option value="Splinting">Splinting</option>
                            <option value="TMD">TMD</option> 
                            <option value="Penyakit Mulut">Penyakit Mulut</option> 
                            <option value="Odontektomi">Odontektomi</option> 
                            <option value="Gingivektomi">Gingivektomi</option> 
                            <option value="Cabut Anak">Cabut Anak</option> 
                            <option value="Garansi">Garansi</option> 
                            <option value="Lainnya">Lainnya</option> 
                            <option value="Rujukan">Rujukan</option>
                            <option value="Selling-Cross Selling">Selling-Cross Selling</option>
                            <option value="Selling-Up Selling">Selling-Up Selling</option>
                            <option value="Veneer Indirect">Veneer Indirect</option>
                            <option value="Veneer Direct">Veneer Direct</option>
                            <option value="Scaling Emergency">Scaling Emergency</option>
                            <option value="Scaling Rutin 6 Bulan">Scaling Rutin 6 Bulan</option>
                            <option value="Scaling Promat">Scaling Promat</option>
                            <option value="Scaling Cross Selling">Scaling Cross Selling</option>
                            <option value="Prosto GTL">Prosto GTL</option>
                            <option value="Prosto Kehilangan Gigi > 70%">Prosto Kehilangan Gigi > 70%</option>
                            <option value="Endo Tunggal">Endo Tunggal</option>
                            <option value="Endo Ganda">Endo Ganda</option>
                            <option value="Ortho Retainer">Ortho Retainer</option>
                            <option value="Ortho Lepas Behel">Ortho Lepas Behel</option>
                            <option value="Ortho Lepasan">Ortho Lepasan</option>
                            <option value="Incline Bite Plane">Incline Bite Plane</option>
                            <option value="Prosto Bridge">Prosto Bridge</option>
                            <option value="Prosto Crown">Prosto Crown</option>
                            <option value="Konservasi Pasak">Konservasi Pasak</option>
                            <option value="Endo Pasak">Endo Pasak</option>
                            <option value="Endo Restorasi">Endo Restorasi</option>
                            <option value="Endo Flare Up">Endo Flare Up</option>
                            <option value="Endo Obturasi">Endo Obturasi</option>
                            <option value="Endo Re-treatment">Endo Re-treatment</option>
                            <option value="Cabut Kompleks">Cabut Kompleks</option>
                            <option value="Perdarahan">Perdarahan</option>
                            <option value="Ortho Crowded">Ortho Crowded</option>
                            <option value="Ortho Crossbite">Ortho Crossbite</option>
                            <option value="Ortho Kelas 3">Ortho Kelas 3</option>
                            <option value="Ortho Multiple Diastema">Ortho Multiple Diastema</option>
                            <option value="Ortho Central Diastema">Ortho Central Diastema</option>
                            <option value="Ortho Protrusi">Ortho Protrusi</option>
                            <option value="Ortho Open Bite">Ortho Open Bite</option>
                            <option value="Ortho Deep Bite">Ortho Deep Bite</option>
                            <option value="Cabut Posterior">Cabut Posterior</option>
                            <option value="Cabut Anterior">Cabut Anterior</option>
                            <option value="Cabut Sisa Akar">Cabut Sisa Akar</option>
                            <option value="Prosto Cekat">Prosto Cekat</option>
                            <option value="Prosto Lepasan">Prosto Lepasan</option> 
                            <option value="Abses">Abses</option>
                            <option value="Sakit Gusi">Sakit Gusi</option>
                            <option value="Kuretase">Kuretase</option>
                            <option value="Sakit Gigi">Sakit Gigi</option>
                            <option value="Scaling Manual">Scaling Manual</option>
                            <option value="Merujuk">Merujuk</option>
                            <option value="Perikoronitis">Perikoronitis</option>
                            <option value="Operculectomi">Operculectomi</option>
                            <option value="Insisi Fistule">Insisi Fistule</option>
                            <option value="Insisi Abses">Insisi Abses</option>
                            <option value="Eksisi Mucocele">Eksisi Mucocele</option>
                            <option value="Eksisi Gingiva">Eksisi Gingiva</option>
                            <option value="Eksisi Polip">Eksisi Polip</option>
                            <option value="Implan">Implan</option>
                            <option value="Ortho Selft Ligating">Ortho Selft Ligating</option>
                            <option value="Ortho Ceramic">Ortho Ceramic</option>
                            <option value="Ortho Versi">Ortho Versi</option>
                            <option value="Ortho Gresi">Ortho Gresi</option>
                            <option value="Ortho Diastema">Ortho Diastema</option>
                        </select>                   
                    </div>
                    <!-- <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Dokter Tamu</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="dokter_tamu" id="exampleInputEmail1" value='' placeholder="">
                    </div> -->
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Anamnesa</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="anamnesa" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Catatan CRM</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="catatan" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Diagnosa</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="diagnosa" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Terapi</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="terapi" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Diskusi Medik</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="diskusi_medik" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Resep</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="resep" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Jadwal Kontrol Berikutnya</label>
                        <input type="text" class="form-control form-control-inline input-medium default-date-picker" name="kontrol" id="exampleInputEmail1" value='' placeholder="" readonly="" required="">
                    </div>
                    <!-- <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Harga</label>
                        <select class="form-control m-bot15 js-example-basic-single patient" name="harga[]" value='' multiple>
                            <?php foreach ($payment_category as $pc) { ?>
                                <option value="<?php echo $pc->category; ?>"> <?php echo $pc->category . ' - Rp ' . $pc->c_price; ?> </option> 
                            <?php } ?> 
                        </select>
                    </div> -->
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Nominal Harga</label>
                        <input type="text" class="form-control form-control-inline input-medium" name="qtyharga" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Foto Kasus / Rontgen / Rujukan / Informed Consent / Dll</label>
                        <input type="file" name="img_url" multiple>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Foto</label>
                        <input type="file" name="img_before">
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Foto Setelah Perawatan (Wajib & Dilarang Posting Foto Bukan Kasus Pasien)</label>
                        <input type="file" name="img_after">
                    </div>
                    <input type="hidden" name="id" value=''>
                    <input type="hidden" name="redirect" value='patient/caseList'>
                    <div class="col-md-12">
                        <button type="submit" name="submit" class="btn btn-info submit_button pull-right">Submit</button>
                    </div>
                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<div class="modal fade" id="caseModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close no-print" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">  <?php echo lang('case'); ?> <?php echo lang('details'); ?></h4>
            </div>
            <div class="modal-body row">
                <form role="form" id="medical_historyEditForm" class="clearfix" action="patient/addMedicalHistory" method="post" enctype="multipart/form-data">
                    <div class="form-group col-md-12 row">
                        <div class="form-group col-md-4 case_date_block">
                            <label for="exampleInputEmail1"><?php echo lang('case'); ?> <?php echo lang('creation'); ?> <?php echo lang('date'); ?></label>
                            <div class="case_date"></div>
                        </div>
                        <div class="form-group col-md-4 case_patient_block">
                            <label for="exampleInputEmail1"><?php echo lang('patient'); ?></label>
                            <div class="case_patient"></div>
                            <div class="case_patient_id"></div>
                            <div class="case_patient_nik"></div>
                        </div>
                        <div class="form-group col-md-4 case_patient_block">
                            <label for="exampleInputEmail1">Dokter</label>
                            <div class="case_dokter"></div>
                        </div> 
                        <div>
                            <hr>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Kompetensi </label>
                        <div class="case_title"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Anamnesa</label>
                        <div class="case_anamnesa"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Catatan CRM</label>
                        <div class="case_catatan"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Diagnosa</label>
                        <div class="case_diagnosa"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Terapi</label>
                        <div class="case_terapi"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Diskusi Medik</label>
                        <div class="case_diskusi_medik"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Resep</label>
                        <div class="case_resep"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Jadwal Kontrol Berikutnya</label>
                        <div class="case_kontrol"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Promat</label>
                        <div class="case_promat"></div>
                        <hr>
                    </div>
                    <!-- <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Nominal Harga (Data lama)</label>
                        <div class="case_harga"></div>
                        <hr>
                    </div> -->
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1">Nominal Harga</label>
                        <div class="case_qtyharga"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label for="exampleInputEmail1"> <?php echo lang('details'); ?></label>
                        <div class="case_details"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label> Image</label>
                        <div class="case_img_url"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label> Image</label>
                        <div class="case_img_before"></div>
                        <hr>
                    </div>
                    <div class="form-group col-md-12">
                        <label> Image After</label>
                        <div class="case_img_after"></div>
                        <hr>
                    </div>


                    <!-- <div class="panel col-md-12">
                        <h5 class="pull-right">
                            <?php echo $settings->title . '<br>' . $settings->address; ?>
                        </h5>
                    </div> -->


                    <!-- <div class="panel col-md-12 no-print">
                        <a class="btn btn-info invoice_button pull-right" onclick="javascript:window.print();"><i class="fa fa-print"></i> <?php echo lang('print'); ?> </a>
                    </div> -->

                </form>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

<style>

    @media print {

        .modal-content{
            width: 100%;
        }


        .modal{
            overflow: hidden;
        }

        .case_date_block{
            width: 50%;
            float: left;
        }

        .case_patient_block{
            width: 50%;
            float: left;
        }

    }



</style>

<?php
$current_user = $this->ion_auth->get_user_id();
if ($this->ion_auth->in_group('Doctor')) {
    $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
}
?>

<script src="common/js/codearistos.min.js"></script>
<script type="text/javascript">
$(".table").on("click", ".editbutton", function () {
    // Get the record's ID via attribute  
    var iid = $(this).attr('data-id');

    $.ajax({
        url: 'patient/editMedicalHistoryByJason?id=' + iid,
        method: 'GET',
        data: '',
        dataType: 'json',
    }).success(function (response) {
        // Populate the form fields with the data returned from server
        var de = response.medical_history.date * 1000;
        var d = new Date(de);
        var da = d.getDate() + '-' + (d.getMonth() + 1) + '-' + d.getFullYear();
        $('#medical_historyEditForm').find('[name="id"]').val(response.medical_history.id).end()
        $('#medical_historyEditForm').find('[name="date"]').val(da).end()
        $('#medical_historyEditForm').find('[name="appointment_id"]').val(response.medical_history.appointment_id).end()
        // $('#medical_historyEditForm').find('[name="patient"]').val(response.medical_history.patient_id).end()
        $('#medical_historyEditForm').find('[name="title"]').val(response.medical_history.title).end()
        $('#medical_historyEditForm').find('[name="dokter_tamu"]').val(response.medical_history.dokter_tamu).end()
        $('#medical_historyEditForm').find('[name="anamnesa"]').val(response.medical_history.anamnesa).end()
        $('#medical_historyEditForm').find('[name="catatan"]').val(response.medical_history.catatan).end()
        $('#medical_historyEditForm').find('[name="diagnosa"]').val(response.medical_history.diagnosa).end()
        $('#medical_historyEditForm').find('[name="terapi"]').val(response.medical_history.terapi).end()
        $('#medical_historyEditForm').find('[name="diskusi_medik"]').val(response.medical_history.diskusi_medik).end()
        $('#medical_historyEditForm').find('[name="resep"]').val(response.medical_history.resep).end()
        $('#medical_historyEditForm').find('[name="kontrol"]').val(response.medical_history.kontrol).end()
        $('#medical_historyEditForm').find('[name="qtyharga"]').val(response.medical_history.qtyharga).end()
        $('#medical_historyEditForm').find('[name="appointment_id"]').val(response.medical_history.appointment_id).end()
        // CKEDITOR.instances['editor'].setData(response.medical_history.description)

        $('.js-example-basic-single-appointment').val(response.medical_history.appointment_id).trigger('change');
        // $('.js-example-basic-single.patient').val(response.medical_history.patient_id).trigger('change');

        $('#myModal2').modal('show');

    });
});
</script>

<script type="text/javascript">
    $(".table").on("click", ".case", function () {
        // Get the record's ID via attribute  
        var iid = $(this).attr('data-id');

        $('.case_date').html("").end()
        $('.case_details').html("").end()
        $('.case_title').html("").end()
        $('.case_anamnesa').html("").end()
        $('.case_catatan').html("").end()
        $('.case_diagnosa').html("").end()
        $('.case_terapi').html("").end()
        $('.case_diskusi_medik').html("").end()
        $('.case_resep').html("").end()
        $('.case_kontrol').html("").end()
        $('.case_harga').html("").end()
        $('.case_qtyharga').html("").end()
        $('.case_promat').html("").end()
        $('.case_patient').html("").end()
        $('.case_patient_id').html("").end()
        $('.case_patient_nik').html("").end()
        $('.case_img_before').html("").end()
        $('.case_img_after').html("").end()
        $('.case_img_url').html("").end()
        $('.case_dokter').html("").end()
        $.ajax({
            url: 'patient/getCaseDetailsByJason?id=' + iid,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            // Populate the form fields with the data returned from server
            var de = response.case.date * 1000;
            var d = new Date(de);


            var monthNames = [
                "January", "February", "March",
                "April", "May", "June", "July",
                "August", "September", "October",
                "November", "December"
            ];

            var day = d.getDate();
            var monthIndex = d.getMonth();
            var year = d.getFullYear();

            var da = day + ' ' + monthNames[monthIndex] + ', ' + year;


            $('.case_date').append(da).end()
            $('.case_patient').append(response.patient.name).end()
            $('.case_patient_id').append('ID: ' + response.patient.id).end()
            $('.case_patient_nik').append('NIK: ' + response.patient.nik).end()
            $('.case_title').append(response.case.title).end()
            $('.case_anamnesa').append(response.case.anamnesa).end()
            $('.case_catatan').append(response.case.catatan).end()
            $('.case_diagnosa').append(response.case.diagnosa).end()
            $('.case_terapi').append(response.case.terapi).end()
            $('.case_diskusi_medik').append(response.case.diskusi_medik).end()
            $('.case_resep').append(response.case.resep).end()
            $('.case_kontrol').append(response.case.kontrol).end()
            $('.case_details').append(response.case.description).end()
            $('.case_img_before').append('<img src=' + response.case.img_before + ' width="550">').end()
            $('.case_img_after').append('<img src=' + response.case.img_after + ' width="550">').end()
            $('.case_img_url').append('<img src=' + response.case.img_url + ' width="550">').end()
            $('.case_dokter').append(response.case.dokter).end()
            $('.case_harga').append(response.case.harga).end()
            $('.case_qtyharga').append(response.case.qtyharga).end()
            $('.case_promat').append(response.case.promat).end()
            $('#caseModal').modal('show');

        });
    });
</script>


<script>

    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,
            //   dom: 'lfrBtip',

            "processing": true,
            "serverSide": true,
            "searchable": true,
            "ajax": {
                url: "patient/getCaseList",
                type: 'POST',
            },
            scroller: {
                loadingIndicator: true
            },
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2],
                    }
                },
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 10,
            order: [[0, "desc"]],
            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            },

        });

        table.buttons().container()
                .appendTo('.custom_buttons');
    });

</script>

<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>
