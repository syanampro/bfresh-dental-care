<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-3">
            <header class="panel-heading clearfix">
                <div class="">
                    Info Pasien
                </div>
            </header> 
            <aside class="profile-nav">
                <section class="">
                    <div class="user-heading round">
                        <?php if($patient->img_url){ ?>
                        <a href="#">
                            <img src="<?php echo $patient->img_url; ?>" alt="">
                        </a>
                        <?php } ?>
                        <h1> <?php echo $patient->name; ?> </h1>
                        <p> <?php echo $patient->email; ?> </p>
                    </div>
                    <ul class="nav nav-pills nav-stacked">
                        <li class="active"> Nama Pasien<span class="label pull-right r-activity"><?php echo $patient->name; ?></span></li>
                        <li>  ID Pasien <span class="label pull-right r-activity"><?php echo $patient->id; ?></span></li>
                        <li>  Agama<span class="label pull-right r-activity"><?php echo $patient->agama; ?></span></li>
                        <li>  Jenis Kelamin<span class="label pull-right r-activity"><?php echo $patient->sex; ?></span></li>
                        <li>  Tanggal Lahir<span class="label pull-right r-activity"><?php echo $patient->birthdate; ?></span></li>
                        <!-- <li>  Alamat<span class="label pull-right r-activity"><?php echo $patient->address; ?></span></li> -->
                        <li>  No Telepon<span class="label pull-right r-activity"><?php echo $patient->phone; ?></span></li>
                        <li>  Email<span class="label pull-right r-activity"><?php echo $patient->email; ?></span></li>
                    </ul>
                </section>
            </aside>
        </section>
        <section class="col-md-9">
            <header class="panel-heading clearfix">
                <div class="col-md-7">
                    Riwayat | <?php echo $patient->name; ?>
                </div>
                <div class="col-md-5 pull-right">
                    <!-- <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button> -->
                </div>
            </header>
            <section class="panel-body">   
                <header class="panel-heading tab-bg-dark-navy-blueee">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#appointments">Reservasi</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#home">Riwayat Perawatan</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#timeline">Garis Waktu</a> 
                        </li>
                    </ul>
                </header>
                <div class="panel">
                    <div class="tab-content">
                        <div id="appointments" class="tab-pane active">
                            <div class="">
                                <?php if (!$this->ion_auth->in_group('Patient')) { ?>
                                    <div class=" no-print">
                                        <a class="btn btn-info btn_width btn-xs" data-toggle="modal" href="#addAppointmentModal">
                                            <i class="fa fa-plus-circle"> </i> <?php echo lang('add_new'); ?> 
                                        </a>
                                    </div>
                                <?php } else { ?>
                                    <div class=" no-print">
                                        <a class="btn btn-info btn_width btn-xs" data-toggle="modal" href="#addAppointmentModal">
                                            <i class="fa fa-plus-circle"> </i> Buat Reservasi
                                        </a>
                                    </div>
                                <?php } ?>
                                <div class="adv-table editable-table ">
                                    <table class="table table-striped table-hover table-bordered" id="">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Waktu</th>
                                                <th>Dokter</th>
                                                <th>Status</th>
                                                <th>Cabang</th>
                                                <!-- <?php if ($this->ion_auth->in_group('admin')) { ?>
                                                    <th class="no-print"><?php echo lang('options'); ?></th>
                                                <?php } ?> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($appointments as $appointment) { ?>
                                                <tr class="">

                                                    <td><?php echo date('d-m-Y', $appointment->date); ?></td>
                                                    <td><?php echo $appointment->time_slot; ?></td>
                                                    <td>
                                                        <?php
                                                        $doctor_details = $this->doctor_model->getDoctorById($appointment->doctor);
                                                        if (!empty($doctor_details)) {
                                                            $appointment_doctor = $doctor_details->name;
                                                        } else {
                                                            $appointment_doctor = '';
                                                        }
                                                        echo $appointment_doctor;
                                                        ?>
                                                    </td>
                                                    <td><?php echo $appointment->status; ?></td>
                                                    <td><?php echo $appointment->hospital; ?></td>
                                                    <!-- <?php if ($this->ion_auth->in_group('admin')) { ?>
                                                        <td class="no-print">
                                                            <button type="button" class="btn btn-info btn-xs btn_width editAppointmentButton" title="<?php echo lang('edit'); ?>" data-toggle="modal" data-id="<?php echo $appointment->id; ?>"><i class="fa fa-edit"></i> </button>   
                                                            <a class="btn btn-info btn-xs btn_width delete_button" title="<?php echo lang('delete'); ?>" href="appointment/delete?id=<?php echo $appointment->id; ?>" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i> </a>
                                                        </td>
                                                    <?php } ?> -->
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div id="home" class="tab-pane">
                            <div class="">
                                <div class="adv-table editable-table ">
                                    <table class="table table-striped table-hover table-bordered" id="">
                                        <thead>
                                            <tr>
                                                <th>Tanggal</th>
                                                <th>Kompetensi</th>
                                                <?php if (!$this->ion_auth->in_group(array('Patient'))) { ?>
                                                    <th>Anamnesa</th>
                                                    <th>Diagnosa</th>
                                                    <th>Terapi</th>
                                                    <th>Resep</th>
                                                <?php } ?>
                                                <th>Dokter</th>
                                                <th>Cabang</th>
                                                <!-- <?php if (!$this->ion_auth->in_group(array('Patient'))) { ?>
                                                    <th class="no-print"><?php echo lang('options'); ?></th>
                                                <?php } ?> -->
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($medical_histories as $medical_history) { ?>
                                                <tr class="">

                                                    <td><?php echo date('d-m-Y', $medical_history->date); ?></td>
                                                    <td><?php echo $medical_history->title; ?></td>
                                                    <?php if (!$this->ion_auth->in_group(array('Patient'))) { ?>
                                                    <td><?php echo $medical_history->anamnesa; ?></td>
                                                    <td><?php echo $medical_history->diagnosa; ?></td>
                                                    <td><?php echo $medical_history->terapi; ?></td>
                                                    <td><?php echo $medical_history->resep; ?></td>
                                                    <?php } ?>
                                                    <td><?php echo $medical_history->dokter; ?></td>
                                                    <td><?php echo $medical_history->hospital; ?></td>
                                                    <!-- <?php if (!$this->ion_auth->in_group(array('Patient'))) { ?>
                                                        <td class="no-print">
                                                            <button type="button" class="btn btn-info btn-xs btn_width editbutton" title="<?php echo lang('edit'); ?>" data-toggle="modal" data-id="<?php echo $medical_history->id; ?>"><i class="fa fa-edit"></i> </button>   
                                                            <a class="btn btn-info btn-xs btn_width delete_button" title="<?php echo lang('delete'); ?>" href="patient/deleteCaseHistory?id=<?php echo $medical_history->id; ?>" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"></i> </a>
                                                        </td>
                                                    <?php } ?> -->
                                                </tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>




                                    <!--
                                                                        <form role="form" action="patient/addMedicalHistory" class="clearfix" method="post" enctype="multipart/form-data">
                                                                            <div class="form-group col-md-12">
                                                                                <label class=""> <?php echo lang('case'); ?> <?php echo lang('history'); ?></label>
                                                                                <div class="">
                                                                                    <textarea class="ckeditor form-control" name="description" id="description" value="" rows="100" cols="50">      
                                    <?php foreach ($medical_histories as $medical_history) { ?>         
                                                                                                                                                                            <td><?php echo $medical_history->description; ?></td>
                                    <?php } ?>
                                                                                    </textarea>
                                                                                </div>
                                                                            </div>
                                    
                                                                            <input type="hidden" name="patient_id" value='<?php echo $patient->id; ?>'>
                                                                            <input type="hidden" name="id" value='<?php echo $medical_history->id ?>'>
                                                                            <div class="form-group col-md-12">
                                                                                <button type="submit" name="submit" class="btn btn-info submit_button pull-right"><?php echo lang('save'); ?></button>
                                                                            </div>
                                                                        </form>
                                    
                                    -->



                                </div>
                            </div>
                        </div>
                        <div id="timeline" class="tab-pane"> 
                            <div class="">
                                <div class="">
                                    <section class="panel ">
                                        <header class="panel-heading">
                                            Timeline
                                        </header>
                                        <!--
                                        <div class=" profile-activity" >
                                            <h5 class="pull-right">12 August 2013</h5>
                                            <div class="activity terques">
                                                <span>
                                                    <i class="fa fa-shopping-cart"></i>
                                                </span>
                                                <div class="activity-desk">
                                                    <div class="panel">
                                                        <div class="">
                                                            <div class="arrow"></div>
                                                            <i class=" fa fa-clock-o"></i>
                                                            <h4>10:45 AM</h4>
                                                            <p>Purchased new equipments for zonal office setup and stationaries.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        -->

                                        <?php
                                        if (!empty($timeline)) {
                                            krsort($timeline);
                                            foreach ($timeline as $key => $value) {
                                                echo $value;
                                            }
                                        }
                                        ?>

                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </section>
    </section>
    <!-- page end-->
</section>
</section>
<!--main content end-->
<!--footer start-->

<?php
$current_user = $this->ion_auth->get_user_id();
if ($this->ion_auth->in_group('Doctor')) {
    $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
}
?>

<!-- Add Appointment Modal-->
<div class="modal fade" id="addAppointmentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Buat Reservasi</h4>
            </div>
            <div class="modal-body">
                <form role="form" action="appointment/addNew" class="clearfix row" method="post" enctype="multipart/form-data">
                    <div class="col-md-4 panel">
                        <label for="exampleInputEmail1">Pasien</label>
                        <select class="form-control m-bot15 js-example-basic-single pos_select" id="pos_select" name="patient" value=''> 
                            <option value="<?php echo $patient->id; ?>"><?php echo $patient->name; ?> </option>
                        </select>
                    </div>
                    <div class="col-md-4 panel">
                        <label for="exampleInputEmail1">Cabang</label>
                        <select class="form-control m-bot15 js-example-basic-single" id="cabang" name="hospital_id" value=''>  
                            <option value="">Pilih .....</option>
                            <?php foreach ($hospitals as $hospital) { ?>
                                <option value="<?php echo $hospital->id; ?>">
                                <?php echo $hospital->name; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-4 panel">
                        <label for="exampleInputEmail1">Dokter</label>
                        <select class="form-control m-bot15 js-example-basic-single" id="adoctors" name="doctor" value=''>  
                            <option value="">Pilih .....</option>
                            <!-- <?php foreach ($doctors as $doctor) { ?>
                                <option value="<?php echo $doctor->id; ?>"<?php
                                if (!empty($payment->doctor)) {
                                    if ($payment->doctor == $doctor->id) {
                                        echo 'selected';
                                    }
                                }
                                ?>><?php echo $doctor->name; ?> </option>
                                    <?php } ?> -->
                        </select>
                    </div>
                    <div class="col-md-4 panel">
                        <label for="exampleInputEmail1">Tanggal</label>
                        <input type="text" class="form-control default-date-picker" id="date" readonly="" name="date" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <div class="col-md-4 panel">
                        <label class="">Waktu Tersedia</label> 
                        <select class="form-control m-bot15" name="time_slot" id="aslots" value=''> 

                        </select>
                    </div>
                    <div class="col-md-4 panel"> 
                        <label for="exampleInputEmail1">Status Reservasi</label>
                        <select class="form-control m-bot15" name="status" value=''>

                            <?php if (!$this->ion_auth->in_group('Patient')) { ?>
                                <option value="Pending Confirmation" <?php
                                ?> > Belum Dikonfirmasi </option>
                                <option value="Confirmed" <?php
                                ?> > Terkonfirmasi </option>
                                <option value="Treated" <?php
                                ?> > Selesai </option>
                                <option value="Cancelled" <?php ?> > Batal </option>
                            <?php } else { ?>
                                <option value="Requested" <?php ?> > Permintaan </option> 
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-8 panel">
                        <label for="exampleInputEmail1">Catatan</label>
                        <input type="text" class="form-control" name="remarks" id="exampleInputEmail1" value='' placeholder="">
                    </div>
                    <input type="hidden" name="redirect" value='patient/medicalHistory?id=<?php echo $patient->id; ?>'>
                    <input type="hidden" name="request" value='<?php
                    if ($this->ion_auth->in_group(array('Patient'))) {
                        echo 'Yes';
                    }
                    ?>'>
                    <div class="col-md-12 panel">
                        <button type="submit" name="submit" class="btn btn-info pull-right">Jadwalkan</button>
                    </div>

                </form>

            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<!-- Add Appointment Modal-->

<style>
    thead {
        background: #f1f1f1; 
        border-bottom: 1px solid #ddd; 
    }
    .btn_width{
        margin-bottom: 20px;
    }
    .tab-content{
        padding: 20px 0px;
    }
    .cke_editable {
        min-height: 1000px;
    }
</style>

<script src="common/js/codearistos.min.js"></script>

<script type="text/javascript">

    $(document).ready(function () {
        $("#cabang").change(function () {
            var hospitalId = $(this).val();
            $('#adoctors').find('option').remove();
            $.ajax({
                url: 'patient/getDoctorsByHospital?hospital_id=' + hospitalId,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).done(function (response) {
                var doctors = response.doctors;
                $.each(doctors, function (key, value) {
                    $('#adoctors').append($('<option>').text(value.name).val(value.id)).end();
                });
            });
        });
    });

    $(document).ready(function () {
        $("#adoctors").change(function () {
            var iid = $('#date').val();
            var doctorr = $('#adoctors').val();
            $('#aslots').find('option').remove();
            $.ajax({
                url: 'schedule/getAvailableSlotByDoctorByDateByJason?date=' + iid + '&doctor=' + doctorr,
                method: 'GET',
                data: '',
                dataType: 'json',
            }).success(function (response) {
                var slots = response.aslots;
                $.each(slots, function (key, value) {
                    $('#aslots').append($('<option>').text(value).val(value)).end();
                });
                if ($('#aslots').has('option').length == 0) {                    //if it is blank. 
                    $('#aslots').append($('<option>').text('Tidak Ada Jadwal').val('Tidak Terpilih')).end();
                }
            });
        });

    });

    $(document).ready(function () {
        var iid = $('#date').val();
        var doctorr = $('#adoctors').val();
        $('#aslots').find('option').remove();
        $.ajax({
            url: 'schedule/getAvailableSlotByDoctorByDateByJason?date=' + iid + '&doctor=' + doctorr,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            var slots = response.aslots;
            $.each(slots, function (key, value) {
                $('#aslots').append($('<option>').text(value).val(value)).end();
            });
            if ($('#aslots').has('option').length == 0) { 
                $('#aslots').append($('<option>').text('Tidak Ada Jadwal').val('Tidak Terpilih')).end();
            }
        });

    });

    $(document).ready(function () {
        $('#date').datepicker({
            format: "dd-mm-yyyy",
            autoclose: true,
        })
                .change(dateChanged)
                .on('changeDate', dateChanged);
    });

    function dateChanged() {
        var iid = $('#date').val();
        var doctorr = $('#adoctors').val();
        $('#aslots').find('option').remove();
        $.ajax({
            url: 'schedule/getAvailableSlotByDoctorByDateByJason?date=' + iid + '&doctor=' + doctorr,
            method: 'GET',
            data: '',
            dataType: 'json',
        }).success(function (response) {
            var slots = response.aslots;
            $.each(slots, function (key, value) {
                $('#aslots').append($('<option>').text(value).val(value)).end();
            });
            if ($('#aslots').has('option').length == 0) { 
                $('#aslots').append($('<option>').text('Tidak Ada Jadwal').val('Tidak Terpilih')).end();
            }
        });
    }
</script>

<script>
    $(document).ready(function () {
        $('#editable-sample').DataTable({
            responsive: true,

            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1],
                    }
                },
            ],

            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: -1,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_ records per page",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            }


        });
    });
</script>

<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>