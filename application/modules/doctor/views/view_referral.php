<!--main content start-->
<style>
    td {
        padding:5px;
    }
</style>
<section id="main-content">
    <section class="wrapper site-min-height">










        <?php
        $doctor = $this->doctor_model->getDoctorById($referral->doctor);
        $patient = $this->patient_model->getPatientById($referral->patient);
        ?>

        <div class="col-md-8 panel bg_container margin_top" id="referral">

            <div class="bg_referral" style="min-height:450px;">

                <div class="panel-body">
                    <div class="col-md-12 pull-right text-center"> 
                        <img src="<?php echo $settings->logo; ?>" height="150"><br>
                        <?php echo $settings->address; ?>
                    </div>
                </div>
                <hr>
                <div class="panel-body">
                    <center><h4>SURAT RUJUKAN</h4></center>
                    <table>
                        <tr>
                            <td width="30%">Yth. Menghadapkan Pasien</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Nama</td>
                            <td>: <?php echo $patient->name ?></td>
                        </tr>
                        <?php
                            $age = '';
                            if (!empty($patient->birthdate)) {
                                $birthDate = strtotime($patient->birthdate);
                                $birthDate = date('m/d/Y', $birthDate);
                                $birthDate = explode("/", $birthDate);
                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1) : (date("Y") - $birthDate[2]));
                                $age = $age . ' Year(s)';
                            }
                        ?>
                        <tr>
                            <td>Umur</td>
                            <td>: <?php echo $age ?></td>
                        </tr>
                        <tr>
                            <td>Alamat</td>
                            <td>: <?php echo $patient->address ?></td>
                        </tr>
                        <tr>
                            <td>Anamnesa</td>
                            <td>: <?php echo $referral->anamnesa ?></td>
                        </tr>
                        <tr>
                            <td>Diagnosa</td>
                            <td>: <?php echo $referral->diagnosa ?></td>
                        </tr>
                        <tr>
                            <td>Terapi Yang Telah Diberikan</td>
                            <td>: <?php echo $referral->teraphist ?></td>
                        </tr>
                    </table>
                </div>
            </div>



            <div class="panel-body referral_footer">
                <div class="col-md-8 pull-left text-left">
                </div>
                <div class="col-md-4 pull-right text-right"> 
                    <center><?php echo ucfirst($settings->city); ?>,<?php echo date('d F Y',strtotime($referral->date));?><br>
                    Salam Sejawat<br>
                    <?php if($settings->signature) : ?>
                    <img src="<?php echo $settings->signature; ?>" width="100px" height="100px" />
                    <?php else: ?>
                    <br><br><br><br><br>
                    <?php endif; ?>
                    <hr> <?php echo lang('signature'); ?></center>
            
                </div>
            </div>


        </div>



        <!-- invoice start-->
        <section class="col-md-4 margin_top">
            <div class="panel-primary clearfix">
                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                <div class="panel_button clearfix">
                    <div class="text-center invoice-btn no-print pull-left">
                        <a class="btn btn-info btn-lg invoice_button" onclick="javascript:window.print();"><i class="fa fa-print"></i> <?php echo lang('print'); ?> </a>
                    </div>
                </div>

                <div class="panel_button clearfix">
                    <div class="text-center invoice-btn no-print pull-left">
                        <a class="btn btn-info btn-sm detailsbutton pull-left download" id="download"><i class="fa fa-download"></i> <?php echo lang('download'); ?> </a>
                    </div>
                </div>
                <div class="panel_button clearfix">
                    <?php if ($this->ion_auth->in_group(array('admin','Doctor'))) { ?>
                        <div class="text-center invoice-btn no-print pull-left">
                            <a class="btn btn-info btn-lg info" href='doctor/referral'><i class="fa fa-medkit"></i> <?php echo lang('all'); ?> <?php echo lang('referral'); ?> </a>
                        </div>
                    <?php } ?>
                </div>
                <div class="panel_button">
                    <?php if ($this->ion_auth->in_group(array('admin', 'Doctor'))) { ?>
                        <div class="text-center invoice-btn no-print pull-left">
                            <a class="btn btn-info btn-lg green" href="doctor/referral_addView"><i class="fa fa-plus-circle"></i> <?php echo lang('add_referral'); ?> </a>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </section>
        <!-- invoice end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->


<style>

    hr {
        margin-top: 10px;
        margin-bottom: 7px;
        border: 0;
        border-top: 1px solid #000;
    }

    .panel-body{
        background: #f1f2f7;
    }

    thead {
        background: transparent;
    }

    .bg_referral {
        margin-top: 10px; 
    }

    .referral_footer{
        margin-bottom: 10px;
    }

    .bg_container{
        border: 1px solid #f1f1f1;
    }

    .panel{
        background: #fff;
    }

    .panel-body{
        background: #fff;
    }

    .margin_top{
        margin-top: 20px;
    }

    .wrapper{
        margin:0px;
        padding: 60px 0px 0px 30px;
    }

    .doctor{
        color: #2f80bf;
        font-family: cursive;
    }

    .hospital{
        color: #2f80bf;
        font-family: cursive;
    }

    hr{
        border-top: 1px solid #f1f1f1;
    }

    .panel_button{
        margin: 10px;
    }




    @media print {

        .wrapper{
            margin:0px;
            padding: 0px 10px 0px 0px;
        }

        .patient{  
            width: 23%;
            float: left;
        }

        .patient_name{  
            width: 31%;
            float: left;
        }

        .text-right{
            float: right;
        }

        .doctor{
            color: #2f80bf !important;
            font-family: cursive;
        }

        .hospital{
            color: #2f80bf !important;
            font-family: cursive;
        }

        .referral{
            float: left;
        }


    }

</style>


<script src="common/js/codearistos.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

<script>


                            $('#download').click(function () {
                                var pdf = new jsPDF('p', 'pt', 'letter');
                                pdf.addHTML($('#referral'), function () {
                                    pdf.save('referral_id_<?php echo $referral->id; ?>.pdf');
                                });
                            });

                            // This code is collected but useful, click below to jsfiddle link.
</script>

