<!--sidebar end-->
<!--main content start-->


<?php
$current_user = $this->ion_auth->get_user_id();
if ($this->ion_auth->in_group('Doctor')) {
    $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
}
?>


<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-8">
            <header class="panel-heading">
                <?php
                if (!empty($referral->id))
                    echo lang('edit_referral');
                else
                    echo lang('add_referral');
                ?>
            </header>
            <div class="panel col-md-12">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <?php echo validation_errors(); ?>
                        <form role="form" action="doctor/referral_add" class="clearfix" method="post" enctype="multipart/form-data">
                            <div class="">
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1"> <?php echo lang('date'); ?></label>
                                    <input type="text" class="form-control default-date-picker" name="date" id="exampleInputEmail1" value='<?php
                                    if (!empty($setval)) {
                                        echo set_value('date');
                                    }
                                    if (!empty($referral->date)) {
                                        echo date('d-m-Y', strtotime($referral->date));
                                    }
                                    ?>' placeholder="" readonly="">
                                </div>
                                <div class="form-group col-md-4">
                                    <label for="exampleInputEmail1"> <?php echo lang('patient'); ?></label>
                                    <select class="form-control m-bot15 js-example-basic-single" name="patient" value=''>
                                        <?php foreach ($patients as $patient) { ?>
                                            <option value="<?php echo $patient->id; ?>" <?php
                                            if (!empty($setval)) {
                                                if ($patient->id == set_value('patient')) {
                                                    echo 'selected';
                                                }
                                            }
                                            if (!empty($referral->patient)) {
                                                if ($patient->id == $referral->patient) {
                                                    echo 'selected';
                                                }
                                            }
                                            ?> > <?php echo $patient->name; ?> </option>
                                                <?php } ?> 
                                    </select>
                                </div>
                                <?php if ($this->ion_auth->in_group('Doctor')) { ?>
                                    <div class="form-group col-md-4"> 
                                        <label for="exampleInputEmail1"> <?php echo lang('doctor'); ?></label>
                                        <select class="form-control m-bot15 js-example-basic-single" name="doctor" value=''>
                                            <?php foreach ($doctors as $doctor) { ?>
                                                <?php if($doctor_id == $doctor->id){ ?>
                                                <option selected value="<?php echo $doctor->id; ?>"> <?php echo $doctor->name; ?> </option>
                                                <?php break;} ?>
                                            <?php } ?> 
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div class="form-group col-md-4"> 
                                        <label for="exampleInputEmail1">Dentist</label>
                                        <select class="form-control m-bot15 js-example-basic-single" name="doctor" value=''>
                                            <?php
                                            foreach ($doctors as $doctor) {
                                                    ?>
                                                <option value="<?php echo $doctor->id; ?>" <?php
                                                if (!empty($referral->doctor)) {
                                                    if ($doctor->id == $referral->doctor) {
                                                        echo 'selected';
                                                    }
                                                }
                                                ?> > <?php echo $doctor->name; ?> </option>
                                                <?php
                                            }
                                            ?> 
                                        </select>
                                    </div>
                                <?php } ?>

                                <div class="form-group col-md-12">
                                    <label class="control-label">Anamnesa</label>
                                    <input type="text" class="form-control" required name="anamnesa" value="<?php echo @$referral->anamnesa; ?>" />
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Diagnosa</label>
                                    <input type="text" class="form-control" required name="diagnosa" value="<?php echo @$referral->diagnosa; ?>" />
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="control-label">Teraphy</label>
                                    <input type="text" class="form-control" required name="teraphist" value="<?php echo @$referral->teraphist; ?>" />
                                </div>

                                <input type="hidden" name="id" value='<?php
                                if (!empty($referral->id)) {
                                    echo $referral->id;
                                }
                                ?>'>

                                <div class="form-group">
                                    <button type="submit" name="submit" class="btn btn-info pull-right"> <?php echo lang('submit'); ?></button>
                                </div>
                            </div>

                            <div class="col-md-5">

                            </div>



                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->



<style>

    form{
        border: 0px;
    }

    .med_selected{
        background: #fff;
        padding: 10px 0px;
        margin: 5px;
    }


    .select2-container--bgform .select2-selection--multiple .select2-selection__choice {
        clear: both !important;
    }

    label {
        display: inline-block;
        margin-bottom: 5px;
        font-weight: 500;
        font-weight: bold;
    }

</style>


<script src="common/js/codearistos.min.js"></script>







<script type="text/javascript">
    $(document).ready(function () {
    });
</script> 
