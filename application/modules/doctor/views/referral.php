<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="panel">
            <header class="panel-heading">
                <?php echo lang('referral'); ?>
                <?php if ($this->ion_auth->in_group(array('admin','Doctor'))) { ?>
                    <div class="col-md-4 no-print pull-right"> 
                        <a href="doctor/referral_addView">
                            <div class="btn-group pull-right">
                                <button id="" class="btn green btn-xs">
                                    <i class="fa fa-plus-circle"></i> <?php echo lang('add_new'); ?>
                                </button>
                            </div>
                        </a>
                    </div>
                <?php } ?>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th> <?php echo lang('date'); ?></th>                                  
                                <th> <?php echo lang('doctor'); ?></th>
                                <th> <?php echo lang('patient'); ?></th>
                                <th> Anamnesa </th>
                                <th> Diagnosas</th>
                                <th> Terapi</th>
                                <th class="no-print"><?php echo lang('options'); ?></th>
                            </tr>
                        </thead>
                        <tbody>

                        <style>

                            .img_url{
                                height:20px;
                                width:20px;
                                background-size: contain; 
                                max-height:20px;
                                border-radius: 100px;
                            }

                        </style>

                        <?php foreach ($referrals as $referral) { ?>
                            <tr class="">
                                <td><?php echo date('d-m-Y', strtotime($referral->date)); ?></td>
                                <td> <?php echo @$this->doctor_model->getDoctorById($referral->doctor)->name; ?></td>
                                <td> <?php echo @$this->patient_model->getPatientById($referral->patient)->name; ?></td>
                                <td><?php echo $referral->anamnesa;?></td>
                                <td><?php echo $referral->diagnosa;?></td>
                                <td><?php echo $referral->teraphist;?></td>
                                <td>
                                    <a class="btn btn-info btn-xs btn_width" href="doctor/referral_view?id=<?php echo $referral->id; ?>"><i class="fa fa-eye"> <?php echo lang('view'); ?> <?php echo lang('referral'); ?> </i></a>   
                                    <a class="btn btn-info btn-xs btn_width" href="doctor/referral_edit?id=<?php echo $referral->id; ?>" data-id="<?php echo $referral->id; ?>"><i class="fa fa-edit"></i> <?php echo lang('edit'); ?>  <?php echo lang('referral'); ?></a>
                                    <a class="btn btn-info btn-xs btn_width delete_button" href="doctor/referral_delete?id=<?php echo $referral->id; ?>" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"> <?php echo lang('delete'); ?></i></a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->




<script src="common/js/codearistos.min.js"></script>

<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,

            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2],
                    }
                },
            ],

            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: -1,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            },

        });

        table.buttons().container()
                .appendTo('.custom_buttons');
    });
</script>


<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>

