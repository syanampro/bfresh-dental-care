<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Materialrequest extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('material_request_model');
        $this->load->model('medicine/medicine_model');
        if (!$this->ion_auth->in_group(array('admin', 'Receptionist'))) {
            redirect('home/permission');
        }
    }

    public function index() {
        if (!$this->ion_auth->in_group(array('Receptionist'))) {
            redirect('home/permission');
        }
        $current_user = $this->ion_auth->get_user_id();
        $data =[];
        $data['datas'] = $this->material_request_model->getData();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('material_request', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function report() {
        if (!$this->ion_auth->in_group(array('admin'))) {
            redirect('home/permission');
        }

        $date_from = date('Y-m-d 00:00:00',strtotime($this->input->post('date_from')));
        $date_to = date('Y-m-d 23:59:59',strtotime($this->input->post('date_to')));

        $data = array();
        $data['datas'] = $this->material_request_model->getDataReportCabang($date_from, $date_to);
        
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('material_request_report', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function statistik() {
        if (!$this->ion_auth->in_group(array('admin'))) {
            redirect('home/permission');
        }

        $date_from = date('Y-m-d 00:00:00',strtotime($this->input->post('date_from')));
        $date_to = date('Y-m-d 23:59:59',strtotime($this->input->post('date_to')));

        $data = array();
        $data['datas'] = $this->material_request_model->getDataStatistikCabang($date_from, $date_to);

        $abc=0;
        foreach($data['datas'] as $dat){
            $x[$abc] = explode(",",$dat->details);
            $lok[$abc] = $dat->hospital_id;
            $abc++;
        }

        $yy=0;
        for($e=0;$e<count($x);$e++){ 
            for($f=0;$f<count($x[$e]);$f++){ 
                $details[$e][$f] = explode("*",$x[$e][$f]);
                $data['isi'][$yy] = explode("*",$x[$e][$f]);
                $yy++;
            }
        }

        $array2 = array();
        foreach ($data['isi'] as $item) {
            $key = $item[1];
            if (isset($array2[$key])) {
                $array2[$key][2] += (int)$item[2];
            } else {
                $array2[$key] = array($item[0], $item[1], (int)$item[2]);
            }
        }
        $data['isis'] = array_values($array2);
        
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('material_request_statistik', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    public function all()
    {
        if (!$this->ion_auth->in_group(array('admin'))) {
            redirect('home/permission');
        }
        $data =[];
        $data['datas'] = $this->material_request_model->getData();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('material_request_all', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addNew() {

        if (!$this->ion_auth->in_group(array('admin', 'Receptionist'))) {
            redirect('home/permission');
        }

        $data = array();

        $data['settings'] = $this->settings_model->getSettings();
        $data['medicines'] = $this->medicine_model->getMedicine();
        $this->load->view('home/dashboard', $data); // just the header file
        $this->load->view('material_request_add', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function add() {

        if (!$this->ion_auth->in_group(array('admin', 'Receptionist'))) {
            redirect('home/permission');
        }

        $id = $this->input->post('id');
        $date = date('Y-m-d H:i:s');
        $current_user = $this->ion_auth->get_user_id();

        $medicine_id = $this->input->post('medicine_id');
        $description = $this->input->post('description');
        $qtys = $this->input->post('qty');

        
        // if ($this->form_validation->run() == FALSE) {
        //     if (!empty($id)) {
        //         redirect('material_request/edit?id=' . $id);
        //     } else {
        //         $this->addNew();
        //     }
        // } else {
            $details = [];
            $total = 0;
            foreach ($qtys as $key => $qty) {
                $details[] = $medicine_id[$key].'*'.$description[$key].'*'.$qty;
                $total += $qty;
            }
            $details = implode(',',$details);
            if (empty($id)) {
                $data = array('date' => $date,
                    'user_id' => $current_user,
                    'details'=>$details,
                    'total'=>$total
                );
                $this->material_request_model->insert($data);
                $this->session->set_flashdata('feedback', 'Added');
            } else {
                $data = array(
                    'details'=>$details,
                    'total'=>$total
                );
                $this->material_request_model->update($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
            }

            if ($this->ion_auth->in_group(array('Receptionist')))
                redirect('materialrequest');
            redirect('materialrequest/all');
        // }
    }

    function view() {
        $id = $this->input->get('id');
        $data['material_request'] = $this->material_request_model->getDataById($id);

        if (!empty($data['material_request']->hospital_id)) {
            if ($data['material_request']->hospital_id != $this->session->userdata('hospital_id')) {
                $this->load->view('home/permission');
            } else {
                $data['settings'] = $this->settings_model->getSettings();
                $this->load->view('home/dashboard', $data); // just the header file
                $this->load->view('material_request_view', $data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            $this->load->view('home/permission');
        }
    }

    function edit() {
        $data = array();
        $id = $this->input->get('id');
        $data['material_request'] = $this->material_request_model->getDataById($id);
        $data['medicines'] = $this->medicine_model->getMedicine();
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard', $data); // just the header file
        $this->load->view('material_request_add', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function delete() {
        $id = $this->input->get('id');
        $this->material_request_model->delete($id);
        $this->session->set_flashdata('feedback', 'Deleted');
        if ($this->ion_auth->in_group(array('Receptionist')))
            redirect('materialrequest');
        redirect('materialrequest/all');
    }



    public function updatestatus() {

        if (!$this->ion_auth->in_group(array('admin','Receptionist'))) {
            redirect('home/permission');
        }

        $id = $this->input->post('id');
        $data = [];
        $data['status'] = $this->input->post('status');
        $matreq = $this->material_request_model->getDataById($id);
        if($data['status'] == 'terima' && $matreq->total > 0){
            $details = explode(",",$matreq->details);
            foreach ($details as $detail) {
                $detail = explode("*",$detail);
                if($detail[0] != '-'){
                    $previous_qty = @$this->db->get_where('medicine', array('id' => $detail[0]))->row()->quantity;
                    if($previous_qty){
                        $new_qty = $previous_qty + $detail[2];
                        $dmed = array('quantity' => $new_qty);
                        $this->medicine_model->updateMedicine($detail[0], $dmed);
                    }
                }
            }
        }
        $this->material_request_model->update($id, $data);
        $this->session->set_flashdata('feedback', 'Updated');

        if ($this->ion_auth->in_group(array('Receptionist')))
            redirect('materialrequest');
        redirect('materialrequest/all');
    }
}
?>