<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Material_request_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insert($data) {
        $data1 = array('hospital_id' => $this->session->userdata('hospital_id'));
        $data2 = array_merge($data, $data1);
        $this->db->insert('material_request', $data2);
    }

    function update($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('material_request', $data);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('material_request');
    }

    public function getData()
    {
        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->join('hospital','hospital.id = material_request.hospital_id');
        $this->db->join('users','users.id = material_request.user_id');
        $this->db->join('users as useradmin','useradmin.id = material_request.admin_id','left');
        $this->db->order_by('date', 'desc');
        $this->db->select('material_request.*,hospital.name as hospital,users.username as receptionist,useradmin.username as admin');
        
        return $this->db->get('material_request')->result();
    }

    public function getDataReportCabang($date_from, $date_to)
    {
        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where("status","request");
        $this->db->where("date BETWEEN '$date_from' AND '$date_to'");
        $this->db->join('hospital','hospital.id = material_request.hospital_id');
        $this->db->join('users','users.id = material_request.user_id');
        $this->db->join('users as useradmin','useradmin.id = material_request.admin_id','left');
        $this->db->order_by('date', 'desc');
        $this->db->select('material_request.*,hospital.name as hospital,users.username as receptionist,useradmin.username as admin');
        
        return $this->db->get('material_request')->result();
    }

    public function getDataStatistikCabang($date_from, $date_to)
    {
        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where("status","terima");
        $this->db->where("date BETWEEN '$date_from' AND '$date_to'");
        $this->db->join('hospital','hospital.id = material_request.hospital_id');
        $this->db->join('users','users.id = material_request.user_id');
        $this->db->join('users as useradmin','useradmin.id = material_request.admin_id','left');
        $this->db->order_by('date', 'desc');
        $this->db->select('material_request.*,hospital.name as hospital,users.username as receptionist,useradmin.username as admin');
        
        return $this->db->get('material_request')->result();
    }

    public function getDataByUserId($current_user)
    {
        $this->db->where('user_id', $current_user);
        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->join('hospital','hospital.id = material_request.hospital_id');
        $this->db->join('users','users.id = material_request.user_id');
        $this->db->join('users as useradmin','useradmin.id = material_request.admin_id','left');
        $this->db->select('material_request.*,hospital.name as hospital,users.username as receptionist,useradmin.username as admin');

        return $this->db->get('material_request')->result();
    }

    public function getDataById($id)
    {
        $this->db->where('material_request.id', $id);
        $this->db->join('hospital','hospital.id = material_request.hospital_id');
        $this->db->join('users','users.id = material_request.user_id');
        $this->db->join('users as useradmin','useradmin.id = material_request.admin_id','left');
        $this->db->select('material_request.*,hospital.name as hospital,users.username as receptionist,useradmin.username as admin');
        $query = $this->db->get('material_request');
        return $query->row();
    }
}
