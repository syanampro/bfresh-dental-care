<!--sidebar end-->
<!--main content start-->

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-12">
            <header class="panel-heading">
                <?php
                if (@$material_request->id)
                    echo lang('edit_material_request');
                else
                    echo lang('add_material_request');
                ?>
            </header>
            <div class="panel col-md-12">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <?php echo validation_errors(); ?>
                        <form role="form" action="materialrequest/add" class="clearfix" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo @$material_request->id; ?>" />
                            <table class="table" id="table">
                                <thead>
                                    <tr>
                                        <th width="50%">Obat / Bahan Stok</th>
                                        <th></th>
                                        <th>Jumlah</th>
                                        <th><button type="button" id="btndetail" class="btn btn-info">+</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!@$material_request->details) : ?>
                                    <tr>
                                        <td>
                                            <select class="form-control m-bot15 js-example-basic-single medicine_select" name="medicine_id[]">  
                                                <option value="-"><?php echo lang('select'); ?></option>
                                                <!-- <?php foreach ($medicines as $medicine) { ?>
                                                    <option data-name="<?php echo $medicine->name; ?>" value="<?php echo $medicine->id; ?>"<?php
                                                    if (!empty($doctor_id)) {
                                                        if ($doctor_id == $doctor->id) {
                                                            echo 'selected';
                                                        }
                                                    }
                                                    ?>><?php echo $medicine->name; ?> </option>
                                                        <?php } ?> -->

                                                    <option data-name="Minosep" value="Minosep">Minosep  (btl)</option>
                                                    <option data-name="Amoxicillin Generic" value="Amoxicillin Generic">Amoxicillin Generic  (tablet)</option>
                                                    <option data-name="Amoxicillin Paten" value="Amoxicillin Paten">Amoxicillin Paten  (tablet)</option>
                                                    <option data-name="Asam Mefenamat Generic" value="Asam Mefenamat Generic">Asam Mefenamat Generic  (tablet)</option>
                                                    <option data-name="Asam Mefenamat Paten" value="Asam Mefenamat Paten">Asam Mefenamat Paten  (tablet)</option>
                                                    <option data-name="Metronidazole" value="Metronidazole">Metronidazole  (tablet)</option>
                                                    <option data-name="Clindamycin" value="Clindamycin">Clindamycin  (tablet)</option>
                                                    <option data-name="Dexamethasone" value="Dexamethasone">Dexamethasone (tablet)</option>
                                                    <option data-name="Samcofenac" value="Samcofenac">Samcofenac (tablet)</option>
                                                    <option data-name="Kaditic" value="Kaditic">Kaditic (tablet)</option>
                                                    <option data-name="Paracetamol" value="Paracetamol">Paracetamol  (tablet)</option>
                                                    <option data-name="Paracetamol Sirup (Sanmol)" value="Paracetamol Sirup (Sanmol)">Paracetamol Sirup (Sanmol) (btl)</option>
                                                    <option data-name="Ibuprofen Sirup" value="Ibuprofen Sirup">Ibuprofen Sirup (btl)</option>
                                                    <option data-name="Amoxcilin Sirup" value="Amoxcilin Sirup">Amoxcilin Sirup (btl)</option>
                                                    
                                                    <option data-name="Alcohol 70%" value="Alcohol 70%">Alcohol 70% (btl)</option>
                                                    <option data-name="Naocl 2.5" value="Naocl 2.5">Naocl 2.5 (btl)</option>
                                                    <option data-name="Nacl/Saline" value="Nacl/Saline">Nacl/Saline (btl)</option>
                                                    <option data-name="Betadin/Iodin" value="Betadin/Iodin">Betadin/Iodin  (btl)</option>
                                                    <option data-name="Aquades" value="Aquades">Aquades  (btl)</option>
                                                    <option data-name="Minyak Highspeed" value="Minyak Highspeed">Minyak Highspeed  (btl)</option>
                                                    <option data-name="Bahan Bleaching" value="Bahan Bleaching">Bahan Bleaching  (pack)</option>
                                                    <option data-name="Microbrush" value="Microbrush">Microbrush  (pack)</option>
                                                    <option data-name="Dental Floss Stik" value="Dental Floss Stik">Dental Floss Stik  (pack)</option>
                                                    <option data-name="Dental Floss Benang" value="Dental Floss Benang">Dental Floss Benang (pcs)</option>
                                                    <option data-name="GIC Fuji 1" value="GIC Fuji 1">GIC Fuji 1 (pack)</option>
                                                    <option data-name="GIC Fuji 2" value="GIC Fuji 2">GIC Fuji 2 (pack)</option>
                                                    <option data-name="GIC Fuji 9 Kecil" value="GIC Fuji 9 Kecil">GIC Fuji 9 Kecil (pack)</option>
                                                    <option data-name="GIC Fuji 9 Besar" value="GIC Fuji 9 Besar">GIC Fuji 9 Besar (pack)</option>
                                                    <option data-name="Calzinol (merk. Zitemp)" value="Calzinol (merk. Zitemp)">Calzinol (merk. Zitemp) (pack)</option>
                                                    <option data-name="Eugenol" value="Eugenol">Eugenol  (pcs)</option>
                                                    <option data-name="Hexabond (Bonding)" value="Hexabond (Bonding)">Hexabond (Bonding) (pcs)</option>
                                                    <option data-name="Tricresol" value="Tricresol">Tricresol  (pcs)</option>
                                                    <option data-name="TSR/OCO" value="TSR/OCO">TSR/OCO (pcs)</option>
                                                    <option data-name="TS (merk. Cevitron)" value="TS (merk. Cevitron)">TS (merk. Cevitron) (pcs)</option>
                                                    <option data-name="Hexa Etch" value="Hexa Etch">Hexa Etch  (pcs)</option>
                                                    <option data-name="Wax Ortho" value="Wax Ortho">Wax Ortho (pack)</option>
                                                    <option data-name="Komposit Flow A1" value="Komposit Flow A1">Komposit Flow A1 (pcs)</option>
                                                    <option data-name="Dental Needles" value="Dental Needles">Dental Needles  (pcs)</option>
                                                    <option data-name="Modelling Wax/malam" value="Modelling Wax/malam">Modelling Wax/malam (pack)</option>
                                                    <option data-name="Biosponge" value="Biosponge">Biosponge  (pcs)</option>
                                                    <option data-name="Wedges Hijau" value="Wedges Hijau">Wedges Hijau (pack)</option>
                                                    <option data-name="Calplus" value="Calplus">Calplus  (pcs)</option>
                                                    <option data-name="Calciplus LC" value="Calciplus LC">Calciplus LC (pcs)</option>
                                                    <option data-name="Kapas" value="Kapas">Kapas  (ball)</option>
                                                    <option data-name="Kasa" value="Kasa">Kasa  (gulung)</option>
                                                    <option data-name="Catton Roll" value="Catton Roll">Catton Roll (pack)</option>
                                                    <option data-name="Polibibs" value="Polibibs">Polibibs  (lembar)</option>
                                                    <option data-name="Masker Medis" value="Masker Medis">Masker Medis  (pack)</option>
                                                    <option data-name="Masker KN95" value="Masker KN95">Masker KN95 (pack)</option>
                                                    <option data-name="Saniter" value="Saniter">Saniter (pcs)</option>
                                                    <option data-name="Handscon Uk. S" value="Handscon Uk. S">Handscon Uk. S (pack)</option>
                                                    <option data-name="Suction" value="Suction">Suction  (pack)</option>
                                                    <option data-name="Ligature Wire Splinting" value="Ligature Wire Splinting">Ligature Wire Splinting (pcs)</option>
                                                    <option data-name="Alginate (Hygent)" value="Alginate (Hygent)">Alginate (Hygent) (pcs)</option>
                                                    <option data-name="Gips Biru" value="Gips Biru">Gips Biru  (pcs)</option>
                                                    <option data-name="Gips Kuning" value="Gips Kuning">Gips Kuning  (pcs)</option>
                                                    <option data-name="Bracket Ceramic" value="Bracket Ceramic">Bracket Ceramic  (pack)</option>
                                                    <option data-name="Roth Bracket" value="Roth Bracket">Roth Bracket  (pack)</option>
                                                    <option data-name="Elastic Ortho 1/4 5.0 Oz" value="Elastic Ortho 1/4 5.0 Oz">Elastic Ortho 1/4 5.0 Oz (pack)</option>
                                                    <option data-name="Elastic Ortho 1/8 3.5 Oz" value="Elastic Ortho 1/8 3.5 Oz">Elastic Ortho 1/8 3.5 Oz (pack)</option>
                                                    <option data-name="Elastic Ortho 5/16 3.5 Oz" value="Elastic Ortho 5/16 3.5 Oz">Elastic Ortho 5/16 3.5 Oz (pack)</option>
                                                    <option data-name="Elastic Ortho 3/16 3.5 Oz" value="Elastic Ortho 3/16 3.5 Oz">Elastic Ortho 3/16 3.5 Oz (pack)</option>
                                                    <option data-name="Elastic Ortho 5/16 5.0 Oz" value="Elastic Ortho 5/16 5.0 Oz">Elastic Ortho 5/16 5.0 Oz (pack)</option>
                                                    <option data-name="Buccal Tube" value="Buccal Tube">Buccal Tube  (pack)</option>
                                                    <option data-name="Paper Point 15-40" value="Paper Point 15-40">Paper Point 15-40 (pack)</option>
                                                    <option data-name="Paper Point 45-80" value="Paper Point 45-80">Paper Point 45-80 (pack)</option>
                                                    <option data-name="Guta Percha Points F1/f3" value="Guta Percha Points F1/f3">Guta Percha Points F1/f3 (pack)</option>
                                                    <option data-name="Guta Percha Points 15-40" value="Guta Percha Points 15-40">Guta Percha Points 15-40 (pack)</option>
                                                    <option data-name="Guta Percha Points 45-80" value="Guta Percha Points 45-80">Guta Percha Points 45-80 (pack)</option>
                                                    <option data-name="Paper Pad" value="Paper Pad">Paper Pad  (bendel)</option>
                                                    <option data-name="Pasak Fiber" value="Pasak Fiber">Pasak Fiber  (pack)</option>
                                                    <option data-name="Interdental Brush" value="Interdental Brush">Interdental Brush  (pcs)</option>
                                                    <option data-name="Articulating Paper" value="Articulating Paper">Articulating Paper  (sheet)</option>
                                                    <option data-name="Sikat Dr.Smith" value="Sikat Dr.Smith">Sikat Dr.Smith  (pcs)</option>
                                                    <option data-name="Wedges ortho abu" value="Wedges ortho abu">Wedges ortho abu (pcs)</option>
                                                    <option data-name="Topical anastesi lolite" value="Topical anastesi lolite">Topical anastesi lolite  (pcs)</option>
                                                    <option data-name="Lidocaine" value="Lidocaine">Lidocaine  (ampul)</option>
                                                    <option data-name="Pehacain" value="Pehacain">Pehacain  (pack)</option>
                                                    <option data-name="Carpul 3%" value="Carpul 3%">Carpul 3% (strip)</option>
                                                    <option data-name="GIC Mini" value="GIC Mini">GIC Mini  (pack)</option>
                                                    <option data-name="CHKM" value="CHKM">CHKM (pcs)</option>
                                                    <option data-name="Komposit A2" value="Komposit A2">Komposit A2 (pcs)</option>
                                                    <option data-name="Komposit A3" value="Komposit A3">Komposit A3 (pcs)</option>
                                                    <option data-name="Komposit A3.5" value="Komposit A3.5">Komposit A3.5 (pcs)</option>
                                                    <option data-name="ZnPO4/Ellite Cement" value="ZnPO4/Ellite Cement">ZnPO4/Ellite Cement  (pack)</option>
                                                    <option data-name="Open coil spring" value="Open coil spring">Open coil spring  (pack)</option>
                                                    <option data-name="Close coil spring" value="Close coil spring">Close coil spring (pack)</option>
                                                    <option data-name="Matrix band" value="Matrix band">Matrix band  (pack)</option>
                                                    <option data-name="H2O2 50%" value="H2O2 50%">H2O2 50% (btl)</option>
                                                    <option data-name="Power O oren" value="Power O oren">Power O oren  (pcs)</option>
                                                    <option data-name="Power O hitam " value="Power O hitam ">Power O hitam   (pcs)</option>
                                                    <option data-name="Power O ungu tua" value="Power O ungu tua">Power O ungu tua (pcs)</option>
                                                    <option data-name="Power O hijau" value="Power O hijau">Power O hijau  (pcs)</option>
                                                    <option data-name="Power O hijau gliter/ metalic" value="Power O hijau gliter/ metalic">Power O hijau gliter/ metalic (pcs)</option>
                                                    <option data-name="Power O biru gliter /metalic" value="Power O biru gliter /metalic">Power O biru gliter /metalic (pcs)</option>
                                                    <option data-name="Power O biru" value="Power O biru">Power O biru  (pcs)</option>
                                                    <option data-name="Power O biru laut" value="Power O biru laut">Power O biru laut  (pcs)</option>
                                                    <option data-name="Power O kuning" value="Power O kuning">Power O kuning  (pcs)</option>
                                                    <option data-name="Power O coklat" value="Power O coklat">Power O coklat  (pcs)</option>
                                                    <option data-name="Power O biru tosca" value="Power O biru tosca">Power O biru tosca  (pcs)</option>
                                                    <option data-name="Power O pink" value="Power O pink">Power O pink  (pcs)</option>
                                                    <option data-name="Power O merah" value="Power O merah">Power O merah  (pcs)</option>
                                                    <option data-name="Power O maron" value="Power O maron">Power O maron (pcs)</option>
                                                    <option data-name="Power O putih" value="Power O putih">Power O putih (pcs)</option>
                                                    <option data-name="Power O navy besar" value="Power O navy besar">Power O navy besar (pcs)</option>
                                                    <option data-name="Power O ungu muda/lilac" value="Power O ungu muda/lilac">Power O ungu muda/lilac (pcs)</option>
                                                    <option data-name="Power O putih bening/transparan" value="Power O putih bening/transparan">Power O putih bening/transparan  (pcs)</option>
                                                    <option data-name="Power O pink tua" value="Power O pink tua">Power O pink tua (pcs)</option>
                                                    <option data-name="Power O hijau tosca" value="Power O hijau tosca">Power O hijau tosca (pcs)</option>
                                                    <option data-name="Power O hijau muda" value="Power O hijau muda">Power O hijau muda (pcs)</option>
                                                    <option data-name="Power O ungu" value="Power O ungu">Power O ungu (pcs)</option>
                                                    <option data-name="Power O merah jambu" value="Power O merah jambu">Power O merah jambu (pcs)</option>
                                                    <option data-name="Power O biru tua" value="Power O biru tua">Power O biru tua (pcs)</option>
                                                    <option data-name="Power O biru SMA" value="Power O biru SMA">Power O biru SMA (pcs)</option>
                                                    <option data-name="Power O pink muda" value="Power O pink muda">Power O pink muda (pcs)</option>
                                                    <option data-name="Power O abu" value="Power O abu">Power O abu (pcs)</option>
                                                    <option data-name="Power chain hijau tosca" value="Power chain hijau tosca">Power chain hijau tosca  (cm)</option>
                                                    <option data-name="Power chain hijau muda" value="Power chain hijau muda">Power chain hijau muda (cm)</option>
                                                    <option data-name="Power chain ungu" value="Power chain ungu">Power chain ungu  (cm)</option>
                                                    <option data-name="Power chain pink muda" value="Power chain pink muda">Power chain pink muda (cm)</option>
                                                    <option data-name="Power chain biru" value="Power chain biru">Power chain biru  (cm)</option>
                                                    <option data-name="Power chain biru muda" value="Power chain biru muda">Power chain biru muda  (cm)</option>
                                                    <option data-name="Power chain bening" value="Power chain bening">Power chain bening  (cm)</option>
                                                    <option data-name="Power chain coklat" value="Power chain coklat">Power chain coklat  (cm)</option>
                                                    <option data-name="Power chain putih" value="Power chain putih">Power chain putih  (cm)</option>
                                                    <option data-name="Power chain  biru dongker" value="Power chain  biru dongker">Power chain  biru dongker  (cm)</option>
                                                    <option data-name="Power chain abu" value="Power chain abu">Power chain abu  (cm)</option>
                                                    <option data-name="Power chain hitam" value="Power chain hitam">Power chain hitam  (cm)</option>
                                                    <option data-name="Power chain biru tosca" value="Power chain biru tosca">Power chain biru tosca (cm)</option>
                                                    <option data-name="Power chain maron" value="Power chain maron">Power chain maron  (cm)</option>
                                                    <option data-name="Power chain merah" value="Power chain merah">Power chain merah  (cm)</option>
                                                    <option data-name="Power chain hijau" value="Power chain hijau">Power chain hijau (cm)</option>
                                                    <option data-name="Power chain biru SMA" value="Power chain biru SMA">Power chain biru SMA (cm)</option>
                                                    <option data-name="Power chain ungu muda" value="Power chain ungu muda">Power chain ungu muda (cm)</option>
                                                    <option data-name="Power chain pink magenta" value="Power chain pink magenta">Power chain pink magenta (cm)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.12 Lower" value="Wire NiTi Ovoid 0.12 Lower">Wire NiTi Ovoid 0.12 Lower (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.14 Lower" value="Wire NiTi Ovoid 0.14 Lower">Wire NiTi Ovoid 0.14 Lower (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.16 Lower" value="Wire NiTi Ovoid 0.16 Lower">Wire NiTi Ovoid 0.16 Lower (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.18 Lower" value="Wire NiTi Ovoid 0.18 Lower">Wire NiTi Ovoid 0.18 Lower (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.12 Upper" value="Wire NiTi Ovoid 0.12 Upper">Wire NiTi Ovoid 0.12 Upper (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.14 Upper" value="Wire NiTi Ovoid 0.14 Upper">Wire NiTi Ovoid 0.14 Upper (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.16 Upper" value="Wire NiTi Ovoid 0.16 Upper">Wire NiTi Ovoid 0.16 Upper (pack)</option>
                                                    <option data-name="Wire NiTi Ovoid 0.18 Upper" value="Wire NiTi Ovoid 0.18 Upper">Wire NiTi Ovoid 0.18 Upper (pack)</option>
                                                    <option data-name="Wire SS Rekta 0.016x022 Lower" value="Wire SS Rekta 0.016x022 Lower">Wire SS Rekta 0.016x022 Lower (pack)</option>
                                                    <option data-name="Wire SS Rekta 0.016x022 Upper" value="Wire SS Rekta 0.016x022 Upper">Wire SS Rekta 0.016x022 Upper (pack)</option>
                                                    <option data-name="Wire SS Rekta 16x16 Lower" value="Wire SS Rekta 16x16 Lower">Wire SS Rekta 16x16 Lower (pack)</option>
                                                    <option data-name="Wire SS Rekta 16x16 Upper" value="Wire SS Rekta 16x16 Upper">Wire SS Rekta 16x16 Upper (pack)</option>
                                                    <option data-name="Wire SS Rekta 0.016x0.016 Lower" value="Wire SS Rekta 0.016x0.016 Lower">Wire SS Rekta 0.016x0.016 Lower (pack)</option>
                                                    <option data-name="Wire SS Rekta 0.016x0.016 Upper" value="Wire SS Rekta 0.016x0.016 Upper">Wire SS Rekta 0.016x0.016 Upper (pack)</option>
                                                    <option data-name="Wire SS Rekta 0.16x022 Lower" value="Wire SS Rekta 0.16x022 Lower">Wire SS Rekta 0.16x022 Lower (pack)</option>
                                                    <option data-name="Wire SS Rekta 0.16x022 Upper" value="Wire SS Rekta 0.16x022 Upper">Wire SS Rekta 0.16x022 Upper (pack)</option>
                                                    <option data-name="Niti reserve 0.012 Upper" value="Niti reserve 0.012 Upper">Niti reserve 0.012 Upper (pack)</option>
                                                    <option data-name="Niti reserve 0.012 Lower" value="Niti reserve 0.012 Lower">Niti reserve 0.012 Lower (pack)</option>
                                                    <option data-name="Niti reserve 0.014 Upper" value="Niti reserve 0.014 Upper">Niti reserve 0.014 Upper (pack)</option>
                                                    <option data-name="Niti reserver 0.014 Lower" value="Niti reserver 0.014 Lower">Niti reserver 0.014 Lower (pack)</option>
                                                    <option data-name="Niti reverse 0.016 Upper" value="Niti reverse 0.016 Upper">Niti reverse 0.016 Upper (pack)</option>
                                                    <option data-name="Niti reverse 0.016 lower" value="Niti reverse 0.016 lower">Niti reverse 0.016 lower (pack)</option>
                                                    <option data-name="Niti reserve 0.016x0.016 lower" value="Niti reserve 0.016x0.016 lower">Niti reserve 0.016x0.016 lower (pack)</option>
                                                    <option data-name="Niti reserve 0.016x0.016 upper" value="Niti reserve 0.016x0.016 upper">Niti reserve 0.016x0.016 upper (pack)</option>
                                                    <option data-name="Safty Box" value="Safty Box">Safty Box (pcs)</option>
                                                    <option data-name="Chlor Ethyl" value="Chlor Ethyl">Chlor Ethyl  (pcs)</option>
                                                    <option data-name="Polishing Strip/Enamel Striping" value="Polishing Strip/Enamel Striping">Polishing Strip/Enamel Striping (pcs)</option>
                                                    <option data-name="Clinpro (TAF)" value="Clinpro (TAF)">Clinpro (TAF) (pack)</option>
                                                    <option data-name="CE Klorofil" value="CE Klorofil">CE Klorofil (pcs)</option>
                                                    <option data-name="Surgical Blades Uk.10" value="Surgical Blades Uk.10">Surgical Blades Uk.10 (pcs)</option>
                                                    <option data-name="Surgical Blades Uk.15" value="Surgical Blades Uk.15">Surgical Blades Uk.15 (pcs)</option>
                                                    <option data-name="Seluloid Strip" value="Seluloid Strip">Seluloid Strip  (pack)</option>
                                                    <option data-name="Paper Point F1-F1" value="Paper Point F1-F1">Paper Point F1-F1 (Pack)</option>
                                                    <option data-name="Kresek Obat" value="Kresek Obat">Kresek Obat  (bendel)</option>
                                                    <option data-name="Kapur Barus KM" value="Kapur Barus KM">Kapur Barus KM (pcs)</option>
                                                    <option data-name="Prostek/vVxal" value="Prostek/vVxal">Prostek/vVxal (btl)</option>
                                                    <option data-name="Pemutih Baju" value="Pemutih Baju">Pemutih Baju (btl)</option>
                                                    <option data-name="Sunlight" value="Sunlight">Sunlight (pcs)</option>
                                                    <option data-name="Tisu" value="Tisu">Tisu (pcs)</option>
                                                    <option data-name="Kresek Merah Besar" value="Kresek Merah Besar">Kresek Merah Besar  (pack)</option>
                                                    <option data-name="Kresek Kuning Medis" value="Kresek Kuning Medis">Kresek Kuning Medis  (pack)</option>
                                                    <option data-name="Kresek Hitam Besar" value="Kresek Hitam Besar">Kresek Hitam Besar  (pack)</option>
                                                    <option data-name="Kresek Hitam Sedang" value="Kresek Hitam Sedang">Kresek Hitam Sedang  (pack)</option>
                                                    <option data-name="Kresek Putih Sedang" value="Kresek Putih Sedang">Kresek Putih Sedang  (pack)</option>
                                                    <option data-name="Kresek Hitam Kecil" value="Kresek Hitam Kecil">Kresek Hitam Kecil (pack)</option>
                                                    <option data-name="Kresek Putih Kecil" value="Kresek Putih Kecil">Kresek Putih Kecil  (pack)</option>
                                                    <option data-name="Pembersih Lantai" value="Pembersih Lantai">Pembersih Lantai (pcs)</option>
                                                    <option data-name="Detergen Cair" value="Detergen Cair">Detergen Cair  (pcs)</option>
                                                    <option data-name="Handwash" value="Handwash">Handwash  (pcs)</option>
                                                    <option data-name="Spirtus" value="Spirtus">Spirtus (btl)</option>
                                                    <option data-name="Plastik Cetik 10x15" value="Plastik Cetik 10x15">Plastik Cetik 10x15 (pack)</option>
                                                    <option data-name="Plastic Cetik 7x10" value="Plastic Cetik 7x10">Plastic Cetik 7x10 (pack)</option>
                                                    <option data-name="Air" value="Air">Air (dus)</option>
                                                    <option data-name="Cling" value="Cling">Cling (pcs)</option>
                                                    <option data-name="Stela Gantung" value="Stela Gantung">Stela Gantung (pcs)</option>
                                                    <option data-name="Bulpoint" value="Bulpoint">Bulpoint (pcs)</option>
                                                    <option data-name="Surat Rujukan Lab Gigi AY" value="Surat Rujukan Lab Gigi AY">Surat Rujukan Lab Gigi AY (bendel)</option>
                                                    <option data-name="Surat Keterangan Dokter" value="Surat Keterangan Dokter">Surat Keterangan Dokter (lembar)</option>
                                                    <option data-name="Surat Rujukan Dokter" value="Surat Rujukan Dokter">Surat Rujukan Dokter (lembar)</option>
                                                    <option data-name="Surat Resep" value="Surat Resep">Surat Resep (lembar)</option>
                                                    <option data-name="Lembar laporan keuangan harian" value="Lembar laporan keuangan harian">Lembar laporan keuangan harian (lembar)</option>
                                                    <option data-name="Informed consent penolakan" value="Informed consent penolakan">Informed consent penolakan (lembar)</option>
                                                    <option data-name="Informed consent persetujuan" value="Informed consent persetujuan">Informed consent persetujuan (pcs)</option>
                                                    <option data-name="Lembar laporan harian" value="Lembar laporan harian">Lembar laporan harian  (pcs)</option>
                                                    <option data-name="Wadah kotak plastik untuk shoes cover kotor" value="Wadah kotak plastik untuk shoes cover kotor">Wadah kotak plastik untuk shoes cover kotor (pcs)</option>
                                                    <option data-name="Shoes Cover" value="Shoes Cover">Shoes Cover (pcs)</option>
                                                    <option data-name="Gunting Kertas" value="Gunting Kertas">Gunting Kertas (pcs)</option>
                                                    <option data-name="Gayung Kamar Mandi" value="Gayung Kamar Mandi">Gayung Kamar Mandi (pcs)</option>
                                                    <option data-name="Sikat Kamar Mandi/WC" value="Sikat Kamar Mandi/WC">Sikat Kamar Mandi/WC (pcs)</option>
                                                    <option data-name="Toples" value="Toples">Toples (pcs)</option>
                                                    <option data-name="Hanger" value="Hanger">Hanger (pcs)</option>
                                                    <option data-name="Staples" value="Staples">Staples (pcs)</option>
                                                    <option data-name="Spons" value="Spons">Spons (pcs)</option>
                                                    <option data-name="Kwitansi" value="Kwitansi">Kwitansi (bendel)</option>
                                                    <option data-name="Pasta Gigi" value="Pasta Gigi">Pasta Gigi (pcs)</option>
                                                    <option data-name="Amplas" value="Amplas">Amplas (pcs)</option>
                                                    <option data-name="Buku Tulis" value="Buku Tulis">Buku Tulis (pcs)</option>
                                                    <option data-name="Lem Paralon/Pipa" value="Lem Paralon/Pipa">Lem Paralon/Pipa (pcs)</option>
                                                    <option data-name="Batrai" value="Batrai">Batrai  (pcs)</option>
                                                    <option data-name="Hadiah Anak Cowok" value="Hadiah Anak Cowok">Hadiah Anak Cowok (pcs)</option>
                                                    <option data-name="Hadiah Anak Cewek" value="Hadiah Anak Cewek">Hadiah Anak Cewek (pcs)</option>
                                                    <option data-name="Pigora Putih" value="Pigora Putih">Pigora Putih (pcs)</option>
                                                    <option data-name="Isi Staples" value="Isi Staples">Isi Staples (pack)</option>
                                                    <option data-name="Stipo cair/ kertas" value="Stipo cair/ kertas">Stipo cair/ kertas (pcs)</option>
                                                    <option data-name="Solasi Kecil" value="Solasi Kecil">Solasi Kecil (pcs)</option>
                                                    <option data-name="Sandal Operasi" value="Sandal Operasi">Sandal Operasi  (pasang)</option>
                                                    <option data-name="APD" value="APD">APD (pcs)</option>
                                                    <option data-name="Braket Holder" value="Braket Holder">Braket Holder (pcs)</option>
                                                    <option data-name="Spatula GIC" value="Spatula GIC">Spatula GIC (pcs)</option>
                                                    <option data-name="Plastis Filling" value="Plastis Filling">Plastis Filling (pcs)</option>
                                                    <option data-name="Semen Stropper" value="Semen Stropper">Semen Stropper (pcs)</option>
                                                    <option data-name="Needle Holder/Arteri Klem" value="Needle Holder/Arteri Klem">Needle Holder/Arteri Klem (pcs)</option>
                                                    <option data-name="Sonde" value="Sonde">Sonde (pcs)</option>
                                                    <option data-name="Escavator" value="Escavator">Escavator (pcs)</option>
                                                    <option data-name="Kaca Mulut Tanpa Tangkai" value="Kaca Mulut Tanpa Tangkai">Kaca Mulut Tanpa Tangkai (pcs)</option>
                                                    <option data-name="Nierbeken" value="Nierbeken">Nierbeken  (pcs)</option>
                                                    <option data-name="Pinset" value="Pinset">Pinset  (pcs)</option>
                                                    <option data-name="Tang Cabut Anak" value="Tang Cabut Anak">Tang Cabut Anak  (pcs)</option>
                                                    <option data-name="Tang Remover Braket" value="Tang Remover Braket">Tang Remover Braket  (pcs)</option>
                                                    <option data-name="Tang Adam Universal" value="Tang Adam Universal">Tang Adam Universal (pcs)</option>
                                                    <option data-name="Scaler Ultrasonic" value="Scaler Ultrasonic">Scaler Ultrasonic (set)</option>
                                                    <option data-name="Tip Scaler" value="Tip Scaler">Tip Scaler  (pcs)</option>
                                                    <option data-name="Arkansas Lowspeed Stone (flam)" value="Arkansas Lowspeed Stone (flam)">Arkansas Lowspeed Stone (flam) (pcs)</option>
                                                    <option data-name="Arkansas Lowspeed Stone (cones)" value="Arkansas Lowspeed Stone (cones)">Arkansas Lowspeed Stone (cones) (pcs)</option>
                                                    <option data-name="Arkansas Lowspeed Stone (round)" value="Arkansas Lowspeed Stone (round)">Arkansas Lowspeed Stone (round) (pcs)</option>
                                                    <option data-name="Finishing (discs)" value="Finishing (discs)">Finishing (discs) (pcs)</option>
                                                    <option data-name="Finishing (cups)" value="Finishing (cups)">Finishing (cups) (pcs)</option>
                                                    <option data-name="Finishing (point)" value="Finishing (point)">Finishing (point) (pcs)</option>
                                                    <option data-name="Round Bur" value="Round Bur">Round Bur  (pcs)</option>
                                                    <option data-name="Bur Brush" value="Bur Brush">Bur Brush (pcs)</option>
                                                    <option data-name="Spatula Alginate" value="Spatula Alginate">Spatula Alginate  (pcs)</option>
                                                    <option data-name="Spatula Semen" value="Spatula Semen">Spatula Semen  (pcs)</option>
                                                    <option data-name="Bein" value="Bein">Bein  (pcs)</option>
                                                    <option data-name="Cryer" value="Cryer">Cryer  (pcs)</option>
                                                    <option data-name="Handle Scapel" value="Handle Scapel">Handle Scapel  (pcs)</option>
                                                    <option data-name="Tang Potong Distal" value="Tang Potong Distal">Tang Potong Distal (pcs)</option>
                                                    <option data-name="Light Curing LED" value="Light Curing LED">Light Curing LED (pcs)</option>
                                                    <option data-name="Bowl (Mangkok)" value="Bowl (Mangkok)">Bowl (Mangkok) (pcs)</option>
                                                    <option data-name="Tensi" value="Tensi">Tensi  (pcs)</option>
                                                    <option data-name="Polishing Kit Komposit" value="Polishing Kit Komposit">Polishing Kit Komposit (pcs)</option>
                                                    <option data-name="Dappen Glass" value="Dappen Glass">Dappen Glass  (pcs)</option>
                                                    <option data-name="Jarum Irigasi" value="Jarum Irigasi">Jarum Irigasi  (pcs)</option>
                                                    <option data-name="Jarum Ekstirpasi" value="Jarum Ekstirpasi">Jarum Ekstirpasi  (pcs)</option>
                                                    <option data-name="Jarum Lentulo" value="Jarum Lentulo">Jarum Lentulo  (pcs)</option>
                                                    <option data-name="Face Shield" value="Face Shield">Face Shield (pcs)</option>
                                                    <option data-name="Cermin Oklusal" value="Cermin Oklusal">Cermin Oklusal (pcs)</option>
                                                    <option data-name="Lampu UV" value="Lampu UV">Lampu UV  (pcs)</option>
                                                    <option data-name="Protaper" value="Protaper">Protaper (pcs)</option>
                                                    <option data-name="Gauge" value="Gauge">Gauge (pcs)</option>
                                                    <option data-name="Kuret" value="Kuret">Kuret (pcs)</option>
                                                    <option data-name="Bur Eve" value="Bur Eve">Bur Eve (pcs)</option>
                                                    <option data-name="Linggual Button" value="Linggual Button">Linggual Button (biji)</option>
                                                    <option data-name="Soflex Disc" value="Soflex Disc">Soflex Disc (pcs)</option>
                                                    <option data-name="Enhance Bur Flaim" value="Enhance Bur Flaim">Enhance Bur Flaim (pcs)</option>
                                                    <option data-name="Finishing Bur" value="Finishing Bur">Finishing Bur (pcs)</option>
                                                    <option data-name="Cermin" value="Cermin">Cermin  (pcs)</option>
                                                    <option data-name="Kontainer Box Stainless" value="Kontainer Box Stainless">Kontainer Box Stainless (pcs)</option>
                                                    <option data-name="Kotak Stainless Untuk Tampon" value="Kotak Stainless Untuk Tampon">Kotak Stainless Untuk Tampon (pcs)</option>
                                                    <option data-name="Brush Lowspeed" value="Brush Lowspeed">Brush Lowspeed  (pcs)</option>
                                                    <option data-name="Dental Penahan Lidah" value="Dental Penahan Lidah">Dental Penahan Lidah (pcs)</option>
                                                    <option data-name="Lowspeed Contra Angel" value="Lowspeed Contra Angel">Lowspeed Contra Angel (pcs)</option>
                                                    <option data-name="Lowspeed Staight" value="Lowspeed Staight">Lowspeed Staight (pcs)</option>
                                                    <option data-name="Matabur Highspeed Endo Acses" value="Matabur Highspeed Endo Acses">Matabur Highspeed Endo Acses (pcs)</option>
                                                    <option data-name="Matabur Round" value="Matabur Round">Matabur Round (pcs)</option>
                                                    <option data-name="Matabur Fissure" value="Matabur Fissure">Matabur Fissure (pcs)</option>
                                                    <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.1" value="Sendok Cetak Penuh Rahang Atas Bawah No.1">Sendok Cetak Penuh Rahang Atas Bawah No.1 (pasang)</option>
                                                    <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.2" value="Sendok Cetak Penuh Rahang Atas Bawah No.2">Sendok Cetak Penuh Rahang Atas Bawah No.2 (pasang)</option>
                                                    <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.3" value="Sendok Cetak Penuh Rahang Atas Bawah No.3">Sendok Cetak Penuh Rahang Atas Bawah No.3 (pasang)</option>
                                                    <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.4" value="Sendok Cetak Penuh Rahang Atas Bawah No.4">Sendok Cetak Penuh Rahang Atas Bawah No.4 (pasang)</option>
                                                    <option data-name="Sendok Cetak Penuh Biru Uk. S" value="Sendok Cetak Penuh Biru Uk. S">Sendok Cetak Penuh Biru Uk. S (pasang)</option>
                                                    <option data-name="Sendok Cetak Penuh Biru Uk. M" value="Sendok Cetak Penuh Biru Uk. M">Sendok Cetak Penuh Biru Uk. M (pasang)</option>
                                                    <option data-name="Sendok Cetak Penuh Biru Uk. L" value="Sendok Cetak Penuh Biru Uk. L">Sendok Cetak Penuh Biru Uk. L (pasang)</option>
                                                    <option data-name="Sendok Cetak Sebagian Biru" value="Sendok Cetak Sebagian Biru">Sendok Cetak Sebagian Biru (pasang)</option>
                                                    <option data-name="Shadeguide Vita" value="Shadeguide Vita">Shadeguide Vita (pcs)</option>
                                                    <option data-name="Benang Jarum Slik 3/0 atau 4/0" value="Benang Jarum Slik 3/0 atau 4/0">Benang Jarum Slik 3/0 atau 4/0 (pack)</option>
                                                    <option data-name="Needle Citoject" value="Needle Citoject">Needle Citoject (pcs)</option>
                                                    <option data-name="Tang Cabut Dewasa" value="Tang Cabut Dewasa">Tang Cabut Dewasa (paket)</option>
                                                    <option data-name="Gunting Benang Bengkok" value="Gunting Benang Bengkok">Gunting Benang Bengkok (pcs)</option>
                                                    <option data-name="Endok Block" value="Endok Block">Endok Block (pcs)</option>
                                                    <option data-name="Box K-File" value="Box K-File">Box K-File (pcs)</option>
                                                    <option data-name="Plugger" value="Plugger">Plugger (pack)</option>
                                                    <option data-name="Spreader" value="Spreader">Spreader (pack)</option>
                                                    <option data-name="Alat Bleaching LED" value="Alat Bleaching LED">Alat Bleaching LED (pcs)</option>
                                                    <option data-name="Probe" value="Probe">Probe (pcs)</option>
                                                    <option data-name="Scaler Hoe Kanan" value="Scaler Hoe Kanan">Scaler Hoe Kanan  (pcs)</option>
                                                    <option data-name="Ligature Splinting Diameter 0.5" value="Ligature Splinting Diameter 0.5">Ligature Splinting Diameter 0.5 (pcs)</option>
                                                    <option data-name="Fiber Splinting" value="Fiber Splinting">Fiber Splinting (pcs)</option>
                                                    <option data-name="Kaca Mata Medis Merah" value="Kaca Mata Medis Merah">Kaca Mata Medis Merah  (pcs)</option>
                                                    <option data-name="Cheek Retractor" value="Cheek Retractor">Cheek Retractor  (pcs)</option>
                                                    <option data-name="Syringe" value="Syringe">Syringe  (pcs)</option>
                                                    <option data-name="K-File No 08" value="K-File No 08">K-File No 08 (pack)</option>
                                                    <option data-name="K-File No 10" value="K-File No 10">K-File No 10 (pack)</option>
                                                    <option data-name="K-File No 15-40" value="K-File No 15-40">K-File No 15-40 (pack)</option>
                                                    <option data-name="K-File No 15" value="K-File No 15">K-File No 15 (pack)</option>
                                                    <option data-name="K-File No 45-80" value="K-File No 45-80">K-File No 45-80 (pack)</option>
                                                    <option data-name="Konektor Polibib" value="Konektor Polibib">Konektor Polibib  (pcs)</option>
                                                    <option data-name="Threeway Syringe" value="Threeway Syringe">Threeway Syringe (pcs)</option>
                                                    <option data-name="Highspeed 2 Hole" value="Highspeed 2 Hole">Highspeed 2 Hole (pcs)</option>
                                                    <option data-name="Highspeed 4 Hole" value="Highspeed 4 Hole">Highspeed 4 Hole (pcs)</option>
                                                    <option data-name="Catridge" value="Catridge">Catridge (pcs)</option>
                                                    <option data-name="Scaler Hoe Kiri" value="Scaler Hoe Kiri">Scaler Hoe Kiri  (pcs)</option>
                                                    <option data-name="Ligature Splinting Diameter 0.3" value="Ligature Splinting Diameter 0.3">Ligature Splinting Diameter 0.3 (pcs)</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="hidden" max="200" name="description[]" required value="" class="form-control description" />
                                        </td>
                                        <td>
                                            <input type="number" min="1" name="qty[]" value="" required class="form-control" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <?php else: ?>
                                        <?php 
                                            $details = explode(",",$material_request->details);
                                        ?>
                                        <?php foreach($details as $idx => $detail): ?>
                                            <?php 
                                                $detail = explode("*",$detail);
                                            ?>
                                            <tr>
                                                <td>
                                                    <select class="form-control m-bot15 js-example-basic-single medicine_select" name="medicine_id[]">  
                                                        <option <?php if($detail[0] == '-'):?>selected<?php endif;?> value="-"><?php echo lang('select'); ?></option>
                                                        <!-- <?php foreach ($medicines as $medicine) { ?>
                                                            <option data-name="<?php echo $medicine->name; ?>" value="<?php echo $medicine->id; ?>"<?php
                                                            if (!empty($detail[0])) {
                                                                if ($detail[0] == $medicine->id) {
                                                                    echo 'selected';
                                                                }
                                                            }
                                                            ?>><?php echo $medicine->name; ?> </option>
                                                                <?php } ?> -->

                                                            <option data-name="Minosep" value="Minosep">Minosep  (btl)</option>
                                                            <option data-name="Amoxicillin Generic" value="Amoxicillin Generic">Amoxicillin Generic  (tablet)</option>
                                                            <option data-name="Amoxicillin Paten" value="Amoxicillin Paten">Amoxicillin Paten  (tablet)</option>
                                                            <option data-name="Asam Mefenamat Generic" value="Asam Mefenamat Generic">Asam Mefenamat Generic  (tablet)</option>
                                                            <option data-name="Asam Mefenamat Paten" value="Asam Mefenamat Paten">Asam Mefenamat Paten  (tablet)</option>
                                                            <option data-name="Metronidazole" value="Metronidazole">Metronidazole  (tablet)</option>
                                                            <option data-name="Clindamycin" value="Clindamycin">Clindamycin  (tablet)</option>
                                                            <option data-name="Dexamethasone" value="Dexamethasone">Dexamethasone (tablet)</option>
                                                            <option data-name="Samcofenac" value="Samcofenac">Samcofenac (tablet)</option>
                                                            <option data-name="Kaditic" value="Kaditic">Kaditic (tablet)</option>
                                                            <option data-name="Paracetamol" value="Paracetamol">Paracetamol  (tablet)</option>
                                                            <option data-name="Paracetamol Sirup (Sanmol)" value="Paracetamol Sirup (Sanmol)">Paracetamol Sirup (Sanmol) (btl)</option>
                                                            <option data-name="Ibuprofen Sirup" value="Ibuprofen Sirup">Ibuprofen Sirup (btl)</option>
                                                            <option data-name="Amoxcilin Sirup" value="Amoxcilin Sirup">Amoxcilin Sirup (btl)</option>
                                                            
                                                            <option data-name="Alcohol 70%" value="Alcohol 70%">Alcohol 70% (btl)</option>
                                                            <option data-name="Naocl 2.5" value="Naocl 2.5">Naocl 2.5 (btl)</option>
                                                            <option data-name="Nacl/Saline" value="Nacl/Saline">Nacl/Saline (btl)</option>
                                                            <option data-name="Betadin/Iodin" value="Betadin/Iodin">Betadin/Iodin  (btl)</option>
                                                            <option data-name="Aquades" value="Aquades">Aquades  (btl)</option>
                                                            <option data-name="Minyak Highspeed" value="Minyak Highspeed">Minyak Highspeed  (btl)</option>
                                                            <option data-name="Bahan Bleaching" value="Bahan Bleaching">Bahan Bleaching  (pack)</option>
                                                            <option data-name="Microbrush" value="Microbrush">Microbrush  (pack)</option>
                                                            <option data-name="Dental Floss Stik" value="Dental Floss Stik">Dental Floss Stik  (pack)</option>
                                                            <option data-name="Dental Floss Benang" value="Dental Floss Benang">Dental Floss Benang (pcs)</option>
                                                            <option data-name="GIC Fuji 1" value="GIC Fuji 1">GIC Fuji 1 (pack)</option>
                                                            <option data-name="GIC Fuji 2" value="GIC Fuji 2">GIC Fuji 2 (pack)</option>
                                                            <option data-name="GIC Fuji 9 Kecil" value="GIC Fuji 9 Kecil">GIC Fuji 9 Kecil (pack)</option>
                                                            <option data-name="GIC Fuji 9 Besar" value="GIC Fuji 9 Besar">GIC Fuji 9 Besar (pack)</option>
                                                            <option data-name="Calzinol (merk. Zitemp)" value="Calzinol (merk. Zitemp)">Calzinol (merk. Zitemp) (pack)</option>
                                                            <option data-name="Eugenol" value="Eugenol">Eugenol  (pcs)</option>
                                                            <option data-name="Hexabond (Bonding)" value="Hexabond (Bonding)">Hexabond (Bonding) (pcs)</option>
                                                            <option data-name="Tricresol" value="Tricresol">Tricresol  (pcs)</option>
                                                            <option data-name="TSR/OCO" value="TSR/OCO">TSR/OCO (pcs)</option>
                                                            <option data-name="TS (merk. Cevitron)" value="TS (merk. Cevitron)">TS (merk. Cevitron) (pcs)</option>
                                                            <option data-name="Hexa Etch" value="Hexa Etch">Hexa Etch  (pcs)</option>
                                                            <option data-name="Wax Ortho" value="Wax Ortho">Wax Ortho (pack)</option>
                                                            <option data-name="Komposit Flow A1" value="Komposit Flow A1">Komposit Flow A1 (pcs)</option>
                                                            <option data-name="Dental Needles" value="Dental Needles">Dental Needles  (pcs)</option>
                                                            <option data-name="Modelling Wax/malam" value="Modelling Wax/malam">Modelling Wax/malam (pack)</option>
                                                            <option data-name="Biosponge" value="Biosponge">Biosponge  (pcs)</option>
                                                            <option data-name="Wedges Hijau" value="Wedges Hijau">Wedges Hijau (pack)</option>
                                                            <option data-name="Calplus" value="Calplus">Calplus  (pcs)</option>
                                                            <option data-name="Calciplus LC" value="Calciplus LC">Calciplus LC (pcs)</option>
                                                            <option data-name="Kapas" value="Kapas">Kapas  (ball)</option>
                                                            <option data-name="Kasa" value="Kasa">Kasa  (gulung)</option>
                                                            <option data-name="Catton Roll" value="Catton Roll">Catton Roll (pack)</option>
                                                            <option data-name="Polibibs" value="Polibibs">Polibibs  (lembar)</option>
                                                            <option data-name="Masker Medis" value="Masker Medis">Masker Medis  (pack)</option>
                                                            <option data-name="Masker KN95" value="Masker KN95">Masker KN95 (pack)</option>
                                                            <option data-name="Saniter" value="Saniter">Saniter (pcs)</option>
                                                            <option data-name="Handscon Uk. S" value="Handscon Uk. S">Handscon Uk. S (pack)</option>
                                                            <option data-name="Suction" value="Suction">Suction  (pack)</option>
                                                            <option data-name="Ligature Wire Splinting" value="Ligature Wire Splinting">Ligature Wire Splinting (pcs)</option>
                                                            <option data-name="Alginate (Hygent)" value="Alginate (Hygent)">Alginate (Hygent) (pcs)</option>
                                                            <option data-name="Gips Biru" value="Gips Biru">Gips Biru  (pcs)</option>
                                                            <option data-name="Gips Kuning" value="Gips Kuning">Gips Kuning  (pcs)</option>
                                                            <option data-name="Bracket Ceramic" value="Bracket Ceramic">Bracket Ceramic  (pack)</option>
                                                            <option data-name="Roth Bracket" value="Roth Bracket">Roth Bracket  (pack)</option>
                                                            <option data-name="Elastic Ortho 1/4 5.0 Oz" value="Elastic Ortho 1/4 5.0 Oz">Elastic Ortho 1/4 5.0 Oz (pack)</option>
                                                            <option data-name="Elastic Ortho 1/8 3.5 Oz" value="Elastic Ortho 1/8 3.5 Oz">Elastic Ortho 1/8 3.5 Oz (pack)</option>
                                                            <option data-name="Elastic Ortho 5/16 3.5 Oz" value="Elastic Ortho 5/16 3.5 Oz">Elastic Ortho 5/16 3.5 Oz (pack)</option>
                                                            <option data-name="Elastic Ortho 3/16 3.5 Oz" value="Elastic Ortho 3/16 3.5 Oz">Elastic Ortho 3/16 3.5 Oz (pack)</option>
                                                            <option data-name="Elastic Ortho 5/16 5.0 Oz" value="Elastic Ortho 5/16 5.0 Oz">Elastic Ortho 5/16 5.0 Oz (pack)</option>
                                                            <option data-name="Buccal Tube" value="Buccal Tube">Buccal Tube  (pack)</option>
                                                            <option data-name="Paper Point 15-40" value="Paper Point 15-40">Paper Point 15-40 (pack)</option>
                                                            <option data-name="Paper Point 45-80" value="Paper Point 45-80">Paper Point 45-80 (pack)</option>
                                                            <option data-name="Guta Percha Points F1/f3" value="Guta Percha Points F1/f3">Guta Percha Points F1/f3 (pack)</option>
                                                            <option data-name="Guta Percha Points 15-40" value="Guta Percha Points 15-40">Guta Percha Points 15-40 (pack)</option>
                                                            <option data-name="Guta Percha Points 45-80" value="Guta Percha Points 45-80">Guta Percha Points 45-80 (pack)</option>
                                                            <option data-name="Paper Pad" value="Paper Pad">Paper Pad  (bendel)</option>
                                                            <option data-name="Pasak Fiber" value="Pasak Fiber">Pasak Fiber  (pack)</option>
                                                            <option data-name="Interdental Brush" value="Interdental Brush">Interdental Brush  (pcs)</option>
                                                            <option data-name="Articulating Paper" value="Articulating Paper">Articulating Paper  (sheet)</option>
                                                            <option data-name="Sikat Dr.Smith" value="Sikat Dr.Smith">Sikat Dr.Smith  (pcs)</option>
                                                            <option data-name="Wedges ortho abu" value="Wedges ortho abu">Wedges ortho abu (pcs)</option>
                                                            <option data-name="Topical anastesi lolite" value="Topical anastesi lolite">Topical anastesi lolite  (pcs)</option>
                                                            <option data-name="Lidocaine" value="Lidocaine">Lidocaine  (ampul)</option>
                                                            <option data-name="Pehacain" value="Pehacain">Pehacain  (pack)</option>
                                                            <option data-name="Carpul 3%" value="Carpul 3%">Carpul 3% (strip)</option>
                                                            <option data-name="GIC Mini" value="GIC Mini">GIC Mini  (pack)</option>
                                                            <option data-name="CHKM" value="CHKM">CHKM (pcs)</option>
                                                            <option data-name="Komposit A2" value="Komposit A2">Komposit A2 (pcs)</option>
                                                            <option data-name="Komposit A3" value="Komposit A3">Komposit A3 (pcs)</option>
                                                            <option data-name="Komposit A3.5" value="Komposit A3.5">Komposit A3.5 (pcs)</option>
                                                            <option data-name="ZnPO4/Ellite Cement" value="ZnPO4/Ellite Cement">ZnPO4/Ellite Cement  (pack)</option>
                                                            <option data-name="Open coil spring" value="Open coil spring">Open coil spring  (pack)</option>
                                                            <option data-name="Close coil spring" value="Close coil spring">Close coil spring (pack)</option>
                                                            <option data-name="Matrix band" value="Matrix band">Matrix band  (pack)</option>
                                                            <option data-name="H2O2 50%" value="H2O2 50%">H2O2 50% (btl)</option>
                                                            <option data-name="Power O oren" value="Power O oren">Power O oren  (pcs)</option>
                                                            <option data-name="Power O hitam " value="Power O hitam ">Power O hitam   (pcs)</option>
                                                            <option data-name="Power O ungu tua" value="Power O ungu tua">Power O ungu tua (pcs)</option>
                                                            <option data-name="Power O hijau" value="Power O hijau">Power O hijau  (pcs)</option>
                                                            <option data-name="Power O hijau gliter/ metalic" value="Power O hijau gliter/ metalic">Power O hijau gliter/ metalic (pcs)</option>
                                                            <option data-name="Power O biru gliter /metalic" value="Power O biru gliter /metalic">Power O biru gliter /metalic (pcs)</option>
                                                            <option data-name="Power O biru" value="Power O biru">Power O biru  (pcs)</option>
                                                            <option data-name="Power O biru laut" value="Power O biru laut">Power O biru laut  (pcs)</option>
                                                            <option data-name="Power O kuning" value="Power O kuning">Power O kuning  (pcs)</option>
                                                            <option data-name="Power O coklat" value="Power O coklat">Power O coklat  (pcs)</option>
                                                            <option data-name="Power O biru tosca" value="Power O biru tosca">Power O biru tosca  (pcs)</option>
                                                            <option data-name="Power O pink" value="Power O pink">Power O pink  (pcs)</option>
                                                            <option data-name="Power O merah" value="Power O merah">Power O merah  (pcs)</option>
                                                            <option data-name="Power O maron" value="Power O maron">Power O maron (pcs)</option>
                                                            <option data-name="Power O putih" value="Power O putih">Power O putih (pcs)</option>
                                                            <option data-name="Power O navy besar" value="Power O navy besar">Power O navy besar (pcs)</option>
                                                            <option data-name="Power O ungu muda/lilac" value="Power O ungu muda/lilac">Power O ungu muda/lilac (pcs)</option>
                                                            <option data-name="Power O putih bening/transparan" value="Power O putih bening/transparan">Power O putih bening/transparan  (pcs)</option>
                                                            <option data-name="Power O pink tua" value="Power O pink tua">Power O pink tua (pcs)</option>
                                                            <option data-name="Power O hijau tosca" value="Power O hijau tosca">Power O hijau tosca (pcs)</option>
                                                            <option data-name="Power O hijau muda" value="Power O hijau muda">Power O hijau muda (pcs)</option>
                                                            <option data-name="Power O ungu" value="Power O ungu">Power O ungu (pcs)</option>
                                                            <option data-name="Power O merah jambu" value="Power O merah jambu">Power O merah jambu (pcs)</option>
                                                            <option data-name="Power O biru tua" value="Power O biru tua">Power O biru tua (pcs)</option>
                                                            <option data-name="Power O biru SMA" value="Power O biru SMA">Power O biru SMA (pcs)</option>
                                                            <option data-name="Power O pink muda" value="Power O pink muda">Power O pink muda (pcs)</option>
                                                            <option data-name="Power O abu" value="Power O abu">Power O abu (pcs)</option>
                                                            <option data-name="Power chain hijau tosca" value="Power chain hijau tosca">Power chain hijau tosca  (cm)</option>
                                                            <option data-name="Power chain hijau muda" value="Power chain hijau muda">Power chain hijau muda (cm)</option>
                                                            <option data-name="Power chain ungu" value="Power chain ungu">Power chain ungu  (cm)</option>
                                                            <option data-name="Power chain pink muda" value="Power chain pink muda">Power chain pink muda (cm)</option>
                                                            <option data-name="Power chain biru" value="Power chain biru">Power chain biru  (cm)</option>
                                                            <option data-name="Power chain biru muda" value="Power chain biru muda">Power chain biru muda  (cm)</option>
                                                            <option data-name="Power chain bening" value="Power chain bening">Power chain bening  (cm)</option>
                                                            <option data-name="Power chain coklat" value="Power chain coklat">Power chain coklat  (cm)</option>
                                                            <option data-name="Power chain putih" value="Power chain putih">Power chain putih  (cm)</option>
                                                            <option data-name="Power chain  biru dongker" value="Power chain  biru dongker">Power chain  biru dongker  (cm)</option>
                                                            <option data-name="Power chain abu" value="Power chain abu">Power chain abu  (cm)</option>
                                                            <option data-name="Power chain hitam" value="Power chain hitam">Power chain hitam  (cm)</option>
                                                            <option data-name="Power chain biru tosca" value="Power chain biru tosca">Power chain biru tosca (cm)</option>
                                                            <option data-name="Power chain maron" value="Power chain maron">Power chain maron  (cm)</option>
                                                            <option data-name="Power chain merah" value="Power chain merah">Power chain merah  (cm)</option>
                                                            <option data-name="Power chain hijau" value="Power chain hijau">Power chain hijau (cm)</option>
                                                            <option data-name="Power chain biru SMA" value="Power chain biru SMA">Power chain biru SMA (cm)</option>
                                                            <option data-name="Power chain ungu muda" value="Power chain ungu muda">Power chain ungu muda (cm)</option>
                                                            <option data-name="Power chain pink magenta" value="Power chain pink magenta">Power chain pink magenta (cm)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.12 Lower" value="Wire NiTi Ovoid 0.12 Lower">Wire NiTi Ovoid 0.12 Lower (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.14 Lower" value="Wire NiTi Ovoid 0.14 Lower">Wire NiTi Ovoid 0.14 Lower (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.16 Lower" value="Wire NiTi Ovoid 0.16 Lower">Wire NiTi Ovoid 0.16 Lower (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.18 Lower" value="Wire NiTi Ovoid 0.18 Lower">Wire NiTi Ovoid 0.18 Lower (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.12 Upper" value="Wire NiTi Ovoid 0.12 Upper">Wire NiTi Ovoid 0.12 Upper (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.14 Upper" value="Wire NiTi Ovoid 0.14 Upper">Wire NiTi Ovoid 0.14 Upper (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.16 Upper" value="Wire NiTi Ovoid 0.16 Upper">Wire NiTi Ovoid 0.16 Upper (pack)</option>
                                                            <option data-name="Wire NiTi Ovoid 0.18 Upper" value="Wire NiTi Ovoid 0.18 Upper">Wire NiTi Ovoid 0.18 Upper (pack)</option>
                                                            <option data-name="Wire SS Rekta 0.016x022 Lower" value="Wire SS Rekta 0.016x022 Lower">Wire SS Rekta 0.016x022 Lower (pack)</option>
                                                            <option data-name="Wire SS Rekta 0.016x022 Upper" value="Wire SS Rekta 0.016x022 Upper">Wire SS Rekta 0.016x022 Upper (pack)</option>
                                                            <option data-name="Wire SS Rekta 16x16 Lower" value="Wire SS Rekta 16x16 Lower">Wire SS Rekta 16x16 Lower (pack)</option>
                                                            <option data-name="Wire SS Rekta 16x16 Upper" value="Wire SS Rekta 16x16 Upper">Wire SS Rekta 16x16 Upper (pack)</option>
                                                            <option data-name="Wire SS Rekta 0.016x0.016 Lower" value="Wire SS Rekta 0.016x0.016 Lower">Wire SS Rekta 0.016x0.016 Lower (pack)</option>
                                                            <option data-name="Wire SS Rekta 0.016x0.016 Upper" value="Wire SS Rekta 0.016x0.016 Upper">Wire SS Rekta 0.016x0.016 Upper (pack)</option>
                                                            <option data-name="Wire SS Rekta 0.16x022 Lower" value="Wire SS Rekta 0.16x022 Lower">Wire SS Rekta 0.16x022 Lower (pack)</option>
                                                            <option data-name="Wire SS Rekta 0.16x022 Upper" value="Wire SS Rekta 0.16x022 Upper">Wire SS Rekta 0.16x022 Upper (pack)</option>
                                                            <option data-name="Niti reserve 0.012 Upper" value="Niti reserve 0.012 Upper">Niti reserve 0.012 Upper (pack)</option>
                                                            <option data-name="Niti reserve 0.012 Lower" value="Niti reserve 0.012 Lower">Niti reserve 0.012 Lower (pack)</option>
                                                            <option data-name="Niti reserve 0.014 Upper" value="Niti reserve 0.014 Upper">Niti reserve 0.014 Upper (pack)</option>
                                                            <option data-name="Niti reserver 0.014 Lower" value="Niti reserver 0.014 Lower">Niti reserver 0.014 Lower (pack)</option>
                                                            <option data-name="Niti reverse 0.016 Upper" value="Niti reverse 0.016 Upper">Niti reverse 0.016 Upper (pack)</option>
                                                            <option data-name="Niti reverse 0.016 lower" value="Niti reverse 0.016 lower">Niti reverse 0.016 lower (pack)</option>
                                                            <option data-name="Niti reserve 0.016x0.016 lower" value="Niti reserve 0.016x0.016 lower">Niti reserve 0.016x0.016 lower (pack)</option>
                                                            <option data-name="Niti reserve 0.016x0.016 upper" value="Niti reserve 0.016x0.016 upper">Niti reserve 0.016x0.016 upper (pack)</option>
                                                            <option data-name="Safty Box" value="Safty Box">Safty Box (pcs)</option>
                                                            <option data-name="Chlor Ethyl" value="Chlor Ethyl">Chlor Ethyl  (pcs)</option>
                                                            <option data-name="Polishing Strip/Enamel Striping" value="Polishing Strip/Enamel Striping">Polishing Strip/Enamel Striping (pcs)</option>
                                                            <option data-name="Clinpro (TAF)" value="Clinpro (TAF)">Clinpro (TAF) (pack)</option>
                                                            <option data-name="CE Klorofil" value="CE Klorofil">CE Klorofil (pcs)</option>
                                                            <option data-name="Surgical Blades Uk.10" value="Surgical Blades Uk.10">Surgical Blades Uk.10 (pcs)</option>
                                                            <option data-name="Surgical Blades Uk.15" value="Surgical Blades Uk.15">Surgical Blades Uk.15 (pcs)</option>
                                                            <option data-name="Seluloid Strip" value="Seluloid Strip">Seluloid Strip  (pack)</option>
                                                            <option data-name="Paper Point F1-F1" value="Paper Point F1-F1">Paper Point F1-F1 (Pack)</option>
                                                            <option data-name="Kresek Obat" value="Kresek Obat">Kresek Obat  (bendel)</option>
                                                            <option data-name="Kapur Barus KM" value="Kapur Barus KM">Kapur Barus KM (pcs)</option>
                                                            <option data-name="Prostek/vVxal" value="Prostek/vVxal">Prostek/vVxal (btl)</option>
                                                            <option data-name="Pemutih Baju" value="Pemutih Baju">Pemutih Baju (btl)</option>
                                                            <option data-name="Sunlight" value="Sunlight">Sunlight (pcs)</option>
                                                            <option data-name="Tisu" value="Tisu">Tisu (pcs)</option>
                                                            <option data-name="Kresek Merah Besar" value="Kresek Merah Besar">Kresek Merah Besar  (pack)</option>
                                                            <option data-name="Kresek Kuning Medis" value="Kresek Kuning Medis">Kresek Kuning Medis  (pack)</option>
                                                            <option data-name="Kresek Hitam Besar" value="Kresek Hitam Besar">Kresek Hitam Besar  (pack)</option>
                                                            <option data-name="Kresek Hitam Sedang" value="Kresek Hitam Sedang">Kresek Hitam Sedang  (pack)</option>
                                                            <option data-name="Kresek Putih Sedang" value="Kresek Putih Sedang">Kresek Putih Sedang  (pack)</option>
                                                            <option data-name="Kresek Hitam Kecil" value="Kresek Hitam Kecil">Kresek Hitam Kecil (pack)</option>
                                                            <option data-name="Kresek Putih Kecil" value="Kresek Putih Kecil">Kresek Putih Kecil  (pack)</option>
                                                            <option data-name="Pembersih Lantai" value="Pembersih Lantai">Pembersih Lantai (pcs)</option>
                                                            <option data-name="Detergen Cair" value="Detergen Cair">Detergen Cair  (pcs)</option>
                                                            <option data-name="Handwash" value="Handwash">Handwash  (pcs)</option>
                                                            <option data-name="Spirtus" value="Spirtus">Spirtus (btl)</option>
                                                            <option data-name="Plastik Cetik 10x15" value="Plastik Cetik 10x15">Plastik Cetik 10x15 (pack)</option>
                                                            <option data-name="Plastic Cetik 7x10" value="Plastic Cetik 7x10">Plastic Cetik 7x10 (pack)</option>
                                                            <option data-name="Air" value="Air">Air (dus)</option>
                                                            <option data-name="Cling" value="Cling">Cling (pcs)</option>
                                                            <option data-name="Stela Gantung" value="Stela Gantung">Stela Gantung (pcs)</option>
                                                            <option data-name="Bulpoint" value="Bulpoint">Bulpoint (pcs)</option>
                                                            <option data-name="Surat Rujukan Lab Gigi AY" value="Surat Rujukan Lab Gigi AY">Surat Rujukan Lab Gigi AY (bendel)</option>
                                                            <option data-name="Surat Keterangan Dokter" value="Surat Keterangan Dokter">Surat Keterangan Dokter (lembar)</option>
                                                            <option data-name="Surat Rujukan Dokter" value="Surat Rujukan Dokter">Surat Rujukan Dokter (lembar)</option>
                                                            <option data-name="Surat Resep" value="Surat Resep">Surat Resep (lembar)</option>
                                                            <option data-name="Lembar laporan keuangan harian" value="Lembar laporan keuangan harian">Lembar laporan keuangan harian (lembar)</option>
                                                            <option data-name="Informed consent penolakan" value="Informed consent penolakan">Informed consent penolakan (lembar)</option>
                                                            <option data-name="Informed consent persetujuan" value="Informed consent persetujuan">Informed consent persetujuan (pcs)</option>
                                                            <option data-name="Lembar laporan harian" value="Lembar laporan harian">Lembar laporan harian  (pcs)</option>
                                                            <option data-name="Wadah kotak plastik untuk shoes cover kotor" value="Wadah kotak plastik untuk shoes cover kotor">Wadah kotak plastik untuk shoes cover kotor (pcs)</option>
                                                            <option data-name="Shoes Cover" value="Shoes Cover">Shoes Cover (pcs)</option>
                                                            <option data-name="Gunting Kertas" value="Gunting Kertas">Gunting Kertas (pcs)</option>
                                                            <option data-name="Gayung Kamar Mandi" value="Gayung Kamar Mandi">Gayung Kamar Mandi (pcs)</option>
                                                            <option data-name="Sikat Kamar Mandi/WC" value="Sikat Kamar Mandi/WC">Sikat Kamar Mandi/WC (pcs)</option>
                                                            <option data-name="Toples" value="Toples">Toples (pcs)</option>
                                                            <option data-name="Hanger" value="Hanger">Hanger (pcs)</option>
                                                            <option data-name="Staples" value="Staples">Staples (pcs)</option>
                                                            <option data-name="Spons" value="Spons">Spons (pcs)</option>
                                                            <option data-name="Kwitansi" value="Kwitansi">Kwitansi (bendel)</option>
                                                            <option data-name="Pasta Gigi" value="Pasta Gigi">Pasta Gigi (pcs)</option>
                                                            <option data-name="Amplas" value="Amplas">Amplas (pcs)</option>
                                                            <option data-name="Buku Tulis" value="Buku Tulis">Buku Tulis (pcs)</option>
                                                            <option data-name="Lem Paralon/Pipa" value="Lem Paralon/Pipa">Lem Paralon/Pipa (pcs)</option>
                                                            <option data-name="Batrai" value="Batrai">Batrai  (pcs)</option>
                                                            <option data-name="Hadiah Anak Cowok" value="Hadiah Anak Cowok">Hadiah Anak Cowok (pcs)</option>
                                                            <option data-name="Hadiah Anak Cewek" value="Hadiah Anak Cewek">Hadiah Anak Cewek (pcs)</option>
                                                            <option data-name="Pigora Putih" value="Pigora Putih">Pigora Putih (pcs)</option>
                                                            <option data-name="Isi Staples" value="Isi Staples">Isi Staples (pack)</option>
                                                            <option data-name="Stipo cair/ kertas" value="Stipo cair/ kertas">Stipo cair/ kertas (pcs)</option>
                                                            <option data-name="Solasi Kecil" value="Solasi Kecil">Solasi Kecil (pcs)</option>
                                                            <option data-name="Sandal Operasi" value="Sandal Operasi">Sandal Operasi  (pasang)</option>
                                                            <option data-name="APD" value="APD">APD (pcs)</option>
                                                            <option data-name="Braket Holder" value="Braket Holder">Braket Holder (pcs)</option>
                                                            <option data-name="Spatula GIC" value="Spatula GIC">Spatula GIC (pcs)</option>
                                                            <option data-name="Plastis Filling" value="Plastis Filling">Plastis Filling (pcs)</option>
                                                            <option data-name="Semen Stropper" value="Semen Stropper">Semen Stropper (pcs)</option>
                                                            <option data-name="Needle Holder/Arteri Klem" value="Needle Holder/Arteri Klem">Needle Holder/Arteri Klem (pcs)</option>
                                                            <option data-name="Sonde" value="Sonde">Sonde (pcs)</option>
                                                            <option data-name="Escavator" value="Escavator">Escavator (pcs)</option>
                                                            <option data-name="Kaca Mulut Tanpa Tangkai" value="Kaca Mulut Tanpa Tangkai">Kaca Mulut Tanpa Tangkai (pcs)</option>
                                                            <option data-name="Nierbeken" value="Nierbeken">Nierbeken  (pcs)</option>
                                                            <option data-name="Pinset" value="Pinset">Pinset  (pcs)</option>
                                                            <option data-name="Tang Cabut Anak" value="Tang Cabut Anak">Tang Cabut Anak  (pcs)</option>
                                                            <option data-name="Tang Remover Braket" value="Tang Remover Braket">Tang Remover Braket  (pcs)</option>
                                                            <option data-name="Tang Adam Universal" value="Tang Adam Universal">Tang Adam Universal (pcs)</option>
                                                            <option data-name="Scaler Ultrasonic" value="Scaler Ultrasonic">Scaler Ultrasonic (set)</option>
                                                            <option data-name="Tip Scaler" value="Tip Scaler">Tip Scaler  (pcs)</option>
                                                            <option data-name="Arkansas Lowspeed Stone (flam)" value="Arkansas Lowspeed Stone (flam)">Arkansas Lowspeed Stone (flam) (pcs)</option>
                                                            <option data-name="Arkansas Lowspeed Stone (cones)" value="Arkansas Lowspeed Stone (cones)">Arkansas Lowspeed Stone (cones) (pcs)</option>
                                                            <option data-name="Arkansas Lowspeed Stone (round)" value="Arkansas Lowspeed Stone (round)">Arkansas Lowspeed Stone (round) (pcs)</option>
                                                            <option data-name="Finishing (discs)" value="Finishing (discs)">Finishing (discs) (pcs)</option>
                                                            <option data-name="Finishing (cups)" value="Finishing (cups)">Finishing (cups) (pcs)</option>
                                                            <option data-name="Finishing (point)" value="Finishing (point)">Finishing (point) (pcs)</option>
                                                            <option data-name="Round Bur" value="Round Bur">Round Bur  (pcs)</option>
                                                            <option data-name="Bur Brush" value="Bur Brush">Bur Brush (pcs)</option>
                                                            <option data-name="Spatula Alginate" value="Spatula Alginate">Spatula Alginate  (pcs)</option>
                                                            <option data-name="Spatula Semen" value="Spatula Semen">Spatula Semen  (pcs)</option>
                                                            <option data-name="Bein" value="Bein">Bein  (pcs)</option>
                                                            <option data-name="Cryer" value="Cryer">Cryer  (pcs)</option>
                                                            <option data-name="Handle Scapel" value="Handle Scapel">Handle Scapel  (pcs)</option>
                                                            <option data-name="Tang Potong Distal" value="Tang Potong Distal">Tang Potong Distal (pcs)</option>
                                                            <option data-name="Light Curing LED" value="Light Curing LED">Light Curing LED (pcs)</option>
                                                            <option data-name="Bowl (Mangkok)" value="Bowl (Mangkok)">Bowl (Mangkok) (pcs)</option>
                                                            <option data-name="Tensi" value="Tensi">Tensi  (pcs)</option>
                                                            <option data-name="Polishing Kit Komposit" value="Polishing Kit Komposit">Polishing Kit Komposit (pcs)</option>
                                                            <option data-name="Dappen Glass" value="Dappen Glass">Dappen Glass  (pcs)</option>
                                                            <option data-name="Jarum Irigasi" value="Jarum Irigasi">Jarum Irigasi  (pcs)</option>
                                                            <option data-name="Jarum Ekstirpasi" value="Jarum Ekstirpasi">Jarum Ekstirpasi  (pcs)</option>
                                                            <option data-name="Jarum Lentulo" value="Jarum Lentulo">Jarum Lentulo  (pcs)</option>
                                                            <option data-name="Face Shield" value="Face Shield">Face Shield (pcs)</option>
                                                            <option data-name="Cermin Oklusal" value="Cermin Oklusal">Cermin Oklusal (pcs)</option>
                                                            <option data-name="Lampu UV" value="Lampu UV">Lampu UV  (pcs)</option>
                                                            <option data-name="Protaper" value="Protaper">Protaper (pcs)</option>
                                                            <option data-name="Gauge" value="Gauge">Gauge (pcs)</option>
                                                            <option data-name="Kuret" value="Kuret">Kuret (pcs)</option>
                                                            <option data-name="Bur Eve" value="Bur Eve">Bur Eve (pcs)</option>
                                                            <option data-name="Linggual Button" value="Linggual Button">Linggual Button (biji)</option>
                                                            <option data-name="Soflex Disc" value="Soflex Disc">Soflex Disc (pcs)</option>
                                                            <option data-name="Enhance Bur Flaim" value="Enhance Bur Flaim">Enhance Bur Flaim (pcs)</option>
                                                            <option data-name="Finishing Bur" value="Finishing Bur">Finishing Bur (pcs)</option>
                                                            <option data-name="Cermin" value="Cermin">Cermin  (pcs)</option>
                                                            <option data-name="Kontainer Box Stainless" value="Kontainer Box Stainless">Kontainer Box Stainless (pcs)</option>
                                                            <option data-name="Kotak Stainless Untuk Tampon" value="Kotak Stainless Untuk Tampon">Kotak Stainless Untuk Tampon (pcs)</option>
                                                            <option data-name="Brush Lowspeed" value="Brush Lowspeed">Brush Lowspeed  (pcs)</option>
                                                            <option data-name="Dental Penahan Lidah" value="Dental Penahan Lidah">Dental Penahan Lidah (pcs)</option>
                                                            <option data-name="Lowspeed Contra Angel" value="Lowspeed Contra Angel">Lowspeed Contra Angel (pcs)</option>
                                                            <option data-name="Lowspeed Staight" value="Lowspeed Staight">Lowspeed Staight (pcs)</option>
                                                            <option data-name="Matabur Highspeed Endo Acses" value="Matabur Highspeed Endo Acses">Matabur Highspeed Endo Acses (pcs)</option>
                                                            <option data-name="Matabur Round" value="Matabur Round">Matabur Round (pcs)</option>
                                                            <option data-name="Matabur Fissure" value="Matabur Fissure">Matabur Fissure (pcs)</option>
                                                            <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.1" value="Sendok Cetak Penuh Rahang Atas Bawah No.1">Sendok Cetak Penuh Rahang Atas Bawah No.1 (pasang)</option>
                                                            <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.2" value="Sendok Cetak Penuh Rahang Atas Bawah No.2">Sendok Cetak Penuh Rahang Atas Bawah No.2 (pasang)</option>
                                                            <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.3" value="Sendok Cetak Penuh Rahang Atas Bawah No.3">Sendok Cetak Penuh Rahang Atas Bawah No.3 (pasang)</option>
                                                            <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.4" value="Sendok Cetak Penuh Rahang Atas Bawah No.4">Sendok Cetak Penuh Rahang Atas Bawah No.4 (pasang)</option>
                                                            <option data-name="Sendok Cetak Penuh Biru Uk. S" value="Sendok Cetak Penuh Biru Uk. S">Sendok Cetak Penuh Biru Uk. S (pasang)</option>
                                                            <option data-name="Sendok Cetak Penuh Biru Uk. M" value="Sendok Cetak Penuh Biru Uk. M">Sendok Cetak Penuh Biru Uk. M (pasang)</option>
                                                            <option data-name="Sendok Cetak Penuh Biru Uk. L" value="Sendok Cetak Penuh Biru Uk. L">Sendok Cetak Penuh Biru Uk. L (pasang)</option>
                                                            <option data-name="Sendok Cetak Sebagian Biru" value="Sendok Cetak Sebagian Biru">Sendok Cetak Sebagian Biru (pasang)</option>
                                                            <option data-name="Shadeguide Vita" value="Shadeguide Vita">Shadeguide Vita (pcs)</option>
                                                            <option data-name="Benang Jarum Slik 3/0 atau 4/0" value="Benang Jarum Slik 3/0 atau 4/0">Benang Jarum Slik 3/0 atau 4/0 (pack)</option>
                                                            <option data-name="Needle Citoject" value="Needle Citoject">Needle Citoject (pcs)</option>
                                                            <option data-name="Tang Cabut Dewasa" value="Tang Cabut Dewasa">Tang Cabut Dewasa (paket)</option>
                                                            <option data-name="Gunting Benang Bengkok" value="Gunting Benang Bengkok">Gunting Benang Bengkok (pcs)</option>
                                                            <option data-name="Endok Block" value="Endok Block">Endok Block (pcs)</option>
                                                            <option data-name="Box K-File" value="Box K-File">Box K-File (pcs)</option>
                                                            <option data-name="Plugger" value="Plugger">Plugger (pack)</option>
                                                            <option data-name="Spreader" value="Spreader">Spreader (pack)</option>
                                                            <option data-name="Alat Bleaching LED" value="Alat Bleaching LED">Alat Bleaching LED (pcs)</option>
                                                            <option data-name="Probe" value="Probe">Probe (pcs)</option>
                                                            <option data-name="Scaler Hoe Kanan" value="Scaler Hoe Kanan">Scaler Hoe Kanan  (pcs)</option>
                                                            <option data-name="Ligature Splinting Diameter 0.5" value="Ligature Splinting Diameter 0.5">Ligature Splinting Diameter 0.5 (pcs)</option>
                                                            <option data-name="Fiber Splinting" value="Fiber Splinting">Fiber Splinting (pcs)</option>
                                                            <option data-name="Kaca Mata Medis Merah" value="Kaca Mata Medis Merah">Kaca Mata Medis Merah  (pcs)</option>
                                                            <option data-name="Cheek Retractor" value="Cheek Retractor">Cheek Retractor  (pcs)</option>
                                                            <option data-name="Syringe" value="Syringe">Syringe  (pcs)</option>
                                                            <option data-name="K-File No 08" value="K-File No 08">K-File No 08 (pack)</option>
                                                            <option data-name="K-File No 10" value="K-File No 10">K-File No 10 (pack)</option>
                                                            <option data-name="K-File No 15-40" value="K-File No 15-40">K-File No 15-40 (pack)</option>
                                                            <option data-name="K-File No 15" value="K-File No 15">K-File No 15 (pack)</option>
                                                            <option data-name="K-File No 45-80" value="K-File No 45-80">K-File No 45-80 (pack)</option>
                                                            <option data-name="Konektor Polibib" value="Konektor Polibib">Konektor Polibib  (pcs)</option>
                                                            <option data-name="Threeway Syringe" value="Threeway Syringe">Threeway Syringe (pcs)</option>
                                                            <option data-name="Highspeed 2 Hole" value="Highspeed 2 Hole">Highspeed 2 Hole (pcs)</option>
                                                            <option data-name="Highspeed 4 Hole" value="Highspeed 4 Hole">Highspeed 4 Hole (pcs)</option>
                                                            <option data-name="Catridge" value="Catridge">Catridge (pcs)</option>
                                                            <option data-name="Scaler Hoe Kiri" value="Scaler Hoe Kiri">Scaler Hoe Kiri  (pcs)</option>
                                                            <option data-name="Ligature Splinting Diameter 0.3" value="Ligature Splinting Diameter 0.3">Ligature Splinting Diameter 0.3 (pcs)</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" max="200" value="<?php echo $detail[1];?>" class="form-control description" disabled />
                                                    <input type="hidden" max="200" name="description[]" required value="<?php echo $detail[1];?>" class="form-control description" />
                                                </td>
                                                <td>
                                                    <input type="number" min="1" name="qty[]" value="<?php echo $detail[2];?>" required class="form-control only-numeric" />
                                                </td>
                                                <td>
                                                    <?php if($idx > 0): ?>
                                                        <button type="button" class="btn btn-danger btnremovedetail">-</button>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <button type="submit" name="submit" class="btn btn-info pull-right"> <?php echo lang('submit'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->



<style>

    form{
        border: 0px;
    }

    .med_selected{
        background: #fff;
        padding: 10px 0px;
        margin: 5px;
    }


    .select2-container--bgform .select2-selection--multiple .select2-selection__choice {
        clear: both !important;
    }

    label {
        display: inline-block;
        margin-bottom: 5px;
        font-weight: 500;
        font-weight: bold;
    }

</style>


<script src="common/js/codearistos.min.js"></script>

<script type="text/javascript">
    var template = '<tr>\
                        <td>\
                            <select class="form-control m-bot15 js-example-basic-single medicine_select" name="medicine_id[]">\
                                <option data-name="" value="">Select</option>\
                                <option data-name="Minosep" value="Minosep">Minosep  (btl)</option>\
                                <option data-name="Amoxicillin Generic" value="Amoxicillin Generic">Amoxicillin Generic  (tablet)</option>\
                                <option data-name="Amoxicillin Paten" value="Amoxicillin Paten">Amoxicillin Paten  (tablet)</option>\
                                <option data-name="Asam Mefenamat Generic" value="Asam Mefenamat Generic">Asam Mefenamat Generic  (tablet)</option>\
                                <option data-name="Asam Mefenamat Paten" value="Asam Mefenamat Paten">Asam Mefenamat Paten  (tablet)</option>\
                                <option data-name="Metronidazole" value="Metronidazole">Metronidazole  (tablet)</option>\
                                <option data-name="Clindamycin" value="Clindamycin">Clindamycin  (tablet)</option>\
                                <option data-name="Dexamethasone" value="Dexamethasone">Dexamethasone (tablet)</option>\
                                <option data-name="Samcofenac" value="Samcofenac">Samcofenac (tablet)</option>\
                                <option data-name="Kaditic" value="Kaditic">Kaditic (tablet)</option>\
                                <option data-name="Paracetamol" value="Paracetamol">Paracetamol  (tablet)</option>\
                                <option data-name="Paracetamol Sirup (Sanmol)" value="Paracetamol Sirup (Sanmol)">Paracetamol Sirup (Sanmol) (btl)</option>\
                                <option data-name="Ibuprofen Sirup" value="Ibuprofen Sirup">Ibuprofen Sirup (btl)</option>\
                                <option data-name="Amoxcilin Sirup" value="Amoxcilin Sirup">Amoxcilin Sirup (btl)</option>\
                                <option data-name="Alcohol 70%" value="Alcohol 70%">Alcohol 70% (btl)</option>\
                                <option data-name="Naocl 2.5" value="Naocl 2.5">Naocl 2.5 (btl)</option>\
                                <option data-name="Nacl/Saline" value="Nacl/Saline">Nacl/Saline (btl)</option>\
                                <option data-name="Betadin/Iodin" value="Betadin/Iodin">Betadin/Iodin  (btl)</option>\
                                <option data-name="Aquades" value="Aquades">Aquades  (btl)</option>\
                                <option data-name="Minyak Highspeed" value="Minyak Highspeed">Minyak Highspeed  (btl)</option>\
                                <option data-name="Bahan Bleaching" value="Bahan Bleaching">Bahan Bleaching  (pack)</option>\
                                <option data-name="Microbrush" value="Microbrush">Microbrush  (pack)</option>\
                                <option data-name="Dental Floss Stik" value="Dental Floss Stik">Dental Floss Stik  (pack)</option>\
                                <option data-name="Dental Floss Benang" value="Dental Floss Benang">Dental Floss Benang (pcs)</option>\
                                <option data-name="GIC Fuji 1" value="GIC Fuji 1">GIC Fuji 1 (pack)</option>\
                                <option data-name="GIC Fuji 2" value="GIC Fuji 2">GIC Fuji 2 (pack)</option>\
                                <option data-name="GIC Fuji 9 Kecil" value="GIC Fuji 9 Kecil">GIC Fuji 9 Kecil (pack)</option>\
                                <option data-name="GIC Fuji 9 Besar" value="GIC Fuji 9 Besar">GIC Fuji 9 Besar (pack)</option>\
                                <option data-name="Calzinol (merk. Zitemp)" value="Calzinol (merk. Zitemp)">Calzinol (merk. Zitemp) (pack)</option>\
                                <option data-name="Eugenol" value="Eugenol">Eugenol  (pcs)</option>\
                                <option data-name="Hexabond (Bonding)" value="Hexabond (Bonding)">Hexabond (Bonding) (pcs)</option>\
                                <option data-name="Tricresol" value="Tricresol">Tricresol  (pcs)</option>\
                                <option data-name="TSR/OCO" value="TSR/OCO">TSR/OCO (pcs)</option>\
                                <option data-name="TS (merk. Cevitron)" value="TS (merk. Cevitron)">TS (merk. Cevitron) (pcs)</option>\
                                <option data-name="Hexa Etch" value="Hexa Etch">Hexa Etch  (pcs)</option>\
                                <option data-name="Wax Ortho" value="Wax Ortho">Wax Ortho (pack)</option>\
                                <option data-name="Komposit Flow A1" value="Komposit Flow A1">Komposit Flow A1 (pcs)</option>\
                                <option data-name="Dental Needles" value="Dental Needles">Dental Needles  (pcs)</option>\
                                <option data-name="Modelling Wax/malam" value="Modelling Wax/malam">Modelling Wax/malam (pack)</option>\
                                <option data-name="Biosponge" value="Biosponge">Biosponge  (pcs)</option>\
                                <option data-name="Wedges Hijau" value="Wedges Hijau">Wedges Hijau (pack)</option>\
                                <option data-name="Calplus" value="Calplus">Calplus  (pcs)</option>\
                                <option data-name="Calciplus LC" value="Calciplus LC">Calciplus LC (pcs)</option>\
                                <option data-name="Kapas" value="Kapas">Kapas  (ball)</option>\
                                <option data-name="Kasa" value="Kasa">Kasa  (gulung)</option>\
                                <option data-name="Catton Roll" value="Catton Roll">Catton Roll (pack)</option>\
                                <option data-name="Polibibs" value="Polibibs">Polibibs  (lembar)</option>\
                                <option data-name="Masker Medis" value="Masker Medis">Masker Medis  (pack)</option>\
                                <option data-name="Masker KN95" value="Masker KN95">Masker KN95 (pack)</option>\
                                <option data-name="Saniter" value="Saniter">Saniter (pcs)</option>\
                                <option data-name="Handscon Uk. S" value="Handscon Uk. S">Handscon Uk. S (pack)</option>\
                                <option data-name="Suction" value="Suction">Suction  (pack)</option>\
                                <option data-name="Ligature Wire Splinting" value="Ligature Wire Splinting">Ligature Wire Splinting (pcs)</option>\
                                <option data-name="Alginate (Hygent)" value="Alginate (Hygent)">Alginate (Hygent) (pcs)</option>\
                                <option data-name="Gips Biru" value="Gips Biru">Gips Biru  (pcs)</option>\
                                <option data-name="Gips Kuning" value="Gips Kuning">Gips Kuning  (pcs)</option>\
                                <option data-name="Bracket Ceramic" value="Bracket Ceramic">Bracket Ceramic  (pack)</option>\
                                <option data-name="Roth Bracket" value="Roth Bracket">Roth Bracket  (pack)</option>\
                                <option data-name="Elastic Ortho 1/4 5.0 Oz" value="Elastic Ortho 1/4 5.0 Oz">Elastic Ortho 1/4 5.0 Oz (pack)</option>\
                                <option data-name="Elastic Ortho 1/8 3.5 Oz" value="Elastic Ortho 1/8 3.5 Oz">Elastic Ortho 1/8 3.5 Oz (pack)</option>\
                                <option data-name="Elastic Ortho 5/16 3.5 Oz" value="Elastic Ortho 5/16 3.5 Oz">Elastic Ortho 5/16 3.5 Oz (pack)</option>\
                                <option data-name="Elastic Ortho 3/16 3.5 Oz" value="Elastic Ortho 3/16 3.5 Oz">Elastic Ortho 3/16 3.5 Oz (pack)</option>\
                                <option data-name="Elastic Ortho 5/16 5.0 Oz" value="Elastic Ortho 5/16 5.0 Oz">Elastic Ortho 5/16 5.0 Oz (pack)</option>\
                                <option data-name="Buccal Tube" value="Buccal Tube">Buccal Tube  (pack)</option>\
                                <option data-name="Paper Point 15-40" value="Paper Point 15-40">Paper Point 15-40 (pack)</option>\
                                <option data-name="Paper Point 45-80" value="Paper Point 45-80">Paper Point 45-80 (pack)</option>\
                                <option data-name="Guta Percha Points F1/f3" value="Guta Percha Points F1/f3">Guta Percha Points F1/f3 (pack)</option>\
                                <option data-name="Guta Percha Points 15-40" value="Guta Percha Points 15-40">Guta Percha Points 15-40 (pack)</option>\
                                <option data-name="Guta Percha Points 45-80" value="Guta Percha Points 45-80">Guta Percha Points 45-80 (pack)</option>\
                                <option data-name="Paper Pad" value="Paper Pad">Paper Pad  (bendel)</option>\
                                <option data-name="Pasak Fiber" value="Pasak Fiber">Pasak Fiber  (pack)</option>\
                                <option data-name="Interdental Brush" value="Interdental Brush">Interdental Brush  (pcs)</option>\
                                <option data-name="Articulating Paper" value="Articulating Paper">Articulating Paper  (sheet)</option>\
                                <option data-name="Sikat Dr.Smith" value="Sikat Dr.Smith">Sikat Dr.Smith  (pcs)</option>\
                                <option data-name="Wedges ortho abu" value="Wedges ortho abu">Wedges ortho abu (pcs)</option>\
                                <option data-name="Topical anastesi lolite" value="Topical anastesi lolite">Topical anastesi lolite  (pcs)</option>\
                                <option data-name="Lidocaine" value="Lidocaine">Lidocaine  (ampul)</option>\
                                <option data-name="Pehacain" value="Pehacain">Pehacain  (pack)</option>\
                                <option data-name="Carpul 3%" value="Carpul 3%">Carpul 3% (strip)</option>\
                                <option data-name="GIC Mini" value="GIC Mini">GIC Mini  (pack)</option>\
                                <option data-name="CHKM" value="CHKM">CHKM (pcs)</option>\
                                <option data-name="Komposit A2" value="Komposit A2">Komposit A2 (pcs)</option>\
                                <option data-name="Komposit A3" value="Komposit A3">Komposit A3 (pcs)</option>\
                                <option data-name="Komposit A3.5" value="Komposit A3.5">Komposit A3.5 (pcs)</option>\
                                <option data-name="ZnPO4/Ellite Cement" value="ZnPO4/Ellite Cement">ZnPO4/Ellite Cement  (pack)</option>\
                                <option data-name="Open coil spring" value="Open coil spring">Open coil spring  (pack)</option>\
                                <option data-name="Close coil spring" value="Close coil spring">Close coil spring (pack)</option>\
                                <option data-name="Matrix band" value="Matrix band">Matrix band  (pack)</option>\
                                <option data-name="H2O2 50%" value="H2O2 50%">H2O2 50% (btl)</option>\
                                <option data-name="Power O oren" value="Power O oren">Power O oren  (pcs)</option>\
                                <option data-name="Power O hitam " value="Power O hitam ">Power O hitam   (pcs)</option>\
                                <option data-name="Power O ungu tua" value="Power O ungu tua">Power O ungu tua (pcs)</option>\
                                <option data-name="Power O hijau" value="Power O hijau">Power O hijau  (pcs)</option>\
                                <option data-name="Power O hijau gliter/ metalic" value="Power O hijau gliter/ metalic">Power O hijau gliter/ metalic (pcs)</option>\
                                <option data-name="Power O biru gliter /metalic" value="Power O biru gliter /metalic">Power O biru gliter /metalic (pcs)</option>\
                                <option data-name="Power O biru" value="Power O biru">Power O biru  (pcs)</option>\
                                <option data-name="Power O biru laut" value="Power O biru laut">Power O biru laut  (pcs)</option>\
                                <option data-name="Power O kuning" value="Power O kuning">Power O kuning  (pcs)</option>\
                                <option data-name="Power O coklat" value="Power O coklat">Power O coklat  (pcs)</option>\
                                <option data-name="Power O biru tosca" value="Power O biru tosca">Power O biru tosca  (pcs)</option>\
                                <option data-name="Power O pink" value="Power O pink">Power O pink  (pcs)</option>\
                                <option data-name="Power O merah" value="Power O merah">Power O merah  (pcs)</option>\
                                <option data-name="Power O maron" value="Power O maron">Power O maron (pcs)</option>\
                                <option data-name="Power O putih" value="Power O putih">Power O putih (pcs)</option>\
                                <option data-name="Power O navy besar" value="Power O navy besar">Power O navy besar (pcs)</option>\
                                <option data-name="Power O ungu muda/lilac" value="Power O ungu muda/lilac">Power O ungu muda/lilac (pcs)</option>\
                                <option data-name="Power O putih bening/transparan" value="Power O putih bening/transparan">Power O putih bening/transparan  (pcs)</option>\
                                <option data-name="Power O pink tua" value="Power O pink tua">Power O pink tua (pcs)</option>\
                                <option data-name="Power O hijau tosca" value="Power O hijau tosca">Power O hijau tosca (pcs)</option>\
                                <option data-name="Power O hijau muda" value="Power O hijau muda">Power O hijau muda (pcs)</option>\
                                <option data-name="Power O ungu" value="Power O ungu">Power O ungu (pcs)</option>\
                                <option data-name="Power O merah jambu" value="Power O merah jambu">Power O merah jambu (pcs)</option>\
                                <option data-name="Power O biru tua" value="Power O biru tua">Power O biru tua (pcs)</option>\
                                <option data-name="Power O biru SMA" value="Power O biru SMA">Power O biru SMA (pcs)</option>\
                                <option data-name="Power O pink muda" value="Power O pink muda">Power O pink muda (pcs)</option>\
                                <option data-name="Power O abu" value="Power O abu">Power O abu (pcs)</option>\
                                <option data-name="Power chain hijau tosca" value="Power chain hijau tosca">Power chain hijau tosca  (cm)</option>\
                                <option data-name="Power chain hijau muda" value="Power chain hijau muda">Power chain hijau muda (cm)</option>\
                                <option data-name="Power chain ungu" value="Power chain ungu">Power chain ungu  (cm)</option>\
                                <option data-name="Power chain pink muda" value="Power chain pink muda">Power chain pink muda (cm)</option>\
                                <option data-name="Power chain biru" value="Power chain biru">Power chain biru  (cm)</option>\
                                <option data-name="Power chain biru muda" value="Power chain biru muda">Power chain biru muda  (cm)</option>\
                                <option data-name="Power chain bening" value="Power chain bening">Power chain bening  (cm)</option>\
                                <option data-name="Power chain coklat" value="Power chain coklat">Power chain coklat  (cm)</option>\
                                <option data-name="Power chain putih" value="Power chain putih">Power chain putih  (cm)</option>\
                                <option data-name="Power chain  biru dongker" value="Power chain  biru dongker">Power chain  biru dongker  (cm)</option>\
                                <option data-name="Power chain abu" value="Power chain abu">Power chain abu  (cm)</option>\
                                <option data-name="Power chain hitam" value="Power chain hitam">Power chain hitam  (cm)</option>\
                                <option data-name="Power chain biru tosca" value="Power chain biru tosca">Power chain biru tosca (cm)</option>\
                                <option data-name="Power chain maron" value="Power chain maron">Power chain maron  (cm)</option>\
                                <option data-name="Power chain merah" value="Power chain merah">Power chain merah  (cm)</option>\
                                <option data-name="Power chain hijau" value="Power chain hijau">Power chain hijau (cm)</option>\
                                <option data-name="Power chain biru SMA" value="Power chain biru SMA">Power chain biru SMA (cm)</option>\
                                <option data-name="Power chain ungu muda" value="Power chain ungu muda">Power chain ungu muda (cm)</option>\
                                <option data-name="Power chain pink magenta" value="Power chain pink magenta">Power chain pink magenta (cm)</option>\
                                <option data-name="Wire NiTi Ovoid 0.12 Lower" value="Wire NiTi Ovoid 0.12 Lower">Wire NiTi Ovoid 0.12 Lower (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.14 Lower" value="Wire NiTi Ovoid 0.14 Lower">Wire NiTi Ovoid 0.14 Lower (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.16 Lower" value="Wire NiTi Ovoid 0.16 Lower">Wire NiTi Ovoid 0.16 Lower (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.18 Lower" value="Wire NiTi Ovoid 0.18 Lower">Wire NiTi Ovoid 0.18 Lower (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.12 Upper" value="Wire NiTi Ovoid 0.12 Upper">Wire NiTi Ovoid 0.12 Upper (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.14 Upper" value="Wire NiTi Ovoid 0.14 Upper">Wire NiTi Ovoid 0.14 Upper (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.16 Upper" value="Wire NiTi Ovoid 0.16 Upper">Wire NiTi Ovoid 0.16 Upper (pack)</option>\
                                <option data-name="Wire NiTi Ovoid 0.18 Upper" value="Wire NiTi Ovoid 0.18 Upper">Wire NiTi Ovoid 0.18 Upper (pack)</option>\
                                <option data-name="Wire SS Rekta 0.016x022 Lower" value="Wire SS Rekta 0.016x022 Lower">Wire SS Rekta 0.016x022 Lower (pack)</option>\
                                <option data-name="Wire SS Rekta 0.016x022 Upper" value="Wire SS Rekta 0.016x022 Upper">Wire SS Rekta 0.016x022 Upper (pack)</option>\
                                <option data-name="Wire SS Rekta 16x16 Lower" value="Wire SS Rekta 16x16 Lower">Wire SS Rekta 16x16 Lower (pack)</option>\
                                <option data-name="Wire SS Rekta 16x16 Upper" value="Wire SS Rekta 16x16 Upper">Wire SS Rekta 16x16 Upper (pack)</option>\
                                <option data-name="Wire SS Rekta 0.016x0.016 Lower" value="Wire SS Rekta 0.016x0.016 Lower">Wire SS Rekta 0.016x0.016 Lower (pack)</option>\
                                <option data-name="Wire SS Rekta 0.016x0.016 Upper" value="Wire SS Rekta 0.016x0.016 Upper">Wire SS Rekta 0.016x0.016 Upper (pack)</option>\
                                <option data-name="Wire SS Rekta 0.16x022 Lower" value="Wire SS Rekta 0.16x022 Lower">Wire SS Rekta 0.16x022 Lower (pack)</option>\
                                <option data-name="Wire SS Rekta 0.16x022 Upper" value="Wire SS Rekta 0.16x022 Upper">Wire SS Rekta 0.16x022 Upper (pack)</option>\
                                <option data-name="Niti reserve 0.012 Upper" value="Niti reserve 0.012 Upper">Niti reserve 0.012 Upper (pack)</option>\
                                <option data-name="Niti reserve 0.012 Lower" value="Niti reserve 0.012 Lower">Niti reserve 0.012 Lower (pack)</option>\
                                <option data-name="Niti reserve 0.014 Upper" value="Niti reserve 0.014 Upper">Niti reserve 0.014 Upper (pack)</option>\
                                <option data-name="Niti reserver 0.014 Lower" value="Niti reserver 0.014 Lower">Niti reserver 0.014 Lower (pack)</option>\
                                <option data-name="Niti reverse 0.016 Upper" value="Niti reverse 0.016 Upper">Niti reverse 0.016 Upper (pack)</option>\
                                <option data-name="Niti reverse 0.016 lower" value="Niti reverse 0.016 lower">Niti reverse 0.016 lower (pack)</option>\
                                <option data-name="Niti reserve 0.016x0.016 lower" value="Niti reserve 0.016x0.016 lower">Niti reserve 0.016x0.016 lower (pack)</option>\
                                <option data-name="Niti reserve 0.016x0.016 upper" value="Niti reserve 0.016x0.016 upper">Niti reserve 0.016x0.016 upper (pack)</option>\
                                <option data-name="Safty Box" value="Safty Box">Safty Box (pcs)</option>\
                                <option data-name="Chlor Ethyl" value="Chlor Ethyl">Chlor Ethyl  (pcs)</option>\
                                <option data-name="Polishing Strip/Enamel Striping" value="Polishing Strip/Enamel Striping">Polishing Strip/Enamel Striping (pcs)</option>\
                                <option data-name="Clinpro (TAF)" value="Clinpro (TAF)">Clinpro (TAF) (pack)</option>\
                                <option data-name="CE Klorofil" value="CE Klorofil">CE Klorofil (pcs)</option>\
                                <option data-name="Surgical Blades Uk.10" value="Surgical Blades Uk.10">Surgical Blades Uk.10 (pcs)</option>\
                                <option data-name="Surgical Blades Uk.15" value="Surgical Blades Uk.15">Surgical Blades Uk.15 (pcs)</option>\
                                <option data-name="Seluloid Strip" value="Seluloid Strip">Seluloid Strip  (pack)</option>\
                                <option data-name="Paper Point F1-F1" value="Paper Point F1-F1">Paper Point F1-F1 (Pack)</option>\
                                <option data-name="Kresek Obat" value="Kresek Obat">Kresek Obat  (bendel)</option>\
                                <option data-name="Kapur Barus KM" value="Kapur Barus KM">Kapur Barus KM (pcs)</option>\
                                <option data-name="Prostek/vVxal" value="Prostek/vVxal">Prostek/vVxal (btl)</option>\
                                <option data-name="Pemutih Baju" value="Pemutih Baju">Pemutih Baju (btl)</option>\
                                <option data-name="Sunlight" value="Sunlight">Sunlight (pcs)</option>\
                                <option data-name="Tisu" value="Tisu">Tisu (pcs)</option>\
                                <option data-name="Kresek Merah Besar" value="Kresek Merah Besar">Kresek Merah Besar  (pack)</option>\
                                <option data-name="Kresek Kuning Medis" value="Kresek Kuning Medis">Kresek Kuning Medis  (pack)</option>\
                                <option data-name="Kresek Hitam Besar" value="Kresek Hitam Besar">Kresek Hitam Besar  (pack)</option>\
                                <option data-name="Kresek Hitam Sedang" value="Kresek Hitam Sedang">Kresek Hitam Sedang  (pack)</option>\
                                <option data-name="Kresek Putih Sedang" value="Kresek Putih Sedang">Kresek Putih Sedang  (pack)</option>\
                                <option data-name="Kresek Hitam Kecil" value="Kresek Hitam Kecil">Kresek Hitam Kecil (pack)</option>\
                                <option data-name="Kresek Putih Kecil" value="Kresek Putih Kecil">Kresek Putih Kecil  (pack)</option>\
                                <option data-name="Pembersih Lantai" value="Pembersih Lantai">Pembersih Lantai (pcs)</option>\
                                <option data-name="Detergen Cair" value="Detergen Cair">Detergen Cair  (pcs)</option>\
                                <option data-name="Handwash" value="Handwash">Handwash  (pcs)</option>\
                                <option data-name="Spirtus" value="Spirtus">Spirtus (btl)</option>\
                                <option data-name="Plastik Cetik 10x15" value="Plastik Cetik 10x15">Plastik Cetik 10x15 (pack)</option>\
                                <option data-name="Plastic Cetik 7x10" value="Plastic Cetik 7x10">Plastic Cetik 7x10 (pack)</option>\
                                <option data-name="Air" value="Air">Air (dus)</option>\
                                <option data-name="Cling" value="Cling">Cling (pcs)</option>\
                                <option data-name="Stela Gantung" value="Stela Gantung">Stela Gantung (pcs)</option>\
                                <option data-name="Bulpoint" value="Bulpoint">Bulpoint (pcs)</option>\
                                <option data-name="Surat Rujukan Lab Gigi AY" value="Surat Rujukan Lab Gigi AY">Surat Rujukan Lab Gigi AY (bendel)</option>\
                                <option data-name="Surat Keterangan Dokter" value="Surat Keterangan Dokter">Surat Keterangan Dokter (lembar)</option>\
                                <option data-name="Surat Rujukan Dokter" value="Surat Rujukan Dokter">Surat Rujukan Dokter (lembar)</option>\
                                <option data-name="Surat Resep" value="Surat Resep">Surat Resep (lembar)</option>\
                                <option data-name="Lembar laporan keuangan harian" value="Lembar laporan keuangan harian">Lembar laporan keuangan harian (lembar)</option>\
                                <option data-name="Informed consent penolakan" value="Informed consent penolakan">Informed consent penolakan (lembar)</option>\
                                <option data-name="Informed consent persetujuan" value="Informed consent persetujuan">Informed consent persetujuan (pcs)</option>\
                                <option data-name="Lembar laporan harian" value="Lembar laporan harian">Lembar laporan harian  (pcs)</option>\
                                <option data-name="Wadah kotak plastik untuk shoes cover kotor" value="Wadah kotak plastik untuk shoes cover kotor">Wadah kotak plastik untuk shoes cover kotor (pcs)</option>\
                                <option data-name="Shoes Cover" value="Shoes Cover">Shoes Cover (pcs)</option>\
                                <option data-name="Gunting Kertas" value="Gunting Kertas">Gunting Kertas (pcs)</option>\
                                <option data-name="Gayung Kamar Mandi" value="Gayung Kamar Mandi">Gayung Kamar Mandi (pcs)</option>\
                                <option data-name="Sikat Kamar Mandi/WC" value="Sikat Kamar Mandi/WC">Sikat Kamar Mandi/WC (pcs)</option>\
                                <option data-name="Toples" value="Toples">Toples (pcs)</option>\
                                <option data-name="Hanger" value="Hanger">Hanger (pcs)</option>\
                                <option data-name="Staples" value="Staples">Staples (pcs)</option>\
                                <option data-name="Spons" value="Spons">Spons (pcs)</option>\
                                <option data-name="Kwitansi" value="Kwitansi">Kwitansi (bendel)</option>\
                                <option data-name="Pasta Gigi" value="Pasta Gigi">Pasta Gigi (pcs)</option>\
                                <option data-name="Amplas" value="Amplas">Amplas (pcs)</option>\
                                <option data-name="Buku Tulis" value="Buku Tulis">Buku Tulis (pcs)</option>\
                                <option data-name="Lem Paralon/Pipa" value="Lem Paralon/Pipa">Lem Paralon/Pipa (pcs)</option>\
                                <option data-name="Batrai" value="Batrai">Batrai  (pcs)</option>\
                                <option data-name="Hadiah Anak Cowok" value="Hadiah Anak Cowok">Hadiah Anak Cowok (pcs)</option>\
                                <option data-name="Hadiah Anak Cewek" value="Hadiah Anak Cewek">Hadiah Anak Cewek (pcs)</option>\
                                <option data-name="Pigora Putih" value="Pigora Putih">Pigora Putih (pcs)</option>\
                                <option data-name="Isi Staples" value="Isi Staples">Isi Staples (pack)</option>\
                                <option data-name="Stipo cair/ kertas" value="Stipo cair/ kertas">Stipo cair/ kertas (pcs)</option>\
                                <option data-name="Solasi Kecil" value="Solasi Kecil">Solasi Kecil (pcs)</option>\
                                <option data-name="Sandal Operasi" value="Sandal Operasi">Sandal Operasi  (pasang)</option>\
                                <option data-name="APD" value="APD">APD (pcs)</option>\
                                <option data-name="Braket Holder" value="Braket Holder">Braket Holder (pcs)</option>\
                                <option data-name="Spatula GIC" value="Spatula GIC">Spatula GIC (pcs)</option>\
                                <option data-name="Plastis Filling" value="Plastis Filling">Plastis Filling (pcs)</option>\
                                <option data-name="Semen Stropper" value="Semen Stropper">Semen Stropper (pcs)</option>\
                                <option data-name="Needle Holder/Arteri Klem" value="Needle Holder/Arteri Klem">Needle Holder/Arteri Klem (pcs)</option>\
                                <option data-name="Sonde" value="Sonde">Sonde (pcs)</option>\
                                <option data-name="Escavator" value="Escavator">Escavator (pcs)</option>\
                                <option data-name="Kaca Mulut Tanpa Tangkai" value="Kaca Mulut Tanpa Tangkai">Kaca Mulut Tanpa Tangkai (pcs)</option>\
                                <option data-name="Nierbeken" value="Nierbeken">Nierbeken  (pcs)</option>\
                                <option data-name="Pinset" value="Pinset">Pinset  (pcs)</option>\
                                <option data-name="Tang Cabut Anak" value="Tang Cabut Anak">Tang Cabut Anak  (pcs)</option>\
                                <option data-name="Tang Remover Braket" value="Tang Remover Braket">Tang Remover Braket  (pcs)</option>\
                                <option data-name="Tang Adam Universal" value="Tang Adam Universal">Tang Adam Universal (pcs)</option>\
                                <option data-name="Scaler Ultrasonic" value="Scaler Ultrasonic">Scaler Ultrasonic (set)</option>\
                                <option data-name="Tip Scaler" value="Tip Scaler">Tip Scaler  (pcs)</option>\
                                <option data-name="Arkansas Lowspeed Stone (flam)" value="Arkansas Lowspeed Stone (flam)">Arkansas Lowspeed Stone (flam) (pcs)</option>\
                                <option data-name="Arkansas Lowspeed Stone (cones)" value="Arkansas Lowspeed Stone (cones)">Arkansas Lowspeed Stone (cones) (pcs)</option>\
                                <option data-name="Arkansas Lowspeed Stone (round)" value="Arkansas Lowspeed Stone (round)">Arkansas Lowspeed Stone (round) (pcs)</option>\
                                <option data-name="Finishing (discs)" value="Finishing (discs)">Finishing (discs) (pcs)</option>\
                                <option data-name="Finishing (cups)" value="Finishing (cups)">Finishing (cups) (pcs)</option>\
                                <option data-name="Finishing (point)" value="Finishing (point)">Finishing (point) (pcs)</option>\
                                <option data-name="Round Bur" value="Round Bur">Round Bur  (pcs)</option>\
                                <option data-name="Bur Brush" value="Bur Brush">Bur Brush (pcs)</option>\
                                <option data-name="Spatula Alginate" value="Spatula Alginate">Spatula Alginate  (pcs)</option>\
                                <option data-name="Spatula Semen" value="Spatula Semen">Spatula Semen  (pcs)</option>\
                                <option data-name="Bein" value="Bein">Bein  (pcs)</option>\
                                <option data-name="Cryer" value="Cryer">Cryer  (pcs)</option>\
                                <option data-name="Handle Scapel" value="Handle Scapel">Handle Scapel  (pcs)</option>\
                                <option data-name="Tang Potong Distal" value="Tang Potong Distal">Tang Potong Distal (pcs)</option>\
                                <option data-name="Light Curing LED" value="Light Curing LED">Light Curing LED (pcs)</option>\
                                <option data-name="Bowl (Mangkok)" value="Bowl (Mangkok)">Bowl (Mangkok) (pcs)</option>\
                                <option data-name="Tensi" value="Tensi">Tensi  (pcs)</option>\
                                <option data-name="Polishing Kit Komposit" value="Polishing Kit Komposit">Polishing Kit Komposit (pcs)</option>\
                                <option data-name="Dappen Glass" value="Dappen Glass">Dappen Glass  (pcs)</option>\
                                <option data-name="Jarum Irigasi" value="Jarum Irigasi">Jarum Irigasi  (pcs)</option>\
                                <option data-name="Jarum Ekstirpasi" value="Jarum Ekstirpasi">Jarum Ekstirpasi  (pcs)</option>\
                                <option data-name="Jarum Lentulo" value="Jarum Lentulo">Jarum Lentulo  (pcs)</option>\
                                <option data-name="Face Shield" value="Face Shield">Face Shield (pcs)</option>\
                                <option data-name="Cermin Oklusal" value="Cermin Oklusal">Cermin Oklusal (pcs)</option>\
                                <option data-name="Lampu UV" value="Lampu UV">Lampu UV  (pcs)</option>\
                                <option data-name="Protaper" value="Protaper">Protaper (pcs)</option>\
                                <option data-name="Gauge" value="Gauge">Gauge (pcs)</option>\
                                <option data-name="Kuret" value="Kuret">Kuret (pcs)</option>\
                                <option data-name="Bur Eve" value="Bur Eve">Bur Eve (pcs)</option>\
                                <option data-name="Linggual Button" value="Linggual Button">Linggual Button (biji)</option>\
                                <option data-name="Soflex Disc" value="Soflex Disc">Soflex Disc (pcs)</option>\
                                <option data-name="Enhance Bur Flaim" value="Enhance Bur Flaim">Enhance Bur Flaim (pcs)</option>\
                                <option data-name="Finishing Bur" value="Finishing Bur">Finishing Bur (pcs)</option>\
                                <option data-name="Cermin" value="Cermin">Cermin  (pcs)</option>\
                                <option data-name="Kontainer Box Stainless" value="Kontainer Box Stainless">Kontainer Box Stainless (pcs)</option>\
                                <option data-name="Kotak Stainless Untuk Tampon" value="Kotak Stainless Untuk Tampon">Kotak Stainless Untuk Tampon (pcs)</option>\
                                <option data-name="Brush Lowspeed" value="Brush Lowspeed">Brush Lowspeed  (pcs)</option>\
                                <option data-name="Dental Penahan Lidah" value="Dental Penahan Lidah">Dental Penahan Lidah (pcs)</option>\
                                <option data-name="Lowspeed Contra Angel" value="Lowspeed Contra Angel">Lowspeed Contra Angel (pcs)</option>\
                                <option data-name="Lowspeed Staight" value="Lowspeed Staight">Lowspeed Staight (pcs)</option>\
                                <option data-name="Matabur Highspeed Endo Acses" value="Matabur Highspeed Endo Acses">Matabur Highspeed Endo Acses (pcs)</option>\
                                <option data-name="Matabur Round" value="Matabur Round">Matabur Round (pcs)</option>\
                                <option data-name="Matabur Fissure" value="Matabur Fissure">Matabur Fissure (pcs)</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.1" value="Sendok Cetak Penuh Rahang Atas Bawah No.1">Sendok Cetak Penuh Rahang Atas Bawah No.1 (pasang)</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.2" value="Sendok Cetak Penuh Rahang Atas Bawah No.2">Sendok Cetak Penuh Rahang Atas Bawah No.2 (pasang)</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.3" value="Sendok Cetak Penuh Rahang Atas Bawah No.3">Sendok Cetak Penuh Rahang Atas Bawah No.3 (pasang)</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.4" value="Sendok Cetak Penuh Rahang Atas Bawah No.4">Sendok Cetak Penuh Rahang Atas Bawah No.4 (pasang)</option>\
                                <option data-name="Sendok Cetak Penuh Biru Uk. S" value="Sendok Cetak Penuh Biru Uk. S">Sendok Cetak Penuh Biru Uk. S (pasang)</option>\
                                <option data-name="Sendok Cetak Penuh Biru Uk. M" value="Sendok Cetak Penuh Biru Uk. M">Sendok Cetak Penuh Biru Uk. M (pasang)</option>\
                                <option data-name="Sendok Cetak Penuh Biru Uk. L" value="Sendok Cetak Penuh Biru Uk. L">Sendok Cetak Penuh Biru Uk. L (pasang)</option>\
                                <option data-name="Sendok Cetak Sebagian Biru" value="Sendok Cetak Sebagian Biru">Sendok Cetak Sebagian Biru (pasang)</option>\
                                <option data-name="Shadeguide Vita" value="Shadeguide Vita">Shadeguide Vita (pcs)</option>\
                                <option data-name="Benang Jarum Slik 3/0 atau 4/0" value="Benang Jarum Slik 3/0 atau 4/0">Benang Jarum Slik 3/0 atau 4/0 (pack)</option>\
                                <option data-name="Needle Citoject" value="Needle Citoject">Needle Citoject (pcs)</option>\
                                <option data-name="Tang Cabut Dewasa" value="Tang Cabut Dewasa">Tang Cabut Dewasa (paket)</option>\
                                <option data-name="Gunting Benang Bengkok" value="Gunting Benang Bengkok">Gunting Benang Bengkok (pcs)</option>\
                                <option data-name="Endok Block" value="Endok Block">Endok Block (pcs)</option>\
                                <option data-name="Box K-File" value="Box K-File">Box K-File (pcs)</option>\
                                <option data-name="Plugger" value="Plugger">Plugger (pack)</option>\
                                <option data-name="Spreader" value="Spreader">Spreader (pack)</option>\
                                <option data-name="Alat Bleaching LED" value="Alat Bleaching LED">Alat Bleaching LED (pcs)</option>\
                                <option data-name="Probe" value="Probe">Probe (pcs)</option>\
                                <option data-name="Scaler Hoe Kanan" value="Scaler Hoe Kanan">Scaler Hoe Kanan  (pcs)</option>\
                                <option data-name="Ligature Splinting Diameter 0.5" value="Ligature Splinting Diameter 0.5">Ligature Splinting Diameter 0.5 (pcs)</option>\
                                <option data-name="Fiber Splinting" value="Fiber Splinting">Fiber Splinting (pcs)</option>\
                                <option data-name="Kaca Mata Medis Merah" value="Kaca Mata Medis Merah">Kaca Mata Medis Merah  (pcs)</option>\
                                <option data-name="Cheek Retractor" value="Cheek Retractor">Cheek Retractor  (pcs)</option>\
                                <option data-name="Syringe" value="Syringe">Syringe  (pcs)</option>\
                                <option data-name="K-File No 08" value="K-File No 08">K-File No 08 (pack)</option>\
                                <option data-name="K-File No 10" value="K-File No 10">K-File No 10 (pack)</option>\
                                <option data-name="K-File No 15-40" value="K-File No 15-40">K-File No 15-40 (pack)</option>\
                                <option data-name="K-File No 15" value="K-File No 15">K-File No 15 (pack)</option>\
                                <option data-name="K-File No 45-80" value="K-File No 45-80">K-File No 45-80 (pack)</option>\
                                <option data-name="Konektor Polibib" value="Konektor Polibib">Konektor Polibib  (pcs)</option>\
                                <option data-name="Threeway Syringe" value="Threeway Syringe">Threeway Syringe (pcs)</option>\
                                <option data-name="Highspeed 2 Hole" value="Highspeed 2 Hole">Highspeed 2 Hole (pcs)</option>\
                                <option data-name="Highspeed 4 Hole" value="Highspeed 4 Hole">Highspeed 4 Hole (pcs)</option>\
                                <option data-name="Catridge" value="Catridge">Catridge (pcs)</option>\
                                <option data-name="Scaler Hoe Kiri" value="Scaler Hoe Kiri">Scaler Hoe Kiri  (pcs)</option>\
                                <option data-name="Ligature Splinting Diameter 0.3" value="Ligature Splinting Diameter 0.3">Ligature Splinting Diameter 0.3 (pcs)</option>\
                            </select>\
                        </td>\
                        <td>\
                            <input type="hidden" max="200" name="description[]" required value="" class="form-control description" />\
                        </td>\
                        <td>\
                            <input type="number" min="1" name="qty[]" value="" required class="form-control only-numeric" />\
                        </td>\
                        <td><button type="button" class="btn btn-danger btnremovedetail">-</button></td>\
                    </tr>';
    $(document).ready(function () {
        $(document).on('click','.btnremovedetail',function(e){
            $(this).parent().parent().remove();
        });

        $(document).on('keypress','.only-numeric',function(e){
            var keyCode = e.which ? e.which : e.keyCode
            if (!(keyCode >= 48 && keyCode <= 57)) {
                return false;
            }
        });

        $("#btndetail").click(function(e){
            $("#table tbody").append(template);
        });

        $(document).on('change','.medicine_select',function(e){
		    var data = $("option:selected",this).data();
            $(this).parent().parent().find('.description').val(data.name);
        });
    });
</script> 