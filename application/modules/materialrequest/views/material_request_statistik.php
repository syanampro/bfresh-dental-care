<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <header class="panel-heading"> 
            Material Request Report
            <div class="col-md-1 pull-right">
                <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button>
            </div>
        </header>
        <div class="col-md-12">
            <div class="col-md-7 row">
                <section>
                    <form role="form" class="f_report" action="materialrequest/statistik" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                    <input type="text" class="form-control dpd1" name="date_from" value="<?php
                                    if (!empty($from)) {
                                        echo $from;
                                    }
                                    ?>" placeholder="<?php echo lang('date_from'); ?>" readonly="">
                                    <span class="input-group-addon"><?php echo lang('to'); ?></span>
                                    <input type="text" class="form-control dpd2" name="date_to" value="<?php
                                    if (!empty($to)) {
                                        echo $to;
                                    }
                                    ?>" placeholder="<?php echo lang('date_to'); ?>" readonly="">
                                </div>
                                <div class="row"></div>
                                <span class="help-block"></span> 
                            </div>
                            <div class="col-md-6 no-print">
                                <button type="submit" name="submit" class="btn btn-info range_submit"><?php echo lang('submit'); ?></button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <!-- Report -->
                    </header>
                    <table class="table table-striped table-advance table-hover" id="editable-sample">
                        <thead>
                            <tr>
                                <!-- <th>Bahan</th> -->
                                <th>Bahan</th>
                                <th class="hidden-phone">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php for($a=0;$a<count($isis);$a++){ ?>
                            <tr>
                                <!-- <td><?php echo $isis[$a][0] ?></td> -->
                                <td><?php echo $isis[$a][1] ?></td>
                                <td><?php echo $isis[$a][2] ?></td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
    </section>
</section>

<script src="common/js/codearistos.min.js"></script>
<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,
            iDisplayLength: 50,
            "order": [[0, "asc"]],
        });
    });
</script>