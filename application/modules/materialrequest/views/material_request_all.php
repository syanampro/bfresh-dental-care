<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="">

            <header class="panel-heading">
                <?php echo lang('material_request'); ?>
                <div class="col-md-4 no-print pull-right"> 
                    <a href="materialrequest/addNew">
                        <div class="btn-group pull-right">
                            <button id="" class="btn green btn-xs">
                                <i class="fa fa-plus-circle"></i> <?php echo lang('add_new'); ?>
                            </button>
                        </div>
                    </a>
                </div>
            </header>
            <div class="panel-body">

                <div class="adv-table editable-table ">

                    <div class="space15"></div>
                    <table class="table table-striped table-hover table-bordered" id="editable-sample">
                        <thead>
                            <tr>
                                <th>id</th>
                                <th><?php echo lang('date'); ?></th>
                                <th><?php echo lang('hospital'); ?></th>  
                                <th><?php echo lang('receptionist'); ?></th>  
                                <th><?php echo lang('total'); ?></th>  
                                <th><?php echo lang('status'); ?></th>  
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($datas as $data) { ?>
                            <tr class="">
                                <td> <?php echo $data->id; ?></td>
                                <td><?php echo date('d-m-Y H:i:s', strtotime($data->date) + 25200); ?></td>
                                <td> <?php echo $data->hospital; ?></td>
                                <td> <?php echo $data->receptionist; ?></td>
                                <td> <?php echo $data->total ?></td>
                                <td>
                                    <?php if($data->status == 'terima'): ?>
                                        <?php echo $data->status; ?>
                                    <?php else: ?>
                                        <form style="padding:0px;" action="materialrequest/updatestatus" method="POST">
                                            <input type="hidden" value="<?php echo $data->id; ?>" name="id" />
                                            <select class="form-control updatestatus" name="status">
                                                <option <?php if($data->status == 'request'): ?> selected <?php endif; ?> value="request">request</option>
                                                <option <?php if($data->status == 'proses'): ?> selected <?php endif; ?> value="proses">proses</option>
                                                <option <?php if($data->status == 'kirim'): ?> selected <?php endif; ?> value="kirim">kirim</option>
                                                <option <?php if($data->status == 'terima'): ?> selected <?php endif; ?> value="terima">terima</option>
                                            </select>
                                        </form>
                                    <?php endif; ?>
                                </td>
                                <td>
                                    <a class="btn btn-info btn-xs btn_width" href="materialrequest/view?id=<?php echo $data->id; ?>"><i class="fa fa-eye"> <?php echo lang('view'); ?> <?php echo lang('data'); ?> </i></a>   
                                    <?php if($data->status == 'request'): ?>  
                                    <a class="btn btn-info btn-xs btn_width" href="materialrequest/edit?id=<?php echo $data->id; ?>" data-id="<?php echo $data->id; ?>"><i class="fa fa-edit"></i> <?php echo lang('edit'); ?>  <?php echo lang('data'); ?></a>
                                    <a class="btn btn-info btn-xs btn_width delete_button" href="materialrequest/delete?id=<?php echo $data->id; ?>" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash-o"> <?php echo lang('delete'); ?></i></a>
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php } ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
<script src="common/js/codearistos.min.js"></script>

<script>
    $(document).ready(function () {
        var table = $('#editable-sample').DataTable({
            responsive: true,

            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                {
                    extend: 'print',
                    exportOptions: {
                        columns: [0, 1, 2],
                    }
                },
            ],

            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 10,
            "order": [[0, "desc"]],

            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            },

        });

        table.buttons().container()
                .appendTo('.custom_buttons');
    });

    $(document).on('change','.updatestatus',function(e){
        $(this).parent().submit();
    });
</script>


<script>
    $(document).ready(function () {
        $(".flashmessage").delay(3000).fadeOut(100);
    });
</script>