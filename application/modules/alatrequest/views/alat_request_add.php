<!--sidebar end-->
<!--main content start-->

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-12">
            <header class="panel-heading">
                <?php
                if (@$alat_request->id)
                    echo 'Edit Alat Request';
                else
                    echo 'Add Alat Request';
                ?>
            </header>
            <div class="panel col-md-12">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <?php echo validation_errors(); ?>
                        <form role="form" action="alatrequest/add" class="clearfix" method="post" enctype="multipart/form-data">
                            <input type="hidden" name="id" value="<?php echo @$alat_request->id; ?>" />
                            <table class="table" id="table">
                                <thead>
                                    <tr>
                                        <th style="width:20%;">Alat</th>
                                        <th>Deskripsi</th>
                                        <th>Alasan</th>
                                        <th style="width:10%;">Jumlah</th>
                                        <th><button type="button" id="btndetail" class="btn btn-info">+</button></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!@$alat_request->details) : ?>
                                    <tr>
                                        <td>
                                            <select class="form-control m-bot15 medicine_select" name="medicine_id[]">  
                                                <option value="-"><?php echo lang('select'); ?></option>
                                                <option data-name='Shoes Cover (optional)' value='Shoes Cover (optional)'>Shoes Cover (optional)</option>
                                                <option data-name='Gunting Kertas' value='Gunting Kertas'>Gunting Kertas</option>
                                                <option data-name='Gelas Disposible' value='Gelas Disposible'>Gelas Disposible</option>
                                                <option data-name='Gayung Kamar Mandi' value='Gayung Kamar Mandi'>Gayung Kamar Mandi</option>
                                                <option data-name='Sikat Kamar Mandi' value='Sikat Kamar Mandi'>Sikat Kamar Mandi</option>
                                                <option data-name='Toples' value='Toples'>Toples</option>
                                                <option data-name='Hanger' value='Hanger'>Hanger</option>
                                                <option data-name='Staples' value='Staples'>Staples</option>
                                                <option data-name='Spons' value='Spons'>Spons</option>
                                                <option data-name='Braket Holder' value='Braket Holder'>Braket Holder</option>
                                                <option data-name='Spatula GIC' value='Spatula GIC'>Spatula GIC</option>
                                                <option data-name='Plastis filling' value='Plastis filling'>Plastis filling</option>
                                                <option data-name='Semen Stropper' value='Semen Stropper'>Semen Stropper</option>
                                                <option data-name='Needle holder/Arteri Klem' value='Needle holder/Arteri Klem'>Needle holder/Arteri Klem</option>
                                                <option data-name='Sonde' value='Sonde'>Sonde</option>
                                                <option data-name='Escavator' value='Escavator'>Escavator</option>
                                                <option data-name='Kaca mulut tanpa tangkai' value='Kaca mulut tanpa tangkai'>Kaca mulut tanpa tangkai</option>
                                                <option data-name='Nierbeken ' value='Nierbeken '>Nierbeken </option>
                                                <option data-name='Pinset ' value='Pinset '>Pinset </option>
                                                <option data-name='Tang Cabut Anak ' value='Tang Cabut Anak '>Tang Cabut Anak </option>
                                                <option data-name='Tang Remover Braket ' value='Tang Remover Braket '>Tang Remover Braket </option>
                                                <option data-name='Tang Adam Universal' value='Tang Adam Universal'>Tang Adam Universal</option>
                                                <option data-name='Scaler Ultrasonic' value='Scaler Ultrasonic'>Scaler Ultrasonic</option>
                                                <option data-name='Tip Scaler ' value='Tip Scaler '>Tip Scaler </option>
                                                <option data-name='Arkansas lowspeed stone (flam)' value='Arkansas lowspeed stone (flam)'>Arkansas lowspeed stone (flam)</option>
                                                <option data-name='Arkansas lowspeed stone (cones)' value='Arkansas lowspeed stone (cones)'>Arkansas lowspeed stone (cones)</option>
                                                <option data-name='Arkansas lowspeed stone (round)' value='Arkansas lowspeed stone (round)'>Arkansas lowspeed stone (round)</option>
                                                <option data-name='Finishing (discs)' value='Finishing (discs)'>Finishing (discs)</option>
                                                <option data-name='Finishing (cups)' value='Finishing (cups)'>Finishing (cups)</option>
                                                <option data-name='Finishing (point)' value='Finishing (point)'>Finishing (point)</option>
                                                <option data-name='Round bur ' value='Round bur '>Round bur </option>
                                                <option data-name='Bur brush' value='Bur brush'>Bur brush</option>
                                                <option data-name='Spatula alginate ' value='Spatula alginate '>Spatula alginate </option>
                                                <option data-name='Spatula semen ' value='Spatula semen '>Spatula semen </option>
                                                <option data-name='Bein ' value='Bein '>Bein </option>
                                                <option data-name='Cryer ' value='Cryer '>Cryer </option>
                                                <option data-name='Handle Scapel ' value='Handle Scapel '>Handle Scapel </option>
                                                <option data-name='Tang Potong Distal' value='Tang Potong Distal'>Tang Potong Distal</option>
                                                <option data-name='Light Curing LED' value='Light Curing LED'>Light Curing LED</option>
                                                <option data-name='Bowl (mangkok)' value='Bowl (mangkok)'>Bowl (mangkok)</option>
                                                <option data-name='Tensi ' value='Tensi '>Tensi </option>
                                                <option data-name='Polishing Kit Komposit' value='Polishing Kit Komposit'>Polishing Kit Komposit</option>
                                                <option data-name='Dappen Glass ' value='Dappen Glass '>Dappen Glass </option>
                                                <option data-name='Jarum Ekstirpasi ' value='Jarum Ekstirpasi '>Jarum Ekstirpasi </option>
                                                <option data-name='Jarum Lentulo ' value='Jarum Lentulo '>Jarum Lentulo </option>
                                                <option data-name='Face Shield' value='Face Shield'>Face Shield</option>
                                                <option data-name='Cermin Oklusal' value='Cermin Oklusal'>Cermin Oklusal</option>
                                                <option data-name='Lampu UV ' value='Lampu UV '>Lampu UV </option>
                                                <option data-name='Protaper' value='Protaper'>Protaper</option>
                                                <option data-name='Gauge' value='Gauge'>Gauge</option>
                                                <option data-name='Kuret' value='Kuret'>Kuret</option>
                                                <option data-name='Bur Eve' value='Bur Eve'>Bur Eve</option>
                                                <option data-name='Linggual Button' value='Linggual Button'>Linggual Button</option>
                                                <option data-name='Soflex Disc' value='Soflex Disc'>Soflex Disc</option>
                                                <option data-name='Enhance Bur Flaim' value='Enhance Bur Flaim'>Enhance Bur Flaim</option>
                                                <option data-name='Finishing Bur' value='Finishing Bur'>Finishing Bur</option>
                                                <option data-name='Cermin ' value='Cermin '>Cermin </option>
                                                <option data-name='Kontainer box stainless' value='Kontainer box stainless'>Kontainer box stainless</option>
                                                <option data-name='Kotak Stainless Untuk Tampon' value='Kotak Stainless Untuk Tampon'>Kotak Stainless Untuk Tampon</option>
                                                <option data-name='Brush lowspeed ' value='Brush lowspeed '>Brush lowspeed </option>
                                                <option data-name='Dental Penahan Lidah' value='Dental Penahan Lidah'>Dental Penahan Lidah</option>
                                                <option data-name='Lowspeed Contra Angel' value='Lowspeed Contra Angel'>Lowspeed Contra Angel</option>
                                                <option data-name='Lowspeed Staight' value='Lowspeed Staight'>Lowspeed Staight</option>
                                                <option data-name='Matabur Highspeed Endo Acses' value='Matabur Highspeed Endo Acses'>Matabur Highspeed Endo Acses</option>
                                                <option data-name='Matabur Round' value='Matabur Round'>Matabur Round</option>
                                                <option data-name='Matabur Fissure' value='Matabur Fissure'>Matabur Fissure</option>
                                                <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.1' value='Sendok Cetak Penuh Rahang Atas Bawah No.1'>Sendok Cetak Penuh Rahang Atas Bawah No.1</option>
                                                <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.2' value='Sendok Cetak Penuh Rahang Atas Bawah No.2'>Sendok Cetak Penuh Rahang Atas Bawah No.2</option>
                                                <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.3' value='Sendok Cetak Penuh Rahang Atas Bawah No.3'>Sendok Cetak Penuh Rahang Atas Bawah No.3</option>
                                                <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.4' value='Sendok Cetak Penuh Rahang Atas Bawah No.4'>Sendok Cetak Penuh Rahang Atas Bawah No.4</option>
                                                <option data-name='Sendok Cetak Penuh Biru Uk. S' value='Sendok Cetak Penuh Biru Uk. S'>Sendok Cetak Penuh Biru Uk. S</option>
                                                <option data-name='Sendok Cetak Penuh Biru Uk. M' value='Sendok Cetak Penuh Biru Uk. M'>Sendok Cetak Penuh Biru Uk. M</option>
                                                <option data-name='Sendok Cetak Penuh Biru Uk. L' value='Sendok Cetak Penuh Biru Uk. L'>Sendok Cetak Penuh Biru Uk. L</option>
                                                <option data-name='Sendok Cetak Sebagian Biru' value='Sendok Cetak Sebagian Biru'>Sendok Cetak Sebagian Biru</option>
                                                <option data-name='Shadeguide Vita' value='Shadeguide Vita'>Shadeguide Vita</option>
                                                <option data-name='Benang Jarum Slik 3/0 atau 4/0' value='Benang Jarum Slik 3/0 atau 4/0'>Benang Jarum Slik 3/0 atau 4/0</option>
                                                <option data-name='Needle Citoject' value='Needle Citoject'>Needle Citoject</option>
                                                <option data-name='Tang Cabut Dewasa' value='Tang Cabut Dewasa'>Tang Cabut Dewasa</option>
                                                <option data-name='Gunting Benang Bengkok' value='Gunting Benang Bengkok'>Gunting Benang Bengkok</option>
                                                <option data-name='Endok Block' value='Endok Block'>Endok Block</option>
                                                <option data-name='Box K-File' value='Box K-File'>Box K-File</option>
                                                <option data-name='Plugger' value='Plugger'>Plugger</option>
                                                <option data-name='Spreader' value='Spreader'>Spreader</option>
                                                <option data-name='Alat Bleaching LED' value='Alat Bleaching LED'>Alat Bleaching LED</option>
                                                <option data-name='Probe' value='Probe'>Probe</option>
                                                <option data-name='Scaler Hoe Kanan dan Kiri Sepasang' value='Scaler Hoe Kanan dan Kiri Sepasang'>Scaler Hoe Kanan dan Kiri Sepasang</option>
                                                <option data-name='Fiber Splinting' value='Fiber Splinting'>Fiber Splinting</option>
                                                <option data-name='Kaca mata medis merah ' value='Kaca mata medis merah '>Kaca mata medis merah </option>
                                                <option data-name='Kaca mata medis putih ' value='Kaca mata medis putih '>Kaca mata medis putih </option>
                                                <option data-name='Cheek retractor ' value='Cheek retractor '>Cheek retractor </option>
                                                <option data-name='Syringe ' value='Syringe '>Syringe </option>
                                                <option data-name='K-File No 08' value='K-File No 08'>K-File No 08</option>
                                                <option data-name='K-File No 10' value='K-File No 10'>K-File No 10</option>
                                                <option data-name='K-File No 15-40' value='K-File No 15-40'>K-File No 15-40</option>
                                                <option data-name='K-File No 15' value='K-File No 15'>K-File No 15</option>
                                                <option data-name='K-File No 45-80' value='K-File No 45-80'>K-File No 45-80</option>
                                                <option data-name='Surgical blades Uk.10' value='Surgical blades Uk.10'>Surgical blades Uk.10</option>
                                                <option data-name='Surgical blades Uk.15' value='Surgical blades Uk.15'>Surgical blades Uk.15</option>
                                                <option data-name='Konektor polibib ' value='Konektor polibib '>Konektor polibib </option>
                                                <option data-name='Threeway syringe' value='Threeway syringe'>Threeway syringe</option>
                                                <option data-name='Highspeed 2 Hole' value='Highspeed 2 Hole'>Highspeed 2 Hole</option>
                                                <option data-name='Highspeed 4 Hole' value='Highspeed 4 Hole'>Highspeed 4 Hole</option>
                                                <option data-name='Catridge' value='Catridge'>Catridge</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" max="200" name="description[]" required value="" class="form-control description" />
                                        </td>
                                        <td>
                                            <input type="text" max="200" name="alasan[]" required value="" class="form-control alasan" />
                                        </td>
                                        <td>
                                            <input type="text" min="1" name="qty[]" value="" required class="form-control" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <?php else: ?>
                                        <?php 
                                            $details = explode(",",$alat_request->details);
                                        ?>
                                        <?php foreach($details as $idx => $detail): ?>
                                            <?php 
                                                $detail = explode("*",$detail);
                                            ?>
                                            <tr>
                                                <td>
                                                    <select class="form-control m-bot15 medicine_select" name="medicine_id[]">  
                                                        <option <?php if($detail[0] == '-'):?>selected<?php endif;?> value="-"><?php echo lang('select'); ?></option>
                                                            <option data-name='Shoes Cover (optional)' value='Shoes Cover (optional)'>Shoes Cover (optional)</option>
                                                            <option data-name='Gunting Kertas' value='Gunting Kertas'>Gunting Kertas</option>
                                                            <option data-name='Gelas Disposible' value='Gelas Disposible'>Gelas Disposible</option>
                                                            <option data-name='Gayung Kamar Mandi' value='Gayung Kamar Mandi'>Gayung Kamar Mandi</option>
                                                            <option data-name='Sikat Kamar Mandi' value='Sikat Kamar Mandi'>Sikat Kamar Mandi</option>
                                                            <option data-name='Toples' value='Toples'>Toples</option>
                                                            <option data-name='Hanger' value='Hanger'>Hanger</option>
                                                            <option data-name='Staples' value='Staples'>Staples</option>
                                                            <option data-name='Spons' value='Spons'>Spons</option>
                                                            <option data-name='Braket Holder' value='Braket Holder'>Braket Holder</option>
                                                            <option data-name='Spatula GIC' value='Spatula GIC'>Spatula GIC</option>
                                                            <option data-name='Plastis filling' value='Plastis filling'>Plastis filling</option>
                                                            <option data-name='Semen Stropper' value='Semen Stropper'>Semen Stropper</option>
                                                            <option data-name='Needle holder/Arteri Klem' value='Needle holder/Arteri Klem'>Needle holder/Arteri Klem</option>
                                                            <option data-name='Sonde' value='Sonde'>Sonde</option>
                                                            <option data-name='Escavator' value='Escavator'>Escavator</option>
                                                            <option data-name='Kaca mulut tanpa tangkai' value='Kaca mulut tanpa tangkai'>Kaca mulut tanpa tangkai</option>
                                                            <option data-name='Nierbeken ' value='Nierbeken '>Nierbeken </option>
                                                            <option data-name='Pinset ' value='Pinset '>Pinset </option>
                                                            <option data-name='Tang Cabut Anak ' value='Tang Cabut Anak '>Tang Cabut Anak </option>
                                                            <option data-name='Tang Remover Braket ' value='Tang Remover Braket '>Tang Remover Braket </option>
                                                            <option data-name='Tang Adam Universal' value='Tang Adam Universal'>Tang Adam Universal</option>
                                                            <option data-name='Scaler Ultrasonic' value='Scaler Ultrasonic'>Scaler Ultrasonic</option>
                                                            <option data-name='Tip Scaler ' value='Tip Scaler '>Tip Scaler </option>
                                                            <option data-name='Arkansas lowspeed stone (flam)' value='Arkansas lowspeed stone (flam)'>Arkansas lowspeed stone (flam)</option>
                                                            <option data-name='Arkansas lowspeed stone (cones)' value='Arkansas lowspeed stone (cones)'>Arkansas lowspeed stone (cones)</option>
                                                            <option data-name='Arkansas lowspeed stone (round)' value='Arkansas lowspeed stone (round)'>Arkansas lowspeed stone (round)</option>
                                                            <option data-name='Finishing (discs)' value='Finishing (discs)'>Finishing (discs)</option>
                                                            <option data-name='Finishing (cups)' value='Finishing (cups)'>Finishing (cups)</option>
                                                            <option data-name='Finishing (point)' value='Finishing (point)'>Finishing (point)</option>
                                                            <option data-name='Round bur ' value='Round bur '>Round bur </option>
                                                            <option data-name='Bur brush' value='Bur brush'>Bur brush</option>
                                                            <option data-name='Spatula alginate ' value='Spatula alginate '>Spatula alginate </option>
                                                            <option data-name='Spatula semen ' value='Spatula semen '>Spatula semen </option>
                                                            <option data-name='Bein ' value='Bein '>Bein </option>
                                                            <option data-name='Cryer ' value='Cryer '>Cryer </option>
                                                            <option data-name='Handle Scapel ' value='Handle Scapel '>Handle Scapel </option>
                                                            <option data-name='Tang Potong Distal' value='Tang Potong Distal'>Tang Potong Distal</option>
                                                            <option data-name='Light Curing LED' value='Light Curing LED'>Light Curing LED</option>
                                                            <option data-name='Bowl (mangkok)' value='Bowl (mangkok)'>Bowl (mangkok)</option>
                                                            <option data-name='Tensi ' value='Tensi '>Tensi </option>
                                                            <option data-name='Polishing Kit Komposit' value='Polishing Kit Komposit'>Polishing Kit Komposit</option>
                                                            <option data-name='Dappen Glass ' value='Dappen Glass '>Dappen Glass </option>
                                                            <option data-name='Jarum Ekstirpasi ' value='Jarum Ekstirpasi '>Jarum Ekstirpasi </option>
                                                            <option data-name='Jarum Lentulo ' value='Jarum Lentulo '>Jarum Lentulo </option>
                                                            <option data-name='Face Shield' value='Face Shield'>Face Shield</option>
                                                            <option data-name='Cermin Oklusal' value='Cermin Oklusal'>Cermin Oklusal</option>
                                                            <option data-name='Lampu UV ' value='Lampu UV '>Lampu UV </option>
                                                            <option data-name='Protaper' value='Protaper'>Protaper</option>
                                                            <option data-name='Gauge' value='Gauge'>Gauge</option>
                                                            <option data-name='Kuret' value='Kuret'>Kuret</option>
                                                            <option data-name='Bur Eve' value='Bur Eve'>Bur Eve</option>
                                                            <option data-name='Linggual Button' value='Linggual Button'>Linggual Button</option>
                                                            <option data-name='Soflex Disc' value='Soflex Disc'>Soflex Disc</option>
                                                            <option data-name='Enhance Bur Flaim' value='Enhance Bur Flaim'>Enhance Bur Flaim</option>
                                                            <option data-name='Finishing Bur' value='Finishing Bur'>Finishing Bur</option>
                                                            <option data-name='Cermin ' value='Cermin '>Cermin </option>
                                                            <option data-name='Kontainer box stainless' value='Kontainer box stainless'>Kontainer box stainless</option>
                                                            <option data-name='Kotak Stainless Untuk Tampon' value='Kotak Stainless Untuk Tampon'>Kotak Stainless Untuk Tampon</option>
                                                            <option data-name='Brush lowspeed ' value='Brush lowspeed '>Brush lowspeed </option>
                                                            <option data-name='Dental Penahan Lidah' value='Dental Penahan Lidah'>Dental Penahan Lidah</option>
                                                            <option data-name='Lowspeed Contra Angel' value='Lowspeed Contra Angel'>Lowspeed Contra Angel</option>
                                                            <option data-name='Lowspeed Staight' value='Lowspeed Staight'>Lowspeed Staight</option>
                                                            <option data-name='Matabur Highspeed Endo Acses' value='Matabur Highspeed Endo Acses'>Matabur Highspeed Endo Acses</option>
                                                            <option data-name='Matabur Round' value='Matabur Round'>Matabur Round</option>
                                                            <option data-name='Matabur Fissure' value='Matabur Fissure'>Matabur Fissure</option>
                                                            <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.1' value='Sendok Cetak Penuh Rahang Atas Bawah No.1'>Sendok Cetak Penuh Rahang Atas Bawah No.1</option>
                                                            <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.2' value='Sendok Cetak Penuh Rahang Atas Bawah No.2'>Sendok Cetak Penuh Rahang Atas Bawah No.2</option>
                                                            <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.3' value='Sendok Cetak Penuh Rahang Atas Bawah No.3'>Sendok Cetak Penuh Rahang Atas Bawah No.3</option>
                                                            <option data-name='Sendok Cetak Penuh Rahang Atas Bawah No.4' value='Sendok Cetak Penuh Rahang Atas Bawah No.4'>Sendok Cetak Penuh Rahang Atas Bawah No.4</option>
                                                            <option data-name='Sendok Cetak Penuh Biru Uk. S' value='Sendok Cetak Penuh Biru Uk. S'>Sendok Cetak Penuh Biru Uk. S</option>
                                                            <option data-name='Sendok Cetak Penuh Biru Uk. M' value='Sendok Cetak Penuh Biru Uk. M'>Sendok Cetak Penuh Biru Uk. M</option>
                                                            <option data-name='Sendok Cetak Penuh Biru Uk. L' value='Sendok Cetak Penuh Biru Uk. L'>Sendok Cetak Penuh Biru Uk. L</option>
                                                            <option data-name='Sendok Cetak Sebagian Biru' value='Sendok Cetak Sebagian Biru'>Sendok Cetak Sebagian Biru</option>
                                                            <option data-name='Shadeguide Vita' value='Shadeguide Vita'>Shadeguide Vita</option>
                                                            <option data-name='Benang Jarum Slik 3/0 atau 4/0' value='Benang Jarum Slik 3/0 atau 4/0'>Benang Jarum Slik 3/0 atau 4/0</option>
                                                            <option data-name='Needle Citoject' value='Needle Citoject'>Needle Citoject</option>
                                                            <option data-name='Tang Cabut Dewasa' value='Tang Cabut Dewasa'>Tang Cabut Dewasa</option>
                                                            <option data-name='Gunting Benang Bengkok' value='Gunting Benang Bengkok'>Gunting Benang Bengkok</option>
                                                            <option data-name='Endok Block' value='Endok Block'>Endok Block</option>
                                                            <option data-name='Box K-File' value='Box K-File'>Box K-File</option>
                                                            <option data-name='Plugger' value='Plugger'>Plugger</option>
                                                            <option data-name='Spreader' value='Spreader'>Spreader</option>
                                                            <option data-name='Alat Bleaching LED' value='Alat Bleaching LED'>Alat Bleaching LED</option>
                                                            <option data-name='Probe' value='Probe'>Probe</option>
                                                            <option data-name='Scaler Hoe Kanan dan Kiri Sepasang' value='Scaler Hoe Kanan dan Kiri Sepasang'>Scaler Hoe Kanan dan Kiri Sepasang</option>
                                                            <option data-name='Fiber Splinting' value='Fiber Splinting'>Fiber Splinting</option>
                                                            <option data-name='Kaca mata medis merah ' value='Kaca mata medis merah '>Kaca mata medis merah </option>
                                                            <option data-name='Kaca mata medis putih ' value='Kaca mata medis putih '>Kaca mata medis putih </option>
                                                            <option data-name='Cheek retractor ' value='Cheek retractor '>Cheek retractor </option>
                                                            <option data-name='Syringe ' value='Syringe '>Syringe </option>
                                                            <option data-name='K-File No 08' value='K-File No 08'>K-File No 08</option>
                                                            <option data-name='K-File No 10' value='K-File No 10'>K-File No 10</option>
                                                            <option data-name='K-File No 15-40' value='K-File No 15-40'>K-File No 15-40</option>
                                                            <option data-name='K-File No 15' value='K-File No 15'>K-File No 15</option>
                                                            <option data-name='K-File No 45-80' value='K-File No 45-80'>K-File No 45-80</option>
                                                            <option data-name='Surgical blades Uk.10' value='Surgical blades Uk.10'>Surgical blades Uk.10</option>
                                                            <option data-name='Surgical blades Uk.15' value='Surgical blades Uk.15'>Surgical blades Uk.15</option>
                                                            <option data-name='Konektor polibib ' value='Konektor polibib '>Konektor polibib </option>
                                                            <option data-name='Threeway syringe' value='Threeway syringe'>Threeway syringe</option>
                                                            <option data-name='Highspeed 2 Hole' value='Highspeed 2 Hole'>Highspeed 2 Hole</option>
                                                            <option data-name='Highspeed 4 Hole' value='Highspeed 4 Hole'>Highspeed 4 Hole</option>
                                                            <option data-name='Catridge' value='Catridge'>Catridge</option>
                                                    </select>
                                                </td>
                                                <td>
                                                    <input type="text" max="200" name="description[]" required value="<?php echo $detail[1];?>" class="form-control description" />
                                                </td>
                                                <td>
                                                    <input type="text" max="200" name="alasan[]" required value="<?php echo $detail[2];?>" class="form-control alasan" />
                                                </td>
                                                <td>
                                                    <input type="text" min="1" name="qty[]" value="<?php echo $detail[3];?>" required class="form-control only-numeric" />
                                                </td>
                                                <td>
                                                    <?php if($idx > 0): ?>
                                                        <button type="button" class="btn btn-danger btnremovedetail">-</button>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                            <div class="form-group">
                                <button type="submit" name="submit" class="btn btn-info pull-right"> <?php echo lang('submit'); ?></button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->



<style>

    form{
        border: 0px;
    }

    .med_selected{
        background: #fff;
        padding: 10px 0px;
        margin: 5px;
    }


    .select2-container--bgform .select2-selection--multiple .select2-selection__choice {
        clear: both !important;
    }

    label {
        display: inline-block;
        margin-bottom: 5px;
        font-weight: 500;
        font-weight: bold;
    }

</style>


<script src="common/js/codearistos.min.js"></script>

<script type="text/javascript">
    var template = '<tr>\
                        <td>\
                            <select class="form-control m-bot15 medicine_select" name="medicine_id[]">\
                                <option value="-"><?php echo lang('select'); ?></option>\
                                <option data-name="Shoes Cover (optional)" value="Shoes Cover (optional)">Shoes Cover (optional)</option>\
                                <option data-name="Gunting Kertas" value="Gunting Kertas">Gunting Kertas</option>\
                                <option data-name="Gelas Disposible" value="Gelas Disposible">Gelas Disposible</option>\
                                <option data-name="Gayung Kamar Mandi" value="Gayung Kamar Mandi">Gayung Kamar Mandi</option>\
                                <option data-name="Sikat Kamar Mandi" value="Sikat Kamar Mandi">Sikat Kamar Mandi</option>\
                                <option data-name="Toples" value="Toples">Toples</option>\
                                <option data-name="Hanger" value="Hanger">Hanger</option>\
                                <option data-name="Staples" value="Staples">Staples</option>\
                                <option data-name="Spons" value="Spons">Spons</option>\
                                <option data-name="Braket Holder" value="Braket Holder">Braket Holder</option>\
                                <option data-name="Spatula GIC" value="Spatula GIC">Spatula GIC</option>\
                                <option data-name="Plastis filling" value="Plastis filling">Plastis filling</option>\
                                <option data-name="Semen Stropper" value="Semen Stropper">Semen Stropper</option>\
                                <option data-name="Needle holder/Arteri Klem" value="Needle holder/Arteri Klem">Needle holder/Arteri Klem</option>\
                                <option data-name="Sonde" value="Sonde">Sonde</option>\
                                <option data-name="Escavator" value="Escavator">Escavator</option>\
                                <option data-name="Kaca mulut tanpa tangkai" value="Kaca mulut tanpa tangkai">Kaca mulut tanpa tangkai</option>\
                                <option data-name="Nierbeken " value="Nierbeken ">Nierbeken </option>\
                                <option data-name="Pinset " value="Pinset ">Pinset </option>\
                                <option data-name="Tang Cabut Anak " value="Tang Cabut Anak ">Tang Cabut Anak </option>\
                                <option data-name="Tang Remover Braket " value="Tang Remover Braket ">Tang Remover Braket </option>\
                                <option data-name="Tang Adam Universal" value="Tang Adam Universal">Tang Adam Universal</option>\
                                <option data-name="Scaler Ultrasonic" value="Scaler Ultrasonic">Scaler Ultrasonic</option>\
                                <option data-name="Tip Scaler " value="Tip Scaler ">Tip Scaler </option>\
                                <option data-name="Arkansas lowspeed stone (flam)" value="Arkansas lowspeed stone (flam)">Arkansas lowspeed stone (flam)</option>\
                                <option data-name="Arkansas lowspeed stone (cones)" value="Arkansas lowspeed stone (cones)">Arkansas lowspeed stone (cones)</option>\
                                <option data-name="Arkansas lowspeed stone (round)" value="Arkansas lowspeed stone (round)">Arkansas lowspeed stone (round)</option>\
                                <option data-name="Finishing (discs)" value="Finishing (discs)">Finishing (discs)</option>\
                                <option data-name="Finishing (cups)" value="Finishing (cups)">Finishing (cups)</option>\
                                <option data-name="Finishing (point)" value="Finishing (point)">Finishing (point)</option>\
                                <option data-name="Round bur " value="Round bur ">Round bur </option>\
                                <option data-name="Bur brush" value="Bur brush">Bur brush</option>\
                                <option data-name="Spatula alginate " value="Spatula alginate ">Spatula alginate </option>\
                                <option data-name="Spatula semen " value="Spatula semen ">Spatula semen </option>\
                                <option data-name="Bein " value="Bein ">Bein </option>\
                                <option data-name="Cryer " value="Cryer ">Cryer </option>\
                                <option data-name="Handle Scapel " value="Handle Scapel ">Handle Scapel </option>\
                                <option data-name="Tang Potong Distal" value="Tang Potong Distal">Tang Potong Distal</option>\
                                <option data-name="Light Curing LED" value="Light Curing LED">Light Curing LED</option>\
                                <option data-name="Bowl (mangkok)" value="Bowl (mangkok)">Bowl (mangkok)</option>\
                                <option data-name="Tensi " value="Tensi ">Tensi </option>\
                                <option data-name="Polishing Kit Komposit" value="Polishing Kit Komposit">Polishing Kit Komposit</option>\
                                <option data-name="Dappen Glass " value="Dappen Glass ">Dappen Glass </option>\
                                <option data-name="Jarum Ekstirpasi " value="Jarum Ekstirpasi ">Jarum Ekstirpasi </option>\
                                <option data-name="Jarum Lentulo " value="Jarum Lentulo ">Jarum Lentulo </option>\
                                <option data-name="Face Shield" value="Face Shield">Face Shield</option>\
                                <option data-name="Cermin Oklusal" value="Cermin Oklusal">Cermin Oklusal</option>\
                                <option data-name="Lampu UV " value="Lampu UV ">Lampu UV </option>\
                                <option data-name="Protaper" value="Protaper">Protaper</option>\
                                <option data-name="Gauge" value="Gauge">Gauge</option>\
                                <option data-name="Kuret" value="Kuret">Kuret</option>\
                                <option data-name="Bur Eve" value="Bur Eve">Bur Eve</option>\
                                <option data-name="Linggual Button" value="Linggual Button">Linggual Button</option>\
                                <option data-name="Soflex Disc" value="Soflex Disc">Soflex Disc</option>\
                                <option data-name="Enhance Bur Flaim" value="Enhance Bur Flaim">Enhance Bur Flaim</option>\
                                <option data-name="Finishing Bur" value="Finishing Bur">Finishing Bur</option>\
                                <option data-name="Cermin " value="Cermin ">Cermin </option>\
                                <option data-name="Kontainer box stainless" value="Kontainer box stainless">Kontainer box stainless</option>\
                                <option data-name="Kotak Stainless Untuk Tampon" value="Kotak Stainless Untuk Tampon">Kotak Stainless Untuk Tampon</option>\
                                <option data-name="Brush lowspeed " value="Brush lowspeed ">Brush lowspeed </option>\
                                <option data-name="Dental Penahan Lidah" value="Dental Penahan Lidah">Dental Penahan Lidah</option>\
                                <option data-name="Lowspeed Contra Angel" value="Lowspeed Contra Angel">Lowspeed Contra Angel</option>\
                                <option data-name="Lowspeed Staight" value="Lowspeed Staight">Lowspeed Staight</option>\
                                <option data-name="Matabur Highspeed Endo Acses" value="Matabur Highspeed Endo Acses">Matabur Highspeed Endo Acses</option>\
                                <option data-name="Matabur Round" value="Matabur Round">Matabur Round</option>\
                                <option data-name="Matabur Fissure" value="Matabur Fissure">Matabur Fissure</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.1" value="Sendok Cetak Penuh Rahang Atas Bawah No.1">Sendok Cetak Penuh Rahang Atas Bawah No.1</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.2" value="Sendok Cetak Penuh Rahang Atas Bawah No.2">Sendok Cetak Penuh Rahang Atas Bawah No.2</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.3" value="Sendok Cetak Penuh Rahang Atas Bawah No.3">Sendok Cetak Penuh Rahang Atas Bawah No.3</option>\
                                <option data-name="Sendok Cetak Penuh Rahang Atas Bawah No.4" value="Sendok Cetak Penuh Rahang Atas Bawah No.4">Sendok Cetak Penuh Rahang Atas Bawah No.4</option>\
                                <option data-name="Sendok Cetak Penuh Biru Uk. S" value="Sendok Cetak Penuh Biru Uk. S">Sendok Cetak Penuh Biru Uk. S</option>\
                                <option data-name="Sendok Cetak Penuh Biru Uk. M" value="Sendok Cetak Penuh Biru Uk. M">Sendok Cetak Penuh Biru Uk. M</option>\
                                <option data-name="Sendok Cetak Penuh Biru Uk. L" value="Sendok Cetak Penuh Biru Uk. L">Sendok Cetak Penuh Biru Uk. L</option>\
                                <option data-name="Sendok Cetak Sebagian Biru" value="Sendok Cetak Sebagian Biru">Sendok Cetak Sebagian Biru</option>\
                                <option data-name="Shadeguide Vita" value="Shadeguide Vita">Shadeguide Vita</option>\
                                <option data-name="Benang Jarum Slik 3/0 atau 4/0" value="Benang Jarum Slik 3/0 atau 4/0">Benang Jarum Slik 3/0 atau 4/0</option>\
                                <option data-name="Needle Citoject" value="Needle Citoject">Needle Citoject</option>\
                                <option data-name="Tang Cabut Dewasa" value="Tang Cabut Dewasa">Tang Cabut Dewasa</option>\
                                <option data-name="Gunting Benang Bengkok" value="Gunting Benang Bengkok">Gunting Benang Bengkok</option>\
                                <option data-name="Endok Block" value="Endok Block">Endok Block</option>\
                                <option data-name="Box K-File" value="Box K-File">Box K-File</option>\
                                <option data-name="Plugger" value="Plugger">Plugger</option>\
                                <option data-name="Spreader" value="Spreader">Spreader</option>\
                                <option data-name="Alat Bleaching LED" value="Alat Bleaching LED">Alat Bleaching LED</option>\
                                <option data-name="Probe" value="Probe">Probe</option>\
                                <option data-name="Scaler Hoe Kanan dan Kiri Sepasang" value="Scaler Hoe Kanan dan Kiri Sepasang">Scaler Hoe Kanan dan Kiri Sepasang</option>\
                                <option data-name="Fiber Splinting" value="Fiber Splinting">Fiber Splinting</option>\
                                <option data-name="Kaca mata medis merah " value="Kaca mata medis merah ">Kaca mata medis merah </option>\
                                <option data-name="Kaca mata medis putih " value="Kaca mata medis putih ">Kaca mata medis putih </option>\
                                <option data-name="Cheek retractor " value="Cheek retractor ">Cheek retractor </option>\
                                <option data-name="Syringe " value="Syringe ">Syringe </option>\
                                <option data-name="K-File No 08" value="K-File No 08">K-File No 08</option>\
                                <option data-name="K-File No 10" value="K-File No 10">K-File No 10</option>\
                                <option data-name="K-File No 15-40" value="K-File No 15-40">K-File No 15-40</option>\
                                <option data-name="K-File No 15" value="K-File No 15">K-File No 15</option>\
                                <option data-name="K-File No 45-80" value="K-File No 45-80">K-File No 45-80</option>\
                                <option data-name="Surgical blades Uk.10" value="Surgical blades Uk.10">Surgical blades Uk.10</option>\
                                <option data-name="Surgical blades Uk.15" value="Surgical blades Uk.15">Surgical blades Uk.15</option>\
                                <option data-name="Konektor polibib " value="Konektor polibib ">Konektor polibib </option>\
                                <option data-name="Threeway syringe" value="Threeway syringe">Threeway syringe</option>\
                                <option data-name="Highspeed 2 Hole" value="Highspeed 2 Hole">Highspeed 2 Hole</option>\
                                <option data-name="Highspeed 4 Hole" value="Highspeed 4 Hole">Highspeed 4 Hole</option>\
                                <option data-name="Catridge" value="Catridge">Catridge</option>\
                            </select>\
                        </td>\
                        <td>\
                            <input type="text" max="200" name="description[]" required value="" class="form-control description" />\
                        </td>\
                        <td>\
                            <input type="text" max="200" name="alasan[]" required value="" class="form-control alasan" />\
                        </td>\
                        <td>\
                            <input type="text" min="1" name="qty[]" value="" required class="form-control only-numeric" />\
                        </td>\
                        <td><button type="button" class="btn btn-danger btnremovedetail">-</button></td>\
                    </tr>';
    $(document).ready(function () {
        $(document).on('click','.btnremovedetail',function(e){
            $(this).parent().parent().remove();
        });

        $(document).on('keypress','.only-numeric',function(e){
            var keyCode = e.which ? e.which : e.keyCode
            if (!(keyCode >= 48 && keyCode <= 57)) {
                return false;
            }
        });

        $("#btndetail").click(function(e){
            $("#table tbody").append(template);
        });

        $(document).on('change','.medicine_select',function(e){
		    var data = $("option:selected",this).data();
            $(this).parent().parent().find('.description').val(data.name);
        });
    });
</script> 
