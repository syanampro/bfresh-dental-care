<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Appointment_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertAppointment($data) {
        // $data1 = array('hospital_id' => $this->session->userdata('hospital_id'));
        // $data2 = array_merge($data, $data1);
        $this->db->insert('appointment', $data);
    }

    function insertAppointmentPasien($data) {
        $this->db->insert('appointment', $data);
    }

    function getAppointment($treated= false) {
        if($treated)
            $this->db->where('status', 'Treated');

        $date_now = date(now());
        $date_to = date(now() + 2592000);
        $date_from = date(now()-2592000); 
        $this->db->order_by('id', 'desc');
        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentCS($treated= false) {
        if($treated)
            $this->db->where('status', 'Treated');

        $date_now = date(now());
        $date_to = date(now() + 259200);
        $date_from = date(now() - 259200); 
        $this->db->order_by('id', 'desc');
        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getRMByAppointmentId($id) {
        $this->db->where('appointment_id', $id);
        $this->db->order_by('id', 'desc');
        $query = $this->db->get('medical_history');
        return $query->row();
    }

    function getAppointmentKhusus($id) {
        $this->db->where('id', $id);
        $query = $this->db->get('appointment');
        return $query->row();
    }

    function AppointmentInput($treated= false) {
        $doctor_id = 0;
        if ($this->ion_auth->in_group(array('Doctor'))) {
            $current_user = $this->ion_auth->get_user_id();
            $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
        }
        if($doctor_id){
            $this->db->where('appointment.doctor', $doctor_id);
        }

        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 
        $this->db->select('appointment.id, patient.name, medical_history.appointment_id');
        $this->db->from('appointment');
        $this->db->join('medical_history', 'medical_history.appointment_id = appointment.id', 'left');
        $this->db->join('patient', 'patient.id = appointment.patient');
        $this->db->where('medical_history.appointment_id', null);
        $this->db->order_by('appointment.id', 'desc');
        $this->db->where('appointment.status', 'Treated');
        // $this->db->where('appointment.hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where('appointment.date >=', $date_from);
        $this->db->where('appointment.date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }

    function AppointmentInputPembayaran($treated= false) {
        $doctor_id = 0;
        if ($this->ion_auth->in_group(array('Doctor'))) {
            $current_user = $this->ion_auth->get_user_id();
            $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
        }
        if($doctor_id){
            $this->db->where('appointment.doctor', $doctor_id);
        }

        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-604800);
        // $date_from = date(now()-2592000);
        $this->db->select('appointment.id, patient.name, payment.appointment_id, doctor.name as namadokter');
        $this->db->from('appointment');
        $this->db->join('payment', 'payment.appointment_id = appointment.id', 'left');
        $this->db->join('patient', 'patient.id = appointment.patient','left');
        $this->db->join('doctor', 'doctor.id = appointment.doctor');
        $this->db->where('payment.appointment_id', null);
        $this->db->order_by('appointment.id', 'desc');
        $this->db->where('appointment.status', 'Treated');
        $this->db->where('appointment.hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where('appointment.date >=', $date_from);
        $this->db->where('appointment.date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }

    function AppointmentEdit($treated= false) {
        $doctor_id = 0;
        if ($this->ion_auth->in_group(array('Doctor'))) {
            $current_user = $this->ion_auth->get_user_id();
            $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
        }
        if($doctor_id){
            $this->db->where('appointment.doctor', $doctor_id);
        }

        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 
        $this->db->select('appointment.id, patient.name');
        $this->db->from('appointment');
        $this->db->join('patient', 'patient.id = appointment.patient');
        $this->db->order_by('appointment.id', 'desc');
        $this->db->where('appointment.status', 'Treated');
        $this->db->where('appointment.hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where('appointment.date >=', $date_from);
        $this->db->where('appointment.date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }

    function getAppointmentAll($treated= false) {
        if($treated)
            $this->db->where('status', 'Treated');
        $this->db->order_by('id', 'desc');
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentBySearch($search) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'desc');
        $this->db->like('id', $search);
        $this->db->or_like('name', $search);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByLimit($limit, $start) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'desc');
        $this->db->limit($limit, $start);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByLimitBySearch($limit, $start, $search) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));

        $this->db->like('id', $search);

        $this->db->order_by('id', 'desc');

        $this->db->or_like('name', $search);
        $this->db->or_like('phone', $search);
        $this->db->or_like('address', $search);

        $this->db->limit($limit, $start);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentForCalendar() {

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'asc');
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByDoctor($doctor) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'desc');
        $this->db->where('doctor', $doctor);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentRequest() {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'desc');
        $this->db->where('request', 'Yes');
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentRequestByDoctor($doctor) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->where('request', 'Yes');
        $this->db->where('doctor', $doctor);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByPatient($patient) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('date', 'desc');
        $this->db->where('patient', $patient);
        $this->db->join('hospital','hospital.id = appointment.hospital_id');
        $this->db->select('appointment.*,hospital.name as hospital');
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByStatus($status) {

        $date_now = date(now());
        $date_to = date(now() + 2592000);
        $date_from = date(now() - 5184000);

        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'desc');
        $this->db->where('status', $status);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByStatusByDoctor($status, $doctor) {
        $date_now = date(now());
        $date_to = date(now() + 2592000);
        $date_from = date(now() - 5184000);

        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->order_by('id', 'desc');
        $this->db->where('status', $status);
        $this->db->where('doctor', $doctor);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentById($id) {
        $date_now = date(now());
        $date_to = date(now() + 5184000);
        $date_from = date(now()-5184000); 

        $this->db->where('id', $id);
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get('appointment');
        return $query->row();
    }

    function getAppointmentByDate($date_from, $date_to) {
        $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $this->db->select('*');
        $this->db->from('appointment');
        $this->db->where('date >=', $date_from);
        $this->db->where('date <=', $date_to);
        $query = $this->db->get();
        return $query->result();
    }

    function getAppointmentByDoctorByToday($doctor_id) {
        // $this->db->where('hospital_id', $this->session->userdata('hospital_id'));
        $today = strtotime(date('Y-m-d'));
        $this->db->where('doctor', $doctor_id);
        $this->db->where('date', $today);
        $query = $this->db->get('appointment');
        return $query->result();
    }

    function getAppointmentByDoctorByMonth($doctor_id) {
        $bulan = date('m');
        $tahun = date('Y');

        $sql="SELECT * FROM appointment WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND doctor = $doctor_id AND status = 'Treated';";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getRMByDoctorByMonth($doctor_id) {
        $bulan = date('m');
        $tahun = date('Y');

        $sql="SELECT * FROM medical_history WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND user_id = $doctor_id;";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getAppointmentByDoctorByMonthSebelumnya($doctor_id) {
        $bulan = date('m', strtotime("-1 month"));
        $tahun = date('Y', strtotime("-1 month"));

        $sql="SELECT * FROM appointment WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND doctor = $doctor_id AND status = 'Treated';";    
        // $sql="SELECT * FROM appointment WHERE MONTH(FROM_UNIXTIME(date)) = 12 AND YEAR(FROM_UNIXTIME(date)) = 2022 AND doctor = $doctor_id AND status = 'Treated';";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getRMByDoctorByMonthSebelumnya($doctor_id) {
        $bulan = date('m', strtotime("-1 month"));
        $tahun = date('Y', strtotime("-1 month"));

        $sql="SELECT * FROM medical_history WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND user_id = $doctor_id;";    
        // $sql="SELECT * FROM medical_history WHERE MONTH(FROM_UNIXTIME(date)) = 12 AND YEAR(FROM_UNIXTIME(date)) = 2022 AND user_id = $doctor_id;";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getAppointmentByCabangByMonth($id) {
        $bulan = date('m');
        $tahun = date('Y');

        $sql="SELECT * FROM appointment WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND hospital_id = $id AND status = 'Treated';";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getRMByCabangByMonth($id) {
        $bulan = date('m');
        $tahun = date('Y');

        $sql="SELECT * FROM medical_history WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND hospital_id = $id;";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getAppointmentByCabangByMonthSebelumnya($id) {
        $bulan = date('m', strtotime("-1 month"));
        $tahun = date('Y', strtotime("-1 month"));

        $sql="SELECT * FROM appointment WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND hospital_id = $id AND status = 'Treated';";    
        // $sql="SELECT * FROM appointment WHERE MONTH(FROM_UNIXTIME(date)) = 12 AND YEAR(FROM_UNIXTIME(date)) = 2022 AND doctor = $doctor_id AND status = 'Treated';";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function getRMByCabangByMonthSebelumnya($id) {
        $bulan = date('m', strtotime("-1 month"));
        $tahun = date('Y', strtotime("-1 month"));

        $sql="SELECT * FROM medical_history WHERE MONTH(FROM_UNIXTIME(date)) = $bulan AND YEAR(FROM_UNIXTIME(date)) = $tahun AND hospital_id = $id;";    
        // $sql="SELECT * FROM medical_history WHERE MONTH(FROM_UNIXTIME(date)) = 12 AND YEAR(FROM_UNIXTIME(date)) = 2022 AND user_id = $doctor_id;";    
        $query = $this->db->query($sql);
        return count($query->result());
    }

    function updateAppointment($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('appointment', $data);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('appointment');
    }

    function updateIonUser($username, $email, $password, $ion_user_id) {
        $uptade_ion_user = array(
            'username' => $username,
            'email' => $email,
            'password' => $password
        );
        $this->db->where('id', $ion_user_id);
        $this->db->update('users', $uptade_ion_user);
    }

}
