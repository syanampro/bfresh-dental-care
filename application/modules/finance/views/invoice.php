<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height" style="padding-left: 0px !important;">
        <!-- invoice start-->
        <section class="col-md-4">
            <div class="panel panel-primary" id="invoice">
                <!--<div class="panel-heading navyblue"> INVOICE</div>-->
                <div class="panel-body" style="font-size: 10px;">
                    <div class="row">
                        <div class="text-center">
                            <div class="">
                                <img alt="" src="https://bfreshgigi.com/wp-content/uploads/elementor/thumbs/logo-bfresh-400-pji13cskq4mbhqlzcacngknhfe97u898w00injkimw.png" width="160px" height="60px">
                                <p style="font-size: 14px; text-align: center; margin: 0px 0px -10px 0px;">
                                    <img src="https://cdn-icons-png.flaticon.com/512/1384/1384031.png" height="15px" width="15px"> bfreshdentalcare &nbsp;&nbsp;&nbsp;<img src="https://cdn-icons-png.flaticon.com/512/1006/1006771.png" height="15px" width="15px"> bfreshgigi.com <br><img src="https://cdn-icons-png.flaticon.com/512/2111/2111774.png" height="15px" width="15px"> 085645262347
                                </p>
                                <!-- <h3 style="font-family: arial;" class="text-center">
                                    <?php echo $settings->title ?>
                                </h3>
                                <h4 style="font-family: arial;" class="text-center">
                                    <?php echo $settings->address ?>
                                </h4>
                                <h4 style="font-family: arial;" class="text-center">
                                    Tel: <?php echo $settings->phone ?>
                                </h4> -->
                            </div>
                        </div>

                        <div class="col-md-12 hr_border">
                            <hr style="border: 1px solid black;">
                        </div>

                        <div class="col-md-12" style="margin-top: -10px;">
                            <div class="col-md-12 pull-left row" style="text-align: left;">
                                <div class="col-md-12 row details" style="">
                                    <p>
                                        <?php $patient_info = $this->db->get_where('patient', array('id' => $payment->patient))->row(); ?>
                                        <label class="control-label" style="font-size:13px;">Nama Pasien</label>
                                        <span style="text-transform: uppercase; font-size:15px;"> <br>
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->name . ' <br>';
                                            } else {
                                                echo 'Tidak Ada Pasien';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div>

                                <div class="col-md-12 row details">
                                    <p>
                                        <label class="control-label" style="font-size:13px;">Dokter</label>
                                        <span style="text-transform: uppercase; font-size:15px;"> <br>
                                            <?php
                                            if (!empty($payment->doctor)) {
                                                $doc_details = $this->doctor_model->getDoctorById($payment->doctor);
                                                if (!empty($doc_details)) {
                                                    echo $doc_details->name . ' <br>';
                                                } else {
                                                    echo $payment->doctor_name . ' <br>';
                                                }
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div>

                                <div class="col-md-12 row details">
                                    <p>
                                        <label class="" style="font-size:13px;">ID Pasien / Tanggal / No. Invoice</label>
                                        <span style="text-transform: uppercase; font-size:15px;"> <br>
                                            <?php
                                            if (!empty($patient_info)) {
                                                $hospital = $this->hospital_model->getHospitalById($patient_info->hospital_id);
                                                echo $hospital->code.'-'.str_pad($patient_info->id, 4, "0", STR_PAD_LEFT) . ' / ';
                                            } else {
                                                echo 'Tidak Ada Pasien / ';
                                            }
                                            ?>
                                        </span>
                                        <span style="text-transform: uppercase; font-size:15px;">
                                            <?php
                                            if (!empty($payment->date)) {
                                                echo date('d-m-Y', $payment->date) . ' / ';
                                            }
                                            ?>
                                        </span>
                                        <span style="text-transform: uppercase; font-size:15px;">
                                            <?php
                                            if (!empty($payment->id)) {
                                                echo $payment->id;
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div>
                                
                                <!-- <div class="col-md-12 row details" style="">
                                    <p>
                                        <label class="control-label" style="font-size:15px;">Alamat</label>
                                        <span style="text-transform: uppercase; font-size:20px;"> <br>
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->address . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <div class="col-md-12 row details" style="">
                                    <p>
                                        <label class="control-label" style="font-size:15px;">No Telp</label>
                                        <span style="text-transform: uppercase; font-size:20px;"> <br>
                                            <?php
                                            if (!empty($patient_info)) {
                                                echo $patient_info->phone . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div> -->
                                <!-- <div class="col-md-12 row details" style="">
                                    <p>
                                        <label class="control-label" style="font-size:15px;">No. <?php echo lang('invoice'); ?> </label>
                                        <span style="text-transform: uppercase; font-size:20px;"> <br>
                                            <?php
                                            if (!empty($payment->id)) {
                                                echo $payment->id;
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div> -->


                                <!-- <div class="col-md-12 row details">
                                    <p>
                                        <label class="control-label" style="font-size:15px;">Tanggal</label>
                                        <span style="text-transform: uppercase; font-size:20px;"> <br>
                                            <?php
                                            if (!empty($payment->date)) {
                                                echo date('d-m-Y', $payment->date) . ' <br>';
                                            }
                                            ?>
                                        </span>
                                    </p>
                                </div> -->
                            </div>
                        </div>
                    </div> 

                    <hr style="border: 1px solid black; margin: 0px !important;">

                    <table class="table table-striped">

                        <thead class="theadd">
                            <tr>
                                <!-- <th>#</th> -->
                                <th style="font-size: 15px;">Layanan</th>
                                <th style="font-size: 15px;">Harga Satuan</th>
                                <th style="font-size: 15px;">Qty</th>
                                <th style="font-size: 15px;">Jumlah</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            if (!empty($payment->category_name)) {
                                $category_name = $payment->category_name;
                                $category_name1 = explode(',', $category_name);
                                $i = 0;

                                $newcategoryname = [];
                                foreach ($category_name1 as $category_name2) {
                                    $category_name3 = explode('*', $category_name2);
                                    if(@$category_name3[4]){
                                        if(@$newcategoryname[$category_name3[4]]){
                                            $newcategoryname[$category_name3[4]][1] += $category_name3[1];
                                        }else{
                                            $newcategoryname[$category_name3[4]] = [
                                                0=>$category_name3[0],
                                                1=>$category_name3[1],
                                                2=>$category_name3[2],
                                                3=>$category_name3[3],
                                            ];
                                        }
                                    }else{
                                        if(@$newcategoryname[$category_name3[0]]){
                                            $newcategoryname[$category_name3[0]][0] = $category_name3[0];
                                            $newcategoryname[$category_name3[0]][1] += $category_name3[1];
                                            $newcategoryname[$category_name3[0]][2] = $category_name3[2];
                                            $newcategoryname[$category_name3[0]][3] = $category_name3[3];
                                        }else{
                                            $newcategoryname[$category_name3[0]] = [
                                                0=>$category_name3[0],
                                                1=>$category_name3[1],
                                                2=>$category_name3[2],
                                                3=>$category_name3[3],
                                            ];
                                        }
                                    }
                                }
                                // var_dump($newcategoryname);
                                $category_name1 = $newcategoryname;
                                foreach ($category_name1 as $category_name3) {
                                    $i = $i + 1;
                                    // $category_name3 = explode('*', $category_name2);
                                    if ($category_name3[3] > 0) {
                                        ?>  

                                        <tr>
                                            <!-- <td><?php echo $i; ?> </td> -->
                                            
                                            <td style="font-size: 15px; font-weight: 100"><?php echo $this->finance_model->getPaymentcategoryById($category_name3[0])->category; ?> </td>
                                            <td style="font-size: 15px; font-weight: 100"><?php echo $settings->currency; ?> <?php echo number_format($category_name3[1],0,",","."); ?> </td>
                                            <td style="font-size: 15px; font-weight: 100"> <?php echo $category_name3[3]; ?> </td>
                                            <td style="font-size: 15px; font-weight: 100"><?php echo $settings->currency; ?> <?php echo number_format($category_name3[1] * $category_name3[3],0,",","."); ?> </td>
                                        </tr> 
                                        <?php
                                    }
                                }
                            }
                            ?>
                            <?php
                            if (!empty($pharmacy_payment->category_name)) {
                                $category_name = $pharmacy_payment->category_name;
                                $category_name1 = explode(',', $category_name);
                                foreach ($category_name1 as $category_name2) {
                                    $category_name3 = explode('*', $category_name2);
                                    if ($category_name3[1] > 0) {
                                        ?>                
                                        <tr>
                                            <td><?php echo $i + 1; ?></td>
                                            <td><?php
                                                $current_medicine = $this->db->get_where('medicine', array('id' => $category_name3[0]))->row();
                                                echo $current_medicine->name;
                                                ?>
                                            </td>
                                            <td class=""><?php echo $settings->currency; ?> <?php echo $category_name3[1]; ?> </td>
                                            <td class=""> <?php echo $category_name3[2]; ?> </td>
                                            <td class=""><?php echo $settings->currency; ?> <?php echo $category_name3[1] * $category_name3[2]; ?> </td>
                                        </tr> 
                                        <?php
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                    <!-- <div class="col-md-12 hr_border" style="margin-top: -20px;">
                        <hr>
                    </div> -->

                    <div style="margin-top: -20px;">
                        <div class="col-lg-12 invoice-block">
                            <ul class="unstyled amounts">
                                <hr>
                                <li style="font-size: 15px;"><strong><?php echo lang('sub_total'); ?> : </strong><?php echo $settings->currency; ?> <?php echo number_format($payment->amount + @$pharmacy_payment->amount,0,",","."); ?></li>
                                <?php if (!empty($payment->discount)) { ?>
                                    <li style="font-size: 15px;"><strong><?php echo lang('discount'); ?></strong> <?php
                                        if ($discount_type == 'percentage') {
                                            echo '(%) : ';
                                        } else {
                                            echo ': ' . $settings->currency;
                                        }
                                        ?> <?php
                                        $discountpharm = 0;
                                        if(@$pharmacy_payment){
                                            $discountpharm = explode('*', $pharmacy_payment->discount);
                                            if (!empty($discount[1])) {
                                                $discountpharm = $discount[1];
                                            }
                                        }
                                        $discount = explode('*', $payment->discount);
                                        if (!empty($discount[1])) {
                                            echo $discount[0] . ' %  =  ' . $settings->currency . ' ' . $discount[1] + $discountpharm;
                                        } else {
                                            echo number_format($discount[0],0,",",".") ;
                                        }
                                        ?></li>
                                    <?php } ?>
                                    <?php if (!empty($payment->vat)) { ?>
                                    <li><strong>VAT :</strong>   <?php
                                        $vatpharm = 0;
                                        if(@$pharmacy_payment){
                                            $vatpharm = $pharmacy_payment->flat_vat;
                                        }
                                        if (!empty($payment->vat)) {
                                            echo $payment->vat;
                                        } else {
                                            echo '0';
                                        }
                                        ?> % = <?php echo $settings->currency . ' ' . $payment->flat_vat + $vatpharm; ?>
                                    </li>
                                    <?php } ?>
                                <li style="font-size: 15px;"><strong><?php echo lang('grand_total'); ?> : <b style="color:red";></strong><?php echo $settings->currency; ?> <?php echo number_format($g = $payment->gross_total + @$pharmacy_payment->gross_total,0,",","."); ?></b></li>
                                <hr>
                                <li style="font-size: 15px;"><strong>Pembayaran Cash: </strong><?php echo $settings->currency; ?> <?php echo number_format($r1= $this->finance_model->getPaymentCashByPaymentId($payment->id),0,",","."); ?></li>
                                <li style="font-size: 15px;"><strong>Pembayaran Transfer: </strong><?php echo $settings->currency; ?> <?php echo number_format($r2 = $this->finance_model->getPaymentTransferByPaymentId($payment->id),0,",","."); ?></li>
                                <li style="font-size: 15px;"><strong>Sisa Pembayaran : </strong><?php echo $settings->currency; ?> <?php echo number_format($g - $r1 - $r2 ,0,",","."); ?></li>
                                <li style="font-size: 15px;"><strong>Promat : </strong><?php echo $payment->promat == "1" ? "Ya" : "Tidak" ?><br><br></li>
                            </ul>
                        </div>
                    </div>

                    
                    <div class="">
                        <div class="" style="text-align: left;">
                            <p style="font-size: 15px;"><strong>Bukti TF : </strong>
                                <?php if (!empty($payment->bukti_tf)){ ?>
                                <a target="_blank" href="<?php echo base_url('/uploads/'.$payment->bukti_tf) ?>">Lihat</a>
                                <?php } else { ?>
                                -
                                <?php }?>
                            </p>
                            <?php if (!empty($payment->remarks) || !empty($payment->diagnosa) || !empty($payment->terapi) || !empty($payment->resep)) { ?>
                            <p style="font-size: 15px;"><strong>Note : </strong>
                                <?php if (!empty($payment->remarks)){ ?>
                                <?php echo $payment->remarks?>
                                <?php } else { ?>
                                -
                                <?php }?>
                            </p>
                            <!-- <p style="font-size: 15px;"><strong>Diagnosa : </strong>
                                <?php if (!empty($payment->diagnosa)){ ?>
                                <?php echo $payment->diagnosa?>
                                <?php } else { ?>
                                -
                                <?php }?>
                            </p>
                            <p style="font-size: 15px;"><strong>Terapi : </strong>
                                <?php if (!empty($payment->terapi)){ ?>
                                <?php echo $payment->terapi?>
                                <?php } else { ?>
                                -
                                <?php }?>
                            </p>
                            <p style="font-size: 15px;"><strong>Resep : </strong>
                                <?php if (!empty($payment->resep)){ ?>
                                <?php echo $payment->resep?>
                                <?php } else { ?>
                                -
                                <?php }?>
                            </p> -->
                            <div style="border:1px solid black; height:100px; width: 200px; text-align: center; padding-top:80px"><?php
                                    if (!empty($payment->doctor)) {
                                        $doc_details = $this->doctor_model->getDoctorById($payment->doctor);
                                        if (!empty($doc_details)) {
                                            echo $doc_details->name . ' <br>';
                                        } else {
                                            echo $payment->doctor_name . ' <br>';
                                        }
                                    }
                                    ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    

                    <div class="col-md-12 hr_border" style="">
                        <hr style="border: 1px solid black;">
                    </div>

                    <div class="col-md-12 invoice_footer">
                        <div class="col-md-12 row pull-left" style="font-size: 15px; margin-top: -15px;">
                            <?php echo lang('user'); ?> : <?php echo $this->ion_auth->user($payment->user)->row()->username; ?><br>
                        </div>
                    </div>
                </div>
        </section>


        <section class="col-md-6">
            <div class="col-md-5 no-print" style="margin-top: 20px;">
                <a href="finance/payment" class="btn btn-info btn-sm info pull-left"><i class="fa fa-arrow-circle-left"></i>  <?php echo lang('back_to_payment_modules'); ?> </a>
                <div class="text-center col-md-12 row">
                    <a class="btn btn-info btn-sm invoice_button pull-left" onclick="javascript:window.print();"><i class="fa fa-print"></i> <?php echo lang('print'); ?> </a>
                    <?php if ($this->ion_auth->in_group(array('admin', 'Accountant'))) { ?>
                        <a href="finance/editPayment?id=<?php echo $payment->id; ?>" class="btn btn-info btn-sm editbutton pull-left"><i class="fa fa-edit"></i> <?php echo lang('edit'); ?> <?php echo lang('invoice'); ?> </a>
                    <?php } ?>
                        <a class="btn btn-info btn-sm detailsbutton pull-left download" id="download"><i class="fa fa-download"></i> <?php echo lang('download'); ?> </a>
                        <?php
                            // Layanan
                            $category_name = $payment->category_name;
                            $category_name1 = explode(',', $category_name);
                            $i = 0;

                            $newcategoryname = [];
                            foreach ($category_name1 as $category_name2) {
                                $category_name3 = explode('*', $category_name2);
                                if(@$category_name3[4]){
                                    if(@$newcategoryname[$category_name3[4]]){
                                        $newcategoryname[$category_name3[4]][1] += $category_name3[1];
                                    }else{
                                        $newcategoryname[$category_name3[4]] = [
                                            0=>$category_name3[0],
                                            1=>$category_name3[1],
                                            2=>$category_name3[2],
                                            3=>$category_name3[3],
                                        ];
                                    }
                                }else{
                                    if(@$newcategoryname[$category_name3[0]]){
                                        $newcategoryname[$category_name3[0]][0] = $category_name3[0];
                                        $newcategoryname[$category_name3[0]][1] += $category_name3[1];
                                        $newcategoryname[$category_name3[0]][2] = $category_name3[2];
                                        $newcategoryname[$category_name3[0]][3] = $category_name3[3];
                                    }else{
                                        $newcategoryname[$category_name3[0]] = [
                                            0=>$category_name3[0],
                                            1=>$category_name3[1],
                                            2=>$category_name3[2],
                                            3=>$category_name3[3],
                                        ];
                                    }
                                }
                            }
                            $category_name1 = $newcategoryname;
                            $layanan = array();
                            foreach($category_name1 as $category_name3) {
                                $i = $i + 1; 
                                if ($category_name3[3] > 0) { 
                                    $layanan[] ="✅ " . $this->finance_model->getPaymentcategoryById($category_name3[0])->category . ", Rp " .number_format($category_name3[1],'0',',','.') . ", " . $category_name3[3] . ", Rp " . number_format($category_name3[1] * $category_name3[3],'0',',','.') . "%0a";
                                }
                            }

                            //subtotal
                            $subtotalll =  $payment->amount + @$pharmacy_payment->amount;
                            $grandtotalll = $payment->gross_total + @$pharmacy_payment->gross_total;
                            $pembayarann = $this->finance_model->getDepositAmountByPaymentId($payment->id) + @$pharmacy_payment->amount_received;
                            $sisa_pembayarann = $grandtotalll - $pembayarann;
                            $url = 'https://finance.bfreshgigi.com/invoice/'.hash('sha256', $payment->id);

                            //dokter
                            $doc_details = $this->doctor_model->getDoctorById($payment->doctor);
                            if (!empty($doc_details)) {
                                $dokterr =  $doc_details->name;
                            } else {
                                $dokterr =  $payment->doctor_name;
                            }
                            
                            if (!empty($patient_info)) {
                                $hospital = $this->hospital_model->getHospitalById($patient_info->hospital_id);
                                $phone = str_replace(" ","",trim($patient_info->phone));
                                if(substr($phone, 0, 1)=='0'){
                                    $phone = '+62'.substr($phone, 1);
                                }
                                $wacontent = "*FAKTUR ELEKTRONIK TRANSAKSI REGULER*%0a%0aB FRESH DENTAL CARE%0aPerumahan Ruko Jaya Harmoni No. 14, Jl. Kesatrian Buduran, Buduran, Sidoarjo, Jawa Timur%0a0856 4526 2347%0abfreshgigi.com%0a%0aID Pasien / Tanggal / No. Invoice :%0a*" . $hospital->code.'-'.str_pad($patient_info->id, 4, "0", STR_PAD_LEFT) . " / " . date('d-m-Y', $payment->date) . " / " . $payment->id . "*%0a%0aDokter Perawat:%0a".$dokterr."%0a%0aPasien Terhormat:%0a" . $patient_info->name . "%0a%0a======================%0aDetail Layanan:%0a". implode("",$layanan) ."======================%0a%0aDetail Biaya:%0aSubtotal: Rp ".number_format($subtotalll,'0',',','.')."%0aGrand total: Rp ".number_format($grandtotalll,'0',',','.')."%0a%0aPembayaran:%0a💵 Cash Rp ".number_format($payment->pembayaran_cash,'0',',','.')."%0a💵 Transfer Rp ".number_format($payment->pembayaran_transfer,'0',',','.')."%0a%0aSisa Pembayaran:%0aRp ".$sisa_pembayarann."%0a%0a======================%0a%0aSyarat dan ketentuan:%0aPerhatian:%0a1. Bila ada keluhan, mohon menghubungi WA 0856.4526.2347%0a2. Bila hasil perawatan alami keluhan dan/atau kerusakan, masa garansi berbeda-beda sesuai dengan kasusnya di rentang waktu 1-30 hari%0a%0aReservasi mandiri melalui bfreshgigi.com%0a%0aTerima kasih telah menggunakan jasa B Fresh Dental Care ☺️%0a%0a*Faktur Online*%0a".$url;
                                
                                // $wacontent = "*FAKTUR ELEKTRONIK TRANSAKSI REGULER*%0a%0aB FRESH DENTAL CARE%0aPerumahan Ruko Jaya Harmoni No. 14, Jl. Kesatrian Buduran, Buduran, Sidoarjo, Jawa Timur%0a0856 4526 2347%0abfreshgigi.com%0a%0aID Pasien / Tanggal / No. Invoice :%0a*" . $hospital->code.'-'.str_pad($patient_info->id, 4, "0", STR_PAD_LEFT) . " / " . date('d-m-Y', $payment->date) . " / " . $payment->id . "*%0a%0aDokter Perawat:%0a".$dokterr."%0a%0aPasien Terhormat:%0a" . $patient_info->name . "%0a%0a======================%0aDetail Layanan:%0a". implode("",$layanan) ."======================%0a%0aDetail Biaya:%0aSubtotal: Rp ".$subtotalll."%0aGrand total: Rp ".$grandtotalll."%0a%0aPembayaran:%0a💵 Cash Rp ".$pembayarann."%0a💵 Transfer Rp ".$pembayarann."%0a%0aSisa Pembayaran:%0aRp ".$sisa_pembayarann."%0a%0a======================%0a%0aSyarat dan ketentuan:%0aPerhatian:%0a1. Bila ada keluhan, mohon menghubungi WA 0856.4526.2347%0a2. Bila hasil perawatan alami keluhan dan/atau kerusakan, masa garansi berbeda-beda sesuai dengan kasusnya di rentang waktu 1-30 hari%0a%0aReservasi mandiri melalui bfreshgigi.com%0a%0aTerima kasih telah menggunakan jasa B Fresh Dental Care ☺️";
                            } else {
                                $phone = null;
                            }
                           ?>

                        <?php if($phone): ?>
                        <!-- <a target="_blank" class="btn btn-success btn-sm pull-left" href="https://api.whatsapp.com/send?phone=<?php echo '6285706679500'; ?>&text=<?php echo $wacontent; ?>">Kirim WA</i></a> -->
                        <a target="_blank" class="btn btn-success btn-sm pull-left" href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>&text=<?php echo $wacontent; ?>">Kirim WA</i></a>
                        <?php endif ?>
                        <a target="_blank" class="btn btn-success btn-sm pull-left" href="<?php echo $url ?>">Invoice Online</i></a>
                </div>
                
                <div class="no-print">
                    <a href="finance/addPaymentView" class="pull-left">
                        <div class="btn-group">
                            <button id="" class="btn btn-info green btn-sm">
                                <i class="fa fa-plus-circle"></i> <?php echo lang('add_another_payment'); ?>
                            </button>
                        </div>
                    </a>

                </div>

            </div>

        </section>


        <style>

            th{
                text-align: center;
            }

            td{
                text-align: center;
            }

            tr.total{
                color: green;
            }



            .control-label{
                width: 100px;
            }



            h1{
                margin-top: 5px;
            }


            .print_width{
                width: 100%;
                float: left;
            } 

            ul.amounts li {
                padding: 0px !important;
            }

            .invoice-list {
                margin-bottom: 10px;
            }




            .panel{
                border: 0px solid #5c5c47;
                background: #fff !important;
                height: 100%;
                margin: 20px 5px 5px 5px;
                border-radius: 0px !important;
                min-height: 100px;
            }



            /* .table.main{
                margin-top: -50px;
            } */



            .control-label{
                margin-bottom: 0px;
            }

            tr.total td{
                color: green !important;
            }

            .theadd th{
                background: #edfafa !important;
                background: #fff!important;
            }

            td{
                font-size: 12px;
                padding: 5px;
                font-weight: bold;
            }

            .details{
                font-weight: bold;
            }

            /* hr{
                border-bottom: 0px solid #f1f1f1 !important;
            } */

            .corporate-id {
                margin-bottom: 5px;
            }

            .adv-table table tr td {
                padding: 5px 10px;
            }



            .btn{
                margin: 10px 10px 10px 0px;
            }

            .invoice_head_left h3{
                color: #2f80bf !important;
                font-family: cursive;
            }


            .invoice_head_right{
                margin-top: 10px;
            }

            .invoice_footer{
                margin-bottom: 10px;
            }










            @media print {

                h1{
                    margin-top: 5px;
                }

                #main-content{
                    padding-top: 0px;
                }

                .print_width{
                    width: 100%;
                    float: left;
                } 

                ul.amounts li {
                    padding: 0px !important;
                }

                .invoice-list {
                    margin-bottom: 10px;
                }

                .wrapper{
                    margin-top: 0px;
                }

                .wrapper{
                    padding: 0px 0px !important;
                    background: #fff !important;

                }

                /* .wrapper{
                    border: 2px solid #777;
                } */

                /* .panel{
                    border: 0px solid #5c5c47;
                    background: #fff !important;
                    padding: 0px 0px;
                    height: 100%;
                    margin: 5px 5px 5px 5px;
                    border-radius: 0px !important;

                } */

                .site-min-height {
                    min-height: 100px;
                }



                /* .table.main{
                    margin-top: -50px;
                } */

                .control-label{
                    margin-bottom: 0px;
                }

                tr.total td{
                    color: green !important;
                }

                .theadd th{
                    background: #777 !important;
                }

                td{
                    font-size: 12px;
                    padding: 5px;
                    font-weight: bold;
                }

                .details{
                    font-weight: bold; 
                }

                /* hr{
                    border-bottom: 0px solid #f1f1f1 !important;
                } */

                .corporate-id {
                    margin-bottom: 5px;
                }

                /* .adv-table table tr td {
                    padding: 5px 10px;
                } */

                .invoice_head{
                    width: 100%;
                }

                .invoice_head_left{
                    float: left;
                    width: 40%;
                    color: #2f80bf;
                    font-family: cursive;
                }

                .invoice_head_right{
                    float: right;
                    width: 40%;
                    margin-top: 10px;
                }

                /* .hr_border{
                    width: 100%;
                } */

                .invoice_footer{
                    margin-bottom: 10px;
                }


            }

        </style>




        <script src="common/js/codearistos.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>

        <script>


                        $('#download').click(function () {
                            var pdf = new jsPDF('p', 'pt', 'letter');
                            pdf.addHTML($('#invoice'), function () {
                                pdf.save('invoice_id_<?php echo $payment->id; ?>.pdf');
                            });
                        });

                        // This code is collected but useful, click below to jsfiddle link.
        </script>



    </section>
    <!-- invoice end-->
</section>
</section>
<!--main content end-->
<!--footer start-->

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
                        $(document).ready(function () {
                            $(".flashmessage").delay(3000).fadeOut(100);
                        });
</script>
