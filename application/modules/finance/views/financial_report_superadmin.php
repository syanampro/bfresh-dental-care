<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <header class="panel-heading"> 
            Laporan Keuangan Cabang
            <div class="col-md-1 pull-right">
                <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button>
            </div>
        </header>

        <section  style="margin-bottom: 130px;">
            <div class="col-md-12">
                <div class="col-md-7 row">
                    <section>
                        <form role="form" class="f_report" action="finance/financialReportAdmin" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control dpd1" name="date_from" value="<?php
                                        if (!empty($from)) {
                                            echo $from;
                                        }
                                        ?>" placeholder="<?php echo lang('date_from'); ?>" readonly="">
                                        <span class="input-group-addon"><?php echo lang('to'); ?></span>
                                        <input type="text" class="form-control dpd2" name="date_to" value="<?php
                                        if (!empty($to)) {
                                            echo $to;
                                        }
                                        ?>" placeholder="<?php echo lang('date_to'); ?>" readonly="">
                                    </div>
                                    <div class="row"></div>
                                    <span class="help-block"></span> 
                                </div>
                                <div class="col-md-6 no-print">
                                    <button type="submit" name="submit" class="btn btn-info range_submit"><?php echo lang('submit'); ?></button>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </section>

        <?php foreach($hospital as $h){ ?>

        <h1><?php echo $h->name; ?></h1>

        <!-- <?php 
        if (!empty($payments[$h->id])) {
            $paid_number = 0;
            foreach ($payments[$h->id] as $payment) {
                $paid_number = $paid_number + 1;
            }
        }
        ?> -->

        <div class="row">
            <div class="col-lg-7">
                <section class="panel">
                    <header class="panel-heading">
                        Omzet Pelayanan
                    </header>
                    <table class="table table-striped table-advance table-hover" id="layanan<?php echo $h->id ?>">
                        <thead>
                            <tr>
                                <th>Nama Dokter</th>
                                <th>Jumlah Pasien</th>
                                <th class="hidden-phone">Nilai</th>
                                <th class="hidden-phone">Cash</th>
                                <th class="hidden-phone">Transfer</th>
                                <th class="hidden-phone">Diskon</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $category_id_for_report = array();
                            $doctor_id_for_report = array();
                            foreach ($payments[$h->id] as $payment) {
                                if(!in_array($payment->doctor,$doctor_id_for_report)){
                                    array_push($doctor_id_for_report,$payment->doctor);
                                }
                            }
                            ?>
                            <?php
                            foreach ($doctors as $doctor) {
                                $category_quantity = 0;
                                $count = 0;
                                
                                if (in_array($doctor->id, $doctor_id_for_report)) {
                                    ?>
                                    <tr class="">
                                        <td><?php echo $doctor->name ?></td>
                                        <td>
                                            <?php

                                            foreach ($payments[$h->id] as $paymentt) {
                                                if($paymentt->doctor == $doctor->id){
                                                    $count++;
                                                }
                                            }
                                            echo $count;
                                            ?>
                                        </td>
                                        <td><?php echo $settings->currency; ?> <?php
                                            foreach ($payments[$h->id] as $payment) {
                                                if($payment->doctor == $doctor->id){
                                                    $category_names_and_amounts = $payment->category_name;
                                                    $gross = null;
                                                    $category_names_and_amounts = explode(',', $category_names_and_amounts);
                                                    foreach ($category_names_and_amounts as $category_name_and_amount) {
                                                        $category_name = explode('*', $category_name_and_amount);
                                                        $amount_per_category[] = (int)$category_name[1] * (int)$category_name[3];
                                                    }
                                                }
                                            }
                                            if (!empty($amount_per_category)) {
                                                echo number_format(array_sum($amount_per_category),0,",",".")." ".$gross;
                                                $total_payment_by_category[$h->id][] = array_sum($amount_per_category);
                                            } else {
                                                echo '0';
                                            }

                                            $amount_per_category = NULL;
                                            ?>
                                        </td>
                                        <td><?php echo $settings->currency; ?> <?php
                                            foreach ($payments[$h->id] as $payment) {
                                                if($payment->doctor == $doctor->id){
                                                    $payment_cash[] = $payment->pembayaran_cash;
                                                }
                                            }
                                            if (!empty($payment_cash)) {
                                                echo number_format(array_sum($payment_cash),0,",",".");
                                                $total_payment_cash[$h->id][] = array_sum($payment_cash);
                                            } else {
                                                echo '0';
                                            }

                                            $payment_cash = NULL;
                                            ?>
                                        </td>
                                        <td><?php echo $settings->currency; ?> <?php
                                            foreach ($payments[$h->id] as $payment) {
                                                if($payment->doctor == $doctor->id){
                                                    $payment_transfer[] = $payment->pembayaran_transfer;
                                                }
                                            }
                                            if (!empty($payment_transfer)) {
                                                echo number_format(array_sum($payment_transfer),0,",",".");
                                                $total_payment_transfer[$h->id][] = array_sum($payment_transfer);
                                            } else {
                                                echo '0';
                                            }

                                            $payment_transfer = NULL;
                                            ?>
                                        </td>
                                        <td><?php echo $settings->currency; ?> <?php
                                            foreach ($payments[$h->id] as $payment) {
                                                if($payment->doctor == $doctor->id){
                                                    $discount_amount[$h->id][] = $payment->flat_discount;
                                                }
                                            }
                                            if (!empty($discount_amount[$h->id])) {
                                                echo number_format(array_sum($discount_amount[$h->id]),0,",",".");
                                                $total_discount_amount[$h->id][] = array_sum($discount_amount[$h->id]);
                                            } else {
                                                echo '0';
                                            }

                                            $discount_amount[$h->id] = NULL;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>

                    <table class="table table-striped table-advance table-hover" id="editable-sample2">
                        <thead>
                            <tr>
                                <th>Tipe</th>
                                <th>Jumlah Pasien</th>
                                <th class="hidden-phone" colspan=2>Nilai</th>

                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                                $terbayar_cash[$h->id] = 0;
                                foreach ($payments[$h->id] as $payment) {
                                    $cash[$h->id][] = (int)$payment->pembayaran_cash;
                                    $jumlah_cash[$h->id] = array_sum($cash[$h->id]);
                                    $terbayar_cash[$h->id] = $terbayar_cash[$h->id] + 1;
                                }

                                $terbayar_trf[$h->id] = 0;
                                foreach ($payments[$h->id] as $paymentt) {
                                    $transfer[$h->id][] = (int)$paymentt->pembayaran_transfer;
                                    $jumlah_transfer[$h->id] = array_sum($transfer[$h->id]);
                                    $terbayar_trf[$h->id] = $terbayar_trf[$h->id] + 1;
                                }
                            ?>
                            <tr>
                                <td>Cash</td>
                                <td><?php echo $terbayar_cash[$h->id] ?></td>
                                <td><?php echo 'Rp '.number_format($jumlah_cash[$h->id],0,",",".") ?></td>
                            </tr>
                            <tr>
                                <td>Transfer</td>
                                <td><?php echo $terbayar_trf[$h->id] ?></td>
                                <td><?php echo 'Rp '.number_format($jumlah_transfer[$h->id],0,",",".") ?></td>
                            </tr>
                        </tbody>

                        <tbody>

                            <tr>
                                <td><h3><?php echo lang('sub_total'); ?> </h3></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($total_payment_by_category[$h->id])) {
                                        echo number_format(array_sum($total_payment_by_category[$h->id]),0,",","."); 
                                    } else {
                                        echo '0';
                                    }
                                    ?> 
                                </td>
                            </tr>

                            <tr>
                                <td><h5>Total Diskon</h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments[$h->id])) {
                                        foreach ($payments[$h->id] as $payment) {
                                            $discount[$h->id][] = $payment->flat_discount;
                                        }
                                        if ($paid_number > 0) {
                                            echo number_format(array_sum($discount[$h->id]),0,",",".");
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><h5><i class="fa fa-money"></i> Omzet Tanpa Diskon</h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments[$h->id])) {
                                        if ($paid_number > 0) {
                                            $gross[$h->id] = array_sum($total_payment_by_category[$h->id]) - array_sum($discount);
                                            echo number_format($gross[$h->id],0,",",".");
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><h5>Omzet dengan Diskon</h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments[$h->id])) {
                                        foreach ($payments[$h->id] as $payment) {
                                            // $hospital_amount[$h->id][] = $payment->hospital_amount;
                                            $hospital_amount[$h->id][] = $payment->hospital_amount - $payment->flat_discount;
                                        }
                                        if ($paid_number > 0) {
                                            $hospital_amount[$h->id] = array_sum($hospital_amount[$h->id]);
                                            echo number_format($hospital_amount[$h->id],0,",",".");
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </div>


            <style>
                .billl{
                    background: #39B24F !important;
                }

                .due{
                    background: #39B1D1 !important;
                }
            </style>


            <div class="col-lg-5">
                <section class="panel">
                    <div class="weather-bg">
                        <div class="panel-body billl">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-money"></i>
                                    Total Omset Tanpa Diskon
                                </div>
                                <div class="col-xs-8">
                                    <div class="degree">
                                        <?php echo $settings->currency; ?>
                                        <?php
                                        if (empty($gross[$h->id])) {
                                            $gross = 0;
                                        }
                                        $gross_bill[$h->id] = $gross[$h->id];
                                        echo number_format($gross_bill[$h->id],0,",",".");
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="panel">
                    <div class="weather-bg">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-money"></i>
                                    Total Omset dengan Diskon
                                </div>
                                <div class="col-xs-8">
                                    <div class="degree">
                                        <?php echo $settings->currency; ?>
                                        <?php
                                        if (!empty($payments[$h->id])) {
                                            if ($paid_number > 0) {
                                                $gross[$h->id] = $hospital_amount[$h->id];
                                                echo number_format($gross[$h->id],0,",",".");
                                            }
                                        } elseif (!empty($payments[$h->id])) {
                                            if (($paid_number > 0)) {
                                                $gross[$h->id] = $hospital_amount[$h->id];
                                                echo number_format($gross[$h->id],0,",",".");
                                            }
                                        } else {
                                            echo '0';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section class="panel">
                    <div class="weather-bg">
                        <div class="panel-body billl">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-money"></i>
                                    Piutang
                                </div>
                                <div class="col-xs-8">
                                    <div class="degree">

                                        <?php echo $settings->currency; ?>
                                        <?php
                                        $deposited_amount = array();
                                        if (!empty($deposits[$h->id])) {
                                            foreach ($deposits[$h->id] as $deposit) {
                                                $deposited_amount[$h->id][] = $deposit->deposited_amount;
                                            }
                                            if ($paid_number > 0) {
                                                $deposited_amount[$h->id] = array_sum($deposited_amount[$h->id]);
                                                echo number_format($gross[$h->id] - $deposited_amount[$h->id],0,",",".");
                                            } else {
                                                echo '0';
                                            }
                                        } else {
                                            echo '0';
                                        }
                                        ?>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <?php
        if (!empty($payments_obat[$h->id])) {
            $paid_number_obat = 0;
            foreach ($payments_obat[$h->id] as $payment) {
                $paid_number_obat = $paid_number_obat + 1;
            }
        }
        ?>

        <div class="row">
            <div class="col-lg-7">
                <section class="panel">
                    <header class="panel-heading">
                        Omzet Obat
                    </header>
                    <table class="table table-striped table-advance table-hover" id="obat<?php echo $h->id ?>">
                        <thead>
                            <tr>
                                <th>Nama Obat</th>
                                <th>Qty</th>
                                <th>Harga Beli</th>
                                <th class="hidden-phone">Harga Jual</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($medicines[$h->id] as $medicine_name) {
                                foreach ($payments_obat[$h->id] as $payment_obat) {
                                    $categories_in_payment_obat = explode(',', $payment_obat->category_name);
                                    foreach ($categories_in_payment_obat as $category_in_payment_obat) {
                                        $category_id_obat = explode('*', $category_in_payment_obat);
                                        if ($category_id_obat[0] == $medicine_name->id) {
                                            $category_id_for_report_obat[] = $category_id_obat[0];
                                        }
                                    }
                                }
                            }
                            $category_id_for_reports_obat = array_unique($category_id_for_report_obat);
                            ?>

                            <?php
                            foreach ($medicines[$h->id] as $category) {

                                if (in_array($category->id, $category_id_for_reports_obat)) {
                                    ?>
                                    <tr class="">
                                        <td><?php echo $category->name ?></td>
                                        <?php
                                        foreach ($payments_obat[$h->id] as $payment_ob) {
                                            $category_names_and_amounts_obat = $payment_ob->category_name;
                                            $category_names_and_amounts_obat = explode(',', $category_names_and_amounts_obat);
                                            foreach ($category_names_and_amounts_obat as $category_name_and_amount_obat) {
                                                $category_name_obat = explode('*', $category_name_and_amount_obat);
                                                if (($category->id == $category_name_obat[0])) {
                                                    $amount_per_category_obat[$h->id][] = $category_name_obat[1] * $category_name_obat[2];
                                                    $cost_per_category_obat[$h->id][] = $category_name_obat[2] * $category_name_obat[3];
                                                    $quantity_obat[$h->id][] = $category_name_obat[2];
                                                }
                                            }
                                        }
                                        ?>
                                        <td>
                                            <?php
                                            if (!empty($quantity_obat[$h->id])) {
                                                echo array_sum($quantity_obat[$h->id]);
                                                $quantity_obat[$h->id][] = array_sum($quantity_obat[$h->id]);
                                            } else {
                                                echo '0';
                                            }

                                            $quantity_obat = NULL;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $settings->currency; ?> 
                                            <?php
                                            if (!empty($cost_per_category_obat[$h->id])) {
                                                echo number_format(array_sum($cost_per_category_obat[$h->id]), 0, ',', '.');
                                                $total_cost_by_category_obat[$h->id][] = array_sum($cost_per_category_obat[$h->id]);
                                            } else {
                                                echo '0';
                                            }

                                            $cost_per_category_obat[$h->id] = NULL;
                                            ?>
                                        </td>
                                        <td>
                                            <?php echo $settings->currency; ?> 
                                            <?php
                                            if (!empty($amount_per_category_obat[$h->id])) {
                                                echo number_format(array_sum($amount_per_category_obat[$h->id]), 0, ',', '.');
                                                $total_payment_by_category_obat[$h->id][] = array_sum($amount_per_category_obat[$h->id]);
                                            } else {
                                                echo '0';
                                            }

                                            $amount_per_category_obat[$h->id] = NULL;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>

                        </tbody>
                    </table>

                    <table class="table table-striped table-advance table-hover">
                        <tbody>
                            <tr>
                                <td><h3> Sub Total</h3></td>
                                <td></td>
                                <td>

                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($total_cost_by_category_obat[$h->id])) {
                                        echo number_format(array_sum($total_cost_by_category_obat[$h->id]), 0, ',', '.');
                                    } else {
                                        echo '0';
                                    }
                                    ?> 
                                </td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($total_payment_by_category_obat[$h->id])) {
                                        echo number_format(array_sum($total_payment_by_category_obat[$h->id]), 0, ',', '.');
                                    } else {
                                        echo '0';
                                    }
                                    ?> 
                                </td>
                            </tr>

                            <tr>
                                <td><h5> Total Diskon</h5></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments_obat[$h->id])) {
                                        foreach ($payments_obat[$h->id] as $payment_o) {
                                            $discount_obat[$h->id][] = $payment_o->flat_discount;
                                        }
                                        if ($paid_number_obat > 0) {
                                            echo number_format(array_sum($discount_obat[$h->id]), 0, ',', '.');
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><h5>Total Penjualan</h5></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments_obat[$h->id])) {
                                        if ($paid_number_obat > 0) {
                                            $gross_obat[$h->id] = array_sum($total_payment_by_category_obat[$h->id]) - array_sum($discount_obat[$h->id]) + array_sum($vat);
                                            echo number_format($gross_obat[$h->id], 0, ',', '.');
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                        </tbody>

                        <thead>
                            <tr>
                                <th colspan=2> Tipe</th>
                                <th> Jumlah Transaksi</th>
                                <th class="hidden-phone">Nilai</th>

                            </tr>
                        </thead>

                        <tbody>
                            <?php 
                                $terbayar_obat_cash[$h->id] = 0;
                                $terbayar_obat_trf[$h->id] = 0;
                                $jumlah_obat_cash[$h->id] = 0;
                                $jumlah_obat_transfer[$h->id] = 0;

                                foreach ($payments_obat[$h->id] as $payment_obatt) {
                                    $cash_obat[$h->id][] = (int)$payment_obatt->pembayaran_cash;
                                    $jumlah_obat_cash[$h->id] = array_sum($cash_obat[$h->id]);
                                    $terbayar_obat_cash[$h->id] = $terbayar_obat_cash[$h->id] + 1;
                                }

                                foreach ($payments_obat[$h->id] as $paymentt_obatt) {
                                    $transfer_obat[$h->id][] = (int)$paymentt_obatt->pembayaran_transfer;
                                    $jumlah_obat_transfer[$h->id] = array_sum($transfer_obat[$h->id]);
                                    $terbayar_obat_trf[$h->id] = $terbayar_obat_trf[$h->id] + 1;
                                }
                            ?>
                            <tr>
                                <td colspan=2>Cash</td>
                                <td><?php echo $terbayar_obat_cash[$h->id] ?></td>
                                <td><?php echo 'Rp '.number_format($jumlah_obat_cash[$h->id],0,",",".") ?></td>
                            </tr>
                            <tr>
                                <td colspan=2>Transfer</td>
                                <td><?php echo $terbayar_obat_trf[$h->id] ?></td>
                                <td><?php echo 'Rp '.number_format($jumlah_obat_transfer[$h->id],0,",",".") ?></td>
                            </tr>
                        </tbody>

                    </table>
                </section>
            </div>
            <div class="col-lg-5">
                <section class="panel">
                    <div class="weather-bg">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-money"></i>
                                    Omzet Obat
                                </div>
                                <div class="col-xs-8">
                                    <div class="degree">
                                        <?php echo $settings->currency; ?>
                                        <?php
                                        if (!empty($payments_obat[$h->id])) {
                                            if (($paid_number_obat > 0)) {
                                                if (!empty($gross_obat[$h->id])) {

                                                    echo number_format($gross_obat[$h->id], 0, ',', '.');
                                                } else {
                                                    $gross_obat[$h->id] = 0;
                                                    echo number_format($gross_obat[$h->id], 0, ',', '.');
                                                }
                                            }
                                        } else {
                                            echo '0';
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <div class="weather-bg">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-4">
                                    <i class="fa fa-money"></i>
                                    Total Omzet Pelayanan + Obat
                                </div>
                                <div class="col-xs-8">
                                    <div class="degree">
                                        <?php
                                            echo number_format($gross_obat[$h->id] + $gross[$h->id], 0, ',', '.');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>

        <?php } ?>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->

<script src="common/js/codearistos.min.js"></script>

<script>

    <?php foreach($hospital as $h){ ?>

        $(document).ready(function () {
            var table = $('#layanan<?php echo $h->id ?>').DataTable({
                responsive: true,
                "processing": true,
                scrollY: 300,
                scroller: {
                    loadingIndicator: true
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    'print',
                    // {
                    //     extend: 'excelHtml5',
                    //     exportOptions: {
                    //         columns: [0, 1, 2, 3, 4],
                    //     }
                    // },
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                iDisplayLength: 50,
                "order": [[0, "desc"]],
                "language": {
                    "lengthMenu": "_MENU_",
                    search: "_INPUT_",
                    "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
                }
            });
            table.buttons().container().appendTo('.custom_buttons');
        });

        $(document).ready(function () {
            var table = $('#obat<?php echo $h->id ?>').DataTable({
                responsive: true,
                "processing": true,
                scrollY: 300,
                scroller: {
                    loadingIndicator: true
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    {
                        extend: 'print',
                        exportOptions: {
                            columns: [0, 1, 2],
                        }
                    },
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                iDisplayLength: 50,
                "order": [[0, "desc"]],
                "language": {
                    "lengthMenu": "_MENU_",
                    search: "_INPUT_",
                    "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
                }
            });
            table.buttons().container().appendTo('.custom_buttons');
        });

    <?php } ?>

</script>