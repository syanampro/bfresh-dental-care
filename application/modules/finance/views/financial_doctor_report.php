<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <header class="panel-heading"> 
            <?php echo lang('financial_report'); ?> 
            <div class="col-md-1 pull-right">
                <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button>
            </div>
        </header>
        <div class="col-md-12">
            <div class="col-md-7 row">
                <section>
                    <form role="form" class="f_report" action="finance/financialDokterReport" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <div class="col-md-6">
                                <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                    <input type="text" class="form-control dpd1" name="date_from" value="<?php
                                    if (!empty($from)) {
                                        echo $from;
                                    }
                                    ?>" placeholder="<?php echo lang('date_from'); ?>" readonly="">
                                    <span class="input-group-addon"><?php echo lang('to'); ?></span>
                                    <input type="text" class="form-control dpd2" name="date_to" value="<?php
                                    if (!empty($to)) {
                                        echo $to;
                                    }
                                    ?>" placeholder="<?php echo lang('date_to'); ?>" readonly="">
                                </div>
                                <div class="row"></div>
                                <span class="help-block"></span> 
                            </div>
                            <div class="col-md-6 no-print">
                                <button type="submit" name="submit" class="btn btn-info range_submit"><?php echo lang('submit'); ?></button>
                            </div>
                        </div>
                    </form>
                </section>
            </div>
        </div>

        <?php
        if (!empty($payments)) {
            $paid_number = 0;
            foreach ($payments as $payment) {
                $paid_number = $paid_number + 1;
            }
        }
        ?>
        <div class="row">
            <div class="col-lg-7">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo lang('income'); ?> 
                    </header>
                    <table class="table table-striped table-advance table-hover">
                        <thead>
                            <tr>
                                <th> Dokter</th>
                                <th> Kunjungan Pasien</th>
                                <th class="hidden-phone"> Omzet</th>
                            </tr>
                        </thead>
                        <tbody>

                            <?php
                            $category_id_for_report = array();
                            $doctor_id_for_report = array();
                            foreach ($payments as $payment) {
                                if(!in_array($payment->doctor,$doctor_id_for_report)){
                                    array_push($doctor_id_for_report,$payment->doctor);
                                }
                            }
                            ?>

                            <?php
                            foreach ($doctors as $doctor) {
                                $category_quantity = 0;
                                $count = 0;
                                
                                if (in_array($doctor->id, $doctor_id_for_report)) {
                                    ?>
                                    <tr class="">
                                        <td><?php echo $doctor->name ?></td>
                                        <td>
                                            <?php
                                            foreach ($payments as $paymentt) {
                                                if($paymentt->doctor == $doctor->id){
                                                    $count++;
                                                }
                                            }
                                            echo $count;
                                            ?>
                                        </td>
                                        <td><?php echo $settings->currency; ?> <?php
                                            foreach ($payments as $payment) {
                                                if($payment->doctor == $doctor->id){
                                                    $category_names_and_amounts = $payment->category_name;
                                                    $category_names_and_amounts = explode(',', $category_names_and_amounts);
                                                    foreach ($category_names_and_amounts as $category_name_and_amount) {
                                                        $category_name = explode('*', $category_name_and_amount);
                                                        $amount_per_category[] = (int)$category_name[1] * (int)$category_name[3];
                                                    }
                                                }
                                            }
                                            if (!empty($amount_per_category)) {
                                                echo number_format(array_sum($amount_per_category),'0',',','.');
                                                $total_payment_by_category[] = array_sum($amount_per_category);
                                            } else {
                                                echo '0';
                                            }
                                            $amount_per_category = NULL;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <td><h3><?php echo lang('sub_total'); ?> </h3></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($total_payment_by_category)) {
                                        echo array_sum($total_payment_by_category);
                                    } else {
                                        echo '0';
                                    }
                                    ?> 
                                </td>
                            </tr>
                            <tr>
                                <td><h5><?php echo lang('total'); ?> <?php echo lang('discount'); ?></h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments)) {
                                        foreach ($payments as $payment) {
                                            $discount[] = $payment->flat_discount;
                                        }
                                        if ($paid_number > 0) {
                                            echo array_sum($discount);
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><h5><i class="fa fa-money"></i> Total</h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments)) {
                                        if ($paid_number > 0) {
                                            $gross = array_sum($total_payment_by_category) - array_sum($discount);
                                            echo $gross;
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <!-- <tr>
                                <td><h5><?php echo lang('total'); ?> <?php echo lang('hospital_amount'); ?></h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments)) {
                                        foreach ($payments as $payment) {
                                            $hospital_amount[] = $payment->hospital_amount - $payment->flat_discount;
                                        }
                                        if ($paid_number > 0) {
                                            $hospital_amount = array_sum($hospital_amount);
                                            echo $hospital_amount;
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><h5><?php echo lang('total'); ?> <?php echo lang('doctors_amount'); ?></h5></td>
                                <td></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($payments)) {
                                        foreach ($payments as $payment) {
                                            $doctor_amount[] = $payment->doctor_amount;
                                        }
                                        if ($paid_number > 0) {
                                            $gross_doctor_amount = array_sum($doctor_amount);
                                            echo $gross_doctor_amount;
                                        } else {
                                            echo '0';
                                        }
                                    } else {
                                        echo '0';
                                    }
                                    ?>
                                </td> -->
                            </tr>

                        </tbody>
                    </table>
                </section>
            </div>


            <style>
                .billl{
                    background: #39B24F !important;
                }

                .due{
                    background: #39B1D1 !important;
                }
            </style>
        </div>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
