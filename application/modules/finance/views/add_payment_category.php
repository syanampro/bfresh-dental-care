<!--sidebar end-->
<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-7">
            <header class="panel-heading">
                <?php
                if (!empty($category->id))
                    echo lang('edit_payment_category');
                else
                    echo lang('create_payment_procedure');
                ?>
            </header>
            <div class="panel-body">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <?php echo validation_errors(); ?>
                        <form role="form" action="finance/addPaymentCategory" class="clearfix" method="post" enctype="multipart/form-data">
                            <div class="form-group"> 
                                <label for="exampleInputEmail1"><?php echo lang('category'); ?> <?php echo lang('name'); ?></label>
                                <input type="text" class="form-control" name="category" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('category');
                                }
                                if (!empty($category->category)) {
                                    echo $category->category;
                                }
                                ?>' placeholder="">    
                            </div> 

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('description'); ?></label>
                                <input type="text" class="form-control" name="description" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('description');
                                }
                                if (!empty($category->description)) {
                                    echo $category->description;
                                }
                                ?>' placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('category'); ?> <?php echo lang('price'); ?></label>
                                <input type="text" class="form-control" name="c_price" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('c_price');
                                }
                                if (!empty($category->c_price)) {
                                    echo $category->c_price;
                                }
                                ?>' placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('doctors_commission'); ?> <?php echo lang('rate'); ?> (%)</label>
                                <input type="text" class="form-control" name="d_commission" id="exampleInputEmail1" value='<?php
                                if (!empty($setval)) {
                                    echo set_value('d_commission');
                                }
                                if (!empty($category->d_commission)) {
                                    echo $category->d_commission;
                                }
                                ?>' placeholder="">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('type'); ?></label>
                                <select class="form-control m-bot15" name="type" value=''>    
                                    <option value="2023konsultasi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023konsultasi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023konsultasi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Konsultasi</option>

                                    <option value="2023scalinggigi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023scalinggigi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023scalinggigi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Scaling Gigi</option>

                                    <option value="2023splinting" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023splinting') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023splinting') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Splinting</option>

                                    <option value="2023cabutgigi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023cabutgigi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023cabutgigi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Cabut Gigi</option>

                                    <option value="2023bedah" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023bedah') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023bedah') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Bedah</option>

                                    <option value="2023tambalgigi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023tambalgigi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023tambalgigi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Tambal Gigi</option>

                                    <option value="2023veneergigi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023veneergigi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023veneergigi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Veneer Gigi</option>

                                    <option value="2023bleaching" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023bleaching') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023bleaching') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Bleaching</option>

                                    <option value="2023behelgigi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023behelgigi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023behelgigi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Behel Gigi</option>

                                    <option value="2023gigitiruan" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023gigitiruan') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023gigitiruan') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Gigi Tiruan</option>

                                    <option value="2023saluranakar" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023saluranakar') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023saluranakar') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Saluran Akar</option>

                                    <option value="2023bedahminor" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == '2023bedahminor') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == '2023bedahminor') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> >Bedah Minor</option>

                                    <!-- <option value="garansi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'garansi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'garansi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Garansi (New)</option> -->

                                    <!-- <option value="extra" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'extra') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'extra') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Extra Treatment (New)</option>
                                     <option value="bedahmulut" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'bedahmulut') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'bedahmulut') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Bedah Mulut (New)</option>
                                    <option value="cekup" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'cekup') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'cekup') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Cek Up  (New)</option>  
                                    <option value="cetakalginat" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'cetakalginat') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'cetakalginat') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Cetak Alginat (New)</option>
                                    <option value="farmakologi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'farmakologi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'farmakologi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Farmakologi (New) </option>
                                    <option value="konservasii" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'konservasii') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'konservasii') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Konservasi (New)</option>
                                    <option value="konsultasi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'konsultasi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'konsultasi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Konsultasi (New) </option>
                                    <option value="oralhygiene" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'oralhygiene') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'oralhygiene') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Oral Hygiene (New) </option>
                                    <option value="orthodonsia" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'orthodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'orthodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Orthodonsia (New) </option>
                                    <option value="pedodonsia" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'pedodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'pedodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Pedodonsia (New) </option>
                                    <option value="periodonsia" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'periodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'periodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Periodonsia (New) </option>
                                    <option value="prostodonsia" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'prostodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'prostodonsia') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Prostodonsia (New) </option>     
                                    <option value="diagnostic" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'diagnostic') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'diagnostic') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > <?php echo lang('diagnostic_test'); ?> </option>
                                    <option value="periodonsi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'periodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'periodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Periodonsi</option>  
                                    <option value="eksodonsi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'eksodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'eksodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Eksodonsi</option>
                                    <option value="orthodonsi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'orthodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'orthodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Orthodonsi</option>  
                                    <option value="prosthodonsi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'prosthodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'prosthodonsi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Prosthodonsi </option>
                                    <option value="konservasi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'konservasi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'konservasi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Konservasi</option>    
                                    <option value="others" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'others') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'others') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > <?php echo lang('others'); ?> </option>
                                    <option value="registrasi" <?php
                                    if (!empty($setval)) {
                                        if (set_value('type') == 'registrasi') {
                                            echo 'selected';
                                        }
                                    }
                                    if (!empty($category->type)) {
                                        if ($category->type == 'registrasi') {
                                            echo 'selected';
                                        }
                                    }
                                    ?> > Registrasi </option> -->
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo lang('can_hide'); ?> </label>
                                <input type="checkbox" class="form-control" name="is_can_hide" value='1' <?php echo @$category->is_can_hide ? 'checked' : '' ?>>
                            </div>

                            <input type="hidden" name="id" value='<?php
                            if (!empty($category->id)) {
                                echo $category->id;
                            }
                            ?>'>

                            <div class="form-group col-md-12">
                                <button type="submit" name="submit" class="btn btn-info pull-right"><?php echo lang('submit'); ?></button>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->
