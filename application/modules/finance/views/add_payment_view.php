<section id="main-content">
    <section class="wrapper site-min-height">
        <section class="">
            <header class="panel-heading">
                <?php
                if (!empty($payment->id))
                    echo lang('edit_payment');
                else
                    echo lang('add_new_payment');
                ?>
            </header>
            <div class="">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div class="">
                            <section class="">
                                <div class="">
                                    <style> 
                                        .payment{
                                            padding-top: 10px;
                                            padding-bottom: 0px;
                                            border: none;

                                        }
                                        .pad_bot{
                                            padding-bottom: 5px;
                                        }  

                                        form{
                                            background: #f1f1f1;
                                            padding: 15px 0px;
                                        }

                                        .modal-body form{
                                            background: #fff;
                                            padding: 21px;
                                        }

                                        .remove{
                                            width: 20%;
                                            float: right;
                                            margin-bottom: 10px;
                                            padding: 10px;
                                            height: 39px;
                                            text-align: center;
                                            border-bottom: 1px solid #f1f1f1;
                                        }

                                        .remove1{
                                            width: 80%;
                                            float: left;
                                            margin-bottom: 10px;
                                            border-bottom: 1px solid #f1f1f1;
                                        }

                                        form input {
                                            border: none;
                                        }

                                        .pos_box_title{
                                            border: none;
                                        }

                                    </style>

                                    <form role="form" id="editPaymentForm" class="clearfix" action="finance/addPayment" method="post" enctype="multipart/form-data">

                                        <div class="col-md-5 row">

                                            <?php if(empty($payment->id)){ ?>
                                            <div class="col-md-12 payment pad_bot">
                                                <label for="exampleInputEmail1">Appointment</label>
                                                <select class="form-control m-bot15 js-example-basic-single" name="appointment_id">  
                                                    <option value=""><?php echo lang('select'); ?></option>
                                                    <?php foreach ($appointmentinput as $ap) { ?>
                                                        <option value="<?php echo $ap->id ?>"><?php echo $ap->id.' - '.$ap->name.' - '.$ap->namadokter; ?> </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <?php } else {?>
                                                <input type="hidden" value="<?php echo $payment->id ?>" name="app_id">
                                                <input type="hidden" value="<?php echo $payment->appointment_id ?>" name="appointment_id">
                                            <?php } ?>

                                            <div class="col-md-12 payment">
                                                <div class="form-group last"> 
                                                    <label for="exampleInputEmail1"> <?php echo lang('select'); ?></label>
                                                    <select name="category_name[]" id="" class="multi-select" multiple="" id="my_multi_select3" >
                                                        <?php foreach ($categoriesbaru as $category) { ?>
                                                            <option class="ooppttiioonn" data-canhide="<?php echo $category->is_can_hide; ?>" data-id="<?php echo $category->c_price; ?>" data-idd="<?php echo $category->id; ?>" data-cat_name="<?php echo $category->category; ?>" value="<?php echo $category->category; ?>" 

                                                                    <?php
                                                                    if (!empty($payment->category_name)) {
                                                                        $category_name = $payment->category_name;
                                                                        $category_name1 = explode(',', $category_name);
                                                                        foreach ($category_name1 as $category_name2) {
                                                                            $category_name3 = explode('*', $category_name2);
                                                                            if ($category_name3[0] == $category->id) {
                                                                                echo 'data-qtity=' . $category_name3[3];
                                                                                echo ' data-price=' . $category_name3[1];
                                                                                if(@$category_name3[4])
                                                                                    echo ' data-hideas=' . @$category_name3[4];
                                                                                else
                                                                                    echo ' data-hideas=""';
                                                                            }
                                                                        }
                                                                    }else{
                                                                        echo ' data-price="'.$category->c_price.'"';
                                                                        echo ' data-hideas=""';
                                                                    }
                                                                    ?>


                                                                    <?php
                                                                    if (!empty($payment->category_name)) {
                                                                        $category_name = $payment->category_name;
                                                                        $category_name1 = explode(',', $category_name);
                                                                        foreach ($category_name1 as $category_name2) {
                                                                            $category_name3 = explode('*', $category_name2);
                                                                            if ($category_name3[0] == $category->id) {
                                                                                echo 'selected';
                                                                            }
                                                                        }
                                                                    }
                                                                    ?>><?php echo $category->category; ?></option>
                                                                <?php } ?>
                                                    </select>
                                                </div>

                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="col-md-12 qfloww">
                                                <div class="row">
                                                <label class=" col-md-4"><?php echo lang('items') ?></label>
                                                <label class=" col-md-3"><?php echo lang('price') ?></label>
                                                <label class="col-md-2"><?php echo lang('qty') ?></label>
                                                <label class="col-md-3"><?php echo lang('hide_as') ?></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Sub Total</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="subtotal" id="subtotal" value='<?php
                                                    if (!empty($payment->amount)) {

                                                        echo $payment->amount;
                                                    }
                                                    ?>' placeholder=" " disabled>
                                                </div>

                                            </div>


                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Diskon<?php
                                                        if ($discount_type == 'percentage') {
                                                            echo ' (%)';
                                                        } 
                                                        ?> </label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="discount" id="dis_id" value='<?php
                                                    if (!empty($payment->discount)) {
                                                        $discount = explode('*', $payment->discount);
                                                        echo $discount[0];
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>' placeholder="">
                                                </div>

                                            </div>

                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Total</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="grsss" id="gross" value='<?php
                                                    if (!empty($payment->gross_total)) {

                                                        echo $payment->gross_total;
                                                    }
                                                    ?>' placeholder=" " disabled>
                                                </div>

                                            </div>


                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Catatan</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="remarks" id="" value='<?php
                                                    if (!empty($payment->remarks)) {

                                                        echo $payment->remarks;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?>' placeholder=" ">
                                                </div>

                                            </div> 

                                            <div class="col-md-12 payment right-six">
                                                <label for="exampleInputEmail1">Promat</label>
                                                <select class="form-control m-bot15 js-example-basic-single" name="promat" required>  
                                                    <option value="0" <?php echo (!empty($payment)) ? ($payment->promat == '0') ? "selected='selected'" : "" : "" ?>>Tidak</option>
                                                    <option value="1" <?php echo (!empty($payment)) ? ($payment->promat == '1') ? "selected='selected'" : "" : "" ?>>Ya</option>
                                                </select>
                                            </div>

                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Jumlah Pembayaran Cash</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="pembayaran_cash" id="pembayaran_cash" value='<?php
                                                    if (!empty($payment->pembayaran_cash)) {

                                                        echo $payment->pembayaran_cash;
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>' placeholder="0">
                                                </div>
                                            </div>

                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Jumlah Pembayaran Transfer</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="pembayaran_transfer" id="pembayaran_transfer" value='<?php
                                                    if (!empty($payment->pembayaran_transfer)) {

                                                        echo $payment->pembayaran_transfer;
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>' placeholder=" ">
                                                </div>
                                            </div>

                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Kekurangan</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="text" class="form-control pay_in" name="kekurangan" id="kekurangan" value='<?php
                                                    if (!empty($payment->kekurangan)) {

                                                        echo $payment->kekurangan;
                                                    } else {
                                                        echo '0';
                                                    }
                                                    ?>' placeholder=" ">
                                                </div>
                                            </div>

                                            <div class="col-md-12 payment right-six">
                                              <label for="exampleInputEmail1">Bukti TF</label>
                                              <input type="file" name="bukti_tf">
                                          </div>

                                            <?php if (!empty($payment)) { ?>
                                            <div class="col-md-12 payment right-six">
                                                <div class="payment_label"> 
                                                    <label for="exampleInputEmail1">Tanggal</label>
                                                </div>
                                                <div class=""> 
                                                    <input type="date" class="form-control pay_in" name="date_edit" value="<?php echo date('Y-m-d',$payment->date) ?>">
                                                </div>
                                            </div>
                                            <?php } ?>


                                            <?php
                                            if (!empty($payment)) {
                                                $deposits = $this->finance_model->getDepositByPaymentId($payment->id);
                                                $i = 1;
                                                foreach ($deposits as $deposit) {

                                                    if (empty($deposit->amount_received_id)) {
                                                        $i = $i + 1;
                                                        ?>
                                                        <div class="col-md-12 payment right-six">
                                                            <div class="payment_label"> 
                                                                <label for="exampleInputEmail1"><?php echo lang('deposit'); ?> <?php
                                                                    echo $i . '<br>';
                                                                    echo date('d/m/Y', $deposit->date);
                                                                    ?> 
                                                                </label>
                                                            </div>
                                                            <div class=""> 
                                                                <input type="text" class="form-control pay_in" name="deposit_edit_amount[]" id="amount_received" value='<?php echo $deposit->deposited_amount; ?>' <?php
                                                                if ($deposit->deposit_type == 'Card') {
                                                                    echo 'readonly';
                                                                }
                                                                ?>>
                                                                <input type="hidden" class="form-control pay_in" name="deposit_edit_id[]" id="amount_received" value='<?php echo $deposit->id; ?>' placeholder=" ">
                                                            </div>

                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>

                                            <div class="col-md-12 payment right-six">
                                                <div class="col-md-12 form-group">
                                                    <button type="submit" name="submit" id="submitBtn" class="btn btn-info pull-right row"><?php echo lang('submit'); ?></button>
                                                </div>
                                            </div>
                                        </div>

                                        <input type="hidden" name="id" value='<?php
                                        if (!empty($payment->id)) {
                                            echo $payment->id;
                                        }
                                        ?>'>
                                        <div class="row">
                                        </div>
                                        <div class="form-group">
                                        </div>

                                </div>
                                </form>

                        </div>
                        </section>
                    </div>
                </div>
            </div>
            </div>
        </section>
    </section>
</section>

<script src="common/js/codearistos.min.js"></script>

<script type="text/javascript">
    document.getElementById("editPaymentForm").addEventListener("submit", function(event) {
        document.getElementById("submitBtn").disabled = true;
    });
</script>

<script>
  $(document).ready(function() {
    $('#pembayaran_cash, #pembayaran_transfer, #kekurangan').on('keyup', function() {
      var val = $(this).val();
      val = val.replace(/\D/g, ''); // Remove non-digit characters
      val = val.replace(/\B(?=(\d{3})+(?!\d))/g, '.'); // Add new dots
      $(this).val(val);
    });
  });
</script>

<script>
  $(document).ready(function () {
    $('#pasien').select2({
      ajax: {
        url: "patient/tampilPasien",
        dataType: "json",
        type: "post",
        delay: 250,
        data: function (params) {
          return {
            cari: params.term,
          };
        },
        processResults: function (data) {
          console.log(data);
          return {
            results: data,
          };
        },
        cache: true,
      },
      placeholder: "Cari Pasien",
      minimumInputLength: 3,
    });
  });
</script>

<script>
  // Inisialisasi opsi untuk 'hide_as' pada select dropdown
  var opthide = '<option value=""><?php echo lang('choose') ?></option>';
  
  // Loop untuk menambahkan opsi kategori pada dropdown 'hide_as'
  <?php foreach ($categoriesbaru as $category) { ?>
    <?php if(!$category->is_can_hide): ?>
      opthide += '<option value="<?php echo $category->id ?>"><?php echo $category->category ?></option>';
    <?php endif; ?>
  <?php } ?>

  // Ketika dokumen telah siap
  $(document).ready(function () {
    var tot = 0;

    // Ketika item multi-select di-klik
    $(".ms-selected").click(function () {
      var idd = $(this).data('idd');
      // Hapus elemen terkait dari DOM saat diklik
      $('#id-div' + idd).remove();
      $('#idinput-' + idd).remove();
      $('#idinputprice-' + idd).remove();
      $('#categoryinput-' + idd).remove();
      $('#idselecthide-' + idd).remove();
    });

    // Loop untuk setiap opsi yang dipilih pada multi-select dropdown
    $.each($('select.multi-select option:selected'), function () {
      var curr_val = $(this).data('id');
      var idd = $(this).data('idd');
      var qtity = $(this).data('qtity');
      var price = $(this).data('price');
      var hideas = $(this).data('hideas');
      var canhide = $(this).data('canhide');

      var cat_name = $(this).data('cat_name');
      
      // Jika elemen belum ada dalam DOM, tambahkan
      if (!$('#idinput-' + idd).length) {
        if (!$('#id-div' + idd).length) {
          $("#editPaymentForm .qfloww").append('<div class="row" id="row'+idd+'"></div>');
          $("#row"+idd).append('<div class="col-md-4" id="id-div' + idd + '">  ' + $(this).data("cat_name") + '</div>');
        }

        var input3 = $('<input>').attr({
          type: 'text',
          class: "col-md-3",
          id: 'idinputprice-' + idd,
          name: 'price[]',
          value: parseFloat(price),
        }).appendTo("#row"+idd);

        var input2 = $('<input>').attr({
            type: 'text',
            class: "col-md-2",
            id: 'idinput-' + idd,
            name: 'quantity[]',
            value: parseFloat(qtity), // Konversi ke angka sebelum ditampilkan
            }).appendTo("#row" + idd);

        if (canhide) {
          $("#row"+idd).append('<select id="idselecthide-'+idd+'" name="hide_to['+idd+']" class="col-md-3">'+opthide+'</select>');
          $("#idselecthide-"+idd).val(hideas);
        } else {
          $("#row"+idd).append('<div class="col-md-3">&nbsp;<br></div>');
        }

        $('<input>').attr({
          type: 'hidden',
          class: "remove",
          id: 'categoryinput-' + idd,
          name: 'category_id[]',
          value: idd,
        }).appendTo("#row"+idd);
      }

      // Ketika nilai pada input berubah
      $('#idinput-' + idd + ', #idinputprice-' + idd).on('input', function () {
        var qty = 0;
        var total = 0;
        $.each($('select.multi-select option:selected'), function () {
          var id1 = $(this).data('idd');
          qty = $('#idinput-' + id1).val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
          ekokk = $('#idinputprice-' + id1).val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
          total = total + qty * ekokk;
        });

        tot = total;
        var discount = $('#dis_id').val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
        var gross = 0;

        if ('<?php echo $discount_type ?>' === 'flat') {
          gross = tot - discount;
        } else {
          gross = tot - tot * discount / 100;
        }

        $('#editPaymentForm').find('[name="subtotal"]').val(tot).end();
        $('#editPaymentForm').find('[name="grsss"]').val(gross);

        var amount_received = $('#amount_received').val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
        var change = amount_received - gross;
        $('#editPaymentForm').find('[name="change"]').val(change).end();
      });

      // Ketika nilai pada input diskon berubah
      $('#dis_id').on('input', function () {
        var val_dis = $('#dis_id').val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
        var amount = $('#subtotal').val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
        var ggggg = 0;

        if ('<?php echo $discount_type ?>' === 'flat') {
          ggggg = amount - val_dis;
        } else {
          ggggg = amount - amount * val_dis / 100;
        }

        $('#editPaymentForm').find('[name="grsss"]').val(ggggg);
        var amount_received = $('#amount_received').val().replace(/\D/g, ''); // Hapus semua karakter kecuali digit
        var change = amount_received - ggggg;
        $('#editPaymentForm').find('[name="change"]').val(change).end();
      });
    });
  });
</script>

<script>
  $(document).ready(function() {
    $('.multi-select').change(function() {
      var tot = 0;

      $(".ms-selected").click(function() {
        var idd = $(this).data('idd');
        $('#id-div' + idd).remove();
        $('#idinput-' + idd).remove();
        $('#idinputprice-' + idd).remove();
        $('#categoryinput-' + idd).remove();
        $('#idselecthide-' + idd).remove();
      });

      $.each($('select.multi-select option:selected'), function() {
        var idd = $(this).data('idd');
        var canhide = $(this).data('canhide');

        if (!$('#idinput-' + idd).length) {
          if (!$('#id-div' + idd).length) {
            $("#editPaymentForm .qfloww").append('<div class="row" id="row' + idd + '"></div>');
            $("#row" + idd).append('<div class="col-md-4" id="id-div' + idd + '">  ' + $(this).data("cat_name") + '</div>');
          }

          var input3 = $('<input>').attr({
            type: 'text',
            class: "col-md-3",
            id: 'idinputprice-' + idd,
            name: 'price[]',
            value: $(this).data('id'),
          }).appendTo("#row" + idd);

          var input2 = $('<input>').attr({
            type: 'text',
            class: "col-md-2",
            id: 'idinput-' + idd,
            name: 'quantity[]',
            value: '1',
          }).appendTo("#row" + idd);

          if (canhide) {
            $("#row" + idd).append('<select id="idselecthide-' + idd + '" name="hide_to[' + idd + ']" class="col-md-3">' + opthide + '</select>');
          } else {
            $("#row" + idd).append('<div class="col-md-3">&nbsp;<br></div>');
          }

          $('<input>').attr({
            type: 'hidden',
            class: "remove",
            id: 'categoryinput-' + idd,
            name: 'category_id[]',
            value: idd,
          }).appendTo("#row" + idd);
        }

        $('#idinput-' + idd + ', #idinputprice-' + idd).keyup(function() {
          var qty = 0;
          var total = 0;

          $.each($('select.multi-select option:selected'), function() {
            var id1 = $(this).data('idd');
            qty = $('#idinput-' + id1).val();
            ekokk = $('#idinputprice-' + id1).val();
            total = total + qty * ekokk;
          });

          tot = total;

          var discount = $('#dis_id').val();
          var gross = tot - discount;
          $('#editPaymentForm').find('[name="subtotal"]').val(tot).end();
          $('#editPaymentForm').find('[name="grsss"]').val(gross);

          var amount_received = $('#amount_received').val();
          var change = amount_received - gross;
          $('#editPaymentForm').find('[name="change"]').val(change).end();
        });

        var sub_total = $(this).data('id') * $('#idinput-' + idd).val();
        tot = tot + sub_total;
      });

      var discount = $('#dis_id').val();
      var gross = 0;

      <?php if ($discount_type == 'flat') { ?>
        gross = tot - discount;
      <?php } else { ?>
        gross = tot - tot * discount / 100;
      <?php } ?>

      $('#editPaymentForm').find('[name="subtotal"]').val(tot).end();
      $('#editPaymentForm').find('[name="grsss"]').val(gross);

      var amount_received = $('#amount_received').val();
      var change = amount_received - gross;
      $('#editPaymentForm').find('[name="change"]').val(change).end();
    });

    $('#dis_id').keyup(function() {
      var val_dis = 0;
      var amount = 0;
      var ggggg = 0;
      amount = $('#subtotal').val();
      val_dis = this.value;

      <?php if ($discount_type == 'flat') { ?>
        ggggg = amount - val_dis;
      <?php } else { ?>
        ggggg = amount - amount * val_dis / 100;
      <?php } ?>

      $('#editPaymentForm').find('[name="grsss"]').val(ggggg);
      var amount_received = $('#amount_received').val();
      var change = amount_received - ggggg;
      $('#editPaymentForm').find('[name="change"]').val(change).end();
    });
  });
</script>

