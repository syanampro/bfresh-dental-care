<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <header class="panel-heading"> 
            Jumlah Pasien
            <div class="col-md-1 pull-right">
                <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button>
            </div>
        </header>

        <section style="margin-bottom:130px;">
            <div class="col-md-7">
                <div class="col-md- row">
                    <section>
                        <form role="form" class="f_report" action="finance/JumlahPasienByDokter" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control dpd1" name="date_from" value="<?php
                                        if (!empty($from)) {
                                            echo $from;
                                        }
                                        ?>" placeholder="<?php echo lang('date_from'); ?>" readonly="">
                                        <span class="input-group-addon"><?php echo lang('to'); ?></span>
                                        <input type="text" class="form-control dpd2" name="date_to" value="<?php
                                        if (!empty($to)) {
                                            echo $to;
                                        }
                                        ?>" placeholder="<?php echo lang('date_to'); ?>" readonly="">
                                    </div>
                                    <div class="row"></div>
                                    <span class="help-block"></span> 
                                </div>
                                <div class="col-md-6 no-print">
                                    <button type="submit" name="submit" class="btn btn-info range_submit"><?php echo lang('submit'); ?></button>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </section>

        <h1>Total Semua</h1>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <table class="table table-striped table-advance table-hover">
                        <thead>
                            <tr>
                                <th>Nama Dokter</th>
                                <th>Jumlah Pasien</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $category_id_for_report = array();
                            $doctor_id_for_report = array();
                            foreach ($appointmentstotal as $appointment) {
                                if(!in_array($appointment->doctor,$doctor_id_for_report)){
                                    array_push($doctor_id_for_report,$appointment->doctor);
                                }
                            }
                            ?>
                            <?php
                            foreach ($doctors as $doctor) {
                                $category_quantity = 0;
                                $count = 0;
                                
                                if (in_array($doctor->id, $doctor_id_for_report)) {
                                    ?>
                                    <tr class="">
                                        <td><?php echo $doctor->name ?></td>
                                        <td>
                                            <?php

                                            foreach ($appointmentstotal as $appointmentt) {
                                                if($appointmentt->doctor == $doctor->id){
                                                    $count++;
                                                }
                                            }
                                            echo $count;
                                            $total_appointment_by_categoryy[] = $count;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <td><h3>Total </h3></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($total_appointment_by_categoryy)) {
                                        echo number_format(array_sum($total_appointment_by_categoryy),0,",","."); 
                                    } else {
                                        echo '0';
                                    }
                                    ?> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>
        

        <?php foreach($hospital as $h){ ?>

        <h1><?php echo $h->name; ?></h1>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <table class="table table-striped table-advance table-hover">
                        <thead>
                            <tr>
                                <th>Nama Dokter</th>
                                <th>Jumlah Pasien</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $category_id_for_report = array();
                            $doctor_id_for_report = array();
                            foreach ($appointments[$h->id] as $appointment) {
                                if(!in_array($appointment->doctor,$doctor_id_for_report)){
                                    array_push($doctor_id_for_report,$appointment->doctor);
                                }
                            }
                            ?>
                            <?php
                            foreach ($doctors as $doctor) {
                                $category_quantity = 0;
                                $count = 0;
                                
                                if (in_array($doctor->id, $doctor_id_for_report)) {
                                    ?>
                                    <tr class="">
                                        <td><?php echo $doctor->name ?></td>
                                        <td>
                                            <?php

                                            foreach ($appointments[$h->id] as $appointmentt) {
                                                if($appointmentt->doctor == $doctor->id){
                                                    $count++;
                                                }
                                            }
                                            echo $count;
                                            $total_appointment_by_category[$h->id][] = $count;
                                            ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                        <tbody>
                            <tr>
                                <td><h3>Total </h3></td>
                                <td>
                                    <?php echo $settings->currency; ?>
                                    <?php
                                    if (!empty($total_appointment_by_category[$h->id])) {
                                        echo number_format(array_sum($total_appointment_by_category[$h->id]),0,",","."); 
                                    } else {
                                        echo '0';
                                    }
                                    ?> 
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </div>
        </div>

        <?php } ?>
        
    </section>
</section>