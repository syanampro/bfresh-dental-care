<!--main content start-->
<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <header class="panel-heading"> 
            Jumlah Omset By Perawatan
            <div class="col-md-1 pull-right">
                <button class="btn btn-info green no-print pull-right" onclick="javascript:window.print();"><?php echo lang('print'); ?></button>
            </div>
        </header>

        <section style="margin-bottom:130px;">
            <div class="col-md-7">
                <div class="col-md- row">
                    <section>
                        <form role="form" class="f_report" action="finance/JumlahOmsetByPerawatanByDate" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                                        <input type="text" class="form-control dpd1" name="date_from" value="<?php
                                        if (!empty($from)) {
                                            echo $from;
                                        }
                                        ?>" placeholder="<?php echo lang('date_from'); ?>" readonly="">
                                        <span class="input-group-addon"><?php echo lang('to'); ?></span>
                                        <input type="text" class="form-control dpd2" name="date_to" value="<?php
                                        if (!empty($to)) {
                                            echo $to;
                                        }
                                        ?>" placeholder="<?php echo lang('date_to'); ?>" readonly="">
                                    </div>
                                    <div class="row"></div>
                                    <span class="help-block"></span> 
                                </div>
                                <div class="col-md-6 no-print">
                                    <button type="submit" name="submit" class="btn btn-info range_submit"><?php echo lang('submit'); ?></button>
                                </div>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
        </section>

        <h1>Total Semua By Kategori</h1>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <table class="table table-striped table-advance table-hover" id="datatable">
                        <thead>
                            <tr>
                                <th>Perawatan</th>
                                <th>Jumlah</th>
                                <th>Omset</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // Array untuk mengumpulkan data berdasarkan id
                            $sums = [];
                            $totalOmset = 0; // Total omset
                            $totalJumlah = 0; // Total jumlah

                            foreach ($paymentpusat as $payment) {
                                $items = explode(",", $payment->category_name);
                                foreach ($items as $item) {
                                    $result = explode("*", $item);
                                    if (count($result) >= 4) {
                                        $category_id = $result[0]; // Nilai ID kategori dari $result[0]

                                        // Query untuk mendapatkan nama kategori dari database
                                        $this->db->select('*');
                                        $this->db->where('id', $category_id);
                                        $query = $this->db->get('payment_category');

                                        if ($query->num_rows() > 0) {
                                            $category = $query->row();
                                            if($category->type == '2023veneergigi' || $category->type == '2023tambalgigi' || $category->type == '2023splinting' || $category->type == '2023scalinggigi' || $category->type == '2023saluranakar' || $category->type == '2023konsultasi' || $category->type == '2023cabutgigi'){
                                                $category_name = 'Perawatan Umum';
                                            } else if($category->type == '2023bedahminor' || $category->type == '2023bedah'){
                                                $category_name = 'Perawatan Odontek';
                                            } else if($category->type == '2023bleaching'){
                                                $category_name = 'Perawatan Bleaching';
                                            } else if($category->type == '2023gigitiruan'){
                                                $category_name = 'Perawatan Gigi Palsu';
                                            } else if($category->type == '2023behelgigi'){
                                                $category_name = 'Perawatan Behel';
                                            }

                                            // Menambahkan atau mengupdate jumlah berdasarkan kategori
                                            if (isset($sums[$category_name])) {
                                                $sums[$category_name]['jumlah']++;
                                                $sums[$category_name]['omset'] += $result[1] * $result[3];
                                            } else {
                                                $sums[$category_name] = [
                                                    'jumlah' => 1,
                                                    'omset' => $result[1] * $result[3]
                                                ];
                                            }

                                            // Menambahkan ke total omset
                                            $totalOmset += $result[1] * $result[3];
                                            $totalJumlah++;
                                        }
                                    }
                                }
                            }

                            // Menampilkan hasil pengumpulan
                            foreach ($sums as $category_name => $data) {
                                ?>
                                <tr>
                                    <td><?php echo $category_name ?></td>
                                    <td><?php echo $data['jumlah'] ?></td>
                                    <td><?php echo 'Rp ' . number_format($data['omset'], 0, ',', '.') ?></td>
                                </tr>
                                <?php
                            }

                            // Menampilkan total omset dan total jumlah
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Total</strong></td>
                                <td><strong><?php echo $totalJumlah ?></strong></td>
                                <td><strong><?php echo 'Rp ' . number_format($totalOmset, 0, ',', '.') ?></strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </section>
            </div>
        </div>

        <h1>Total Semua</h1>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <table class="table table-striped table-advance table-hover" id="datatable2">
                        <thead>
                            <tr>
                                <th>Perawatan</th>
                                <th>Jumlah</th>
                                <th>Omset</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // Array untuk mengumpulkan data berdasarkan id
                            $sums = [];
                            $totalOmset = 0; // Total omset
                            $totalJumlah = 0; // Total jumlah

                            foreach ($paymentpusat as $payment) {
                                $items = explode(",", $payment->category_name);
                                foreach ($items as $item) {
                                    $result = explode("*", $item);
                                    if (count($result) >= 4) {
                                        $category_id = $result[0]; // Nilai ID kategori dari $result[0]

                                        // Query untuk mendapatkan nama kategori dari database
                                        $this->db->select('category');
                                        $this->db->where('id', $category_id);
                                        $query = $this->db->get('payment_category');

                                        if ($query->num_rows() > 0) {
                                            $category = $query->row();
                                            $category_name = $category->category;

                                            // Menambahkan atau mengupdate jumlah berdasarkan kategori
                                            if (isset($sums[$category_name])) {
                                                $sums[$category_name]['jumlah']++;
                                                $sums[$category_name]['omset'] += $result[1] * $result[3];
                                            } else {
                                                $sums[$category_name] = [
                                                    'jumlah' => 1,
                                                    'omset' => $result[1] * $result[3]
                                                ];
                                            }

                                            // Menambahkan ke total omset
                                            $totalOmset += $result[1] * $result[3];
                                            $totalJumlah++;
                                        }
                                    }
                                }
                            }

                            // Menampilkan hasil pengumpulan
                            foreach ($sums as $category_name => $data) {
                                ?>
                                <tr>
                                    <td><?php echo $category_name ?></td>
                                    <td><?php echo $data['jumlah'] ?></td>
                                    <td><?php echo 'Rp ' . number_format($data['omset'], 0, ',', '.') ?></td>
                                </tr>
                                <?php
                            }

                            // Menampilkan total omset dan total jumlah
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Total</strong></td>
                                <td><strong><?php echo $totalJumlah ?></strong></td>
                                <td><strong><?php echo 'Rp ' . number_format($totalOmset, 0, ',', '.') ?></strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </section>
            </div>
        </div>
        
        <?php foreach($hospital as $h){ ?>

            <h1><?php echo $h->name.' By Kategori'; ?></h1>

            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <table class="table table-striped table-advance table-hover" id="<?php echo 'kategori'.$h->id ?>">
                            <thead>
                                <tr>
                                    <th>Perawatan</th>
                                    <th>Jumlah</th>
                                    <th>Omset</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // Array untuk mengumpulkan data berdasarkan id
                                $sums = [];
                                $totalOmset = 0; // Total omset
                                $totalJumlah = 0; // Total jumlah

                                foreach ($paymentcabang[$h->id] as $payment) {
                                    $items = explode(",", $payment->category_name);
                                    foreach ($items as $item) {
                                        $result = explode("*", $item);
                                        if (count($result) >= 4) {
                                            $category_id = $result[0]; // Nilai ID kategori dari $result[0]

                                            // Query untuk mendapatkan nama kategori dari database
                                            $this->db->select('*');
                                            $this->db->where('id', $category_id);
                                            $query = $this->db->get('payment_category');

                                            if ($query->num_rows() > 0) {
                                                $category = $query->row();
                                                if($category->type == '2023veneergigi' || $category->type == '2023tambalgigi' || $category->type == '2023splinting' || $category->type == '2023scalinggigi' || $category->type == '2023saluranakar' || $category->type == '2023konsultasi' || $category->type == '2023cabutgigi'){
                                                    $category_name = 'Perawatan Umum';
                                                } else if($category->type == '2023bedahminor' || $category->type == '2023bedah'){
                                                    $category_name = 'Perawatan Odontek';
                                                } else if($category->type == '2023bleaching'){
                                                    $category_name = 'Perawatan Bleaching';
                                                } else if($category->type == '2023gigitiruan'){
                                                    $category_name = 'Perawatan Gigi Palsu';
                                                } else if($category->type == '2023behelgigi'){
                                                    $category_name = 'Perawatan Behel';
                                                }

                                                // Menambahkan atau mengupdate jumlah berdasarkan kategori
                                                if (isset($sums[$category_name])) {
                                                    $sums[$category_name]['jumlah']++;
                                                    $sums[$category_name]['omset'] += $result[1] * $result[3];
                                                } else {
                                                    $sums[$category_name] = [
                                                        'jumlah' => 1,
                                                        'omset' => $result[1] * $result[3]
                                                    ];
                                                }

                                                // Menambahkan ke total omset
                                                $totalOmset += $result[1] * $result[3];
                                                $totalJumlah++;
                                            }
                                        }
                                    }
                                }

                                // Menampilkan hasil pengumpulan
                                foreach ($sums as $category_name => $data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $category_name ?></td>
                                        <td><?php echo $data['jumlah'] ?></td>
                                        <td><?php echo 'Rp ' . number_format($data['omset'], 0, ',', '.') ?></td>
                                    </tr>
                                    <?php
                                }

                                // Menampilkan total omset dan total jumlah
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <td><strong>Total</strong></td>
                                    <td><strong><?php echo $totalJumlah ?></strong></td>
                                    <td><strong><?php echo 'Rp ' . number_format($totalOmset, 0, ',', '.') ?></strong></td>
                                </tr>
                            </tfoot>
                        </table>
                    </section>
                </div>
            </div>

        <h1><?php echo $h->name; ?></h1>

        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <table class="table table-striped table-advance table-hover" id="<?php echo 'nonkategori'.$h->id ?>">
                        <thead>
                            <tr>
                                <th>Perawatan</th>
                                <th>Jumlah</th>
                                <th>Omset</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            // Array untuk mengumpulkan data berdasarkan id
                            $sums = [];
                            $totalOmset = 0; // Total omset
                            $totalJumlah = 0; // Total jumlah

                            foreach ($paymentcabang[$h->id] as $payment) {
                                $items = explode(",", $payment->category_name);
                                foreach ($items as $item) {
                                    $result = explode("*", $item);
                                    if (count($result) >= 4) {
                                        $category_id = $result[0]; // Nilai ID kategori dari $result[0]

                                        // Query untuk mendapatkan nama kategori dari database
                                        $this->db->select('category');
                                        $this->db->where('id', $category_id);
                                        $query = $this->db->get('payment_category');

                                        if ($query->num_rows() > 0) {
                                            $category = $query->row();
                                            $category_name = $category->category;

                                            // Menambahkan atau mengupdate jumlah berdasarkan kategori
                                            if (isset($sums[$category_name])) {
                                                $sums[$category_name]['jumlah']++;
                                                $sums[$category_name]['omset'] += $result[1] * $result[3];
                                            } else {
                                                $sums[$category_name] = [
                                                    'jumlah' => 1,
                                                    'omset' => $result[1] * $result[3]
                                                ];
                                            }

                                            // Menambahkan ke total omset
                                            $totalOmset += $result[1] * $result[3];
                                            $totalJumlah++;
                                        }
                                    }
                                }
                            }

                            // Menampilkan hasil pengumpulan
                            foreach ($sums as $category_name => $data) {
                                ?>
                                <tr>
                                    <td><?php echo $category_name ?></td>
                                    <td><?php echo $data['jumlah'] ?></td>
                                    <td><?php echo 'Rp ' . number_format($data['omset'], 0, ',', '.') ?></td>
                                </tr>
                                <?php
                            }

                            // Menampilkan total omset dan total jumlah
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td><strong>Total</strong></td>
                                <td><strong><?php echo $totalJumlah ?></strong></td>
                                <td><strong><?php echo 'Rp ' . number_format($totalOmset, 0, ',', '.') ?></strong></td>
                            </tr>
                        </tfoot>
                    </table>
                </section>
            </div>
        </div>

        <?php } ?>
        
    </section>
</section>

<script src="common/js/codearistos.min.js"></script>

<script>
    $(document).ready(function () {
        var table = $('#datatable').DataTable({
            responsive: true,
            "processing": true,
            scroller: {
                loadingIndicator: true
            },
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print',
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 10,
            "order": [[0, "desc"]],
            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            }
        });
        table.buttons().container().appendTo('.custom_buttons');
    });

    $(document).ready(function () {
        var tablee = $('#datatable2').DataTable({
            responsive: true,
            "processing": true,
            scroller: {
                loadingIndicator: true
            },
            dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                'copyHtml5',
                'excelHtml5',
                'csvHtml5',
                'pdfHtml5',
                'print',
            ],
            aLengthMenu: [
                [10, 25, 50, 100, -1],
                [10, 25, 50, 100, "All"]
            ],
            iDisplayLength: 10,
            "order": [[0, "desc"]],
            "language": {
                "lengthMenu": "_MENU_",
                search: "_INPUT_",
                "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
            }
        });
        tablee.buttons().container().appendTo('.custom_buttons');
    });

    <?php foreach($hospital as $h){ ?>

        $(document).ready(function () {
            var e = $('#<?php echo 'kategori'.$h->id ?>').DataTable({
                responsive: true,
                "processing": true,
                scroller: {
                    loadingIndicator: true
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    'print',
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                iDisplayLength: 10,
                "order": [[0, "desc"]],
                "language": {
                    "lengthMenu": "_MENU_",
                    search: "_INPUT_",
                    "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
                }
            });
            e.buttons().container().appendTo('.custom_buttons');
        });

        $(document).ready(function () {
            var f = $('#<?php echo 'nonkategori'.$h->id ?>').DataTable({
                responsive: true,
                "processing": true,
                scroller: {
                    loadingIndicator: true
                },
                dom: "<'row'<'col-sm-3'l><'col-sm-5 text-center'B><'col-sm-4'f>>" +
                        "<'row'<'col-sm-12'tr>>" +
                        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5',
                    'print',
                ],
                aLengthMenu: [
                    [10, 25, 50, 100, -1],
                    [10, 25, 50, 100, "All"]
                ],
                iDisplayLength: 10,
                "order": [[0, "desc"]],
                "language": {
                    "lengthMenu": "_MENU_",
                    search: "_INPUT_",
                    "url": "common/assets/DataTables/languages/<?php echo $this->language; ?>.json" 
                }
            });
            f.buttons().container().appendTo('.custom_buttons');
        });
    <?php } ?>
</script>