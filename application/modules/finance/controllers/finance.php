<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once 'vendor/autoload.php';
use Twilio\Rest\Client;

class Finance extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('finance_model');
        $this->load->model('pharmacy_model');
        $this->load->model('appointment/appointment_model');
        $this->load->model('doctor/doctor_model');
        $this->load->model('medicine/medicine_model');
        $this->load->model('patient/patient_model');
        $this->load->model('finance/pharmacy_model');
        $this->load->model('accountant/accountant_model');
        $this->load->model('receptionist/receptionist_model');
        $this->load->module('paypal');

        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        if (!$this->ion_auth->in_group(array('superadmin','admin', 'Accountant', 'Receptionist', 'Nurse', 'Laboratorist', 'Doctor'))) {
            redirect('home/permission');
        }
    }

    function financialReportAdmin() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }

        $data = array();
        $data['payment_categories'] = $this->finance_model->getPaymentCategory();
        $data['doctors'] = $this->doctor_model->getDoctorAdmin();
        $data['expense_categories'] = $this->finance_model->getExpenseCategorySuperadmin();

        function getHospital() {
            $query = $this->db->get('hospital');
            return $query->result();
        }

        $data['hospital'] = $this->hospital_model->getHospital();

        foreach($data['hospital'] as $h){
            $data['payments'][$h->id] = $this->finance_model->getPaymentByDateSuperadmin($date_from, $date_to, $h->id);
            $data['payments_transfer'][$h->id] = $this->finance_model->getPaymentByDateTransferSuperadmin($date_from, $date_to, $h->id);
            $data['payments_cash'][$h->id] = $this->finance_model->getPaymentByDateCashSuperadmin($date_from, $date_to, $h->id);
            $data['ot_payments'][$h->id] = $this->finance_model->getOtPaymentByDate($date_from, $date_to);
            $data['deposits'][$h->id] = $this->finance_model->getDepositsByDateAdmin($date_from, $date_to, $h->id);
            $data['expenses'][$h->id] = $this->finance_model->getExpenseByDateAdmin($date_from, $date_to, $h->id);

            $data['medicines'][$h->id] = $this->medicine_model->getMedicineAdmin($h->id);
            $data['payments_obat'][$h->id] = $this->pharmacy_model->getPaymentByDateAdmin($date_from, $date_to, $h->id);
        }
 
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('financial_report_superadmin', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function JumlahPasienByDokter() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }

        $data = array();
        $data['payment_categories'] = $this->finance_model->getPaymentCategory();
        $data['doctors'] = $this->doctor_model->getDoctorAdmin();
        $data['expense_categories'] = $this->finance_model->getExpenseCategorySuperadmin();

        function getHospital() {
            $query = $this->db->get('hospital');
            return $query->result();
        }

        $data['hospital'] = $this->hospital_model->getHospital();

        foreach($data['hospital'] as $h){
            $data['appointments'][$h->id] = $this->finance_model->getAppointmentByDateSuperadmin($date_from, $date_to, $h->id);
        }

        $data['appointmentstotal'] = $this->finance_model->getAppointmentByDateSuperadminTotal($date_from, $date_to);
 
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('jumlah_pasien_by_dokter', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function JumlahOmsetByPerawatanByDate() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }

        $data = array();
        $data['doctors'] = $this->doctor_model->getDoctorAdmin();

        function getHospital() {
            $query = $this->db->get('hospital');
            return $query->result();
        }

        $data['hospital'] = $this->hospital_model->getHospital();

        foreach($data['hospital'] as $h){
            $data['paymentcabang'][$h->id] = $this->finance_model->getOmsetByPerawatanCabangByDate($date_from, $date_to, $h->id);
        }
        $data['paymentpusat'] = $this->finance_model->getOmsetByPerawatanByDate($date_from, $date_to);
 
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('jumlah_omset_by_perawatan_by_date', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    public function index() {

        redirect('finance/financial_report');
    }

    public function payment() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }

        $data['settings'] = $this->settings_model->getSettings();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('payment', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function amountDistribution() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['settings'] = $this->settings_model->getSettings();
        $data['payments'] = $this->finance_model->getPayment();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('amount_distribution', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addPaymentView() {
        $data = array();
        $data['appointmentinput'] = $this->appointment_model->AppointmentInputPembayaran();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->finance_model->getPaymentCategory();
        $data['categoriesbaru'] = $this->finance_model->getPaymentCategoryBaru();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctorAppKhusus();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_payment_view', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addPaymentViewNonPasien() {
        $data = array();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->finance_model->getPaymentCategory();
        $data['categoriesbaru'] = $this->finance_model->getPaymentCategoryBaru();
        $data['patients'] = $this->patient_model->getPatient();
        $data['doctors'] = $this->doctor_model->getDoctorAppKhususCS();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_payment_view_nonpasien', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addPayment() {
        $id = $this->input->post('app_id');
        $date_edit = strtotime($this->input->post('date_edit'));
        $appointment_id = $this->input->post('appointment_id');
        $promat = $this->input->post('promat');
        $pembayaran_transfer = str_replace('.', '',($this->input->post('pembayaran_transfer')));
        $pembayaran_cash = str_replace('.', '',($this->input->post('pembayaran_cash')));
        $kekurangan = str_replace('.', '',($this->input->post('kekurangan')));
        $appointment = $this->appointment_model->getAppointmentKhusus($appointment_id);
        $patient = $appointment->patient;
        $item_selected = array();
        $price = array();
        $hide_to = array();
        $quantity = array();
        $category_selected = array();
        $category_selected = $this->input->post('category_name');
        $item_selected = $this->input->post('category_id');
        $quantity = $this->input->post('quantity');
        $price = $this->input->post('price');
        $hide_to = $this->input->post('hide_to');
        $remarks = $this->input->post('remarks');
        $diagnosa = $this->input->post('diagnosa');
        $terapi = $this->input->post('terapi');
        $resep = $this->input->post('resep');

        $pharmacy_payment_id = $this->input->post('pharmacy_payment_id');
        if(!$pharmacy_payment_id)
            $pharmacy_payment_id = null;

        if (empty($item_selected)) {
            $this->session->set_flashdata('feedback', 'Pilih Layanan Dulu');
            $this->session->set_flashdata('feedback_type', 'error');
            redirect('finance/addPaymentView');
        } else {
            $item_quantity_array = array();
            $item_quantity_array = array_combine($item_selected, $quantity);
            $item_price_array = array_combine($item_selected, $price);
        }
        $cat_and_price = array();
        if (!empty($item_quantity_array)) {
            foreach ($item_quantity_array as $key => $value) {
                $current_item = $this->finance_model->getPaymentCategoryById($key);
                // $category_price = $current_item->c_price;
                $category_price = $item_price_array[$key];
                $category_hideto = @$hide_to[$key] ? '*'.$hide_to[$key] : '';
                $category_type = $current_item->type;
                $qty = $value;
                $cat_and_price[] = $key . '*' . $category_price . '*' . $category_type . '*' . $qty.$category_hideto;
                $amount_by_category[] = $category_price * $qty;
            }
            $category_name = implode(',', $cat_and_price);
        } else {
            $this->session->set_flashdata('feedback', 'Kolom Wajib Diisi');
            $this->session->set_flashdata('feedback_type', 'error');
            redirect('finance/addPaymentView');
        }

        $add_date = date('m/d/y');
        $doctor = $appointment->doctor;
        $date = time();
        $date_string = date('d-m-y', $date);
        $discount = str_replace('.', '',($this->input->post('discount')));
        if (empty($discount)) {
            $discount = 0;
        }

        if((int)$pembayaran_transfer + (int)$pembayaran_cash != (int)array_sum($amount_by_category) - (int)$discount ){
            $this->session->set_flashdata('feedback', 'Terjadi Selisih Antara Subtotal & Nominal Pembayaran');
            $this->session->set_flashdata('feedback_type', 'error');
            redirect('finance/addPaymentView');
        }

        $amount_received = str_replace('.', '',($this->input->post('pembayaran_transfer'))) + str_replace('.', '',($this->input->post('pembayaran_cash')));

        $deposit_type = $this->input->post('deposit_type');
        $user = $this->ion_auth->get_user_id();

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

            if (!empty($p_name)) {

                $limit = $this->patient_model->getLimit();
                if ($limit <= 0) {
                    $this->session->set_flashdata('feedback', lang('patient_limit_exceed'));
                    redirect('patient');
                }

                $data_p = array(
                    'patient_id' => $patient_id,
                    'name' => $p_name,
                    'email' => $p_email,
                    'phone' => $p_phone,
                    'sex' => $p_gender,
                    'age' => $p_age,
                    'add_date' => $add_date,
                    'how_added' => 'from_pos'
                );
                $username = $this->input->post('p_name');
                if ($this->ion_auth->email_check($p_email)) {
                    $this->session->set_flashdata('feedback', 'This Email Address Is Already Registered');
                } else {
                    $dfg = 5;
                    $this->ion_auth->register($username, $password, $p_email, $dfg);
                    $ion_user_id = $this->db->get_where('users', array('email' => $p_email))->row()->id;
                    $this->patient_model->insertPatient($data_p);
                    $patient_user_id = $this->db->get_where('patient', array('email' => $p_email))->row()->id;
                    $id_info = array('ion_user_id' => $ion_user_id);
                    $this->patient_model->updatePatient($patient_user_id, $id_info);
                    $this->hospital_model->addHospitalIdToIonUser($ion_user_id, $this->hospital_id);
                }
            }

            if (!empty($d_name)) {

                $limit = $this->doctor_model->getLimit();
                if ($limit <= 0) {
                    $this->session->set_flashdata('feedback', lang('doctor_limit_exceed'));
                    redirect('doctor');
                }

                $data_d = array(
                    'name' => $d_name,
                    'email' => $d_email,
                    'phone' => $d_phone,
                );
                $username = $this->input->post('d_name');
                if ($this->ion_auth->email_check($d_email)) {
                    $this->session->set_flashdata('feedback', 'This Email Address Is Already Registered');
                } else {
                    $dfgg = 4;
                    $this->ion_auth->register($username, $password, $d_email, $dfgg);
                    $ion_user_id = $this->db->get_where('users', array('email' => $d_email))->row()->id;
                    $this->doctor_model->insertDoctor($data_d);
                    $doctor_user_id = $this->db->get_where('doctor', array('email' => $d_email))->row()->id;
                    $id_info = array('ion_user_id' => $ion_user_id);
                    $this->doctor_model->updateDoctor($doctor_user_id, $id_info);
                    $this->hospital_model->addHospitalIdToIonUser($ion_user_id, $this->hospital_id);
                }
            }

            if ($patient == 'add_new') {
                $patient = $patient_user_id;
            }

            if ($doctor == 'add_new') {
                $doctor = $doctor_user_id;
            }

            $amount = array_sum($amount_by_category);
            $sub_total = $amount;
            $discount_type = $this->finance_model->getDiscountType();
            if (!empty($doctor)) {
                $all_cat_name = explode(',', $category_name);
                foreach ($all_cat_name as $indiviual_cat_nam) {
                    $indiviual_cat_nam1 = explode('*', $indiviual_cat_nam);
                    $qty = $indiviual_cat_nam1[3];
                    $d_commission = $this->finance_model->getPaymentCategoryById($indiviual_cat_nam1[0])->d_commission;
                    $h_commission = 100 - $d_commission;
                    $hospital_amount_per_unit = ($indiviual_cat_nam1[1] * $h_commission / 100);
                    $hospital_amount_by_category[] = $hospital_amount_per_unit * $qty;
                }
                $hospital_amount = array_sum($hospital_amount_by_category);
                if ($discount_type == 'flat') {
                    $flat_discount = $discount;
                    // $hospital_amount = array_sum($hospital_amount_by_category);
                    $gross_total = $sub_total - $flat_discount;
                    if($d_commission == 0){
                        $doctor_amount = 0;
                    } else {
                        $doctor_amount = $amount - $hospital_amount - $flat_discount; 
                    }
                } else {
                    $flat_discount = $sub_total * ($discount / 100);
                    $gross_total = $sub_total - $flat_discount;
                    // $hospital_amount = array_sum($hospital_amount_by_category);
                    $doctor_amount = $amount - $hospital_amount - $flat_discount;
                }
            } else {
                $doctor_amount = '0';
                if ($discount_type == 'flat') {
                    $flat_discount = $discount;
                    $gross_total = $sub_total - $flat_discount;
                    $hospital_amount = $gross_total;
                } else {
                    $flat_discount = $amount * ($discount / 100);
                    $gross_total = $sub_total - $flat_discount;
                    $hospital_amount = $gross_total;
                }
            }

            $data = array();

            if (!empty($patient)) {
                $patient_details = $this->patient_model->getPatientById($patient);
                $patient_name = $patient_details->name;
                $patient_phone = $patient_details->phone;
                $patient_address = $patient_details->address;
                $patient_email = $patient_details->email_bayar;
            } else {
                $patient_name = 0;
                $patient_phone = 0;
                $patient_address = 0;
            }

            if (!empty($doctor)) {
                $doctor_details = $this->doctor_model->getDoctorById($doctor);
                $doctor_name = $doctor_details->name;
            } else {
                $doctor_name = 0;
            }

            //img Bukti TF
            $file_name = $_FILES['bukti_tf']['name'];            
            $file_name_pieces = explode('_', $file_name);
            $new_file_name = '';
            foreach ($file_name_pieces as $piece) {
                if ($count !== 1) {
                    $piece = ucfirst($piece);
                }

                $new_file_name .= $piece;
                $count++;
            }
            $config = array(
                'file_name' => $new_file_name,
                'upload_path' => "./uploads/",
                'allowed_types' => "gif|jpg|png|jpeg|pdf",
                'overwrite' => False,
                'max_size' => "20480000"
            );
            $this->load->library('Upload', $config);
            $this->upload->initialize($config);

            if ($this->upload->do_upload('bukti_tf')) {
                $path = $this->upload->data();
            }

            if (empty($id)) {
                $data = array(
                    'category_name' => $category_name,
                    'patient' => $patient,
                    'date' => $date,
                    'amount' => $sub_total,
                    'doctor' => $doctor,
                    'discount' => $discount,
                    'flat_discount' => $flat_discount,
                    'gross_total' => $gross_total,
                    'status' => 'unpaid',
                    'hospital_amount' => $hospital_amount,
                    'doctor_amount' => $doctor_amount,
                    'user' => $user,
                    'patient_name' => $patient_name,
                    'patient_phone' => $patient_phone,
                    'patient_address' => $patient_address,
                    'doctor_name' => $doctor_name,
                    'date_string' => $date_string,
                    'remarks' => $remarks,
                    'pharmacy_payment_id' => $pharmacy_payment_id,
                    'diagnosa' => $diagnosa,
                    'terapi' => $terapi,
                    'resep' => $resep,
                    'pembayaran_cash' => $pembayaran_cash,
                    'pembayaran_transfer' => $pembayaran_transfer,
                    'kekurangan' => $kekurangan,
                    'promat' => $promat,
                    'appointment_id' => $appointment_id,
                    'bukti_tf' => $new_file_name,
                );

                $this->finance_model->insertPayment($data);
                $inserted_id = $this->db->insert_id();

                if ($deposit_type == 'Card') {
                    $gateway = $this->settings_model->getSettings()->payment_gateway;
                    if ($gateway == 'PayPal') {

                        $card_type = $this->input->post('card_type');
                        $card_number = $this->input->post('card_number');
                        $expire_date = $this->input->post('expire_date');
                        $cvv = $this->input->post('cvv');

                        $all_details = array(
                            'patient' => $patient,
                            'date' => $date,
                            'amount' => $sub_total,
                            'doctor' => $doctor,
                            'discount' => $discount,
                            'flat_discount' => $flat_discount,
                            'gross_total' => $gross_total,
                            'status' => 'unpaid',
                            'hospital_amount' => $hospital_amount,
                            'doctor_amount' => $doctor_amount,
                            'patient_name' => $patient_name,
                            'patient_phone' => $patient_phone,
                            'patient_address' => $patient_address,
                            'doctor_name' => $doctor_name,
                            'date_string' => $date_string,
                            'remarks' => $remarks,
                            'diagnosa' => $diagnosa,
                            'terapi' => $terapi,
                            'resep' => $resep,
                            'deposited_amount' => $amount_received,
                            'payment_id' => $inserted_id,
                            'card_type' => $card_type,
                            'card_number' => $card_number,
                            'expire_date' => $expire_date,
                            'cvv' => $cvv,
                            'from' => 'pos',
                            'user' => $user,
                            'pembayaran_cash' => $pembayaran_cash,
                            'pembayaran_transfer' => $pembayaran_transfer,
                            'kekurangan' => $kekurangan,
                            'promat' => $promat,
                            'appointment_id' => $appointment_id,
                            'bukti_tf' => $new_file_name,
                        );
                        //    $data_payments['all_details'] = $all_details;
                        //    $this->load->view('home/dashboard'); // just the header file
                        //    $this->load->view('paypal/confirmation', $data_payments);
                        //    $this->load->view('home/footer'); // just the header file
                        $this->paypal->Do_direct_payment($all_details);
                    } elseif ($gateway == 'Pay U Money') {
                        redirect("payu/check1?deposited_amount=" . "$amount_received" . '&payment_id=' . $inserted_id);
                    } else {
                        $this->session->set_flashdata('feedback', 'Payment failed. No Gateway Selected');
                        redirect("finance/invoice?id=" . "$inserted_id");
                    }
                } else {
                    $data1 = array(
                        'date' => $date,
                        'patient' => $patient,
                        'deposited_amount' => $amount_received,
                        'payment_id' => $inserted_id,
                        'amount_received_id' => $inserted_id . '.' . 'gp',
                        'deposit_type' => $deposit_type,
                        'user' => $user
                    );
                    $this->finance_model->insertDeposit($data1);

                    $data_payment = array('amount_received' => $amount_received, 'deposit_type' => $deposit_type);
                    $this->finance_model->updatePayment($inserted_id, $data_payment);

                    // kirim email
                    // $hospital_dokter = $this->hospital_model->getHospitalById($doctor_details->hospital_id);
                    $hospital_dokter = $this->hospital_model->getHospitalById($this->hospital_id);
                    $hospital_pasien = $this->hospital_model->getHospitalById($patient_details->hospital_id);
                    $alamat = $hospital_dokter->address;

                    $category_name1 = explode(',', $category_name);
                    $i = 0;
                    
                    $newcategoryname = [];
                    foreach ($category_name1 as $category_name2) {
                        $category_name3 = explode('*', $category_name2);
                        if(@$category_name3[4]){
                            if(@$newcategoryname[$category_name3[4]]){
                                $newcategoryname[$category_name3[4]][1] += $category_name3[1];
                            }else{
                                $newcategoryname[$category_name3[4]] = [
                                    0=>$category_name3[0],
                                    1=>$category_name3[1],
                                    2=>$category_name3[2],
                                    3=>$category_name3[3],
                                ];
                            }
                        }else{
                            if(@$newcategoryname[$category_name3[0]]){
                                $newcategoryname[$category_name3[0]][0] = $category_name3[0];
                                $newcategoryname[$category_name3[0]][1] += $category_name3[1];
                                $newcategoryname[$category_name3[0]][2] = $category_name3[2];
                                $newcategoryname[$category_name3[0]][3] = $category_name3[3];
                            }else{
                                $newcategoryname[$category_name3[0]] = [
                                    0=>$category_name3[0],
                                    1=>$category_name3[1],
                                    2=>$category_name3[2],
                                    3=>$category_name3[3],
                                ];
                            }
                        }
                    }
                    
                    $lay = $newcategoryname;
                    $layanan = array();
                    foreach($lay as $category_name3) {
                        $i = $i + 1; 
                        if ($category_name3[3] > 0) { 
                            $layanan[] ="✅ " . $this->finance_model->getPaymentcategoryById($category_name3[0])->category . ", Rp " . $category_name3[1] . ", " . $category_name3[3] . "x, Rp " . $category_name3[1] * $category_name3[3]."\n";
                        }
                    }

                    try {

                        // Twilio
                        $phoneeee = str_replace(" ","",trim($patient_phone));
                        if(substr($phoneeee, 0, 1)=='0'){
                            $phoneeee = '+62'.substr($phoneeee, 1);
                        }

                        $urlphone = "whatsapp:".$phoneeee;

                        $sid    = "AC59317831851a7c8c8cf4b02116db2a47";
                        $token  = "8223b126ce124d581b4f843ea19463fd";
                        $twilio = new Client($sid, $token);

                        $message = $twilio->messages
                        ->create($urlphone,
                                [
                                    "contentSid" => "HX18ab743a47bd77a4af25afc81d38d846",
                                    "from" => "whatsapp:+6285883939039",
                                    "contentVariables" => json_encode([
                                        "1" => $patient_name,
                                        "2" => $hospital_dokter->name,
                                        "3" => $hospital_pasien->code.'-'.str_pad($patient, 4, "0", STR_PAD_LEFT),
                                        "4" => "drg ".$doctor_name,
                                        "5" => date('d F Y',$date),
                                        "6" => "BFDC".$inserted_id,
                                        "7" => "APPO".$appointment_id,
                                        "8" => "Rp ".number_format($gross_total,0,',','.'),
                                        "9" => "BFDC".$inserted_id,
                                        "10" => "https://finance.bfreshgigi.com/invoice/".hash('sha256', $inserted_id),
                                    ]),
                                    "messagingServiceSid" => "MG69110b9ed6e24e2ebc0e1c727df8bae6"
                                ]
                        );

                        $this->session->set_flashdata('feedback', 'Pembayaran Berhasil & Notif WA Terkirim');
                        redirect("finance/invoice?id=" . "$inserted_id");
                    
                    } catch (Exception $e) {

                        $this->session->set_flashdata('feedback', 'Pembayaran Berhasil & Notif WA Gagal');
                        redirect("finance/payment");
                    
                    }

                }
            } else {
                $deposit_edit_amount = $this->input->post('deposit_edit_amount');
                $deposit_edit_id = $this->input->post('deposit_edit_id');
                if (!empty($deposit_edit_amount)) {
                    $deposited_edit = array_combine($deposit_edit_id, $deposit_edit_amount);
                    foreach ($deposited_edit as $key_deposit => $value_deposit) {
                        $data_deposit = array(
                            'deposited_amount' => $value_deposit
                        );
                        $this->finance_model->updateDeposit($key_deposit, $data_deposit);
                    }
                }

                $a_r_i = $id . '.' . 'gp';
                $deposit_id = $this->db->get_where('patient_deposit', array('amount_received_id' => $a_r_i))->row();

                $data = array(
                    'category_name' => $category_name,
                    'patient' => $patient,
                    'doctor' => $doctor,
                    'amount' => $sub_total,
                    'discount' => $discount,
                    'flat_discount' => $flat_discount,
                    'gross_total' => $gross_total,
                    'amount_received' => $amount_received,
                    'hospital_amount' => $hospital_amount,
                    'doctor_amount' => $doctor_amount,
                    'user' => $user,
                    'patient_name' => $patient_details->name,
                    'patient_phone' => $patient_details->phone,
                    'patient_address' => $patient_details->address,
                    'patient_address' => $patient_details->address,
                    'doctor_name' => $doctor_details->name,
                    'remarks' => $remarks,
                    'diagnosa' => $diagnosa,
                    'terapi' => $terapi,
                    'resep' => $resep,
                    'pembayaran_cash' => $pembayaran_cash,
                    'pembayaran_transfer' => $pembayaran_transfer,
                    'kekurangan' => $kekurangan,
                    'promat' => $promat,
                    'date' => $date_edit,
                    'pharmacy_payment_id'=>$pharmacy_payment_id,
                    'deposit_type'=>$deposit_type,
                    'bukti_tf' => $new_file_name,
                );

                if (!empty($deposit_id->id)) {
                    $data1 = array(
                        'date' => $date,
                        'patient' => $patient,
                        'payment_id' => $id,
                        'deposited_amount' => $amount_received,
                        'user' => $user
                    );
                    $this->finance_model->updateDeposit($deposit_id->id, $data1);
                } else {
                    $data1 = array(
                        'date' => $date,
                        'patient' => $patient,
                        'payment_id' => $id,
                        'deposited_amount' => $amount_received,
                        'amount_received_id' => $id . '.' . 'gp',
                        'user' => $user
                    );
                    $this->finance_model->insertDeposit($data1);
                }
                $this->finance_model->updatePayment($id, $data);

                $hospital_dokter = $this->hospital_model->getHospitalById($this->hospital_id);
                $hospital_pasien = $this->hospital_model->getHospitalById($patient_details->hospital_id);
                $alamat = $hospital_dokter->address;

                // Twilio
                $phoneeee = str_replace(" ","",trim($patient_phone));
                if(substr($phoneeee, 0, 1)=='0'){
                    $phoneeee = '+62'.substr($phoneeee, 1);
                }

                $urlphone = "whatsapp:".$phoneeee;

                $sid    = "AC59317831851a7c8c8cf4b02116db2a47";
                $token  = "8223b126ce124d581b4f843ea19463fd";
                $twilio = new Client($sid, $token);

                $message = $twilio->messages
                ->create($urlphone,
                        [
                            "contentSid" => "HX0643310fd95667f699071d0bcca45ff2",
                            "from" => "whatsapp:+6285883939039",
                            "contentVariables" => json_encode([
                                "1" => $patient_name,
                                "2" => $hospital_dokter->name,
                                "3" => $hospital_pasien->code.'-'.str_pad($patient, 4, "0", STR_PAD_LEFT),
                                "4" => "drg ".$doctor_name,
                                "5" => date('d F Y',$date),
                                "6" => "BFDC".$id,
                                "7" => "APPO".$appointment_id,
                                "8" => "Rp ".number_format($gross_total,0,',','.'),
                                "9" => "BFDC".$id,
                                "10" => "https://finance.bfreshgigi.com/invoice/".hash('sha256', $id),
                            ]),
                            "messagingServiceSid" => "MG69110b9ed6e24e2ebc0e1c727df8bae6"
                        ]
                );

                $this->session->set_flashdata('feedback', 'Updated');
                redirect("finance/invoice?id=" . "$id");
            }
        // }
    }

    // public function addPayment(){

    //     $sid    = "AC270d4c5938820ffc7bec31970938811d";
    //     $token  = "b4a70e8d553140f6b027aee0185c6540";
    //     $twilio = new Client($sid, $token);

    //     $message = $twilio->messages
    //     ->create("whatsapp:+6285706679500", // to
    //         array(
    //         "from" => "whatsapp:+14155238886",
    //         "body" => "bfresh"
    //         )
    //     );

    //     print($message->sid);
    // }

    public function addPaymentNonPasien() {
        $promat = $this->input->post('promat');
        $pembayaran_transfer = str_replace('.', '',($this->input->post('pembayaran_transfer')));
        $pembayaran_cash = str_replace('.', '',($this->input->post('pembayaran_cash')));
        $item_selected = array();
        $price = array();
        $hide_to = array();
        $quantity = array();
        $category_selected = array();
        $category_selected = $this->input->post('category_name');
        $item_selected = $this->input->post('category_id');
        $quantity = $this->input->post('quantity');
        $price = $this->input->post('price');
        $hide_to = $this->input->post('hide_to');

        if (empty($item_selected)) {
            $this->session->set_flashdata('feedback', 'Select a Item');
            redirect('finance/addPaymentViewNonPasien');
        } else {
            $item_quantity_array = array();
            $item_quantity_array = array_combine($item_selected, $quantity);
            $item_price_array = array_combine($item_selected, $price);
        }
        $cat_and_price = array();
        if (!empty($item_quantity_array)) {
            foreach ($item_quantity_array as $key => $value) {
                $current_item = $this->finance_model->getPaymentCategoryById($key);
                $category_price = $item_price_array[$key];
                $category_hideto = @$hide_to[$key] ? '*'.$hide_to[$key] : '';
                $category_type = $current_item->type;
                $qty = $value;
                $cat_and_price[] = $key . '*' . $category_price . '*' . $category_type . '*' . $qty.$category_hideto;
                $amount_by_category[] = $category_price * $qty;
            }
            $category_name = implode(',', $cat_and_price);
        } else {
            $this->session->set_flashdata('feedback', 'Atend The Required Fields');
            redirect('finance/addPaymentViewNonPasien');
        }
        
        $add_date = date('m/d/y');
        $doctor = $this->input->post('doctor');
        $date = strtotime($this->input->post('date'));
        $date_string = date('d-m-y', $date);
        $discount = str_replace('.', '',($this->input->post('discount')));
        if (empty($discount)) {
            $discount = 0;
        }

        $amount_received = str_replace('.', '',($this->input->post('pembayaran_transfer'))) + str_replace('.', '',($this->input->post('pembayaran_cash')));

        $deposit_type = $this->input->post('deposit_type');
        $user = $this->ion_auth->get_user_id();

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');

        $amount = array_sum($amount_by_category);
        $sub_total = $amount;
        $discount_type = $this->finance_model->getDiscountType();
        if (!empty($doctor)) {
            $all_cat_name = explode(',', $category_name);
            foreach ($all_cat_name as $indiviual_cat_nam) {
                $indiviual_cat_nam1 = explode('*', $indiviual_cat_nam);
                $qty = $indiviual_cat_nam1[3];
                $d_commission = $this->finance_model->getPaymentCategoryById($indiviual_cat_nam1[0])->d_commission;
                $h_commission = 100 - $d_commission;
                $hospital_amount_per_unit = ($indiviual_cat_nam1[1] * $h_commission / 100);
                $hospital_amount_by_category[] = $hospital_amount_per_unit * $qty;
            }
            $hospital_amount = array_sum($hospital_amount_by_category);
            if ($discount_type == 'flat') {
                $flat_discount = $discount;
                $gross_total = $sub_total - $flat_discount;
                if($d_commission == 0){
                    $doctor_amount = 0;
                } else {
                    $doctor_amount = $amount - $hospital_amount - $flat_discount; 
                }
            } else {
                $flat_discount = $sub_total * ($discount / 100);
                $gross_total = $sub_total - $flat_discount;
                $doctor_amount = $amount - $hospital_amount - $flat_discount;
            }
        } else {
            $doctor_amount = '0';
            if ($discount_type == 'flat') {
                $flat_discount = $discount;
                $gross_total = $sub_total - $flat_discount;
                $hospital_amount = $gross_total;
            } else {
                $flat_discount = $amount * ($discount / 100);
                $gross_total = $sub_total - $flat_discount;
                $hospital_amount = $gross_total;
            }
        }

        $data = array();

        if (!empty($doctor)) {
            $doctor_details = $this->doctor_model->getDoctorById($doctor);
            $doctor_name = $doctor_details->name;
        } else {
            $doctor_name = 0;
        }

        if (empty($id)) {
            $data = array(
                'category_name' => $category_name,
                'patient' => $patient,
                'date' => $date,
                'amount' => $sub_total,
                'doctor' => $doctor,
                'discount' => $discount,
                'flat_discount' => $flat_discount,
                'gross_total' => $gross_total,
                'status' => 'unpaid',
                'hospital_amount' => $hospital_amount,
                'doctor_amount' => $doctor_amount,
                'user' => $user,
                'patient_name' => $patient_name,
                'patient_phone' => $patient_phone,
                'patient_address' => $patient_address,
                'doctor_name' => $doctor_name,
                'date_string' => $date_string,
                'remarks' => $remarks,
                'pharmacy_payment_id' => $pharmacy_payment_id,
                'diagnosa' => $diagnosa,
                'terapi' => $terapi,
                'resep' => $resep,
                'pembayaran_cash' => $pembayaran_cash,
                'pembayaran_transfer' => $pembayaran_transfer,
                'promat' => $promat,
            );

            $this->finance_model->insertPayment($data);
            $inserted_id = $this->db->insert_id();

            $data1 = array(
                'date' => $date,
                'patient' => $patient,
                'deposited_amount' => $amount_received,
                'payment_id' => $inserted_id,
                'amount_received_id' => $inserted_id . '.' . 'gp',
                'deposit_type' => $deposit_type,
                'user' => $user
            );
            $this->finance_model->insertDeposit($data1);

            $data_payment = array('amount_received' => $amount_received, 'deposit_type' => $deposit_type);
            $this->finance_model->updatePayment($inserted_id, $data_payment);

            // kirim email
            // $hospital_dokter = $this->hospital_model->getHospitalById($doctor_details->hospital_id);
            $hospital_dokter = $this->hospital_model->getHospitalById($this->hospital_id);
            $hospital_pasien = $this->hospital_model->getHospitalById($patient_details->hospital_id);
            $alamat = $hospital_dokter->address;

            $category_name1 = explode(',', $category_name);
            $i = 0;
            
            $newcategoryname = [];
            foreach ($category_name1 as $category_name2) {
                $category_name3 = explode('*', $category_name2);
                if(@$category_name3[4]){
                    if(@$newcategoryname[$category_name3[4]]){
                        $newcategoryname[$category_name3[4]][1] += $category_name3[1];
                    }else{
                        $newcategoryname[$category_name3[4]] = [
                            0=>$category_name3[0],
                            1=>$category_name3[1],
                            2=>$category_name3[2],
                            3=>$category_name3[3],
                        ];
                    }
                }else{
                    if(@$newcategoryname[$category_name3[0]]){
                        $newcategoryname[$category_name3[0]][0] = $category_name3[0];
                        $newcategoryname[$category_name3[0]][1] += $category_name3[1];
                        $newcategoryname[$category_name3[0]][2] = $category_name3[2];
                        $newcategoryname[$category_name3[0]][3] = $category_name3[3];
                    }else{
                        $newcategoryname[$category_name3[0]] = [
                            0=>$category_name3[0],
                            1=>$category_name3[1],
                            2=>$category_name3[2],
                            3=>$category_name3[3],
                        ];
                    }
                }
            }
            
            $this->session->set_flashdata('feedback', 'Added');
            redirect("finance/invoice?id=" . "$inserted_id");

        } else {
            $deposit_edit_amount = $this->input->post('deposit_edit_amount');
            $deposit_edit_id = $this->input->post('deposit_edit_id');
            if (!empty($deposit_edit_amount)) {
                $deposited_edit = array_combine($deposit_edit_id, $deposit_edit_amount);
                foreach ($deposited_edit as $key_deposit => $value_deposit) {
                    $data_deposit = array(
                        'deposited_amount' => $value_deposit
                    );
                    $this->finance_model->updateDeposit($key_deposit, $data_deposit);
                }
            }


            $a_r_i = $id . '.' . 'gp';
            $deposit_id = $this->db->get_where('patient_deposit', array('amount_received_id' => $a_r_i))->row();

            $data = array(
                'category_name' => $category_name,
                'doctor' => $doctor,
                'amount' => $sub_total,
                'discount' => $discount,
                'flat_discount' => $flat_discount,
                'gross_total' => $gross_total,
                'amount_received' => $amount_received,
                'hospital_amount' => $hospital_amount,
                'doctor_amount' => $doctor_amount,
                'user' => $user,
                'patient_name' => $patient_details->name,
                'patient_phone' => $patient_details->phone,
                'patient_address' => $patient_details->address,
                'patient_address' => $patient_details->address,
                'doctor_name' => $doctor_details->name,
                'pembayaran_cash' => $pembayaran_cash,
                'pembayaran_transfer' => $pembayaran_transfer,
                'promat' => $promat,
                'date' => $date,
                'deposit_type'=>$deposit_type
            );

            if (!empty($deposit_id->id)) {
                $data1 = array(
                    'date' => $date,
                    'patient' => $patient,
                    'payment_id' => $id,
                    'deposited_amount' => $amount_received,
                    'user' => $user
                );
                $this->finance_model->updateDeposit($deposit_id->id, $data1);
            } else {
                $data1 = array(
                    'date' => $date,
                    'patient' => $patient,
                    'payment_id' => $id,
                    'deposited_amount' => $amount_received,
                    'amount_received_id' => $id . '.' . 'gp',
                    'user' => $user
                );
                $this->finance_model->insertDeposit($data1);
            }
            $this->finance_model->updatePayment($id, $data);
            $this->session->set_flashdata('feedback', 'Updated');
            redirect("finance/invoice?id=" . "$id");
        }
    }

    function editPayment() {
        if ($this->ion_auth->in_group(array('admin'))) {
            $data = array();
            $data['appointmentinput'] = $this->appointment_model->AppointmentInputPembayaran();
            $data['discount_type'] = $this->finance_model->getDiscountType();
            $data['settings'] = $this->settings_model->getSettings();
            // $data['categories'] = $this->finance_model->getPaymentCategory();
            $data['categoriesbaru'] = $this->finance_model->getPaymentCategoryBaru();
            $data['patients'] = $this->patient_model->getPatient();
            $data['doctors'] = $this->doctor_model->getDoctor();
            $id = $this->input->get('id');
            $data['payment'] = $this->finance_model->getPaymentById($id);
            // var_dump($data['payment']);die();
            
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_payment_view', $data);
            $this->load->view('home/footer'); // just the footer file
        }
    }

    function delete() {
        if ($this->ion_auth->in_group(array('admin', 'Accountant', 'Receptionist'))) {
            $id = $this->input->get('id');
            $this->finance_model->deletePayment($id);
            $this->finance_model->deleteDepositByInvoiceId($id);
            $this->session->set_flashdata('feedback', 'Deleted');
            redirect('finance/payment');
        }
    }

    public function otPayment() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['settings'] = $this->settings_model->getSettings();
        $data['ot_payments'] = $this->finance_model->getOtPayment();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('ot_payment', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addOtPaymentView() {
        $data = array();
        $data['doctors'] = $this->doctor_model->getDoctor();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->finance_model->getPaymentCategory();
        $data['patients'] = $this->patient_model->getPatient();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_ot_payment', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addOtPayment() {
        $id = $this->input->post('id');
        $patient = $this->input->post('patient');
        $doctor_c_s = $this->input->post('doctor_c_s');
        $doctor_a_s_1 = $this->input->post('doctor_a_s_1');
        $doctor_a_s_2 = $this->input->post('doctor_a_s_2');
        $doctor_anaes = $this->input->post('doctor_anaes');
        $n_o_o = $this->input->post('n_o_o');

        $c_s_f = $this->input->post('c_s_f');
        $a_s_f_1 = $this->input->post('a_s_f_1');
        $a_s_f_2 = $this->input->post('a_s_f_2');
        $anaes_f = $this->input->post('anaes_f');
        $ot_charge = $this->input->post('ot_charge');
        $cab_rent = $this->input->post('cab_rent');
        $seat_rent = $this->input->post('seat_rent');
        $others = $this->input->post('others');

        $discount = $this->input->post('discount');
        $vat = $this->input->post('vat');
        $amount_received = $this->input->post('amount_received');

        $date = time();
        $user = $this->ion_auth->get_user_id();

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');


        $this->form_validation->set_rules('patient', 'Patient', 'trim|required|min_length[2]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('doctor_c_s', 'Consultant surgeon', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('doctor_a_s_1', 'Assistant Surgeon (1)', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('doctor_a_s_2', 'Assistant Surgeon(2)', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('doctor_anaes', 'Anaesthisist', 'trim|min_length[2]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('n_o_o', 'Nature Of Operation', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('c_s_f', 'Consultant Surgeon Fee', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('a_s_f_1', 'Assistant surgeon fee', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('a_s_f_2', 'Assistant surgeon fee', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('anaes_f', 'Anaesthesist', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('ot_charge', 'OT Charge', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('cab_rent', 'Cabin Rent', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('seat_rent', 'Seat Rent', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('others', 'Others', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('discount', 'Discount', 'trim|min_length[1]|max_length[100]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            echo 'form validate noe nai re';
        } else {
            $doctor_fees = $c_s_f + $a_s_f_1 + $a_s_f_2 + $anaes_f;
            $hospital_fees = $ot_charge + $cab_rent + $seat_rent + $others;
            $amount = $doctor_fees + $hospital_fees;
            $discount_type = $this->finance_model->getDiscountType();

            if ($discount_type == 'flat') {
                $amount_with_discount = $amount - $discount;
                $gross_total = $amount_with_discount + $amount_with_discount * ($vat / 100);
                $flat_discount = $discount;
                $flat_vat = $amount_with_discount * ($vat / 100);
                $hospital_fees = $hospital_fees - $flat_discount;
            } else {
                $flat_discount = $amount * ($discount / 100);
                $amount_with_discount = $amount - $amount * ($discount / 100);
                $gross_total = $amount_with_discount + $amount_with_discount * ($vat / 100);
                $discount = $discount . '*' . $amount * ($discount / 100);
                $flat_vat = $amount_with_discount * ($vat / 100);
                $hospital_fees = $hospital_fees - $flat_discount;
            }

            $data = array();


            if (empty($id)) {
                $data = array(
                    'patient' => $patient,
                    'doctor_c_s' => $doctor_c_s,
                    'doctor_a_s_1' => $doctor_a_s_1,
                    'doctor_a_s_2' => $doctor_a_s_2,
                    'doctor_anaes' => $doctor_anaes,
                    'n_o_o' => $n_o_o,
                    'c_s_f' => $c_s_f,
                    'a_s_f_1' => $a_s_f_1,
                    'a_s_f_2' => $a_s_f_2,
                    'anaes_f' => $anaes_f,
                    'ot_charge' => $ot_charge,
                    'cab_rent' => $cab_rent,
                    'seat_rent' => $seat_rent,
                    'others' => $others,
                    'discount' => $discount,
                    'date' => $date,
                    'amount' => $amount,
                    'doctor_fees' => $doctor_fees,
                    'hospital_fees' => $hospital_fees,
                    'gross_total' => $gross_total,
                    'flat_discount' => $flat_discount,
                    'amount_received' => $amount_received,
                    'status' => 'unpaid',
                    'user' => $user
                );
                $this->finance_model->insertOtPayment($data);
                $inserted_id = $this->db->insert_id();
                $data1 = array(
                    'date' => $date,
                    'patient' => $patient,
                    'deposited_amount' => $amount_received,
                    'amount_received_id' => $inserted_id . '.' . 'ot',
                    'user' => $user
                );
                $this->finance_model->insertDeposit($data1);

                $this->session->set_flashdata('feedback', 'Added');
                redirect("finance/otInvoice?id=" . "$inserted_id");
            } else {
                $a_r_i = $id . '.' . 'ot';
                $deposit_id = $this->db->get_where('patient_deposit', array('amount_received_id' => $a_r_i))->row()->id;
                $data = array(
                    'patient' => $patient,
                    'doctor_c_s' => $doctor_c_s,
                    'doctor_a_s_1' => $doctor_a_s_1,
                    'doctor_a_s_2' => $doctor_a_s_2,
                    'doctor_anaes' => $doctor_anaes,
                    'n_o_o' => $n_o_o,
                    'c_s_f' => $c_s_f,
                    'a_s_f_1' => $a_s_f_1,
                    'a_s_f_2' => $a_s_f_2,
                    'anaes_f' => $anaes_f,
                    'ot_charge' => $ot_charge,
                    'cab_rent' => $cab_rent,
                    'seat_rent' => $seat_rent,
                    'others' => $others,
                    'discount' => $discount,
                    'amount' => $amount,
                    'doctor_fees' => $doctor_fees,
                    'hospital_fees' => $hospital_fees,
                    'gross_total' => $gross_total,
                    'flat_discount' => $flat_discount,
                    'amount_received' => $amount_received,
                    'user' => $user
                );
                $data1 = array(
                    'date' => $date,
                    'patient' => $patient,
                    'deposited_amount' => $amount_received,
                    'user' => $user
                );
                $this->finance_model->updateDeposit($deposit_id, $data1);
                $this->finance_model->updateOtPayment($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
                redirect("finance/otInvoice?id=" . "$id");
            }
        }
    }

    function editOtPayment() {
        if ($this->ion_auth->in_group(array('admin', 'Accountant','Receptionist'))) {
            $data = array();
            $data['discount_type'] = $this->finance_model->getDiscountType();
            $data['settings'] = $this->settings_model->getSettings();
            $data['patients'] = $this->patient_model->getPatient();
            $id = $this->input->get('id');
            $data['ot_payment'] = $this->finance_model->getOtPaymentById($id);
            $data['doctors'] = $this->doctor_model->getDoctor();
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_ot_payment', $data);
            $this->load->view('home/footer'); // just the footer file
        }
    }

    function otInvoice() {
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['ot_payment'] = $this->finance_model->getOtPaymentById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('ot_invoice', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function otPaymentDetails() {
        $id = $this->input->get('id');
        $patient = $this->input->get('patient');
        $data['patient'] = $this->patient_model->getPatientByid($patient);
        $data['settings'] = $this->settings_model->getSettings();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['ot_payment'] = $this->finance_model->getOtPaymentById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('ot_payment_details', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function otPaymentDelete() {
        if ($this->ion_auth->in_group(array('admin', 'Accountant'))) {
            $id = $this->input->get('id');
            $this->finance_model->deleteOtPayment($id);
            $this->session->set_flashdata('feedback', 'Deleted');
            redirect('finance/otPayment');
        }
    }

    function addPaymentByPatient() {
        $data = array();
        $id = $this->input->get('id');
        $data['patient'] = $this->patient_model->getPatientById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('choose_payment_type', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function addPaymentByPatientView() {
        $id = $this->input->get('id');
        $type = $this->input->get('type');
        $data = array();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->finance_model->getPaymentCategory();
        $data['doctors'] = $this->doctor_model->getDoctor();

        $data['patient'] = $this->patient_model->getPatientById($id);

        $hospital = $this->hospital_model->getHospitalById($data['patient']->hospital_id);
        $data['patientidf'] = $hospital->code.'-'.str_pad($data['patient']->id, 4, "0", STR_PAD_LEFT);
        if ($type == 'gen') {
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_payment_view_single', $data);
            $this->load->view('home/footer'); // just the footer fi
        } else {
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('add_ot_payment_view_single', $data);
            $this->load->view('home/footer'); // just the footer fi
        }
    }

    public function paymentCategory() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['categories'] = $this->finance_model->getPaymentCategory();
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('payment_category', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addPaymentCategoryView() {
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_payment_category');
        $this->load->view('home/footer'); // just the header file
    }

    public function addPaymentCategory() {
        $id = $this->input->post('id');
        $category = $this->input->post('category');
        $type = $this->input->post('type');
        $description = $this->input->post('description');
        $c_price = $this->input->post('c_price');
        $d_commission = $this->input->post('d_commission');
        if(!$d_commission)
            $d_commission = 0;
        $is_can_hide = $this->input->post('is_can_hide') ? 1 : 0;
        if (empty($c_price)) {
            $c_price = 0;
        }


        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('category', 'Category', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('c_price', 'Category price', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('d_commission', 'Doctor Commission rate', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('type', 'Type', 'trim|min_length[1]|max_length[100]|xss_clean');

        if ($this->form_validation->run() == FALSE) {
            if (!empty($id)) {
                $this->session->set_flashdata('feedback', 'Validation Error !');
                redirect('finance/editPaymentCategory?id=' . $id);
            } else {
                $data = array();
                $data['setval'] = 'setval';
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_payment_category', $data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            $data = array();
            $data = array('category' => $category,
                'description' => $description,
                'type' => $type,
                'c_price' => $c_price,
                'd_commission' => $d_commission,
                'is_can_hide' => $is_can_hide,
            );
            if (empty($id)) {
                $this->finance_model->insertPaymentCategory($data);
                $this->session->set_flashdata('feedback', 'Added');
            } else {
                $this->finance_model->updatePaymentCategory($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
            }
            redirect('finance/paymentCategory');
        }
    }

    function editPaymentCategory() {
        $data = array();
        $id = $this->input->get('id');
        $data['category'] = $this->finance_model->getPaymentCategoryById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_payment_category', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function deletePaymentCategory() {
        $id = $this->input->get('id');
        $this->finance_model->deletePaymentCategory($id);
        redirect('finance/paymentCategory');
    }

    public function expense() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['settings'] = $this->settings_model->getSettings();
        $data['expenses'] = $this->finance_model->getExpense();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('expense', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addExpenseView() {
        $data = array();
        $data['settings'] = $this->settings_model->getSettings();
        $data['categories'] = $this->finance_model->getExpenseCategory();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_expense_view', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addExpense() {
        $id = $this->input->post('id');
        $category = $this->input->post('category');
        $date = time();
        $amount = $this->input->post('amount');
        $user = $this->ion_auth->get_user_id();
        $note = $this->input->post('note');


        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('category', 'Category', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('amount', 'Amount', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('note', 'Note', 'trim|min_length[1]|max_length[100]|xss_clean');


        if ($this->form_validation->run() == FALSE) {
            if (!empty($id)) {
                $this->session->set_flashdata('feedback', 'Validation Error !');
                redirect('finance/editExpense?id=' . $id);
            } else {
                $data = array();
                $data['setval'] = 'setval';
                $data['settings'] = $this->settings_model->getSettings();
                $data['categories'] = $this->finance_model->getExpenseCategory();
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_expense_view', $data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            $data = array();
            if (empty($id)) {
                $data = array(
                    'category' => $category,
                    'date' => $date,
                    'amount' => $amount,
                    'note' => $note,
                    'user' => $user
                );
            } else {
                $data = array(
                    'category' => $category,
                    'amount' => $amount,
                    'note' => $note,
                    'user' => $user,
                );
            }
            if (empty($id)) {
                $this->finance_model->insertExpense($data);
                $this->session->set_flashdata('feedback', 'Added');
            } else {
                $this->finance_model->updateExpense($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
            }
            redirect('finance/expense');
        }
    }

    function editExpense() {
        $data = array();
        $data['categories'] = $this->finance_model->getExpenseCategory();
        $data['settings'] = $this->settings_model->getSettings();
        $id = $this->input->get('id');
        $data['expense'] = $this->finance_model->getExpenseById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_expense_view', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function deleteExpense() {
        $id = $this->input->get('id');
        $this->finance_model->deleteExpense($id);
        redirect('finance/expense');
    }

    public function expenseCategory() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $data['categories'] = $this->finance_model->getExpenseCategory();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('expense_category', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function addExpenseCategoryView() {
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_expense_category');
        $this->load->view('home/footer'); // just the header file
    }

    public function addExpenseCategory() {
        $id = $this->input->post('id');
        $category = $this->input->post('category');
        $description = $this->input->post('description');

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('category', 'Category', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('description', 'Description', 'trim|required|min_length[1]|max_length[100]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            if (!empty($id)) {
                $this->session->set_flashdata('feedback', 'Validation Error !');
                redirect('finance/editExpenseCategory?id=' . $id);
            } else {
                $data = array();
                $data['setval'] = 'setval';
                $this->load->view('home/dashboard'); // just the header file
                $this->load->view('add_expense_category', $data);
                $this->load->view('home/footer'); // just the header file
            }
        } else {
            $data = array();
            $data = array('category' => $category,
                'description' => $description
            );
            if (empty($id)) {
                $this->finance_model->insertExpenseCategory($data);
                $this->session->set_flashdata('feedback', 'Added');
            } else {
                $this->finance_model->updateExpenseCategory($id, $data);
                $this->session->set_flashdata('feedback', 'Updated');
            }
            redirect('finance/expenseCategory');
        }
    }

    function editExpenseCategory() {
        $data = array();
        $id = $this->input->get('id');
        $data['category'] = $this->finance_model->getExpenseCategoryById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('add_expense_category', $data);
        $this->load->view('home/footer'); // just the footer file
    }

    function deleteExpenseCategory() {
        $id = $this->input->get('id');
        $this->finance_model->deleteExpenseCategory($id);
        redirect('finance/expenseCategory');
    }

    function invoice() {
        $id = $this->input->get('id');
        $data['payment'] = $this->finance_model->getPaymentById($id);
        $data['pharmacy_payment'] = false;
        if($data['payment']->pharmacy_payment_id)
            $data['pharmacy_payment'] = $this->pharmacy_model->getPaymentById($data['payment']->pharmacy_payment_id);
        // $patient_hospital_id = $this->patient_model->getPatientById( $data['payment']->patient)->hospital_id;
        // if ($patient_hospital_id != $this->session->userdata('hospital_id')) {
        //     redirect('home/permission');
        // }
        $data['settings'] = $this->settings_model->getSettings();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('invoice', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function expenseInvoice() {
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['expense'] = $this->finance_model->getExpenseById($id);
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('expense_invoice', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function amountReceived() {
        $id = $this->input->post('id');
        $amount_received = $this->input->post('amount_received');
        $previous_amount_received = $this->db->get_where('payment', array('id' => $id))->row()->amount_received;
        $amount_received = $amount_received + $previous_amount_received;
        $data = array();
        $data = array('amount_received' => $amount_received);
        $this->finance_model->amountReceived($id, $data);
        redirect('finance/invoice?id=' . $id);
    }

    function otAmountReceived() {
        $id = $this->input->post('id');
        $amount_received = $this->input->post('amount_received');
        $previous_amount_received = $this->db->get_where('ot_payment', array('id' => $id))->row()->amount_received;
        $amount_received = $amount_received + $previous_amount_received;
        $data = array();
        $data = array('amount_received' => $amount_received);
        $this->finance_model->otAmountReceived($id, $data);
        redirect('finance/oTinvoice?id=' . $id);
    }

    function patientPaymentHistory() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $patient = $this->input->get('patient');
        if (empty($patient)) {
            $patient = $this->input->post('patient');
        }

        $patient_hospital_id = $this->patient_model->getPatientById($patient)->hospital_id;
        if ($patient_hospital_id != $this->session->userdata('hospital_id')) {
            redirect('home/permission');
        }

        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }

        $data['date_from'] = $date_from;
        $data['date_to'] = $date_to;

        if (!empty($date_from)) {
            $data['payments'] = $this->finance_model->getPaymentByPatientIdByDate($patient, $date_from, $date_to);
            $data['deposits'] = $this->finance_model->getDepositByPatientIdByDate($patient, $date_from, $date_to);
        } else {
            $data['payments'] = $this->finance_model->getPaymentByPatientId($patient);
            $data['pharmacy_payments'] = $this->pharmacy_model->getPaymentByPatientId($patient);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByPatientId($patient);
            $data['deposits'] = $this->finance_model->getDepositByPatientId($patient);
        }

        $data['patient'] = $this->patient_model->getPatientByid($patient);
        $data['settings'] = $this->settings_model->getSettings();

        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('patient_deposit', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function deposit() {
        $id = $this->input->post('id');
        $patient = $this->input->post('patient');
        $payment_id = $this->input->post('payment_id');
        $date = time();

        $deposited_amount = $this->input->post('deposited_amount');

        $deposit_type = $this->input->post('deposit_type');

        if (empty($deposit_type)) {
            $deposit_type = 'Cash';
        }

        $user = $this->ion_auth->get_user_id();

        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
        $this->form_validation->set_rules('patient', 'Patient', 'trim|min_length[1]|max_length[100]|xss_clean');
        $this->form_validation->set_rules('deposited_amount', 'Deposited Amount', 'trim|min_length[1]|max_length[100]|xss_clean');
        if ($this->form_validation->run() == FALSE) {
            redirect('finance/patientPaymentHistory?patient=' . $patient);
        } else {
            $data = array();
            $data = array('patient' => $patient,
                'date' => $date,
                'payment_id' => $payment_id,
                'deposited_amount' => $deposited_amount,
                'deposit_type' => $deposit_type,
                'user' => $user
            );
            if (empty($id)) {
                if ($deposit_type == 'Card') {
                    $payment_details = $this->finance_model->getPaymentById($payment_id);
                    $gateway = $this->settings_model->getSettings()->payment_gateway;
                    if ($gateway == 'PayPal') {
                        $card_type = $this->input->post('card_type');
                        $card_number = $this->input->post('card_number');
                        $expire_date = $this->input->post('expire_date');
                        $cvv = $this->input->post('cvv');

                        $all_details = array(
                            'patient' => $payment_details->patient,
                            'date' => $payment_details->date,
                            'amount' => $payment_details->amount,
                            'doctor' => $payment_details->doctor_name,
                            'discount' => $payment_details->discount,
                            'flat_discount' => $payment_details->flat_discount,
                            'gross_total' => $payment_details->gross_total,
                            'status' => 'unpaid',
                            'patient_name' => $payment_details->patient_name,
                            'patient_phone' => $payment_details->patient_phone,
                            'patient_address' => $payment_details->patient_address,
                            'deposited_amount' => $deposited_amount,
                            'payment_id' => $payment_details->id,
                            'card_type' => $card_type,
                            'card_number' => $card_number,
                            'expire_date' => $expire_date,
                            'cvv' => $cvv,
                            'from' => 'patient_payment_details',
                            'user' => $user
                        );
                        $this->paypal->Do_direct_payment($all_details);
                    } elseif ($gateway == 'Pay U Money') {
                        redirect("payu/check?deposited_amount=" . "$deposited_amount" . '&payment_id=' . $payment_id);
                    } else {
                        $this->session->set_flashdata('feedback', 'Payment failed. No Gateway Selected');
                        redirect("finance/invoice?id=" . "$payment_id");
                    }
                } else {
                    $this->finance_model->insertDeposit($data);
                    $this->session->set_flashdata('feedback', 'Added');
                }
            } else {
                $this->finance_model->updateDeposit($id, $data);

                $amount_received_id = $this->finance_model->getDepositById($id)->amount_received_id;
                if (!empty($amount_received_id)) {
                    $amount_received_payment_id = explode('.', $amount_received_id);
                    $payment_id = $amount_received_payment_id[0];
                    $data_amount_received = array('amount_received' => $deposited_amount);
                    $this->finance_model->updatePayment($amount_received_payment_id[0], $data_amount_received);
                }

                $this->session->set_flashdata('feedback', 'Updated');
            }
            redirect('finance/patientPaymentHistory?patient=' . $patient);
        }
    }

    function editDepositByJason() {
        $id = $this->input->get('id');
        $data['deposit'] = $this->finance_model->getDepositById($id);
        echo json_encode($data);
    }

    function deleteDeposit() {
        $id = $this->input->get('id');
        $patient = $this->input->get('patient');

        $amount_received_id = $this->finance_model->getDepositById($id)->amount_received_id;
        if (!empty($amount_received_id)) {
            $amount_received_payment_id = explode('.', $amount_received_id);
            $payment_id = $amount_received_payment_id[0];
            $data_amount_received = array('amount_received' => NULL);
            $this->finance_model->updatePayment($amount_received_payment_id[0], $data_amount_received);
        }

        $this->finance_model->deleteDeposit($id);

        redirect('finance/patientPaymentHistory?patient=' . $patient);
    }

    function invoicePatientTotal() {
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['payments'] = $this->finance_model->getPaymentByPatientIdByStatus($id);
        $data['ot_payments'] = $this->finance_model->getOtPaymentByPatientIdByStatus($id);
        $data['patient_id'] = $id;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('invoicePT', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function lastPaidInvoice() {
        $id = $this->input->get('id');
        $data['settings'] = $this->settings_model->getSettings();
        $data['discount_type'] = $this->finance_model->getDiscountType();
        $data['payments'] = $this->finance_model->lastPaidInvoice($id);
        $data['ot_payments'] = $this->finance_model->lastOtPaidInvoice($id);
        $data['patient_id'] = $id;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('LPInvoice', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function makePaid() {
        $id = $this->input->get('id');
        $patient_id = $this->finance_model->getPaymentById($id)->patient;
        $data = array();
        $data = array('status' => 'paid');
        $data1 = array();
        $data1 = array('status' => 'paid-last');
        $this->finance_model->makeStatusPaid($id, $patient_id, $data, $data1);
        $this->session->set_flashdata('feedback', 'Paid');
        redirect('finance/invoice?id=' . $id);
    }

    function makePaidByPatientIdByStatus() {
        $id = $this->input->get('id');
        $data = array();
        $data = array('status' => 'paid-last');
        $data1 = array();
        $data1 = array('status' => 'paid');
        $this->finance_model->makePaidByPatientIdByStatus($id, $data, $data1);
        $this->session->set_flashdata('feedback', 'Paid');
        redirect('patient');
    }

    function makeOtStatusPaid() {
        $id = $this->input->get('id');
        $this->finance_model->makeOtStatusPaid($id);
        $this->session->set_flashdata('feedback', 'Paid');
        redirect("finance/otInvoice?id=" . "$id");
    }

    function doctorsCommission() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }
        $data['doctors'] = $this->doctor_model->getDoctor();
        $data['payments'] = $this->finance_model->getPaymentByDate($date_from, $date_to);
        $data['ot_payments'] = $this->finance_model->getOtPaymentByDate($date_from, $date_to);
        $data['settings'] = $this->settings_model->getSettings();
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('doctors_commission', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function docComDetails() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }
        $doctor = $this->input->get('id');
        if (empty($doctor)) {
            $doctor = $this->input->post('doctor');
        }
        $data['doctor'] = $doctor;
        if (!empty($date_from)) {
            $data['payments'] = $this->finance_model->getPaymentByDoctorDate($doctor, $date_from, $date_to);
        } else {
            $data['payments'] = $this->finance_model->getPaymentByDoctor($doctor);
        }
        $data['settings'] = $this->settings_model->getSettings();
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('doc_com_view', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function financialReport() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }
        // var_dump($date_to);die();
        $data = array();
        $data['payment_categories'] = $this->finance_model->getPaymentCategory();
        $data['doctors'] = $this->doctor_model->getDoctor();
        $data['expense_categories'] = $this->finance_model->getExpenseCategory();

        $data['payments'] = $this->finance_model->getPaymentByDate($date_from, $date_to);
        // var_dump($data['payments']);die();
        $data['payments_transfer'] = $this->finance_model->getPaymentByDateTransfer($date_from, $date_to);
        $data['payments_cash'] = $this->finance_model->getPaymentByDateCash($date_from, $date_to);
        $data['ot_payments'] = $this->finance_model->getOtPaymentByDate($date_from, $date_to);
        $data['deposits'] = $this->finance_model->getDepositsByDate($date_from, $date_to);
        $data['expenses'] = $this->finance_model->getExpenseByDate($date_from, $date_to);

        $data['medicines'] = $this->medicine_model->getMedicine();
        $data['payments_obat'] = $this->pharmacy_model->getPaymentByDate($date_from, $date_to);
 
        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('financial_report', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function financialDokterReport() {
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }
        $data = array();
        $data['payment_categories'] = $this->finance_model->getPaymentCategory();
        $data['doctors'] = $this->doctor_model->getDoctor();
        $data['expense_categories'] = $this->finance_model->getExpenseCategory();

        $data['payments'] = $this->finance_model->getPaymentDokterNew($date_from, $date_to);
        $data['ot_payments'] = $this->finance_model->getOtPaymentByDate($date_from, $date_to);
        $data['deposits'] = $this->finance_model->getDepositsByDate($date_from, $date_to);
        $data['expenses'] = $this->finance_model->getExpenseByDate($date_from, $date_to);

        $data['from'] = $this->input->post('date_from');
        $data['to'] = $this->input->post('date_to');
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('financial_doctor_report', $data);
        $this->load->view('home/footer'); // just the footer fi
    }

    function UserActivityReport() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        if ($this->ion_auth->in_group(array('Accountant'))) {
            $user = $this->ion_auth->get_user_id();
            $data['user'] = $this->accountant_model->getAccountantByIonUserId($user);
        }
        if ($this->ion_auth->in_group(array('Receptionist'))) {
            $user = $this->ion_auth->get_user_id();
            $data['user'] = $this->receptionist_model->getReceptionistByIonUserId($user);
        }
        $hour = 0;
        $TODAY_ON = $this->input->get('today');
        $YESTERDAY_ON = $this->input->get('yesterday');
        $ALL = $this->input->get('all');

        $today = strtotime($hour . ':00:00');
        $today_last = strtotime($hour . ':00:00') + 86399;
        $data['payments'] = $this->finance_model->getPaymentByUserIdByDate($user, $today, $today_last);
        $data['ot_payments'] = $this->finance_model->getOtPaymentByUserIdByDate($user, $today, $today_last);
        $data['deposits'] = $this->finance_model->getDepositByUserIdByDate($user, $today, $today_last);
        $data['day'] = 'Today';
        if (!empty($YESTERDAY_ON)) {
            $today = strtotime($hour . ':00:00');
            $yesterday = strtotime('-1 day', $today);
            $data['day'] = 'Yesterday';
            $data['payments'] = $this->finance_model->getPaymentByUserIdByDate($user, $yesterday, $today);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByUserIdByDate($user, $yesterday, $today);
            $data['deposits'] = $this->finance_model->getDepositByUserIdByDate($user, $yesterday, $today);
        }
        if (!empty($ALL)) {
            $data['day'] = 'All';
            $data['payments'] = $this->finance_model->getPaymentByUserId($user);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByUserId($user);
            $data['deposits'] = $this->finance_model->getDepositByUserId($user);
        }
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('user_activity_report', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function UserActivityReportDateWise() {
        $data = array();
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        if ($this->ion_auth->in_group(array('Accountant'))) {
            $user = $this->input->post('user');
            $data['user'] = $this->accountant_model->getAccountantByIonUserId($user);
        }
        if ($this->ion_auth->in_group(array('Receptionist'))) {
            $user = $this->input->post('user');
            $data['user'] = $this->receptionist_model->getReceptionistByIonUserId($user);
        }
        $date_from = strtotime($this->input->post('date_from'));
        $date_to = strtotime($this->input->post('date_to'));
        if (!empty($date_to)) {
            $date_to = $date_to + 86399;
        }

        $data['date_from'] = $date_from;
        $data['date_to'] = $date_to;

        $data['payments'] = $this->finance_model->getPaymentByUserIdByDate($user, $date_from, $date_to);
        $data['ot_payments'] = $this->finance_model->getOtPaymentByUserIdByDate($user, $date_from, $date_to);
        $data['deposits'] = $this->finance_model->getDepositByUserIdByDate($user, $date_from, $date_to);
        $data['settings'] = $this->settings_model->getSettings();
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('user_activity_report', $data);
        $this->load->view('home/footer'); // just the header file
    }

    function AllUserActivityReport() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $user = $this->input->get('user');

        if (!empty($user)) {
            $user_group = $this->db->get_where('users_groups', array('user_id' => $user))->row()->group_id;
            if ($user_group == '3') {
                $data['user'] = $this->accountant_model->getAccountantByIonUserId($user);
            }
            if ($user_group == '10') {
                $data['user'] = $this->receptionist_model->getReceptionistByIonUserId($user);
            }
            $data['settings'] = $this->settings_model->getSettings();
            $hour = 0;
            $TODAY_ON = $this->input->get('today');
            $YESTERDAY_ON = $this->input->get('yesterday');
            $ALL = $this->input->get('all');

            $today = strtotime($hour . ':00:00');
            $today_last = strtotime($hour . ':00:00') + 86399;
            $data['payments'] = $this->finance_model->getPaymentByUserIdByDate($user, $today, $today_last);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByUserIdByDate($user, $today, $today_last);
            $data['deposits'] = $this->finance_model->getDepositByUserIdByDate($user, $today, $today_last);
            $data['day'] = 'Today';

            if (!empty($YESTERDAY_ON)) {
                $today = strtotime($hour . ':00:00');
                $yesterday = strtotime('-1 day', $today);
                $data['payments'] = $this->finance_model->getPaymentByUserIdByDate($user, $yesterday, $today);
                $data['ot_payments'] = $this->finance_model->getOtPaymentByUserIdByDate($user, $yesterday, $today);
                $data['deposits'] = $this->finance_model->getDepositByUserIdByDate($user, $yesterday, $today);
                $data['day'] = 'Yesterday';
            }

            if (!empty($ALL)) {
                $data['payments'] = $this->finance_model->getPaymentByUserId($user);
                $data['ot_payments'] = $this->finance_model->getOtPaymentByUserId($user);
                $data['deposits'] = $this->finance_model->getDepositByUserId($user);
                $data['day'] = 'All';
            }


            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('user_activity_report', $data);
            $this->load->view('home/footer'); // just the header file
        }

        if (empty($user)) {
            $hour = 0;
            $today = strtotime($hour . ':00:00');
            $today_last = strtotime($hour . ':00:00') + 86399;
            $data['accountants'] = $this->accountant_model->getAccountant();
            $data['receptionists'] = $this->receptionist_model->getReceptionist();
            $data['settings'] = $this->settings_model->getSettings();
            $data['payments'] = $this->finance_model->getPaymentByDate($today, $today_last);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByDate($today, $today_last);
            $data['deposits'] = $this->finance_model->getDepositsByDate($today, $today_last);
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('all_user_activity_report', $data);
            $this->load->view('home/footer'); // just the header file
        }
    }

    function AllUserActivityReportDateWise() {
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login', 'refresh');
        }
        $user = $this->input->post('user');

        if (!empty($user)) {
            $user_group = $this->db->get_where('users_groups', array('user_id' => $user))->row()->group_id;
            if ($user_group == '3') {
                $data['user'] = $this->accountant_model->getAccountantByIonUserId($user);
            }
            if ($user_group == '10') {
                $data['user'] = $this->receptionist_model->getReceptionistByIonUserId($user);
            }
            $date_from = strtotime($this->input->post('date_from'));
            $date_to = strtotime($this->input->post('date_to'));
            if (!empty($date_to)) {
                $date_to = $date_to + 86399;
            }

            $data['settings'] = $this->settings_model->getSettings();
            $data['date_from'] = $date_from;
            $data['date_to'] = $date_to;
            $data['payments'] = $this->finance_model->getPaymentByUserIdByDate($user, $date_from, $date_to);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByUserIdByDate($user, $date_from, $date_to);
            $data['deposits'] = $this->finance_model->getDepositByUserIdByDate($user, $date_from, $date_to);



            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('user_activity_report', $data);
            $this->load->view('home/footer'); // just the header file
        }

        if (empty($user)) {
            $hour = 0;
            $today = strtotime($hour . ':00:00');
            $today_last = strtotime($hour . ':00:00') + 86399;
            $data['accountants'] = $this->accountant_model->getAccountant();
            $data['receptionists'] = $this->receptionist_model->getReceptionist();
            $data['settings'] = $this->settings_model->getSettings();
            $data['payments'] = $this->finance_model->getPaymentByDate($today, $today_last);
            $data['ot_payments'] = $this->finance_model->getOtPaymentByDate($today, $today_last);
            $data['deposits'] = $this->finance_model->getDepositsByDate($today, $today_last);
            $this->load->view('home/dashboard'); // just the header file
            $this->load->view('all_user_activity_report', $data);
            $this->load->view('home/footer'); // just the header file
        }
    }

    function getPayment() {
        $requestData = $_REQUEST;
        $start = $requestData['start'];
        $limit = $requestData['length'];
        $search = $this->input->post('search')['value'];
        $settings = $this->settings_model->getSettings();

        if ($limit == -1) {
            if (!empty($search)) {
                $data['payments'] = $this->finance_model->getPaymentBysearch($search);
            } else {
                $data['payments'] = $this->finance_model->getPayment();
            }
        } else {
            if (!empty($search)) {
                $data['payments'] = $this->finance_model->getPaymentByLimitBySearch($limit, $start, $search);
            } else {
                $data['payments'] = $this->finance_model->getPaymentByLimit($limit, $start);
            }
        }
        //  $data['payments'] = $this->finance_model->getPayment();

        foreach ($data['payments'] as $payment) {
            $date = date('d-m-y', $payment->date);

            $discount = $payment->discount;
            if (empty($discount)) {
                $discount = 0;
            }

            $options2 = '<a class="btn btn-info btn-xs invoicebutton" title="' . lang('invoice') . '" style="color: #fff;" href="finance/invoice?id=' . $payment->id . '"><i class="fa fa-file-text"></i> ' . lang('invoice') . '</a>';

            if ($this->ion_auth->in_group(array('admin', 'Accountant'))) {
                $options1 = ' <a class="btn btn-info btn-xs editbutton" title="' . lang('edit') . '" href="finance/editPayment?id=' . $payment->id . '"><i class="fa fa-edit"> </i> ' . lang('edit') . '</a>';
                $options3 = '<a class="btn btn-info btn-xs delete_button" title="' . lang('delete') . '" href="finance/delete?id=' . $payment->id . '" onclick="return confirm(\'Are you sure you want to delete this item?\');"><i class="fa fa-trash-o"></i> ' . lang('delete') . '</a>';
            }

            if (empty($options1)) {
                $options1 = '';
            }

            if (empty($options3)) {
                $options3 = '';
            }

            $doctor_details = $this->doctor_model->getDoctorById($payment->doctor);

            if (!empty($doctor_details)) {
                $doctor = $doctor_details->name;
            } else {
                if (!empty($payment->doctor_name)) {
                    $doctor = $payment->doctor_name;
                } else {
                    $doctor = $payment->doctor_name;
                }
            }

            $patient_info = $this->db->get_where('patient', array('id' => $payment->patient))->row();
            if (!empty($patient_info)) {
                $patient_details = $patient_info->name . '</br>' . $patient_info->address . '</br>' . $patient_info->phone . '</br>';
            } else {
                $patient_details = ' ';
            }

            if($payment->promat == '0'){
                $promat = 'Tidak';
            } elseif ($payment->promat == '1') {
                $promat = 'Ya';
            } else {
                $promat = null;
            }

            $info[] = array(
                $payment->id,
                $patient_details,
                $doctor,
                $date,
                $settings->currency . ' ' . number_format($payment->amount,0,",","."),
                $settings->currency . ' ' . number_format($discount,0,",","."),
                $settings->currency . ' ' . number_format($payment->gross_total,0,",","."),
                $settings->currency . ' ' . number_format($this->finance_model->getDepositAmountByPaymentId($payment->id),0,",","."),
                $settings->currency . ' ' . number_format(($payment->gross_total - $this->finance_model->getDepositAmountByPaymentId($payment->id)),0,",","."),
                // $payment->deposit_type,
                $promat,
                $payment->remarks,
                // $options2 . ' ' . $options3,
                $options1 . ' ' . $options2 . ' ' . $options3,
                    //  $options2
            );
        }







        if (!empty($data['payments'])) {
            $output = array(
                "draw" => intval($requestData['draw']),
                "recordsTotal" => $this->db->get('payment')->num_rows(),
                "recordsFiltered" => $this->db->where('hospital_id', $this->session->userdata('hospital_id'))->get('payment')->num_rows(),
                "data" => $info
            );
        } else {
            $output = array(
                // "draw" => 1,
                "recordsTotal" => 0,
                "recordsFiltered" => 0,
                "data" => []
            );
        }




        echo json_encode($output);
    }

}

/* End of file finance.php */
/* Location: ./application/modules/finance/controllers/finance.php */