<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Receptionist extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('checklist_model');
        $this->load->model('receptionist/receptionist_model');
        if (!$this->ion_auth->in_group(array('admin', 'Receptionist','spv'))) {
            redirect('home/permission');
        }
    }

    public function index() {
        $data =[];
        $data['poins'] = $this->point();
        $current_user = $this->ion_auth->get_user_id();
        $receptionist_id = $this->db->get_where('receptionist', array('ion_user_id' => $current_user))->row()->id;
        $pointid = $this->checklist_model->getSingleChecklistReceptionist(date('Y-m'),date('d'),$receptionist_id);
        $pointids = [];
        if($pointid)
            $pointids = explode(",",$pointid->point_id);
        $data['pointids'] = $pointids;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('receptionist_view', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function point()
    {
        $i=1;
        $poin = [
            $i++ => "SOP HARIAN",
            $i++ => "1. Menyalakan lampu dan AC, membersihkan lantai klinik (nyapu, ngepel)",
            $i++ => "2. Membersihkan kaca hari Senin, Rabu, Jumat",
            $i++ => "3. Kuku tangan asisten wajib pendek dan tidak berkutek",
            $i++ => "4. Membersihkan dental unit, peralatan& lantai usai perawatan",
            $i++ => "5. Mensterilkan alat. Mencuci APD, rendam APD dengan air sabun selama 1 jam. lalu bilas air bersih. Melipat dan menyimpan APD bersih",
            $i++ => "6. Membersihkan bahan dan perlengkapan praktek, semprot ruangan,  menyalakan sinar Ultraviolet",
            $i++ => "7. Membersihkan dental unit, peralatan& lantai usai perawatan",
            $i++ => "8. Mematikan lampu ruangan, mengunci pintu",
            $i++ => "9. Menanyakan keperluan pasien: Assalamu'alaikum bapak/ibu, saya (sebut nama pasien), ada yang bisa saya bantu? silakan duduk bapak ibu",
            $i++ => "10. Mencatat/ambilkan data pasien di rekam medis, menyebut nama pasien & mempersilahkan kumur",
            $i++ => "11. Berdoa dengan dokter, dipimpin dokter dan evaluasi singkat kinerja sebelumnya",
            $i++ => "12. Menyiapkan status pasien reservasi di meja dokter",
            $i++ => "13. Membantu dokter dalam merawat pasien. Bapak ibu (sebut nama pasien) silakan berkumur dulu, air kumur jangan ditelan, krn mengandung obat kumur",
            $i++ => "14. Menyalakan Kompresor, memastikan Dental Unit berfungsi baik (nyalakan, aliran air, bor, lowspeed, scaler, lampu)",
            $i++ => "15. Melakukan pengecekan bahan-bahan dasar",
            $i++ => "16. Mematikan kompresor. 2 hari sekali membuka tombol bawah  kompresor, sehingga air keluar",
            $i++ => "17. Memberikan 6 like & 3 komen di instagram bfreshdentalcare dg akun pribadi",
            $i++ => "18. Berikan 10 like  & 5 komen dari akun ig bebas di HP Klinik ke ig pusat (bfreshdentalcare)",
            $i++ => "19. Meminta ke pasien untuk review google maps cabang dan follow ig bfreshdentalcare",
            $i++ => "20. Menyiapkan status pasien reservasi & memberi data ke admin pusat tentang ultah pasien",
            $i++ => "21. Menulis status perawatan harian, menulis daftar pasien baru",
            $i++ => "22. Melakukan pelaporan jumlah pasien dan keuangan ke pusat",
            $i++ => "23. Menyimpan uang dalam kotak box didalam lemari",
            $i++ => "24. Untuk kasus gigi palsu lakukan pencetakan, hubungi lab, mengirimkan permintaan lab",
            $i++ => "25. Paraf Asisten (untuk pengganti paraf diberi nama)<br>",
            $i++ => "",
            $i++ => "SOP MINGGUAN",
            $i++ => "1. Menguras dan membersihkan kamar mandi  seminggu 2x atau terlihat kotor",
            $i++ => "2. Menyerahkan sampah medis ke pusat",
            $i++ => "3. Memberikan minyak pada handpiece highspeed dan lowspeed dalam satu minggu sekali",
            $i++ => "4. Mengisi laporan PLN",
            $i++ => "5. Menerima barang dan bahan",
            $i++ => "6. Mencatat jurnal service peralatan",
            $i++ => "7. Membuat laporan mingguan dan bulanan ",
            $i++ => "8. Pengecekan dan mutasi bahan, daftar pasien baru, laporan gigi palsu, laporan ortho, honor dokter & asisten",
        ];
        return $poin;
    }

    public function save()
    {
        $poins = $this->point();
        $month = date('Y-m');
        $day = date('d');
        $current_user = $this->ion_auth->get_user_id();
        $receptionist_id = $this->db->get_where('receptionist', array('ion_user_id' => $current_user))->row()->id;
        $checks = $this->input->post('check');

        foreach ($poins as $no => $poin) {
            $data = [
                'month'=>$month,
                'day'=>$day,
                'user_id'=>$current_user,
                'receptionist_id'=>$receptionist_id,
                'is_checked'=> @$checks[$no] ? 1 : 0,
                'point_id'=>$no
            ];
            $exist = $this->checklist_model->getByMonth($month,$day,$current_user,$no);
            if($exist){
                $this->checklist_model->updateChecklist($exist->id,$data);
            }else{
                $this->checklist_model->insertChecklist($data);
            }
        }
		$this->session->set_flashdata('message', '<p>Data Tersimpan</p>');
		redirect('checklist/receptionist', 'refresh'); 
    }

    public function report() {
        $data =[];
        $data['poins'] = $this->point();
        $data['receptionists'] = $this->receptionist_model->getReceptionist();
        $data['receptionist_id'] = $this->input->get('receptionist_id');
        $data['month'] = $this->input->get('month');
        $data['end'] = 0;
        if($data['month']){
            $data['end'] = date('t',strtotime($data['month'].'-01'));
        }
        $checklists = [];
        $checklist = $this->checklist_model->getDataReceptionist($data);
        foreach ($checklist as $c) {
            $checklists[$c->point_id] = explode(",",$c->day);
        }
        $data['checklists'] = $checklists;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('receptionist_report', $data);
        $this->load->view('home/footer'); // just the header file
    }
}
?>