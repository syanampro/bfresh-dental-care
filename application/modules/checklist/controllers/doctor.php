<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Doctor extends MX_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('checklist_model');
        $this->load->model('doctor/doctor_model');
        if (!$this->ion_auth->in_group(array('admin', 'Doctor', 'spv'))) {
            redirect('home/permission');
        }
    }

    public function index() {
        $data =[];
        $data['poins'] = $this->point();
        $current_user = $this->ion_auth->get_user_id();
        $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
        $pointid = $this->checklist_model->getSingleChecklist(date('Y-m'),date('d'),$doctor_id);
        $pointids = [];
        if($pointid)
            $pointids = explode(",",$pointid->point_id);
        $data['pointids'] = $pointids;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('doctor_view', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function report() {
        $data =[];
        $data['poins'] = $this->point();
        $data['doctors'] = $this->doctor_model->getDoctor();
        $data['doctor_id'] = $this->input->get('doctor_id');
        $data['month'] = $this->input->get('month');
        $data['end'] = 0;
        if($data['month']){
            $data['end'] = date('t',strtotime($data['month'].'-01'));
        }
        $checklists = [];
        $checklist = $this->checklist_model->getData($data);
        foreach ($checklist as $c) {
            $checklists[$c->point_id] = explode(",",$c->day);
        }
        $data['checklists'] = $checklists;
        $this->load->view('home/dashboard'); // just the header file
        $this->load->view('doctor_report', $data);
        $this->load->view('home/footer'); // just the header file
    }

    public function point()
    {
        $i=1;
        $poin = [
            $i++ => "Datang dalam kondisi sehat dan fit",
            $i++ => "Mengucapkan salam: Assalamu'alaikum (saat masuk klinik)",
            $i++ => "Menanyakan kesehatan asisten: mbak (panggil nama asisten), apa kabar hari ini? Apa sudah sarapan? Gimana kondis| kesehatanmu?",
            $i++ => "Menyimpan tas/perbekalan di rang khusus yang telah disediakan klinik.",
            $i++ => "Memimpin doa bersama, evaluasi kerja, yel-yel: Sebelum melakukan operasional klinik BFRESH (mari kita berdoa dulu agar klinik berjalan lancar dan bernilal ibadah. Al Fatihah suara keras",
            $i++ => "Menanyakan pasien yang telah resevasi: Mbak (sebut nama asisten), apa ada pasien reservasi ke saya hari ini? Mana ya statusnya? Apa sudah disiapkan",
            $i++ => "Mempelajari status pasien reservasi",
            $i++ => "Dokter membaca Status pasien tersebut. Berkoordinasi dengan dokter yang sebelum nya telah merawat pasien tersebut jika diperlukan dengan WA atau telpon.",
            $i++ => "Mengecek perlengkapan DU, apa sudah nyala, apa tombol bawah sudah terpasang, apa tombol atas sudah terbuka, apakah setiap 2 hari sekali air kompresor sudah dibuang?",
            $i++ => "Mengecek perlengkapan di lemari sterilisasi: apakah sudah selesal disterilkan. Apakah alat alat sudah siap untuk dipakal 5-7 pasien hari ini",
            $i++ => "Cuci tangan sebelum setiap melakukan perawatan ke pasien dengan 6 langkah cuci tangan, pakai APD, cuci tangan kembali",
            $i++ => "Dokter (Pakai APD) Memperkenalkan diri ke pasien : Bapak/ibu (sebut nama) perkenalkan saya dokter (sebutkan nama) yang akan merawat bapak (sebut nama)",
            $i++ => "Menyebut nama 1 pasien 5-7 kali dalam 1 kali kunjungan",
            $i++ => "Memastikan pasien tidak sedang demam, pilek, batuk. Menanyakan keluhan pasien, kemudian mempersilakan pasien kumur terlebih dahulu",
            $i++ => "Mendokumentasikan after dan before perawatan dan melaporkan ke dokter kordinator",
            $i++ => "Meminta pasien review Google Maps & follow ig Bfreshdentalcare",
            $i++ => "Menulis status pasien dengan standar B FRESH dan Member paraf di setiap akhir penulisan status di form status pasien",
            $i++ => "Cuci tangan setiap selesai merawat pasien dengan 6 langkah cuci tangan",
            $i++ => "Mengecek APD bersih dan kotor sudah terpisah",
            $i++ => "Mengecek kompresor, putar kunci angin kompresor, buka mur bawah kompresor. Dan memastikan sudah mati",
            $i++ => "Mengecek perlengkapan DU (termasuk tombol on off, memastikan kompresor sudah off)",
            $i++ => "Memberi 6 like dan 3 komen di instagram bfreshdentalcare",
            $i++ => "Dokter tanda tangan di buku kehadiran dokter dan hitungan uang",
            $i++ => "Kuku tangan dokter diwajibkan pendek dan tidak berkutek",
            $i++ => "Paraf Dokter (Bagi Dokter pengganti di beri nama)",
        ];
        return $poin;
    }

    public function save()
    {
        $poins = $this->point();
        $month = date('Y-m');
        $day = date('d');
        $current_user = $this->ion_auth->get_user_id();
        $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
        $checks = $this->input->post('check');

        foreach ($poins as $no => $poin) {
            $data = [
                'month'=>$month,
                'day'=>$day,
                'user_id'=>$current_user,
                'doctor_id'=>$doctor_id,
                'is_checked'=> @$checks[$no] ? 1 : 0,
                'point_id'=>$no
            ];
            $exist = $this->checklist_model->getByMonth($month,$day,$current_user,$no);
            if($exist){
                $this->checklist_model->updateChecklist($exist->id,$data);
            }else{
                $this->checklist_model->insertChecklist($data);
            }
        }
		$this->session->set_flashdata('message', '<p>Data Tersimpan</p>');
		redirect('checklist/doctor', 'refresh'); 
    }
}
?>