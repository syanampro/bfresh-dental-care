<!--sidebar end-->
<!--main content start-->

<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-12">
            <header class="panel-heading">
                <?php
                    echo lang('daily_checklist').' '.lang('receptionist');
                ?>
            </header>
            <div class="panel col-md-12">
                <div class="row">
                    <form>
                    <div class="col-md-8">
                        <select class="form-control m-bot15 js-example-basic-single add_doctor" name="receptionist_id" value=''>  
                            <option value=""><?php echo lang('select'); ?></option>
                            <?php foreach ($receptionists as $receptionist) { ?>
                                <option value="<?php echo $receptionist->id; ?>"<?php
                                if (!empty($receptionist_id)) {
                                    if ($receptionist_id == $receptionist->id) {
                                        echo 'selected';
                                    }
                                }
                                ?>><?php echo $receptionist->name; ?> </option>
                                    <?php } ?>
                        </select>
                    </div>
                    <div class="col-md-2">
                        <input type="text" class="form-control monthpicker" id="date" readonly="" name="month" value='<?php echo @$month;?>' placeholder="">
                    </div>
                    <div class="col-md-2">
                        <button class="btn btn-success">Submit</button>
                    </div>
                    </form>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12" style="overflow:auto;">
                        <div class="adv-table editable-table ">
                            <div class="clearfix">
                                <table class="table" style="max-width:140%;width:140%;">
                                    <thead>
                                        <tr>
                                            <!-- <th style="width:5%;">No</th> -->
                                            <th width="250px" style="width:40%;">Detail Pekerjaan</th>
                                            <?php for($i=1;$i<=$end;$i++):?>
                                            <th><?php echo $i; ?> </th>
                                            <?php endfor;?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                            $totaldaily = [];
                                            $grandtotal = 0;
                                        ?>
                                        <?php foreach ($poins as $no => $poin) :?>
                                            <tr>
                                                <td><?php echo $poin; ?></td>
                                                <?php for($i=1;$i<=$end;$i++):?>
                                                <?php if(in_array($i,$checklists[$no])) {$totaldaily[$i]++;$grandtotal++;} ?>
                                                <td><?php echo in_array($i,$checklists[$no]) ? '<i class="fa fa-check"></i>' : '' ?></td>
                                                <?php endfor;?>
                                            </tr>
                                        <?php endforeach; ?>
                                        <tr>
                                            <td>Summary</td>
                                            <?php for($i=1;$i<=$end;$i++):?>
                                            <td><?php echo @$totaldaily[$i] ? $totaldaily[$i] : '' ?></td>
                                            <?php endfor;?>
                                            <td><?php echo $grandtotal ?></td>
                                        </tr>
                                        <?php //var_dump($checklists); ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->



<style>

    form{
        border: 0px;
    }

    .med_selected{
        background: #fff;
        padding: 10px 0px;
        margin: 5px;
    }


    .select2-container--bgform .select2-selection--multiple .select2-selection__choice {
        clear: both !important;
    }

    label {
        display: inline-block;
        margin-bottom: 5px;
        font-weight: 500;
        font-weight: bold;
    }

</style>


<script src="common/js/codearistos.min.js"></script>







<script type="text/javascript">
    $(document).ready(function () {
        $(".monthpicker").datepicker({
            format: "yyyy-mm",
            viewMode: "months", 
            minViewMode: "months",
            autoclose:true,
            clearBtn:true
        });
    });
</script> 
