<!--sidebar end-->
<!--main content start-->


<?php
$current_user = $this->ion_auth->get_user_id();
if ($this->ion_auth->in_group('Doctor')) {
    $doctor_id = $this->db->get_where('doctor', array('ion_user_id' => $current_user))->row()->id;
}
?>


<section id="main-content">
    <section class="wrapper site-min-height">
        <!-- page start-->
        <section class="col-md-8">
            <header class="panel-heading">
                <?php
                    echo lang('daily_checklist').' '.lang('receptionist');
                ?>
            </header>
            <div class="panel col-md-12">
                <div class="adv-table editable-table ">
                    <div class="clearfix">
                        <div id="infoMessage"><p><?php echo $message; ?></p></div>
                        <?php echo validation_errors(); ?>
                        <form role="form" action="checklist/receptionist/save" class="clearfix" method="post" enctype="multipart/form-data">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <!-- <th>No</th> -->
                                        <th>Detail Pekerjaan</th>
                                        <th width="10%"><?php echo date('d-m-Y'); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($poins as $no => $poin) :?>
                                        <tr>
                                            <!-- <td><?php echo $no; ?></td> -->
                                            <td><?php echo $poin; ?></td>
                                            <td><input type="checkbox" <?php if(in_array($no,$pointids)) echo 'checked'; ?> name="check[<?php echo $no ?>]" /></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <button class="btn btn-success"> Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
        <!-- page end-->
    </section>
</section>
<!--main content end-->
<!--footer start-->



<style>

    form{
        border: 0px;
    }

    .med_selected{
        background: #fff;
        padding: 10px 0px;
        margin: 5px;
    }


    .select2-container--bgform .select2-selection--multiple .select2-selection__choice {
        clear: both !important;
    }

    label {
        display: inline-block;
        margin-bottom: 5px;
        font-weight: 500;
        font-weight: bold;
    }

</style>


<script src="common/js/codearistos.min.js"></script>







<script type="text/javascript">
    $(document).ready(function () {
    });
</script> 
