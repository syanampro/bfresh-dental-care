<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Checklist_model extends CI_model {

    function __construct() {
        parent::__construct();
        $this->load->database();
    }

    function insertChecklist($data) {
        $data1 = array('hospital_id' => $this->session->userdata('hospital_id'));
        $data2 = array_merge($data, $data1);
        $this->db->insert('checklist', $data2);
    }

    function updateChecklist($id, $data) {
        $this->db->where('id', $id);
        $this->db->update('checklist', $data);
    }

    function delete($id) {
        $this->db->where('id', $id);
        $this->db->delete('checklist');
    }

    function getByMonth($month,$day,$user_id,$poinid) {
        $this->db->where('month', $month);
        $this->db->where('day', $day);
        $this->db->where('user_id', $user_id);
        $this->db->where('point_id', $poinid);
        $query = $this->db->get('checklist');
        return $query->row();
    }

    public function getData($data)
    {
        if($data['month'] && $data['doctor_id'])
            return $this->db->query("select point_id,group_concat(day) as day from checklist where month = '".$data['month']."' and doctor_id = ".$data['doctor_id']." and is_checked = 1 group by point_id order by point_id asc")->result();
        return null;
    }

    public function getDataReceptionist($data)
    {
        if($data['month'] && $data['receptionist_id'])
            return $this->db->query("select point_id,group_concat(day) as day from checklist where month = '".$data['month']."' and receptionist_id = ".$data['receptionist_id']." and is_checked = 1 group by point_id order by point_id asc")->result();
        return null;
    }

    public function getSingleChecklist($month,$day,$doctor_id)
    {
        if($month && $doctor_id && $day)
            return $this->db->query("select day,group_concat(point_id) as point_id from checklist where month = '".$month."' and doctor_id = ".$doctor_id." and day = ".$day." and is_checked = 1 group by day order by day asc")->row();
        return null;
    }

    public function getSingleChecklistReceptionist($month,$day,$receptionist_id)
    {
        if($month && $receptionist_id && $day)
            return $this->db->query("select day,group_concat(point_id) as point_id from checklist where month = '".$month."' and receptionist_id = ".$receptionist_id." and day = ".$day." and is_checked = 1 group by day order by day asc")->row();
        return null;
    }
}
