<!DOCTYPE html>
<html lang="en">
    <head>
        <base href="<?php echo base_url(); ?>">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="Rizvi">
        <meta name="keyword" content="Php, Hospital, Clinic, Management, Software, Php, CodeIgniter, Hms, Accounting">
        <link rel="shortcut icon" href="uploads/favicon.png">

        <title>Register - 
            <?php
            $this->db->where('hospital_id', 'superadmin');
            echo $this->db->get('settings')->row()->system_vendor;
            ?>
        </title>

        <!-- Bootstrap core CSS -->
        <link href="common/css/bootstrap.min.css" rel="stylesheet">
        <link href="common/css/bootstrap-reset.css" rel="stylesheet">
        <!--external css-->
        <link href="common/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
        <!-- Custom styles for this template -->
        <link href="common/css/style.css" rel="stylesheet">
        <link href="common/css/style-responsive.css" rel="stylesheet" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
        <style>
            .day.available:before {
                border-color:#1dce6d
            }
        </style>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
        <!--[if lt IE 9]>
        <script src="js/html5shiv.js"></script>
        <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="login-body">

        <div class="container">

            <style>


                form{

                    padding: 0px !important;
                    border: none;


                }


            </style>

            <form class="form-signin" style="max-width:700px;" method="post" action="auth/registersave">
                <h2 class="login form-signin-heading">
                    REGISTER
                </h2>
                <div id="infoMessage"><p><?php echo $message; ?></p></div>
                <div class="login-wrap">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">    
                                <label for="exampleInputEmail1">Cabang</label>
                                <select class="form-control select2" id="hospital" required name="hospital"> 
                                    <?php foreach ($hospitals as $hospital) { ?>                                        
                                        <option value="<?php echo $hospital->id; ?>"><?php echo $hospital->name; ?> </option>
                                    <?php } ?> 
                                </select>
                            </div>
                            <!-- <div class="form-group">    
                                <label for="exampleInputEmail1">Dokter</label>
                                <select class="form-control select2" name="doctor" id="doctor" value=''> 
                                    <option value="">Pilih Dokter</option>
                                </select>
                            </div>
                            <div class="form-group">    
                                <label for="exampleInputEmail1">Perawatan</label>
                                <select class="form-control select2" id="payment_category" required name="remarks" value=''> 
                                    <option value="">Pilih Perawatan</option>
                                </select>
                            </div>
                            <div class="form-group">    
                                <label for="exampleInputEmail1">Periksa Tanggal?</label>
                                <input name="date" class="form-control datepicker" type="text" readonly required value="" />
                            </div>
                            <div class="form-group">    
                                <label for="exampleInputEmail1">Slot</label>
                                <select class="form-control m-bot15" name="time_slot" id="aslots" value=''></select>
                            </div> -->
                        </div>
                        <!-- <div class="col-md-5">
                            <div class="form-group">  
                                <div id="datepicker" class="pull-right"></div>
                            </div>
                        </div> -->
                    </div>
                    <hr/>
                    <h4>Data Pasien</h4>
                    <div class="row">
                        <div class="form-group col-md-6">    
                            <label for="exampleInputEmail1">Email</label>
                            <input type="email" class="form-control" name="email" required id="email" />
                        </div>
                        <div class="form-group col-md-6">    
                            <label for="exampleInputEmail1">Nama</label>
                            <input type="name" class="form-control" style="text-transform: capitalize;" name="name" required id="email" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">No. WA Aktif</label>
                            <input type="text" class="form-control" name="phone" required id="exampleInputEmail1" value='' placeholder="">
                        </div>
                        <div class="form-group col-md-6">
                            <label>Tanggal Lahir</label>
                            <input class="form-control form-control-inline input-medium datepicker" type="text" name="birthdate" value="" placeholder="" readonly="" required>      
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Jenis Kelamin</label>
                            <select class="form-control" style="margin-bottom:15px;" name="sex" value='' required>
                                <option value="Male"> Laki </option>
                                <option value="Female"> Perempuan </option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="exampleInputEmail1">Alamat</label>
                            <input type="text" class="form-control" style="text-transform: capitalize;" name="address" required id="exampleInputEmail1" value='' placeholder="">
                        </div>
                        <div class="form-group col-md-6">
                            <label>RT RW</label>
                            <input class="form-control" type="text" name="rtrw" value="" placeholder="" required>      
                        </div>
                        <div class="form-group col-md-6">
                            <label>Kelurahan</label>
                            <input class="form-control" type="text" name="kelurahan" value="" placeholder="" required>      
                        </div>
                        <div class="form-group col-md-6">
                            <label>Kecamatan</label>
                            <input class="form-control" type="text" name="kecamatan" value="" placeholder="" required>      
                        </div>
                        <div class="form-group col-md-6">
                            <label>Kota</label>
                            <input class="form-control" type="text" name="kota" value="" placeholder="" required>      
                        </div>
                        <div class="form-group col-md-6">
                            <label>Kode Pos</label>
                            <input class="form-control" type="text" name="kodepos" value="" placeholder="" required>      
                        </div>
                        <div class="form-group col-md-6">
                            <label>Pronvinsi</label>
                            <input class="form-control" type="text" name="provinsi" value="" placeholder="" required>      
                        </div>
                    </div>
                    <button class="btn btn-lg btn-login btn-block" id="btnsave" type="submit">Register</button>
                </div>
            </form>

        </div>
        <!-- js placed at the end of the document so the pages load faster -->
        <script src="common/js/jquery.js"></script>
        <script src="common/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
        <script>
            $(".datepicker").datepicker({
                format:'dd-mm-yyyy',
                autoclose:true
            });
            // $('#datepicker').datepicker({
            //     format:'yyyy-mm-dd',
            //     daysOfWeekDisabled:[],
            //     startDate: moment().add(0,'days').toDate(),
            //     endDate: moment().add(20,'days').toDate(),
            //     beforeShowDay: function(d) {
            //         console.log(d);
            //     }
            // });
            // $('#datepicker').on('changeDate', function() {
            //     $("input[name=date]").val($('#datepicker').datepicker('getFormattedDate'));
            //     getSlot();
            // });
            $("input[name=date]").on('change',function() {
                getSlot();
            });

            $("#hospital").change(function(e){
                var h = $(this).val();
                $("#doctor").html('<option value="">Pilih Dokter</option>');
                $("#payment_category").html('<option value="">Pilih Perawatan</option>');
                if(h){
                    disableButton(true,'Loading...');
                    $.get("auth/dataregister",{h:h}, function( data ) {
                        $.each(data.doctors,function(i,v){
                            $("#doctor").append('<option value="'+v.id+'">'+v.name+'</option>');
                        });
                        $.each(data.paymentcategorys,function(i,v){
                            if(v.type == 'registrasi')
                                $("#payment_category").append('<option value="'+v.description+'">'+v.description+'</option>');
                        });
                    })
                    .always(function(e){
                        disableButton(false,'');
                    });
                }
            });

            $("#doctor").change(function(e){
                getSlot();
            });

            function disableButton(disable,txt){
                var btn = $("#btnsave");
                if(disable){
                    btn.attr('disabled',disable);
                    btn.html('<i>'+txt+'</i>');
                }else{
                    btn.attr('disabled',disable);
                    btn.html('Register');
                }
            }

            function getSlot(){
                var d = $("input[name=date]").val();
                var i = $("#doctor").val();
                $('#aslots').find('option').remove();
                if(d && i){
                    disableButton(true,'Loading...');
                    $.ajax({
                        url: 'auth/getAvailableSlotByDoctorByDateByAppointmentIdByJason?date=' + d + '&doctor=' + i + '&appointment_id=',
                        method: 'GET',
                        data: '',
                        dataType: 'json',
                    }).success(function (response) {
                        var slots = response.aslots;
                        $.each(slots, function (key, value) {
                            $('#aslots').append($('<option>').text(value).val(value)).end();
                        });
                        //   $("#default-step-1 .button-next").trigger("click");
                        if ($('#aslots').has('option').length == 0) {                    //if it is blank. 
                            $('#aslots').append($('<option>').text('Maaf saat ini tidak tersedia slot').val('Not Selected')).end();
                        }


                        // Populate the form fields with the data returned from server
                        //  $('#default').find('[name="staff"]').val(response.appointment.staff).end()
                    }).always(function(r){
                        disableButton(false,'');
                    });
                }
            }

            $("#email").change(function(e){
                // var e = $(this).val();
                // if(e){
                //     disableButton(true,'Loading...');
                //     $.get("auth/getPatientByEmail",{email:e}, function( data ) {
                //         if(data.success){
                //             $("input[name=name]").val(data.patient.name);
                //             $("input[name=address]").val(data.patient.address);
                //             $("input[name=phone]").val(data.patient.phone);
                //         }
                //     })
                //     .always(function(e){
                //         disableButton(false,'');
                //     });
                // }
            });
            $("#hospital").change();
        </script>

    </body>
</html>
