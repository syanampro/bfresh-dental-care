<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://bfreshdental.com/xmlrpc.php">
    <meta name='robots' content='index, follow, max-image-preview:large, max-snippet:-1, max-video-preview:-1' />
    <!-- This site is optimized with the Yoast SEO plugin v17.5 - https://yoast.com/wordpress/plugins/seo/ -->
    <title>Klinik Dokter Gigi Terdekat Sidoarjo | Hubungi 0856-4526-2347 -</title>
    <meta name="description" content="Klinik Dokter Gigi Terdekat. B-Fresh Dental &amp; Skincare siap menjadi solusi untuk permasalahan Gigi dan Kulit Wajah Anda. Hub : 0856-4526-2347" />
    <link rel="canonical" href="https://bfreshdental.com/" />
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Klinik Dokter Gigi Terdekat Sidoarjo | Hubungi 0856-4526-2347 -" />
    <meta property="og:description" content="Klinik Dokter Gigi Terdekat. B-Fresh Dental &amp; Skincare siap menjadi solusi untuk permasalahan Gigi dan Kulit Wajah Anda. Hub : 0856-4526-2347" />
    <meta property="og:url" content="https://bfreshdental.com/" />
    <meta property="article:modified_time" content="2021-10-28T03:08:17+00:00" />
    <meta property="og:image" content="https://bfreshdental.com/wp-content/uploads/2021/03/banner.png" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:label1" content="Est. reading time" />
    <meta name="twitter:data1" content="9 minutes" />
    <script type="application/ld+json" class="yoast-schema-graph">
      {
        "@context": "https://schema.org",
        "@graph": [{
          "@type": "WebSite",
          "@id": "https://bfreshdental.com/#website",
          "url": "https://bfreshdental.com/",
          "name": "",
          "description": "",
          "potentialAction": [{
            "@type": "SearchAction",
            "target": {
              "@type": "EntryPoint",
              "urlTemplate": "https://bfreshdental.com/?s={search_term_string}"
            },
            "query-input": "required name=search_term_string"
          }],
          "inLanguage": "en-US"
        }, {
          "@type": "ImageObject",
          "@id": "https://bfreshdental.com/#primaryimage",
          "inLanguage": "en-US",
          "url": "https://bfreshdental.com/wp-content/uploads/2021/03/banner.png",
          "contentUrl": "https://bfreshdental.com/wp-content/uploads/2021/03/banner.png",
          "width": 750,
          "height": 501,
          "caption": "Klinik Dokter Gigi Terdekat Sidoarjo"
        }, {
          "@type": "WebPage",
          "@id": "https://bfreshdental.com/#webpage",
          "url": "https://bfreshdental.com/",
          "name": "Klinik Dokter Gigi Terdekat Sidoarjo | Hubungi 0856-4526-2347 -",
          "isPartOf": {
            "@id": "https://bfreshdental.com/#website"
          },
          "primaryImageOfPage": {
            "@id": "https://bfreshdental.com/#primaryimage"
          },
          "datePublished": "2021-03-25T04:27:53+00:00",
          "dateModified": "2021-10-28T03:08:17+00:00",
          "description": "Klinik Dokter Gigi Terdekat. B-Fresh Dental & Skincare siap menjadi solusi untuk permasalahan Gigi dan Kulit Wajah Anda. Hub : 0856-4526-2347",
          "breadcrumb": {
            "@id": "https://bfreshdental.com/#breadcrumb"
          },
          "inLanguage": "en-US",
          "potentialAction": [{
            "@type": "ReadAction",
            "target": ["https://bfreshdental.com/"]
          }]
        }, {
          "@type": "BreadcrumbList",
          "@id": "https://bfreshdental.com/#breadcrumb",
          "itemListElement": [{
            "@type": "ListItem",
            "position": 1,
            "name": "Home"
          }]
        }]
      }
    </script>
    <!-- / Yoast SEO plugin. -->
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel='dns-prefetch' href='//s.w.org' />
    <link rel='dns-prefetch' href='//c0.wp.com' />
    <link rel="alternate" type="application/rss+xml" title=" &raquo; Feed" href="https://bfreshdental.com/feed/" />
    <link rel="alternate" type="application/rss+xml" title=" &raquo; Comments Feed" href="https://bfreshdental.com/comments/feed/" />
    <script type="text/javascript">
      window._wpemojiSettings = {
        "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/72x72\/",
        "ext": ".png",
        "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/13.1.0\/svg\/",
        "svgExt": ".svg",
        "source": {
          "concatemoji": "https:\/\/bfreshdental.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.8.2"
        }
      };
      ! function(e, a, t) {
        var n, r, o, i = a.createElement("canvas"),
          p = i.getContext && i.getContext("2d");

        function s(e, t) {
          var a = String.fromCharCode;
          p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, e), 0, 0);
          e = i.toDataURL();
          return p.clearRect(0, 0, i.width, i.height), p.fillText(a.apply(this, t), 0, 0), e === i.toDataURL()
        }

        function c(e) {
          var t = a.createElement("script");
          t.src = e, t.defer = t.type = "text/javascript", a.getElementsByTagName("head")[0].appendChild(t)
        }
        for (o = Array("flag", "emoji"), t.supports = {
            everything: !0,
            everythingExceptFlag: !0
          }, r = 0; r < o.length; r++) t.supports[o[r]] = function(e) {
          if (!p || !p.fillText) return !1;
          switch (p.textBaseline = "top", p.font = "600 32px Arial", e) {
            case "flag":
              return s([127987, 65039, 8205, 9895, 65039], [127987, 65039, 8203, 9895, 65039]) ? !1 : !s([55356, 56826, 55356, 56819], [55356, 56826, 8203, 55356, 56819]) && !s([55356, 57332, 56128, 56423, 56128, 56418, 56128, 56421, 56128, 56430, 56128, 56423, 56128, 56447], [55356, 57332, 8203, 56128, 56423, 8203, 56128, 56418, 8203, 56128, 56421, 8203, 56128, 56430, 8203, 56128, 56423, 8203, 56128, 56447]);
            case "emoji":
              return !s([10084, 65039, 8205, 55357, 56613], [10084, 65039, 8203, 55357, 56613])
          }
          return !1
        }(o[r]), t.supports.everything = t.supports.everything && t.supports[o[r]], "flag" !== o[r] && (t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && t.supports[o[r]]);
        t.supports.everythingExceptFlag = t.supports.everythingExceptFlag && !t.supports.flag, t.DOMReady = !1, t.readyCallback = function() {
          t.DOMReady = !0
        }, t.supports.everything || (n = function() {
          t.readyCallback()
        }, a.addEventListener ? (a.addEventListener("DOMContentLoaded", n, !1), e.addEventListener("load", n, !1)) : (e.attachEvent("onload", n), a.attachEvent("onreadystatechange", function() {
          "complete" === a.readyState && t.readyCallback()
        })), (n = t.source || {}).concatemoji ? c(n.concatemoji) : n.wpemoji && n.twemoji && (c(n.twemoji), c(n.wpemoji)))
      }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
      img.wp-smiley,
      img.emoji {
        display: inline !important;
        border: none !important;
        box-shadow: none !important;
        height: 1em !important;
        width: 1em !important;
        margin: 0 .07em !important;
        vertical-align: -0.1em !important;
        background: none !important;
        padding: 0 !important;
      }
    </style>
    <link rel='stylesheet' id='wp-block-library-css' href='https://c0.wp.com/c/5.8.2/wp-includes/css/dist/block-library/style.min.css' type='text/css' media='all' />
    <style id='wp-block-library-inline-css' type='text/css'>
      .has-text-align-justify {
        text-align: justify;
      }
    </style>
    <link rel='stylesheet' id='mediaelement-css' href='https://c0.wp.com/c/5.8.2/wp-includes/js/mediaelement/mediaelementplayer-legacy.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='wp-mediaelement-css' href='https://c0.wp.com/c/5.8.2/wp-includes/js/mediaelement/wp-mediaelement.min.css' type='text/css' media='all' />
    <link rel='stylesheet' id='a3-pvc-style-css' href='https://bfreshdental.com/wp-content/plugins/page-views-count/assets/css/style.min.css?ver=2.4.12' type='text/css' media='all' />
    <link rel='stylesheet' id='rock-business-google-fonts-css' href='https://fonts.googleapis.com/css?family=Montserrat%3A300%2C400%2C500%2C600%2C700&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min.css?ver=4.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='slick-theme-css-css' href='https://bfreshdental.com/wp-content/themes/rock-business/assets/css/slick-theme.min.css?ver=v2.2.0' type='text/css' media='all' />
    <link rel='stylesheet' id='slick-css-css' href='https://bfreshdental.com/wp-content/themes/rock-business/assets/css/slick.min.css?ver=v1.8.0' type='text/css' media='all' />
    <link rel='stylesheet' id='rock-business-style-css' href='https://bfreshdental.com/wp-content/themes/rock-business/style.css?ver=5.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.13.0' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-frontend-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/css/frontend.min.css?ver=3.4.7' type='text/css' media='all' />
    <style id='elementor-frontend-inline-css' type='text/css'>
      @font-face {
        font-family: eicons;
        src: url(https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/fonts/eicons.eot?5.10.0);
        src: url(https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/fonts/eicons.eot?5.10.0#iefix) format("embedded-opentype"), url(https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/fonts/eicons.woff2?5.10.0) format("woff2"), url(https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/fonts/eicons.woff?5.10.0) format("woff"), url(https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/fonts/eicons.ttf?5.10.0) format("truetype"), url(https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/eicons/fonts/eicons.svg?5.10.0#eicon) format("svg");
        font-weight: 400;
        font-style: normal
      }
    </style>
    <link rel='stylesheet' id='elementor-post-12-css' href='https://bfreshdental.com/wp-content/uploads/elementor/css/post-12.css?ver=1635740034' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-post-15-css' href='https://bfreshdental.com/wp-content/uploads/elementor/css/post-15.css?ver=1635740034' type='text/css' media='all' />
    <link rel='stylesheet' id='a3pvc-css' href='//bfreshdental.com/wp-content/uploads/sass/pvc.min.css?ver=1616488777' type='text/css' media='all' />
    <link rel='stylesheet' id='google-fonts-1-css' href='https://fonts.googleapis.com/css?family=Roboto%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CRoboto+Slab%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic%7CPoppins%3A100%2C100italic%2C200%2C200italic%2C300%2C300italic%2C400%2C400italic%2C500%2C500italic%2C600%2C600italic%2C700%2C700italic%2C800%2C800italic%2C900%2C900italic&#038;display=auto&#038;ver=5.8.2' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-shared-0-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-solid-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='elementor-icons-fa-brands-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/font-awesome/css/brands.min.css?ver=5.15.3' type='text/css' media='all' />
    <link rel='stylesheet' id='jetpack_css-css' href='https://c0.wp.com/p/jetpack/10.3/css/jetpack.css' type='text/css' media='all' />
    <script type='text/javascript' src='https://c0.wp.com/c/5.8.2/wp-includes/js/jquery/jquery.min.js' id='jquery-core-js'></script>
    <script type='text/javascript' src='https://c0.wp.com/c/5.8.2/wp-includes/js/jquery/jquery-migrate.min.js' id='jquery-migrate-js'></script>
    <link rel="https://api.w.org/" href="https://bfreshdental.com/wp-json/" />
    <link rel="alternate" type="application/json" href="https://bfreshdental.com/wp-json/wp/v2/pages/15" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://bfreshdental.com/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://bfreshdental.com/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 5.8.2" />
    <link rel='shortlink' href='https://bfreshdental.com/' />
    <link rel="alternate" type="application/json+oembed" href="https://bfreshdental.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fbfreshdental.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://bfreshdental.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fbfreshdental.com%2F&#038;format=xml" />
    <style type='text/css'>
      img#wpstats {
        display: none
      }
    </style>
    <style type="text/css">
      .recentcomments a {
        display: inline !important;
        padding: 0 !important;
        margin: 0 !important;
      }
    </style>
  </head>
  <body data-rsssl=1 class="home page-template page-template-elementor_header_footer page page-id-15 wp-custom-logo group-blog right-sidebar elementor-default elementor-template-full-width elementor-kit-12 elementor-page elementor-page-15">
    <div id="page" class="site">
      <a class="skip-link screen-reader-text" href="#content">Skip to content</a>
      <header id="masthead" class="site-header" role="banner">
        <div class="wrapper">
          <div class="site-branding">
            <div class="site-logo">
              <a href="<?= base_url();?>" class="custom-logo-link" rel="home" aria-current="page">
                <img width="952" height="364" src="https://bfreshgigi.com/uploads/logo-bfresh-400.png" class="custom-logo" alt="" srcset="https://bfreshgigi.com/uploads/logo-bfresh-400.png 952w, https://bfreshgigi.com/uploads/logo-bfresh-400.png 300w, https://bfreshgigi.com/uploads/logo-bfresh-400.png 768w" sizes="(max-width: 952px) 100vw, 952px" />
              </a>
            </div>
            <!-- .site-logo -->
            <div id="site-identity">
              <h1 class="site-title">
                <a href="https://bfreshgigi.com/" rel="home"></a>
              </h1>
            </div>
            <!-- #site-identity -->
          </div>
          <!-- .site-branding -->
          <!-- #site-navigation -->
        </div>
        <!-- .wrapper -->
      </header>
      <!-- header ends here -->
      <div id="content" class="site-content">
        <div data-elementor-type="wp-page" data-elementor-id="15" class="elementor elementor-15" data-elementor-settings="[]">
          <div class="elementor-section-wrap">
            <section class="elementor-section elementor-top-section elementor-element elementor-element-5418712e elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="5418712e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;gradient&quot;,&quot;shape_divider_bottom&quot;:&quot;triangle-asymmetrical&quot;,&quot;shape_divider_bottom_negative&quot;:&quot;yes&quot;}">
              <div class="elementor-background-overlay"></div>
              <div class="elementor-shape elementor-shape-bottom" data-negative="true">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1000 100" preserveAspectRatio="none">
                  <path class="elementor-shape-fill" d="M737.9,94.7L0,0v100h1000V0L737.9,94.7z" />
                </svg>
              </div>
              <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-71685746" data-id="71685746" data-element_type="column">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-6794dca3 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6794dca3" data-element_type="section">
                      <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-41c52595 animated-slow elementor-invisible" data-id="41c52595" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInLeft&quot;}">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-21e9dcce elementor-widget elementor-widget-heading" data-id="21e9dcce" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                <h6 class="elementor-heading-title elementor-size-default">Ukir Senyum Bahagia Anda dengan Gigi yang Bersih dan Sehat </h6>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-27dd8f99 elementor-widget elementor-widget-text-editor" data-id="27dd8f99" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <p>B Fresh Dental Care siap menjadi solusi untuk permasalahan Mulut dan Gigi Anda. Hadir untuk keluarga Indonesia kami siap memberikan pelayanan terbaik bagi Anda. Kunjungi B Fresh terdekat di Sidoarjo dan Gresik.</p>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-549079b8 elementor-mobile-align-center elementor-align-left elementor-widget elementor-widget-button" data-id="549079b8" data-element_type="widget" data-widget_type="button.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper" style="margin-bottom: 10px;">
                                  <a href="<?= base_url('auth/register');?>" class="elementor-button-link elementor-button elementor-size-md" role="button">
                                    <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-icon elementor-align-icon-left">
                                      </span>
                                      <span class="elementor-button-text">Belum Pernah Periksa? <br><br><strong>Daftar Disini</strong></span>
                                    </span>
                                  </a>
                                </div>
                                <div class="elementor-button-wrapper">
                                  <a href="<?= base_url('auth/login');?>" class="elementor-button-link elementor-button elementor-size-md" role="button">
                                    <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-icon elementor-align-icon-left">
                                      </span>
                                      <span class="elementor-button-text">Sudah Pernah Periksa? <br><br><strong>Daftar Disini</strong></span>
                                    </span>
                                  </a>
                                </div>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-20db06a elementor-widget elementor-widget-spacer" data-id="20db06a" data-element_type="widget" data-widget_type="spacer.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-spacer">
                                  <div class="elementor-spacer-inner"></div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-609ec25c animated-slow elementor-invisible" data-id="609ec25c" data-element_type="column" data-settings="{&quot;animation&quot;:&quot;fadeInRight&quot;}">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <div class="elementor-element elementor-element-d10d527 elementor-widget elementor-widget-image" data-id="d10d527" data-element_type="widget" data-widget_type="image.default">
                      <div class="elementor-widget-container">
                        <img width="640" height="428" src="https://bfreshdental.com/wp-content/uploads/2021/03/banner.png" class="attachment-large size-large" alt="Klinik Dokter Gigi Terdekat Sidoarjo" loading="lazy" srcset="https://bfreshdental.com/wp-content/uploads/2021/03/banner.png 750w, https://bfreshdental.com/wp-content/uploads/2021/03/banner-300x200.png 300w" sizes="(max-width: 640px) 100vw, 640px" />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!-- <section class="elementor-section elementor-top-section elementor-element elementor-element-89e9ccb elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="89e9ccb" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-521c6e67" data-id="521c6e67" data-element_type="column">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <div class="elementor-element elementor-element-413f3dff elementor-widget elementor-widget-spacer" data-id="413f3dff" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-e763ab0 elementor-widget elementor-widget-image" data-id="e763ab0" data-element_type="widget" data-widget_type="image.default">
                      <div class="elementor-widget-container">
                        <img width="85" height="85" src="https://bfreshdental.com/wp-content/uploads/2021/03/2.png" class="attachment-large size-large" alt="" loading="lazy" />
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-13049ba elementor-widget elementor-widget-heading" data-id="13049ba" data-element_type="widget" data-widget_type="heading.default">
                      <div class="elementor-widget-container">
                        <h6 class="elementor-heading-title elementor-size-default">Layanan Kesehatan Gigi dan Mulut Terlengkap</h6>
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-f9ef7d2 elementor-widget elementor-widget-heading" data-id="f9ef7d2" data-element_type="widget" data-widget_type="heading.default">
                      <div class="elementor-widget-container">
                        <h6 class="elementor-heading-title elementor-size-default">Perawatan dan layanan yang kami sediakan berada di tempat yang nyaman, Berikut daftar layanan kami</h6>
                      </div>
                    </div>
                    <div class="elementor-element elementor-element-c2a4df9 elementor-widget elementor-widget-spacer" data-id="c2a4df9" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-9033afe elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9033afe" data-element_type="section">
                      <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-09de7d0" data-id="09de7d0" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-8f3a3cc elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="8f3a3cc" data-element_type="widget" data-widget_type="image-box.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-box-wrapper">
                                  <figure class="elementor-image-box-img">
                                    <img width="650" height="433" src="https://bfreshdental.com/wp-content/uploads/2021/03/obat-sakit-gigi-berlubang-untuk-mengatasi-nyeri.jpg" class="attachment-full size-full" alt="Biaya Ke Dokter Gigi Sidoarjo | 0856-4526-2347" loading="lazy" />
                                  </figure>
                                  <div class="elementor-image-box-content">
                                    <h3 class="elementor-image-box-title">Obati Gigi dan Gusi Sakit </h3>
                                    <p class="elementor-image-box-description">Saat anda mengalami masalah pada ggi dan gusi anda hingga merasa sakit, silahkan hubungi kami Bfresh Dental Care, kami akan memberikan pelayanan terbaik kepada anda hingga sembuh dengan para dokter yang berpengalaman.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-a868a77 elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="a868a77" data-element_type="widget" data-widget_type="image-box.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-box-wrapper">
                                  <figure class="elementor-image-box-img">
                                    <img width="650" height="433" src="https://bfreshdental.com/wp-content/uploads/2021/03/layanan-kami-3.jpg" class="attachment-full size-full" alt="Bedah Mulut Sidoarjo Bfresh | Hubungi 0856-4526-2347" loading="lazy" />
                                  </figure>
                                  <div class="elementor-image-box-content">
                                    <h3 class="elementor-image-box-title">Endodontik</h3>
                                    <p class="elementor-image-box-description">Endodontik merupakan perawatan untuk memperbaiki gigi yang rusak parah atau terkena infeksi. Perawatan ini dapat mengatasi masalah gigi tanpa perlu mencabutnya, sehingga gigi Anda bisa digunakan kembali dengan nyaman.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-03fcaa1" data-id="03fcaa1" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-6cc386a elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="6cc386a" data-element_type="widget" data-widget_type="image-box.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-box-wrapper">
                                  <figure class="elementor-image-box-img">
                                    <img width="650" height="433" src="https://bfreshdental.com/wp-content/uploads/2021/03/048856000_1599720161-Gigi-Berlubang-Ditambal-atau-Dicabut-shutterstock_1013667112-1.jpg" class="attachment-full size-full" alt="Dokter Gigi Terdekat 24 Jam Sidoarjo | 0856-4526-2347" loading="lazy" />
                                  </figure>
                                  <div class="elementor-image-box-content">
                                    <h3 class="elementor-image-box-title">Tambal & Cabut gigi</h3>
                                    <p class="elementor-image-box-description">siapa yang sering mengalami gigi lubang ?. masalah yang sering mengganggu ini akan kami berikan solusi penanganannya sesuai keinginan dan masalah penyakitnya. taklupa kami juga akan memberikan edukasi dalam menjaga gigi anda .</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-94909a6 elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="94909a6" data-element_type="widget" data-widget_type="image-box.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-box-wrapper">
                                  <figure class="elementor-image-box-img">
                                    <img width="650" height="433" src="https://bfreshdental.com/wp-content/uploads/2021/03/layanan-kami-6.jpg" class="attachment-full size-full" alt="Ahli Gigi Terdekat Sidoarjo | 0856-4526-2347" loading="lazy" />
                                  </figure>
                                  <div class="elementor-image-box-content">
                                    <h3 class="elementor-image-box-title">Pemasangan Kawat Gigi</h3>
                                    <p class="elementor-image-box-description">Apakah posisi gigi anda tidak rapi, atau posisi rahang yang tidak normal, kemudian ingin menggunakan kawat gigi untuk merapikannya ? Anda berada di tempat yang tepat, karena kami menyediakan layanan tersebut.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b67191e" data-id="b67191e" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-462d32f elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="462d32f" data-element_type="widget" data-widget_type="image-box.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-box-wrapper">
                                  <figure class="elementor-image-box-img">
                                    <img width="650" height="433" src="https://bfreshdental.com/wp-content/uploads/2021/03/layanan-kami-4.jpg" class="attachment-full size-full" alt="Biaya Bedah Mulut Sidoarjo | 0856-4526-2347" loading="lazy" />
                                  </figure>
                                  <div class="elementor-image-box-content">
                                    <h3 class="elementor-image-box-title">Veneer</h3>
                                    <p class="elementor-image-box-description">Veneer gigi adalah prosedur kecantikan yang dikerjakan oleh dokter gigi untuk memperbaiki warna, bentuk, dan posisi gigi, serta memperbaiki gigi yang gompal. Jika anda mengalami masalah ini silahkan kunjungi kami.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-7e3556f elementor-position-top elementor-vertical-align-top elementor-widget elementor-widget-image-box" data-id="7e3556f" data-element_type="widget" data-widget_type="image-box.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-image-box-wrapper">
                                  <figure class="elementor-image-box-img">
                                    <img width="650" height="433" src="https://bfreshdental.com/wp-content/uploads/2021/03/layanan-kami-5.jpg" class="attachment-full size-full" alt="Dokter Bedah Mulut Sidoarjo | 0856-4526-2347" loading="lazy" />
                                  </figure>
                                  <div class="elementor-image-box-content">
                                    <h3 class="elementor-image-box-title">Odontektomi</h3>
                                    <p class="elementor-image-box-description">Salah satu layanan kami adalah odontektomi, mencabut gigi dengan metode operasi. Namun untuk melakukannya tentu harus dilakukan diagnonas terlebih dahulu sehingga dokter akan memberikan rekomendasi untuk operasi gigi.</p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </section> -->
            <!-- <section class="elementor-section elementor-top-section elementor-element elementor-element-beadd7c elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="beadd7c" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0f3aed2" data-id="0f3aed2" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-69e9d8e elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="69e9d8e" data-element_type="section">
                      <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e6346cd" data-id="e6346cd" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-f7a56e4 elementor-widget elementor-widget-image" data-id="f7a56e4" data-element_type="widget" data-widget_type="image.default">
                              <div class="elementor-widget-container">
                                <img width="566" height="550" src="https://bfreshdental.com/wp-content/uploads/2021/03/gambar-1-1.jpg" class="attachment-large size-large" alt="Dr Gigi Terdekat Sidoarjo | 0856-4526-2347" loading="lazy" srcset="https://bfreshdental.com/wp-content/uploads/2021/03/gambar-1-1.jpg 566w, https://bfreshdental.com/wp-content/uploads/2021/03/gambar-1-1-300x292.jpg 300w" sizes="(max-width: 566px) 100vw, 566px" />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-a0178b7" data-id="a0178b7" data-element_type="column" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-c1532ff elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="c1532ff" data-element_type="widget" data-widget_type="icon-list.default">
                              <div class="elementor-widget-container">
                                <ul class="elementor-icon-list-items">
                                  <li class="elementor-icon-list-item">
                                    <span class="elementor-icon-list-icon">
                                      <i aria-hidden="true" class="fas fa-tooth"></i>
                                    </span>
                                    <span class="elementor-icon-list-text">Bleaching & bersihkan karang gigi</span>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-d87c13a elementor-widget elementor-widget-text-editor" data-id="d87c13a" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <p>Layanan kami selanjutnya adalah pembersihan karang gigi dan pemutihan gigi, jika anda mengalami masalah pada gigi yang menguning dan ingin memutihkan gigi anda bisa melayani kami.</p>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-1d07f03 elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="1d07f03" data-element_type="widget" data-widget_type="icon-list.default">
                              <div class="elementor-widget-container">
                                <ul class="elementor-icon-list-items">
                                  <li class="elementor-icon-list-item">
                                    <span class="elementor-icon-list-icon">
                                      <i aria-hidden="true" class="fas fa-tooth"></i>
                                    </span>
                                    <span class="elementor-icon-list-text">Splinting & Replantasi gigi</span>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-ae1086d elementor-widget elementor-widget-text-editor" data-id="ae1086d" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <p>JIka anda mengalami gigi goyang, kami juga menyediakan layanan splinting dan bisa menguatkan gigi yang goyang. selain itu kami juga menyediakan layaan replantasi gigi.</p>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-ad5b6d3 elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="ad5b6d3" data-element_type="widget" data-widget_type="icon-list.default">
                              <div class="elementor-widget-container">
                                <ul class="elementor-icon-list-items">
                                  <li class="elementor-icon-list-item">
                                    <span class="elementor-icon-list-icon">
                                      <i aria-hidden="true" class="fas fa-tooth"></i>
                                    </span>
                                    <span class="elementor-icon-list-text">Eksisi dan insisi gusi dan mukosa mulut</span>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-ee388a7 elementor-widget elementor-widget-text-editor" data-id="ee388a7" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <p>JIka anda mengalami masalah kelainan di rongga mulut seperti gigi bungsu yang terpendam atau tumbuh miring, implan gigi, kecelakaan yang menyebabkan patah tulang rahang, kelainan celah bibir dan celah langit-langit serta tumor di rongga mulut. Kami bisa menangani masalah tersebut dengan eksisi dan isisi gusi dan mukosa mulut.</p>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-c21e001 elementor-icon-list--layout-traditional elementor-list-item-link-full_width elementor-widget elementor-widget-icon-list" data-id="c21e001" data-element_type="widget" data-widget_type="icon-list.default">
                              <div class="elementor-widget-container">
                                <ul class="elementor-icon-list-items">
                                  <li class="elementor-icon-list-item">
                                    <span class="elementor-icon-list-icon">
                                      <i aria-hidden="true" class="fas fa-tooth"></i>
                                    </span>
                                    <span class="elementor-icon-list-text">Perawatan Kecantikan di @bfreshskincare</span>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-4d0e5f8 elementor-widget elementor-widget-text-editor" data-id="4d0e5f8" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <p>Apakah anda mengalami masalah kulit pada wajah, anda bisa berkonsultasi pada kami, karena kami juga memiliki layanan untuk perawatan kecantikan.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                  </div>
                </div>
              </div>
            </section> -->
            <!-- <section class="elementor-section elementor-top-section elementor-element elementor-element-43d6ce6 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="43d6ce6" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-54b5424" data-id="54b5424" data-element_type="column">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <div class="elementor-element elementor-element-4550e3e elementor-widget elementor-widget-spacer" data-id="4550e3e" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section> -->
            <!-- <section class="elementor-section elementor-top-section elementor-element elementor-element-1a126690 elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="1a126690" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-75d0b4d5" data-id="75d0b4d5" data-element_type="column">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <div class="elementor-element elementor-element-403a18c elementor-widget elementor-widget-spacer" data-id="403a18c" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-6655111a elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6655111a" data-element_type="section">
                      <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-70b4283e" data-id="70b4283e" data-element_type="column">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-e68247f elementor-widget elementor-widget-heading" data-id="e68247f" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                <h6 class="elementor-heading-title elementor-size-default">We Love to See You Smile </h6>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-fcc01c3 elementor-widget elementor-widget-heading" data-id="fcc01c3" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                <h6 class="elementor-heading-title elementor-size-default">Dapatkan Penawaran Spesial Pemeriksaan Gigi</h6>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-51809a30" data-id="51809a30" data-element_type="column">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-26cac3fb elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="26cac3fb" data-element_type="widget" data-widget_type="button.default">
                              <div class="elementor-widget-container">
                                <div class="elementor-button-wrapper">
                                  <a href="http://bit.ly/AppointmentBfresh" class="elementor-button-link elementor-button elementor-size-md" role="button">
                                    <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-icon elementor-align-icon-left">
                                        <i aria-hidden="true" class="fas fa-arrow-circle-right"></i>
                                      </span>
                                      <span class="elementor-button-text">Jadwalkan Kunjunganmu Sekarang</span>
                                    </span>
                                  </a>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <div class="elementor-element elementor-element-b150d02 elementor-widget elementor-widget-spacer" data-id="b150d02" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section> -->
            <section class="elementor-section elementor-top-section elementor-element elementor-element-b1cf8a6 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="b1cf8a6" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-80fb3fb" data-id="80fb3fb" data-element_type="column">
                  <div class="elementor-widget-wrap elementor-element-populated">
                    <div class="elementor-element elementor-element-bcd0203 elementor-widget elementor-widget-spacer" data-id="bcd0203" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                    <section class="elementor-section elementor-inner-section elementor-element elementor-element-9f02770 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="9f02770" data-element_type="section">
                      <div class="elementor-container elementor-column-gap-default">
                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-fa85f16" data-id="fa85f16" data-element_type="column">
                          <div class="elementor-widget-wrap elementor-element-populated">
                            <div class="elementor-element elementor-element-300ea13 elementor-widget elementor-widget-heading" data-id="300ea13" data-element_type="widget" data-widget_type="heading.default">
                              <div class="elementor-widget-container">
                                <h2>Bfresh Dental Care</h2>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-5131adf elementor-widget elementor-widget-text-editor" data-id="5131adf" data-element_type="widget" data-widget_type="text-editor.default">
                              <div class="elementor-widget-container">
                                <p>B FRESH DENTAL CARE merupakan layanan dokter gigi terpercaya di Sidoarjo, Gresik dan akan terus menambah lokasi di beberapa kota lain. Saat ini B FRESH DENTAL CARE terdapat di 8 Lokasi, yaitu di Sidoarjo: Buduran, Candi, Wonoayu, Tanggulangin, Taman, Waru. Di Gresik: GKB dan Bunder.</p>
                                <p>Sesuai dengan namanya, pasien yang datang dengan wajah kesel, bibir manyun, setelah perawatan di sini wajah menjadi merona, senyum manis FRESH! Yuk, buktikan langsung ke B FRESH DENTAL CARE.</p>
                                <p>B FRESH terjangkau tarifnya, terjangkau lokasinya, memberikan banyak keuntungan: dokter gigi on time, profesional, ramah, dan amanah. Periksa gigi tanpa antri dengan sistem reservasi online yang mudah. Tarif perawatan yang ramah di kantong. Tim yang humble dan cekatan. Tersedia member card yang memberikan banyak benefit. Lengkap dengan voucher perawatan yang bisa dipakai sendiri atau dihadiahkan kepada orang lain.</p>
                                <p>Program-program menarik hadir setiap pekan, yaitu giveaway, promat harga hemat, merchandise, konsultasi gratis, bingkisan sedekah, dentist goes to school, edukasi komunitas gratis dan kuis berhadiah.</p>
                                <p>Adapun perawatannya meliputi: pengobatan sakit gigi dan gusi, tambal gigi berlubang, pasang kawat gigi (behel), pasang gigi palsu, cabut gigi, obat sakit gigi, veneer, bleaching (memutihkan gigi), gingivectomy (pemotongan gusi), odontektomi (cabut gigi miring), splinting (mengikat gigi goyang), scaling (membersihkan karang gigi), mengobati sariawan, menangani bau mulut, menangani hilangkan noda gigi, perawatan saluran akar dan perawatan gigi lainnya.</p>
                                <p>Kami menerima masukan dari teman teman semua untuk B FRESH lebih baik, demi semakin meningkatnya kualitas kesehatan masyarakat.</p>
                                <p>Follow instagram <a href="https://instagram.com/bfreshdentalcare">@bfreshdentalcare</a> <br>Shopping Voucher perawatan <a href="shopee.co.id/bfreshdentalcare">shopee.co.id/bfreshdentalcare</a></p>
                              </div>
                            </div>
                            <div class="elementor-element elementor-element-00c0520 elementor-widget elementor-widget-button" data-id="00c0520" data-element_type="widget" data-widget_type="button.default">
                              <div class="elementor-widget-container">
                                <!-- <div class="elementor-button-wrapper">
                                  <a href="http://bit.ly/PromoBfresh" class="elementor-button-link elementor-button elementor-size-sm" role="button">
                                    <span class="elementor-button-content-wrapper">
                                      <span class="elementor-button-text">Dapatkan Promo</span>
                                    </span>
                                  </a>
                                </div> -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </section>
                    <div class="elementor-element elementor-element-804f242 elementor-widget elementor-widget-spacer" data-id="804f242" data-element_type="widget" data-widget_type="spacer.default">
                      <div class="elementor-widget-container">
                        <div class="elementor-spacer">
                          <div class="elementor-spacer-inner"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="elementor-section elementor-top-section elementor-element elementor-element-6bf1ac70 elementor-section-content-middle elementor-reverse-mobile elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="6bf1ac70" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
              <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-29c2dd2e" data-id="29c2dd2e" data-element_type="column">
                <div class="elementor-widget-wrap elementor-element-populated">
                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-eb35a39 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="eb35a39" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Wonoayu</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                              <p> Ruko Banar, Jl. Banar Pilang (100 meter selatan perempatan Pilang) Wonoayu <br><a href="https://g.co/kgs/CpdXJh">https://g.co/kgs/CpdXJh</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Taman</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                            <p> Perum. Grand Royal Regency blok F2 no 1 (sederet ruko) Wage Taman <br><a href="https://g.co/kgs/BXqdji">https://g.co/kgs/BXqdji</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Waru</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                            <p> Jl. Belahan Sawah (depan Grand Delta Sari) Wedoro Waru <br><a href="https://g.co/kgs/WrmmyS">https://g.co/kgs/WrmmyS</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Buduran</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                            <p> Ruko Jaya Harmoni no 14 jl. Kesatrian Buduran  <br><a href="https://g.co/kgs/YkZAKs">https://g.co/kgs/YkZAKs</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  <section class="elementor-section elementor-inner-section elementor-element elementor-element-eb35a39 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="eb35a39" data-element_type="section">
                    <div class="elementor-container elementor-column-gap-default">
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Candi</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                            <p> Mutiara Residence A 2 Bligo Candi (depan SDN 1 Bligo)  <br><a href="https://g.co/kgs/JmFrkW">https://g.co/kgs/JmFrkW</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh GKB Manyar</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                            <p> Ruko GKB jl. Brotonegoro no 28 depan balai desa Yosowilangun Manyar Gresik <br><a href="https://g.co/kgs/Pe8Cha">https://g.co/kgs/Pe8Cha</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Tanggulangin</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                              <p> Jl. Raya Wates Kedensari depan pasar wisata Tanggulangin <br><a href="https://g.co/kgs/5FSerX">https://g.co/kgs/5FSerX</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="elementor-column elementor-col-25 elementor-inner-column elementor-element elementor-element-1897209" data-id="1897209" data-element_type="column">
                        <div class="elementor-widget-wrap elementor-element-populated">
                          <div class="elementor-element elementor-element-52846dc elementor-widget elementor-widget-heading" data-id="52846dc" data-element_type="widget" data-widget_type="heading.default">
                            <div class="elementor-widget-container">
                              <div class="elementor-heading-title elementor-size-default"><h4 style="color: white;">Bfresh Bunder Gresik</h4></div>
                            </div>
                          </div>
                          <div class="elementor-element elementor-element-647dfb3 elementor-widget elementor-widget-text-editor">
                            <div class="elementor-widget-container">
                              <p> Grand Bunder 2 no 11, Kabupaten Gresik <br><a href="https://g.co/kgs/cZ4RzR">https://g.co/kgs/cZ4RzR</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </section>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </div>
      </div>
      <link rel='stylesheet' id='e-animations-css' href='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/animations/animations.min.css?ver=3.4.7' type='text/css' media='all' />
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/slick/slick.min.js?ver=1.8.1' id='jquery-slick-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/themes/rock-business/assets/js/jquery.matchHeight.min.js?ver=2017417' id='jquery-match-height-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/themes/rock-business/assets/js/navigation.min.js?ver=20151215' id='rock-business-navigation-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/themes/rock-business/assets/js/skip-link-focus-fix.min.js?ver=20151215' id='rock-business-skip-link-focus-fix-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/themes/rock-business/assets/js/custom.min.js?ver=20151215' id='rock-business-custom-js-js'></script>
      <script type='text/javascript' src='https://c0.wp.com/c/5.8.2/wp-includes/js/wp-embed.min.js' id='wp-embed-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/jquery-numerator/jquery-numerator.min.js?ver=0.2.1' id='jquery-numerator-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/js/webpack.runtime.min.js?ver=3.4.7' id='elementor-webpack-runtime-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=3.4.7' id='elementor-frontend-modules-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2' id='elementor-waypoints-js'></script>
      <script type='text/javascript' src='https://c0.wp.com/c/5.8.2/wp-includes/js/jquery/ui/core.min.js' id='jquery-ui-core-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=5.3.6' id='swiper-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/share-link/share-link.min.js?ver=3.4.7' id='share-link-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.8.1' id='elementor-dialog-js'></script>
      <script type='text/javascript' id='elementor-frontend-js-before'>
        var elementorFrontendConfig = {
          "environmentMode": {
            "edit": false,
            "wpPreview": false,
            "isScriptDebug": false
          },
          "i18n": {
            "shareOnFacebook": "Share on Facebook",
            "shareOnTwitter": "Share on Twitter",
            "pinIt": "Pin it",
            "download": "Download",
            "downloadImage": "Download image",
            "fullscreen": "Fullscreen",
            "zoom": "Zoom",
            "share": "Share",
            "playVideo": "Play Video",
            "previous": "Previous",
            "next": "Next",
            "close": "Close"
          },
          "is_rtl": false,
          "breakpoints": {
            "xs": 0,
            "sm": 480,
            "md": 768,
            "lg": 1025,
            "xl": 1440,
            "xxl": 1600
          },
          "responsive": {
            "breakpoints": {
              "mobile": {
                "label": "Mobile",
                "value": 767,
                "default_value": 767,
                "direction": "max",
                "is_enabled": true
              },
              "mobile_extra": {
                "label": "Mobile Extra",
                "value": 880,
                "default_value": 880,
                "direction": "max",
                "is_enabled": false
              },
              "tablet": {
                "label": "Tablet",
                "value": 1024,
                "default_value": 1024,
                "direction": "max",
                "is_enabled": true
              },
              "tablet_extra": {
                "label": "Tablet Extra",
                "value": 1200,
                "default_value": 1200,
                "direction": "max",
                "is_enabled": false
              },
              "laptop": {
                "label": "Laptop",
                "value": 1366,
                "default_value": 1366,
                "direction": "max",
                "is_enabled": false
              },
              "widescreen": {
                "label": "Widescreen",
                "value": 2400,
                "default_value": 2400,
                "direction": "min",
                "is_enabled": false
              }
            }
          },
          "version": "3.4.7",
          "is_static": false,
          "experimentalFeatures": {
            "e_dom_optimization": true,
            "a11y_improvements": true,
            "e_import_export": true,
            "landing-pages": true,
            "elements-color-picker": true,
            "admin-top-bar": true
          },
          "urls": {
            "assets": "https:\/\/bfreshdental.com\/wp-content\/plugins\/elementor\/assets\/"
          },
          "settings": {
            "page": [],
            "editorPreferences": []
          },
          "kit": {
            "active_breakpoints": ["viewport_mobile", "viewport_tablet"],
            "global_image_lightbox": "yes",
            "lightbox_enable_counter": "yes",
            "lightbox_enable_fullscreen": "yes",
            "lightbox_enable_zoom": "yes",
            "lightbox_enable_share": "yes",
            "lightbox_title_src": "title",
            "lightbox_description_src": "description"
          },
          "post": {
            "id": 15,
            "title": "",
            "excerpt": "",
            "featuredImage": false
          }
        };
      </script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/js/frontend.min.js?ver=3.4.7' id='elementor-frontend-js'></script>
      <script type='text/javascript' src='https://bfreshdental.com/wp-content/plugins/elementor/assets/js/preloaded-modules.min.js?ver=3.4.7' id='preloaded-modules-js'></script>
      <script src='https://stats.wp.com/e-202146.js' defer></script>
      <script>
        _stq = window._stq || [];
        _stq.push(['view', {
          v: 'ext',
          j: '1:10.3',
          blog: '192343389',
          post: '15',
          tz: '0',
          srv: 'bfreshdental.com'
        }]);
        _stq.push(['clickTrackerInit', '192343389', '15']);
      </script>
  </body>
</html>
<!-- Page generated by LiteSpeed Cache 4.4.3 on 2021-11-15 04:24:54 -->